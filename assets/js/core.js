$(function() {

    var parseQueryString = function (query) {
        if (query === undefined) return {};
        return query.split("&").reduce(function(params, paramPair) {
            var pair = paramPair.split("=");
            params[pair[0]] = pair[1];
            return params;
        }, {});
    };

    var Timer = function (timeout, callback) {
        var timeoutID = setTimeout(function refresh() {
            callback();
            timeoutID = setTimeout(refresh, timeout);
        }, timeout);

        this.stop = function () {
            clearTimeout(timeoutID);
        };

        this.restart = function () {
            this.stop();
            timeoutID = setTimeout(function refresh() {
                callback();
                timeoutID = setTimeout(refresh, timeout);
            }, timeout);
        }
    };

    $(document).on('DOMNodeInserted', '.commentable', function () {
        var commentable = $(this);
        if (commentable.hasClass('proccessed')) return;

        var commentsContainer = commentable.find('.comments-container');
        commentable.on('show.bs.collapse', function () {
            commentsContainer.load(commentsContainer.attr('url'));
        });
        
        var refreshTimer = new Timer(10000, function () {
            if (commentsContainer.is(':visible')) {
                commentsContainer.load(commentsContainer.attr('url'));
            }
        });

        commentable.on('submit', '.comment-form', function(e) {
            e.preventDefault();
            var form = $(this);
            var postData = form.serializeArray();
            form.load(form.attr('action'), postData, function () {
                commentsContainer.load(commentsContainer.attr('url').split('?')[0]);
                refreshTimer.restart();
                var comments_count_holder = commentable.prev(".panel").find(".comments-count");
                comments_count_holder.html(parseInt(comments_count_holder.html())+1);
            });
        });

        commentsContainer.on('click', '.pagination a', function (e) {
            refreshTimer.restart();
        });

        commentable.addClass('proccessed');
    });

    $(document).on('click', '.pagination a', function (e) {
        var paginableContainer = $(this).closest('.paginable');
        if (paginableContainer.length > 0) {
            e.preventDefault();
            var urlWithPage = $(this).attr('href');
            paginableContainer.load(urlWithPage);
            paginableContainer.attr('url', urlWithPage);
        }
    });

    $('.filterable').each(function () {
        var filterableContainer = $(this);
        filterableContainer.on('submit', '.filter-form', function(e) {
            e.preventDefault();
            var filterForm = $(this);
            var urlParts = filterableContainer.attr('url').split("?");
            var queryParams = parseQueryString(urlParts[1]);
            filterForm.find(':input').each(function(index, elm) {
                delete queryParams[this.name];
            });
            delete queryParams['p']; //clear page on filter or do it on server?
            filterForm.serializeArray().forEach(function(param) {
                queryParams[param.name] = param.value;
            });
            var geturl = $.get(urlParts[0],  queryParams, function(html, textStatus, request) {
                filterableContainer.attr('url', this.url);
                filterableContainer.html(html);
            });
        });
    });
    
    $('.initializible').each(function () {
        var container = $(this);
        if($.trim(container.html())=='') {
            container.load(container.attr('url'));
        }            
    });
    
    $(".date").datepicker({
        dateFormat: "m.yy", 
        changeMonth: true, 
        changeYear: true 
    });
    
    $(".slider").each(function(index) {

        var slider = $(this);
        var sliderValue = $(".slider-value[for='" + slider.attr('for') + "']");
        
        var min = parseInt(slider.attr('min'));
        var max = parseInt(slider.attr('max'));
        var field = $("input[name='" + slider.attr('for') + "']");
        
        slider.slider({
            min: min,
            max: max,
            value: field.val() || min,
            animate: "fast",
            slide: function( event, ui ) {
                field.val(ui.value);
                sliderValue.text(ui.value);
            },
            create: function( event, ui ) {
                field.val(field.val() || min);
                sliderValue.text(field.val() || min);
            }
        });

        sliderValue.click(function(e) {
            e.preventDefault();
        });
    });

    $('.likeable').first().each(function() {
        $('.like-button').click(function(e) {
            var likeButton = $(this);
            e.preventDefault();
            $.post(likeButton.attr('href'), function (likeData) {
                var likeCountElement = $('.like-count');
                likeCountElement.text(likeData.likeCount);
                likeCountElement.toggle(likeData.likeCount > 0);
                likeCountElement.toggleClass("hidden", likeData.likeCount == 0);
                likeButton.toggleClass('glyphicon-heart', likeData.liked);
                likeButton.toggleClass('glyphicon-heart-empty', !likeData.liked);
            }, 'json');
        });
    });

    $(".review-form").first().each(function() {
        var form = $(this);

        var formConfirm = $('#review_form_confirm').first();
        if (formConfirm.length) {
            form.submit(function (e) {
                e.preventDefault();
                formConfirm.modal();
                formConfirm.find('a').click(function (event) {
                    event.preventDefault();
                    var link = $(this);
                    if (link.attr('href')) {
                        form.attr('action', link.attr('href'));
                    }
                    form.off();
                    form.submit();
                });
            });
        }
    });
    
    $('select.custom-select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;
    
        $this.addClass('select-hidden'); 
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option:selected').text());
    
        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);
    
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }
    
        var $listItems = $list.children('li');
    
        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });
    
        $listItems.click(function(e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
        });

        $('body').click(function (event) {
            if (!$( event.target ).closest(".select-styled, .select-options").length) {
                $styledSelect.removeClass('active').next('ul.select-options').hide();
            }
        });

    });

    $("#review_form").first().each(function() {
        var form = $(this);

        var hasCritic = $('[name="has_critic"]');

        hasCritic.change(function() {
            form.toggleClass('review-has-critic',this.checked);
        });

        form.find('.select-options li').click(function() {
            form.removeClass('article-status-refused article-status-critic-refused article-status-on-critic article-status-repeat-critic article-status-accepted article-status-published article-status-withdraw');
            var value = $(this).attr("rel");
            form.addClass(value);

            if (value === 'article-status-refused'
                || value === 'article-status-on-critic') {
                hasCritic.prop('checked', false).change();
            } else if (
                value === 'article-status-critic-refused'
                || value === 'article-status-repeat-critic'
            ) {
                hasCritic.prop('checked', true).change();
            }
        });
    });

    $("#conference_review_form").first().each(function() { 
        var form = $(this); 
        var hasCulture = $('[name="has_culture"]'); 
        hasCulture.change(function() { 
            form.toggleClass('has-culture',this.checked); 
        }); 
    }); 

    $('#not_competent_form').first().each(function() {
        if ($('#standart_imhotop_form').length) {
            var not_competent_form = $('#not_competent_form');
            var form = $('#standart_imhotop_form');
            var not_competent_checkbox = not_competent_form.find('[name="not_competent"]');
            var submit_competent = not_competent_form.find('#submit-competent');

            function userCompetentChanged() {
                var not_competent = not_competent_checkbox.prop("checked");
                form.toggleClass('user-not-competent',not_competent);
                submit_competent.toggleClass("competent", !not_competent);
            }

            not_competent_checkbox.change(userCompetentChanged);
            userCompetentChanged();
        }
    });

    $(".question-form").first().each(function() {
        var form = $(this);

        var formConfirm = $('#question_form_confirm').first();
        if (formConfirm.length) {
            form.submit(function (e) {
                e.preventDefault();
                formConfirm.modal();
                formConfirm.find('a').click(function (event) {
                    event.preventDefault();
                    var link = $(this);
                    if (link.attr('href')) {
                        form.attr('action', link.attr('href'));
                    }
                    form.off();
                    form.submit();
                });
            });
        }
    });
    
    $('#top10_form').first().each(function () {
        var form = $(this);
        form.find('.select-options li').click(function() {
            form.submit();
        });
        form.find('[type="checkbox"]').change(function() {
            form.submit();
        });
    });

    $('.journal-table').first().each(function () {
        var form = $(this);
        form.find('.select-options li').click(function() {
            form.submit();
        });
    });

    $('.star-rating').barrating({
        theme: 'fontawesome-stars'
    });

    $('.popup-tip-pointer').tooltip({
        content : $('.popup-tip').html()
    });

    var resize = function() {
        this.style.height = 'auto';
        this.style.height = this.scrollHeight+'px';
    }

    $("textarea").change(resize)
                .keydown(resize)
                .click(resize);

    $("textarea, input[type='text']").attr("maxlength", "1500");

    $('.browse-avatar').first().each(function() {
        $('<span></span>', {'class': 'btn btn-default btn-file', 'text' : 'Загрузить'}).insertAfter($(this));
        $('<input></input>', {'type': 'file', 'id': 'open-file-dialog', 'name' : 'file'}).insertAfter($(this)).hide();

        $('span.btn-file').click(function() {
            $('#open-file-dialog').click();
        });
        $("#open-file-dialog").change(function(e) {
            var formData = new FormData();
            formData.append('file', $("#open-file-dialog")[0].files[0]);
            $.ajax({
                url: base_url()+'upload-avatar',
                    type : 'POST',
                    data : formData,
                    processData: false, 
                    contentType: false,
                    dataType: "json",
                    success: function(image) {
                        $('input[name="avatar"]').val(image.src);
                        $('.avatar-input-image img').attr('src', image.src);
                    }
            });
        });
    });

    $('.avatar-image-wrapper').first().each(function() {
        if ($('input[name="avatar"]').val() != '') {
            $(this).append($('<span></span', { 'class':'remove-avatar', 'text': String.fromCharCode(10006) }));
            var formData = new FormData();
            formData.append('remove_avatar', 'yes');
            $('.remove-avatar').click(function() {
                $.ajax({
                    url: base_url()+'remove-avatar',
                        type : 'POST',
                        data : formData,
                        processData: false, 
                        contentType: false,
                        dataType: "json",
                        success: function(response) {
                            $('input[name="avatar"]').val('');
                            $('.avatar-input-image img').attr('src', response.default_avatar_path);
                        }
                });
            });
        }
        
    });

    // FACEBOOK 
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    //initialize page
    var anchor = location.hash.replace('#','');
    if(anchor != '') {
        var anchorSelector = '#' + anchor;
        var anchorScroll = function () {
            $('html, body').animate({ scrollTop: $(anchorSelector).offset().top}, 1000);
            $('body').off('DOMNodeInserted', anchorSelector, anchorScroll);
        };
        $('body').on('DOMNodeInserted', anchorSelector, anchorScroll);
    }
    

        
});