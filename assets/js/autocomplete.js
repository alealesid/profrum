$(function(){

    $.widget("my.combobox", $.ui.autocomplete, {
        lastSuggest : [],
        options : {},
        _init: function() {
            var widgetObject = this;
            var element = $(this.element);
            
            element.blur(function() {
                var val = element.val();
                if (val.length == 0) return;
                var result = $.grep(widgetObject.lastSuggest, function(item){ return item.label == val; });
                if (result.length == 0 || (result.length == 1 && result[0].value == "Не найдено")) {
                    element.val('');
                    element.attr('placeholder', 'Не найдено');
                }
            });

            $.ui.autocomplete.prototype._init.call(this);
        },

        _suggest: function(content) {
            this.lastSuggest = content;
            $.ui.autocomplete.prototype._suggest.call(this, content);
        }
    });

    $('.autocomplete').each(function() {
        var element = $(this);
        element.autocomplete({
            appendTo: element.parent().parent(), 
            minLength: 3,
            source: element.attr('url')
        });
    });

    $('.combobox').each(function() {
        var element = $(this);
        element.combobox({
            appendTo: element.parent().parent(),
            minLength: 3,
            source: element.attr('url')
        });
    });

});