<?php

$config = [
    'smtp' => [
        'host' => 'localhost',
        'port' => '25',
        'username' => '',
        'password' => '',
        'from_address' => 'noreply@profrum',
        'from_title' => 'Professional Rumors'
    ],
    'research' => [
        'review_threshold' => 0
    ]
];