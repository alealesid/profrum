<? $heading = false; ?>
<? include partial('cms/base') ?>

<? bingo_domain('gallery') ?>
<? startblock('admin_header') ?>
<script type=text/javascript src="<?=t_url('gallery/script/swfobject.js')?>"></script>
<script type=text/javascript src="<?=t_url('gallery/script/jquery-uploadify.js')?>"></script>
<script type=text/javascript src="<?=t_url('gallery/script/jquery-jcrop.js')?>"></script>

<link type="text/css" rel="stylesheet" href="<?=t_url('gallery/style/gallery.css')?>" media="screen">
<link type="text/css" rel="stylesheet" href="<?=t_url('gallery/style/uploadify.css')?>" media="screen">
<link type="text/css" rel="stylesheet" href="<?=t_url('gallery/style/jcrop.css')?>" media="screen">

<script>
    $(function() {     
        $("#uploadify").uploadify({
            cancelImg:"<?=t_url('gallery/style/images/cancel.png')?>",
            uploader:"<?=t_url('gallery/script/uploadify.swf')?>",
            script:"<?=url("admin/gallery-upload/$gallery->id")?>",
            auto:true,fileDesc:"Images",fileExt:"*.jpg;*.png;*.jpeg",
            hideButton: true,width:100,height:20,wmode:'transparent',queueID:'image_queue',
            multi: true,removeCompleted:false,
            onComplete:function (event,queueID,fileObj,response,data) 
            {
                $("#uploadify"+queueID).remove();
                var photo = eval('('+response+')');
                if (photo.error) {
                    alert("<?=_t('File upload failed. Contact your system administrator. Error code: ')?>"+photo.error);
                    return;
                }
                $("table.images").append(""
                    +'<tr>'
                    +'    <td class="check"><input name="images['+photo.id+'][check]" type="checkbox"></td>'
                    +'    <td class="thumb">'
                    +'      <a class="pic colorbox" rel="photos" href="'+photo.url+'">'
                    +'        <img src="'+photo.thumb+'">'
                    +'      </a>'
                    +'    </td>'
                    +'    <td class="title">'
                    +'      <input name="images['+photo.id+'][title]" type="text">'
                    +'      <textarea name="images['+photo.id+'][description]" rows=3></textarea>'
                    +'    </td>'
                    +'</tr>'
                );
                $("a.pic").colorbox({transition:"none"});
            },
            onError: function(event,ID,fileObj,errorObj) { /*_D(errorObj.type + ' Error:' + errorObj.info);*/ }
        });
        $("#with_selected").change(function(){
            if ($(this).val()!=0) $("#images").submit();
        });       
        $("#check_all").change(function(){
            var flag = this.checked;
            $("td.check input").each(function(){this.checked=flag;});
        });
        $("table.images tbody").sortable({
            forceHelperSize:true,
            items:"tr:not(.th)",
            distance: 15,
            helper:function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            }
        });
    });
</script>
<? endblock() ?>

<? startblock('content') ?>

<? if ($gallery->id): ?>
    <h1>
        <?=_t("Gallery edit")?> (<?=$gallery->id?> - <?=$gallery->title?>)
        <span class="page_actions">
            <a href="#"><span id="uploadify"></span><?=_t("upload images")?></a>
        </span>
    </h1>
<? else: ?>
    <h1><?=_t("Gallery create")?></h1>
<? endif ?>

<div class="form">
    <?=$form?>
    <? if ($gallery->id): ?>
    <div id="image_queue"></div>

    <form class="images" autocomplete="off" method="post" id="images" spellcheck="false">
        <select name="with_selected" id="with_selected">
            <option value=0><?=_t("..with selected")?></option>
            <option value="delete"><?=_t("Delete selected")?></option>
        </select>
        <button type="submit" class="inline"><?=_t("Save images sorting and options")?></button>
        <table class="images"><tbody>
            <tr class="th">
                <th><input id="check_all" type="checkbox"></th>
                <th><?=_t("thumb")?></th>
                <th><?=_t("title/description")?></th>
            </tr>
        <? foreach ($gallery->images as $image): ?>
            <tr>
                <td class="check"><input name="images[<?=$image->id?>][check]" type="checkbox"></td>
                <td class="thumb">
                    <a class="pic colorbox" rel="photos" href="<?=$image->imageUrl()?>">
                        <img src="<?=$image->thumbUrl()?>">
                    </a>
                </td>
                <td class="title">
                    <input name="images[<?=$image->id?>][title]" type="text" value="<?=htmlspecialchars($image->title)?>">
                    <textarea name="images[<?=$image->id?>][description]" rows=3><?=htmlspecialchars($image->description)?></textarea>
                </td>
            </tr>
        
        <? endforeach ?>
        </tbody></table>
    </form>
    <? endif ?>
</div>

<? endblock() ?>           
