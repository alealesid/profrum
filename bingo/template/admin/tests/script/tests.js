$(function(){

    var select = $("[name='type']");
    var updateControls = function () {
        if (select.val()=="sort") {
            $("fieldset.weights").show();
        } else {
            $("fieldset.weights").hide();
        }
    }

    $('textarea.space').tabby().attr("spellcheck",false);
    if (select.length) {
        select.change(updateControls);
        updateControls();
    }
})