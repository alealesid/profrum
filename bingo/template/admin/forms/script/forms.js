$(document).ready(function(){
    $(".drag_helper:first").parents("table").sortable({
        items: "tr",
        handle: ".drag_helper",
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        }
    });
});
