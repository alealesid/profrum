/**
 * Bingo plugin.
 */

(function() {
	var DOM = tinymce.DOM;

	tinymce.create('tinymce.plugins.Bingo', {
		mceTout : 0,

		init : function(ed, url) {
			var t = this, tbId = ed.getParam('wordpress_adv_toolbar', 'toolbar2'), last = 0, moreHTML, nextpageHTML;
			moreHTML = '<img src="' + url + '/img/trans.gif" class="mceBingoMore mceItemNoResize" />';

			// Register commands
			ed.addCommand('Bingo_More', function() {
				ed.execCommand('mceInsertContent', 0, moreHTML);
			});

			// Register buttons
			ed.addButton('bingo_more', {
				title : '----------',
				image : url + '/img/more.gif',
				cmd : 'Bingo_More'
			});

			// Add listeners to handle more break
			t._handleMoreBreak(ed, url);
		},

		getInfo : function() {
			return {
				longname : 'Bingo Plugin',
				author : 'Bingo', // add Moxiecode?
				authorurl : 'http://bingophp.com',
				infourl : 'http://bingophp.com',
				version : '1.0'
			};
		},

		_handleMoreBreak : function(ed, url) {
			var moreHTML;
			moreHTML = '<img src="' + url + '/img/trans.gif" class="mceBingoMore mceItemNoResize" />';

			// Load plugin specific CSS into editor
			ed.onInit.add(function() {
				ed.dom.loadCSS(url + '/css/content.css');
			});

			// Display morebreak instead if img in element path
			ed.onPostRender.add(function() {
				if (ed.theme.onResolveName) {
					ed.theme.onResolveName.add(function(th, o) {
						if (o.node.nodeName == 'IMG') {
							if ( ed.dom.hasClass(o.node, 'mceBingoMore') )
								o.name = 'bingomore';
						}

					});
				}
			});

			// Replace morebreak with images
			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(/<!--more(.*?)-->/g, moreHTML);
			});

			// Replace images with morebreak
			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<img[^>]+>/g, function(im) {
						if (im.indexOf('class="mceBingoMore') !== -1) {
							var m, moretext = (m = im.match(/alt="(.*?)"/)) ? m[1] : '';
							im = '<!--more'+moretext+'-->';
						}
						return im;
					});
			});

			// Set active buttons if user selected pagebreak or more break
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('bingo_more', n.nodeName === 'IMG' && ed.dom.hasClass(n, 'mceBingoMore'));
			});
		}
	});

	// Register plugin
	tinymce.PluginManager.add('bingo', tinymce.plugins.Bingo);
})();
