_O = function(obj) { $("body").append("<p>"+obj.toString()+"</p>");}
_D = function(obj) { $("body").append("<p><pre>"+print_r(obj)+"</pre></p>");}
function print_r(arr, level) {
    if (level>3) return;
    var print_red_text = "";
    if(!level) {level = 0}
    var level_padding = "";
    for(var j=0; j<level+1; j++) level_padding += "    ";
    if(typeof(arr) == 'object') {
        for(var item in arr) {
            var value = arr[item];
            if(typeof(value) == 'object') {
                print_red_text += level_padding + "'" + item + "' :\n";
                print_red_text += print_r(value,level+1);
        } 
            else if (typeof(value) !='function') 
                print_red_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
        }
    } 
    else  print_red_text = "===>"+arr+"<===("+typeof(arr)+")";
    return print_red_text;
}
