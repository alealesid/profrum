<script type="text/javascript">
    function openKCFinder(field_name, url, type, win) {
        var prefix = (base_url || "/") + "upload/CMS/";
        var path_param = (url.indexOf(prefix)==0) ? ("&path="+encodeURIComponent(url.substring(prefix.length))) : "";
        
        tinyMCE.activeEditor.windowManager.open({
            file:base_url+"/admin/files/browse.php?opener=tinymce&lang="+locale_lang+"&type=files"+path_param,
            title: 'File Browser',
            width: 800,
            height: 600,
            resizable: "yes",
            inline: true,
            close_previous: "no",
            popup_css: false
        }, {
            window: win,
            input: field_name
        });
        return false;
    }
</script>
<script type="text/javascript">
    $(function() {
        $("textarea.tinymce").each(function(){
            $(this).prev("label")
                .addClass("source_preview")
                .append(
                    "<a class='source' href=#><?=_t("Source")?></a>"+
                    "<a class='preview active' href=#><?=_t("Preview")?></a>"
                );

            var me = this;
            $(this).tinymce({
                mode:'exact',
                script_url : "<?=t_url('cms/script/tiny_mce/tiny_mce.js')?>",

                // General options
                theme : "advanced",
                plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,bingo",

                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontsizeselect,|,hr,|,sub,sup,|,forecolor,backcolor,|,fullscreen",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,help,|,media,bingo_more",
                theme_advanced_buttons3 : "",
                theme_advanced_buttons4 : "",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,
                theme_advanced_resize_horizontal : false,
                width: "100%",
    
                paste_remove_styles: true,

                file_browser_callback : "openKCFinder",
                language:"<?=_t("en")?>",

                entity_encoding : "raw",
                verify_html : false,
                relative_urls : false,
                accessibility_warnings : false,
                init_instance_callback: function(ed) {
                    $(me).trigger("mce_loaded",me);
                    ed.dom.bind(ed.getWin(), ['dragenter', 'dragover', 'draggesture', 'drag', 'drop'], function(e) {
                        if (e.dataTransfer && e.dataTransfer.files && e.dataTransfer.files.length) {
                            var file = e.dataTransfer.files[0];
                            var formData = new FormData();
                            formData.append("dir","files");
                            formData.append('upload[]', file, file.name);
                            
                            var xhr = new XMLHttpRequest();
                            xhr.open('POST', (base_url || "/") + "admin/files/browse.php?type=files&act=upload", true);
                            xhr.onload = function () {
                                if (xhr.status === 200) {
                                    if (xhr.responseText.substr(0, 1) != '/') {
                                        alert('File upload error: '.xhr.responseText);
                                    } else {
                                        ed.execCommand('mceInsertContent',false,
                                            '<p>'+ $("<img>",{src:(base_url||"/")+"upload/CMS/files"+xhr.responseText})[0].outerHTML +'</p>');
                                    }
                                } else {
                                    alert('File upload error! Status: '+xhr.status);
                                };                            
                            };
                            xhr.send(formData);
                            
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        }
                    });
                }
            });
        });

        $("a.source").click(function(e){
            e.preventDefault();
            var id  = $(this).parent().nextAll('.tinymce').attr("id");
            tinyMCE.execCommand('mceRemoveControl', false, id);
            $(this).next().removeClass("active");
            $(this).addClass("active");
        });
        $("a.preview").click(function(e){
            e.preventDefault();
            var id  = $(this).parent().nextAll('.tinymce').attr("id");
            tinyMCE.execCommand('mceAddControl', false, id);
            $(this).prev().removeClass("active");
            $(this).addClass("active");
        });
    });
</script>