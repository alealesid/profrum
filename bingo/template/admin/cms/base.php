<? bingo_domain('cms') ?>
<? \Bingo\Action::run('admin_pre_header'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?= \Bingo\Action::filter('admin_title',array($title)) ?></title>
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-ui.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-editable-select.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-colorbox.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/jquery-treeTable.css')?>" media="screen">
    <link type="text/css" rel="stylesheet" href="<?=t_url('cms/style/core.css')?>" media="screen">
    
    <script type="text/javascript">
        var base_url="<?=base_url()?>"
        var template_base = "<?=t_url('')?>";
        var locale = "<?=bingo_get_locale()?>";
        var locale_lang = "<?=_t('en')?>";
        var browse_text = "<?=_t('Browse')?>";
    </script>
    <script src="<?=t_url('cms/script/debug.js')?>" type="text/javascript" charset="utf-8"></script>

    <script src="<?=t_url('cms/script/jquery.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-json.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-cookie.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-ui.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-ui-timepicker-addon.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-editable-select.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-colorbox.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-treeTable.js')?>" type="text/javascript" charset="utf-8"></script>
    <script src="<?=t_url('cms/script/jquery-tagsinput.js')?>" type="text/javascript" charset="utf-8"></script>

    <script src="<?=t_url('cms/script/tiny_mce/jquery.tinymce.js')?>" type="text/javascript" charset="utf-8"></script>
    
    <script src="<?=t_url('cms/script/core.js')?>" type="text/javascript" charset="utf-8"></script>

    <? \Bingo\Action::run('admin_header'); ?>
    <? emptyblock('admin_header',SUPERBLOCK) ?>
</head>
<body class="admin<?if(isset($disable_menu)&&$disable_menu) echo ' without_menu'?>">
<div class="pagewidth">
<div id="navtop">
    <? $user = \Bingo\Routing::$controller->user; ?>
    <? if ($user): ?>
        <?=_t("Welcome",'cms')?>,
        <?=anchor("admin/user-profile",$user->login)?> |
        <a href="<?=url('/')?>" target="_blank"><?=_t("Goto website","cms")?></a> |
        <?=anchor("admin/logout",_t("Logout","cms"))?>
    <? endif ?>
</div>
<div id="logo"><?=_t("Control panel")?></div>
<? if (!isset($disable_menu) || !$disable_menu): ?>
<?
    function renderMenu($items) {
        foreach ($items as $title=>$sub) {
            if (is_array($sub)) {
                echo "<li>$title";
                echo "<ul>";
                renderMenu($sub);
                echo "</ul></li>";
            } else {
                echo "<li><a href='".url($sub)."'>$title</a></li>";
            }
        }
    }
?>
<ul class='menu'>
    <? renderMenu(\Admin::$menu) ?>
</ul>
<? endif ?>

<div id="content" <? if (isset($content_class)) echo "class='$content_class'" ?>>
    <? if (!isset($heading) || $heading): ?>
    <h1>
        <? if (isset($heading)): ?>
            <?=$heading?>
        <? else: ?>
            <?=$title?>
        <? endif ?>
        <? if (isset($page_actions)): ?>
        <span class="page_actions">
            <? foreach ($page_actions as $url=>$action): ?>
                <?=anchor($url,$action)?>
            <? endforeach ?>
        </span>
        <? endif ?>
    </h1>
    <? endif ?>
    <? if (isset($message)): ?>
        <div class="message">
            <?=$message?>
        </div>
    <? endif ?>
    <? if (isset($error_message)): ?>
        <div class="error-message">
            <?=$error_message?>
        </div>
    <? endif ?>
    <? if (get_flash('error',false)): ?>
        <div class="error-message">
            <?=get_flash('error',true)?>
        </div>
    <? endif ?>
    <? if (get_flash('info',false)): ?>
        <div class="info">
            <?=get_flash('info',true)?>
        </div>
    <? endif ?>
    <? emptyblock('content') ?>
</div>
<div style="clear:both"></div>
</div>
</body>
</html>
