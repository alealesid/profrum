<? include partial('layout') ?>

<? startblock('content') ?>

    <h3><?= $post->title ?></h3>
    <? $posts = \Blog\Models\Post::findBy(array('type'=>'post'),'date DESC',0,10) ?>
    <? foreach ($posts as $post): ?>
        <article class="post">
            <div class="post-header">
                <h3 class="post-title"><a href="<?=$post->get_url()?>"><?= $post->title ?></a></h3>
                <span class="date">Posted on <?= $post->date->format("d.m.Y") ?></span>
            </div>
            <div class="post-content">
                <?= $post->get_excerpt() ?>
            </div>
            <div class="post-footer">
                <span class="comments">
                    <a href="<?=$post->get_url()?>#comments">
                        <? $comment_count = count($post->comments) ?>
                        <?= $comment_count ?>
                        <? if ($comment_count == 1): ?>
                            comment
                        <? else: ?>
                            comments 
                        <? endif ?>
                    </a>
                </span>
            </div>
        </article>

    <? endforeach ?>

<? endblock() ?>