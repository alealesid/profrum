<? include partial('shop/base') ?>

<? startblock('main') ?>

    <? $cats = \Shop\Models\Category::findBy(array(),"item_order") ?>
    <div class="categories">
    <? foreach ($cats as $cat): ?>
        <div class='category'>
            <a href="<?= $cat->get_url() ?>">
                <? if ($src = $cat->get_image_url(150,150)): ?>
                    <img src='<?= $src ?>' alt="<?= $cat->title ?>">
                <? endif ?>
                <span class='title'><?= $cat->title ?></span>
            </a>
        </div>
    <? endforeach ?>
    </div>

<? endblock() ?>