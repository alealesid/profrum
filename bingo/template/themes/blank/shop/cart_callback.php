<?
    $cart = \Shop\Models\Cart::getInstance();
    $count = $cart->getCount();
    $total = $cart->getTotal();
?>

<a href="<?=url('cart')?>" id="cart">
    <span class="title">
        <?= _t("Your cart").($count ? (" (".$count.")") : "") ?>
    </span>
    <span class="description">
        <? if ($count): ?>
            <?= _t('Total') ?>
            <b><?= \Shop\Models\Product::get_price_static($total) ?></b>
        <? else: ?>
            <?= _t('Cart is empty') ?>
        <? endif ?>
    </span>
</a>