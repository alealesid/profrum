<? include partial('shop/base') ?>

<? startblock('main') ?>

    <h3><?= $product->title ?></h3>

    <div id="product-image">
        <? if (count($product->images)): ?>
            <img src="<?= $product->get_image_url(400,400) ?>" alt="">
        <? endif ?>
    </div>

    <div id="product-info">
        <table>
            <? if ($product->special_price): ?>
                <tr>
                    <td><?= _t('Price')?></td>
                    <td class="special_price">
                        <?= $product->get_price() ?>
                    </td>
                </tr>
                <tr>
                    <td><?= _t("Old price") ?></td>
                    <td class="old_price">
                        <?= $product->get_price($product->price) ?>
                    </td>
                </tr>
            <? else: ?>
                <tr>
                    <td><?= _t('Price')?></td>
                    <td class="price">
                        <?= $product->get_price() ?>
                    </td>
                </tr>
            
            <? endif ?>
            <tr>
                <td><?= _t('Category')?></td>
                <td class="category">
                    <a href="<?= $product->category->get_url() ?>">
                        <?= $product->category->title ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td><?= _t('Availability')?></td>
                <td class="category">
                    <?= $product->get_storage_state() ?>
                </td>
            </tr>
        </table>

        <form id="order" action="<?=url('cart')?>" method="post">
            <label><?= _t('Quantity:') ?></label>
            <input value="1" name="quantity">
            <button class="red" type="submit" name="product" value="<?= $product->id ?>">
                <?= _t('Add to cart') ?>
            </button>
        </form>

        <? if (count($product->images)>1): ?>
            <ul id="product-thumbs">
                <? for ($i=0;$i<count($product->images);$i++): ?>
                    <li>
                        <a href="<?= $product->get_image_url(400,400,$i) ?>">
                            <img src="<?= $product->get_image_url(100,100,$i) ?>" alt="">
                        </a>
                    </li>
                <? endfor ?>
            </ul>
        <? endif ?>
    </div>
    <div id="product-description"><?= $product->content ?></div>

<? endblock() ?>