<? include partial('shop/base') ?>

<? startblock('main') ?>

    <? $children = \Shop\Models\Category::findBy(array('parent'=>$category,'disabled'=>false)) ?>
    <div class="categories">
    <? foreach ($children as $cat): ?>
        <div class="category">
            <a href="<?= $cat->get_url() ?>">
                <? if ($src = $cat->get_image_url(150,150)): ?>
                    <img src='<?= $src ?>' alt="<?= $cat->title ?>">
                <? endif ?>
                <span class='title'><?= $cat->title ?></span>
            </a>
        </div>
    <? endforeach ?>
    </div>

    <? $N = count($products) ?>
    <? if ($N): ?>
        <?= $sorting_form ?>
        <div class="products">
        <? foreach ($products as $product): ?>
            <div class="product">
                <a href="<?= $product->get_url() ?>">
                    <? if ($src = $product->get_image_url(150,150)): ?>
                        <img src='<?= $src ?>' alt="<?= $product->title ?>">
                    <? endif ?>
                    <? if ($product->price): ?>
                        <? if ($product->special_price): ?>
                            <span class="special_price"><?= $product->get_price() ?></span>
                            <span class="old_price"><?= $product->get_price($product->price)?></span>
                        <? else: ?>
                            <span class="price"><?= $product->get_price() ?></span>
                        <? endif ?>
                    <? endif ?>
                    <span class='title'><?= $product->list_title ? : $product->title ?></span>
                </a>
            </div>
        <? endforeach ?>
        </div>
    <? endif ?>


<? endblock() ?>