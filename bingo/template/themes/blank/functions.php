<?php

function image_url($file,$w,$h,$adaptive=false) {
    return \Bingo\ImageResizer::get_url($file,$w,$h,$outer=false,$adaptive);
}

$menuForm = \Bingo\Form::create()
    ->text('label',_t('Label','blank'))
    ->text('url',_t('Url','blank'))
;

$imgForm = \Bingo\Form::create()
    ->text('path',_t('Image'))->add_class("browse_file image")
;

$snippetForm = \Bingo\Form::create()
    ->fieldset(_t('Info'))
    ->text('title',_t('Site title'))
    ->text('description',_t('Site description'))
    
    ->fieldset(_t('Header images'))
    ->form_list('header_images',$imgForm,false,true)
    
    ->fieldset(_t('Menu','blank'))
    ->form_list('menu',$menuForm,false,true)->add_class("menu_editor")
;

\Layout\Widget::registerLayout('default',array(
    'label' => _t('General','blank'),
    'snippetForm' => $snippetForm,
    'widgetPlaces' => array()
));

\Layout\Widget::registerLayout('shop',array(
    'label' => _t('Shop','blank'),
    'widgetPlaces'=>array(
        'sidebar'=>array(
            'label'=>_t('Sidebar')
        )
    )
));