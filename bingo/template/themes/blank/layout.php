<!doctype html>
<html>
<head>
    <title><?= $title ?></title>
    <meta charset="utf-8" />

    <script>var base_url = "<?=url('')?>"</script>
    
    <? if (isset($_GET['dev']) && \CMS\Models\User::checkLoggedIn()): ?>
        <? 
            if (isset($_POST['css'])) {
                file_put_contents(__DIR__."/assets/style.css",$_POST['css']);
                if (isset($_POST['js'])) {
                    file_put_contents(__DIR__."/assets/script.js",$_POST['js']);
                }
                die();
            }
        ?>
    
        <script tea="<?=t_url('assets/tea/makefile.tea')?>"></script>
        <script src="http://code.jquery.com/jquery-1.7.2.js"></script>
        <script src="/~boomyjee/teacss/lib/teacss.js"></script>
        <script>
            teacss.buildCallback = function (files) {
                var dir = teacss.path.absolute("../src/teacss-ui/style")+"/";
                var css = files['/default.css'];
                while (css.indexOf(dir)!=-1) css = css.replace(dir,"");
                $.post(location.href,{css:css,js:files['/default.js']},function(data){
                    alert('ok');
                });
            }
            teacss.update();
        </script>    
    <? else: ?>
        <link rel="stylesheet" type="text/css" href="<?=t_url('assets/style.css')?>">
        <script src="<?=t_url('assets/script.js')?>"></script>
    <? endif ?>
</head>
<body>
    <div id="page">
        <div id="header">
            <? include partial('shop/cart_callback') ?>
            <div id="info">
                <h1 id="site-title"><a href="<?= url('')?>"><?= snippet('title') ?></a></h1>
                <h2 id="site-description"><?= snippet('description') ?></h2>
            </div>
            <div id="logged_info">
                <? $user = \Shop\Models\User::checkLoggedIn() ?>
                <? if ($user): ?>
                    <?=_t('Logged as')?>
                    <a href="<?=url('profile')?>"><?= $user->name ?></a>
                    |
                    <a href="<?=url('logout')?>"><?=_t('Logout')?></a>
                <? else: ?>
                    <a href="<?=url('login')?>"><?=_t('Login')?></a>
                <? endif ?>
            </div>            
            <?
                $images = snippet('header_images');
                $image = $images[array_rand($images)]['path'];
                $image = image_url($image,900,200,true);
            ?>
            <img src="<?= $image ?>">
            <div id="menu">
                <ul>
                    <? foreach (snippet('menu') as $item): ?> 
                        <?
                            $cls = ($item['url']==\Bingo\Routing::$uri) ? "current-menu-item":"";
                        ?>
                        <li class="<?= $cls ?>">
                            <a href="<?=url($item['url'])?>"><?= $item['label'] ?></a>
                        </li>
                    <? endforeach ?>
                </ul>
            </div>
        </div>
        <div id="content">
            <? emptyblock('content') ?>
        </div>
        <div id="footer">
            Powered by Bingo Blank Theme
        </div>
    </div>
</body>
</html>