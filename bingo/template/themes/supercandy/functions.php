<?php

require __DIR__."/_lib/candy.php";

\Blog\Configuration::registerPostType('portfolio',
    array(
        'builtin'=>false,
        'show_ui'=>true,
        'label'=> _t('Portfolio'),
        'singular_label' => _t('Project'),
        'hierarchical' => false
    )
);

\Bingo\Action::add('add_meta_box',function($post,$form) use (&$imgForm) {
    if ($post->type!='portfolio') return;
    
    $form->text('data[preview_image]',_t("Thumbnail",'portfolio'),'',
                @$post->data['preview_image'],array('before'=>'content'))->add_class("browse_file image");
    $form->form_list('data[images]',$imgForm,@$post->data['images'],true,
                 array('label'=>'Images','before'=>'content'));
    
    $form->tabs("data[tabs]",@$post->data['tabs']);
        $form->tab("Tab1","boo");
            $form->text("boo","Boo");
        $form->close_tab();
        $form->tab("Tab2","boo 2");
            $form->text("boo","Boo 2");
        $form->close_tab();
    $form->close_tabs();
},10,2);


$menuForm = \Bingo\Form::create()
    ->text('label',_t('Label','blank'))
    ->text('url',_t('Url','blank'))
;

$imgForm = \Bingo\Form::create()
    ->text('url',_t('Image'))->add_class("browse_file image")
;

$snippetForm = \Bingo\Form::create()
    ->fieldset(_t('Info'))
    ->text('title',_t('Site title'))
    ->text('description',_t('Site description'))
    
    ->fieldset(_t('Header images'))
    ->form_list('header_images',$imgForm,false,true)
    
    ->fieldset(_t('Menu','blank'))
    ->form_list('menu',$menuForm,false,true)->add_class("menu_editor")
;

\Layout\Widget::registerLayout('default',array(
    'label' => _t('General','blank'),
    'snippetForm' => $snippetForm,
    'widgetPlaces' => array()
));

\Layout\Widget::registerLayout('shop',array(
    'label' => _t('Shop','blank'),
    'widgetPlaces'=>array(
        'sidebar'=>array(
            'label'=>_t('Sidebar')
        )
    )
));