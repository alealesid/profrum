<?

template("layout",
block("#content",function ($data) { ?>
    <? extract($data) ?>

    <h3><?= _t("Cart") ?></h3>
    <form method="post">
        <? if ($cart->getCount()): ?>
            <table class="cart">
                <tr>
                    <th colspan="2"><?= _t('Product') ?></th>
                    <th></th>
                    <th><?= _t('Price') ?></th>
                    <th class="subtotal"><?= _t('Total') ?></th>
                </tr>
                <? foreach ($cart->products as $id=>$qty): ?>
                    <? $product = \Shop\Models\Product::find($id) ?>
                    <? if ($product): ?>
                        <tr>
                            <td class="thumb">
                                <? if (count($product->images)): ?>
                                    <a href="<?=$product->get_url()?>">
                                        <img src="<?=$product->get_image_url(100,100)?>">
                                    </a>
                                <? endif ?>
                            </td>
                            <td class="title">
                                <a href="<?=$product->get_url()?>">
                                    <?= $product->title ?>
                                </a>
                            </td>
                            <td class="qty">
                                <a class="remove" href="#">x</a>
                                <input value="<?=$qty?>" name="<?='products['.$id.'][quantity]'?>">
                                x
                            </td>
                            <td class="price">
                                <?= $product->get_price() ?>
                            </td>
                            <td class="subtotal">
                                <?= $product->get_price($product->get_price_value() * $qty) ?>
                            </td>
                            
                        </tr>
                    <? endif ?>
                <? endforeach ?>
                
                <tr>
                    <td colspan="4"></td>
                    <td class="subtotal">
                        <b><?= \Shop\Models\Product::get_price_static($cart->getTotal()) ?></b>
                    </td>
                </tr>
            </table>
        
            <div class="buttons">
                <button class="recalc" name="action" value="recalc">
                    <?=_t('Recalculate') ?>
                </button>
                <button class="order red" name="action" value="order">
                    <?=_t('Make order') ?>
                </button>
            </div>
        
        <? else: ?>
            <p><b><?= _t('Cart is empty.') ?></b></p>
            <p><?= _t('Order some goods or give us a call - we will find something for you') ?></p>
        <? endif ?>
    </form>

<? })
);