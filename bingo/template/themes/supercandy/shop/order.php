<?

template("layout",
block("#content",function ($data) { ?>
    <? extract($data) ?>

    <?
        $shipping_price = count($shipping) ? reset($shipping)->getPrice() : 0;
        $options = \CMS\Models\Option::get('shop_options');
        $currencyPre = @$options['currencyPre'];
        $currencyPost = @$options['currencyPost'];
    ?>
    <script>
        var total = <?=$total?>;
        var shipping = {};
        <? foreach ($shipping as $key=>$s): ?>
            shipping[<?=$key?>] = <?=$s->getPrice()?>;
        <? endforeach ?>
        $(document).ready(function(){
            var get_price = function (val) {
                return "<?=$currencyPre?>"+val+"<?=$currencyPost?>";
            }
            $("[name=shipping]").change(function(){ 
                var val = shipping[$(this).val()];
                $("#shipping_price").html(get_price(val));
                $("#total_price").html(get_price(total + val));
                $("#shipping_row").css("display",val ? "":"none");
            });
        })
    </script>

    <h3><?= _t("Order") ?></h3>

    <? if (!$user): ?>
        <p>
            Вы сейчас не авторизованы. Для того чтобы зарегистрироваться или зайти в наш магазин вам не придется заполнять
            никаких данных. Достаточно только иметь аккаунт в одном из популярных сервисов (VKontakte, Mail.RU, GMail, Facebook и другие).
            Мы не спрашиваем вашу почту и другие личные данные (но вы можете заполнить их для удобства заказа).
        </p>
        <p>
            Авторизация позволит нам узнавать вас при последующих покупках, начислять бонусы, давать скидки и делать
            специальные предложения.
        </p>

        <div class="tabs">
        <ul>
            <li class="active">
                <a><?=_t('Order without logging in') ?></a>
            </li>
            <li>
                <a href="<?=url('login?redirect=order')?>">
                    <?= _t('Log me in') ?>
                </a>
            </li>
        </ul>
        <div>
    <? endif ?>

    <div class="order_form"><?= $form ?></div>
    <div class="order_cart">
        <table class="cart">
            <tr>
                <th colspan="2"><?= _t('Product') ?></th>
                <th class="subtotal"><?= _t('Total') ?></th>
            </tr>
            <? foreach ($cart->products as $id=>$qty): ?>
                <? $product = \Shop\Models\Product::find($id) ?>
                <? if ($product): ?>
                    <tr>
                        <td class="thumb">
                            <? if (count($product->images)): ?>
                                <a href="<?=$product->get_url()?>">
                                    <img src="<?=$product->get_image_url(100,100)?>">
                                </a>
                            <? endif ?>
                        </td>
                        <td class="title">
                            <a href="<?=$product->get_url()?>">
                                <?= $product->title ?>
                            </a>
                        </td>
                        <td class="subtotal">
                            <?=$qty?>
                            x
                            <?= $product->get_price($product->get_price_value() * $qty) ?>
                        </td>
                        
                    </tr>
                <? endif ?>
            <? endforeach ?>
            
            <tr id="shipping_row" style='<?=$shipping_price ? "":"display:none"?>'>
                <td></td>
                <td><?= _t('Shipping') ?></td>
                <td class="subtotal" id="shipping_price">
                    <?= \Shop\Models\Product::get_price_static($shipping_price) ?>
                </td>
            </tr> 
            <tr>
                <td colspan="2"></td>
                <td class="subtotal">
                    <b id="total_price">
                        <?= \Shop\Models\Product::get_price_static($total + $shipping_price) ?>
                    </b>
                </td>
            </tr>         
        </table>
    </div>
            
    <? if (!$user): ?>
        </div></div>
    <? endif ?>

<? })
);