$(document).ready(function(){
    $("#product-thumbs li:first").addClass("current");
    $("#product-thumbs a").click(function(e){
        e.preventDefault();
        $("#product-image img").attr("src",$(this).attr("href"));
        $("#product-thumbs li").removeClass("current");
        $(this).parent().addClass("current");
    })

    $("#sorting_form select").change(function(){this.form.submit();});
    $("form#order button").click(function(e){
        e.preventDefault();

        $.ajax({
            url:base_url+"cart/callback",
            data:{product:$("form#order button").val(),quantity:$("form#order input[name=quantity]").val()},
            type:"post",
            success: function(data){
                $("#cart").replaceWith($(data));
                var off = $("#product-image img").offset();
                var dst = $("#cart").offset();
                var item = $("#product-image img").clone()
                    .css({position:"absolute",left:off.left,top:off.top,"z-index":1000})
                    .appendTo("body");

                var thumb = $("#thumbs a:first").attr("href");
                if (thumb) item.attr("src",thumb);
                
                item.animate({left:dst.left,top:dst.top,opacity:0,width:200,height:200},500,function(){$(this).remove()});
            }
        });
    });

    $(".cart .remove").click(function(){
        $(this).next().val(0);
        $(".recalc").click();
    });
})