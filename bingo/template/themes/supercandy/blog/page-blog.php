<? 

template("layout",
    block("#content",
          block("post_title"),
          block("blog_posts")
    )
);