<? 

template("layout",
    block("#content",
        block("post_title"),
        block("slider",
            array(
                'one_src' => '{{ one.url | image_url : "900x400" }}'
            )
        ),
        block("post_content")
    )
);