<? 

template("layout",
    block("#content",
          block("post_title"),
          block("post_content")
    )
);