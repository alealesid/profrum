<? 

template("layout",
    block("#content",
          block("post_title"),
          block("post_content",array(
              'class' => 'oh'
          )),
          block("google_map",array(
              'zoom' => 16,
              'coordinates' => '39.187945,-96.576262',
              'class' => 'oh'
          ))
    )
);