<?php 

attribute("title",function($data){ return $data['title']; });
template(
    block("container#page",
        block("container#header",
            function () { ?>
                <? include partial('shop/cart_callback') ?>
                <div id="info">
                    <h1 id="site-title"><a href="<?= url('')?>"><?= snippet('title') ?></a></h1>
                    <h2 id="site-description"><?= snippet('description') ?></h2>
                </div>
                <div id="logged_info">
                    <? $user = \Shop\Models\User::checkLoggedIn() ?>
                    <? if ($user): ?>
                        <?=_t('Logged as')?>
                        <a href="<?=url('profile')?>"><?= $user->name ?></a>
                        |
                        <a href="<?=url('logout')?>"><?=_t('Logout')?></a>
                    <? else: ?>
                        <a href="<?=url('login')?>"><?=_t('Login')?></a>
                    <? endif ?>
                </div>            
                <?
                    $images = snippet('header_images');
                    $image = $images[array_rand($images)]['url'];
                    $image = image_url($image,900,200,true);
                ?>
                <img src="<?= $image ?>">
            <? },
            block("menu#menu",
                array(
                    'items' => array_map(function($item){
                        $item['link'] = url($item['url']);
                        return $item;
                    },snippet('menu'))
                )
            )
        ),
        block("container#content"),
        block("container#footer", function () { ?>
            Powered by Bingo Blank Theme  
        <? })
    )
);