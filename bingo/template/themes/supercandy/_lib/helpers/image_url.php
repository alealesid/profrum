<?php

function image_url($file,$w,$h,$adaptive=false) {
    return \Bingo\ImageResizer::get_url($file,$w,$h,$outer=false,$adaptive);
}