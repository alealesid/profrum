<?php

Component::$api = new CandyTemplaterApi;
Component::$api->settings = (object)array();
Component::$api->templates = (object)array();
Component::$api->id_counter = 0;

function attribute($key,$value) {
    global $attributes;
    if (isset($attributes[$key])) return;
    if ($value instanceof Closure) $value = $value(\Bingo\Routing::$controller->data);
    $attributes[$key] = $value;
}

function block($name) {
    $parts = split("#",$name);
    $name = $parts[0];
    $id = @$parts[1];
    
    $args = func_get_args();
    $params = isset($args[1]) ? $args[1] : false;
    if (!is_array($params)) $params = array();
    $params['id'] = $id ?:'id_'.(Component::$api->id_counter++);
    $params['type'] = $name;

    $cmp = (object)array(
        'children' => array(),
        'value' => (object)$params
    );
    
    foreach ($args as $arg) {
        if ($arg instanceof Closure) {
            $child = (object)array(
                'value' => (object)array(
                    'type' => 'callable',
                    'function' => $arg,
                    'id' => 'id_'.(Component::$api->id_counter++)
                )
            );
            $cmp->children[] = $child;
        } 
        else if (is_object($arg)) {
            $cmp->children[] = $arg;
        }
    }
    return $cmp;
}

function template($parent=false) {
    
    global $current_template,$attributes;
    $child_template = $current_template;
    
    $args = func_get_args();
    $blocks = array("root#root");
    foreach ($args as $arg) {
        if (!is_string($arg)) $blocks[] = $arg;
    }
    $root = $current_template = call_user_func_array('block',$blocks);
    $current_template->name = uniqid();
    
    if (is_string($parent)) {
        include partial($parent);
    }
    
    Component::$api->templates->{$current_template->name} = $current_template;
    if ($child_template) {
        $child_template->value->parentTemplate = $current_template->name;
    }
    
    if (!$child_template) {
        ?>
            <!doctype html>
            <html>
            <head>
                <meta charset="utf-8" />
                <title><?= @$attributes['title'] ?></title>
                <script>var base_url = "<?=url('')?>"</script>
                
                <? if (isset($_GET['dev']) && \CMS\Models\User::checkLoggedIn()): ?>
                    <? 
                        if (isset($_POST['css'])) {
                            file_put_contents(__DIR__."/../../assets/style.css",$_POST['css']);
                            if (isset($_POST['js'])) {
                                file_put_contents(__DIR__."/../../assets/script.js",$_POST['js']);
                            }
                            die();
                        }
                    ?>
                
                    <script tea="<?=t_url('assets/tea/makefile.tea')?>"></script>
                    <script src="http://code.jquery.com/jquery-1.7.2.js"></script>
                    <script src="/~boomyjee/teacss/lib/teacss.js"></script>
                    <script>
                        teacss.buildCallback = function (files) {
                            var dir = teacss.path.absolute("../src/teacss-ui/style")+"/";
                            var css = files['/default.css'];
                            while (css.indexOf(dir)!=-1) css = css.replace(dir,"");
                            $.post(location.href,{css:css,js:files['/default.js']},function(data){
                                alert('ok');
                            });
                        }
                        teacss.update();
                    </script>    
                <? else: ?>
                    <link rel="stylesheet" type="text/css" href="<?=t_url('assets/style.css')?>">
                    <script src="<?=t_url('assets/script.js')?>"></script>
                <? endif ?>
            </head>
                <? Component::$api->view($current_template->name,false,false) ?>
            </html>
        <?
    }
    $current_template = $child_template;
}

