<?php

include __DIR__."/LiquidHelpers.php";

class CandyTemplaterApi extends TemplaterApi {
    function __construct() {
        parent::__construct(false);
        $this->base_url = dirname(INDEX_URL);
        $this->base_dir = dirname(INDEX_DIR);
        $this->settingsPath = __DIR__."/../../settings.json";
    }
    
    function publish() {
        $files = $_REQUEST['files'];
        foreach ($files as $path=>$text) {
            $path = __DIR__."/../publish".$path;

            $mark = "data:image/png;base64,";
            if (strpos($text,$mark)===0)
                $text = base64_decode(substr($text,strlen($mark)));
            
            $res = file_put_contents($path,$text);
        }
        copy(__DIR__."/../settings.json",__DIR__."/../publish/settings.json");
    }
    
    function getDataSources() {
        $res = array();
        $types = \Blog\Configuration::$post_types;
        foreach ($types as $key=>$type) {
            $posts = \Blog\Models\Post::findBy(array('type'=>$key),'id ASC');
            foreach ($posts as $post) {
                $res[$key."/".$post->id] = $type->singular_label.": " . $post->title;
            }
        }
        return $res;
    }
    
    function liquid($template,$dataSource) {
        $data = \Bingo\Routing::$controller->data;
        
        $current = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $data['url'] = trim(str_replace(url(""),"",$current),"/");
        
        $liquid = new \LiquidTemplate();
        $liquid->registerFilter("LiquidThemeFilters");
        $tpl = $liquid->parse($template);
        return $tpl->render($data);        
    }
    
    function getComponents() {
        $me = $this;
        $list = parent::getComponents();
        $list['callable'] = array(
            'update' => function ($val) {
                $data = \Bingo\Routing::$controller ? \Bingo\Routing::$controller->data : array();
                ob_start();
                $f = $val['function'];
                $f($data);
                $html = ob_get_clean();
                return array(
                    'html' => '<div>'.$html."</div>"
                );
            }        
        );
        
        $list['liquid'] = array(
            'name' => 'Template',
            'description' => 'Liquid template block',
            'category' => 'Blocks',
            'update' => function ($val,$dataSource) use ($me) {
                $template = @$val['template']; 
                return array(
                    'form' => "<textarea name='template' rows='20' spellcheck='false'>".htmlentities($template)."</textarea>",
                    'value' => $val,
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }
        );
        
        $list['post_title'] = array(
            'name' => 'Post Title',
            'description' => 'Post or page title',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                return array(
                    'html' => '<div>'.$me->liquid('<h2 class="page-title">{{ post.title }}</h2>',$dataSource)."</div>"
                );
            }
        );
        
        $list['post_content'] = array(
            'name' => 'Post Content',
            'description' => 'Post or page content',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                return array(
                    'html' => '<div>'.$me->liquid('{{ post.content }}',$dataSource)."</div>"
                );
            }
        );        
        
        $list['post_comments'] = array(
            'name' => 'Post Comments',
            'description' => 'Post comments and comment form',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $template = '
                    <div class="comments">
                    {% for comment in post.comments %}
                        <div class="comment" id="comment-{{ comment.id }}">
                            {% if comment.type=="pingback" or comment.type=="trackback" %}
                            {% else %}
                                {{ comment.author }} says on {{ comment.date }}:
                                {% if not comment.approved %}
                                    <em class="comment-awaiting-moderation">Your comment is awaiting moderation.</em>
                                {% endif %}
                            
                                <div class="comment-body">
                                    {{ comment.content }}
                                </div>
                            
                            {% endif %}
                        </div>
                    {% endfor %}
                    </div>                
                    {{ post.comment_form | raw }}
                ';
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }            
        );
        
        $list['blog_posts'] = array(
            'name' => 'Blog Posts',
            'description' => 'Latest posts',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $template = '
                    {% assign posts = "post_type=post&post_count=10&post_status=publish" | posts %}
                    {% for post in posts %}
                        <article class="post">
                            <div class="post-header">
                                <h3 class="post-title"><a href="{{ post.permalink }}">{{ post.title }}</a></h3>
                                <span class="date">Posted on {{ post.date }}</span>
                            </div>
                            <div class="post-content">
                                {{ post.excerpt }}
                            </div>
                            <div class="post-footer">
                                <span class="author">
                                    {% if post.author %}
                                        <i class="icon icon-user"></i>
                                        Written by <span class="author-name">{{ post.author }}</span>
                                    {% endif %}
                                </span>
                                <span class="comments">
                                    <i class="icon icon-comments"></i>
                                    <a href="{{ post.permalink }}#comments">
                                        {{ post.comment_count }} 
                                        {% if post.comment_count == 1 %}
                                            comment
                                        {% else %}
                                            comments 
                                        {% endif %}
                                    </a>
                                </span>
                            </div>
                        </article>
                    {% endfor %}
                ';
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }
        );
        
        $list['portfolio_list'] = array(
            'name' => 'Portfolio list',
            'description' => 'Portfolio works with tags',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                
                $columns = (int)@$val['columns'];
                if (!$columns) $columns = 4;
                
                $settings = $me->getSettings();
                $width = ((int)$settings['theme']['sheet']['width']);
                $thumbWidth = (int)(($width - 10*($columns-1)) / $columns);
                
                $template = '
                    <div class="tag-filter">
                        <a href="#" class="tag-all active">All tags</a>
                        {% assign tags = "portfolio" | tags %}
                        {% for tag in tags %}
                            <a href="#" class="tag-{{ tag }}">{{ tag }}</a>
                        {% endfor %}
                    </div>
                    {% assign posts = "post_type=portfolio" | posts %}
                    <div class="portfolio">
                        {% for work in posts %}
                            <div style="width:'.$thumbWidth.'px;height:'.$thumbWidth.'px" class="work tag-all{% for tag in work.tags %} tag-{{ tag }}{% endfor %}">
                            <img src="{{ work.preview_image | image_url: \''.$thumbWidth."x".$thumbWidth.'\' }}">
                            <a class="enlarge" href="{{ work.permalink }}">Enlarge</a>
                            <a class="visit" href="{{ work.url }}" target="_blank">Visit website</a>
                            <h3>{{ work.title }}</h3>
                            <p>{{ work.excerpt | truncatewords: 10 }}</p>
                        </div>
                        {% endfor %}
                    </div>
                ';
                return array(
                    'form' => '
                        <label>Number of columns</label>
                        <input name="columns" value="'.$columns.'">
                    ',
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>",
                    'value' => $val
                );
            }
        );
        
        $list['slider'] = array(
            'name' => 'Slider',
            'description' => 'Slider based on user data',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $src = @$val['src'] ? : "post.images";
                $one_src = @$val['one_src'] ? : "{{ one.url }}";
                $template = '
                    <div id="gallery">
                        {% assign images = '.$src.' %}
                        {% for one in images %}
                        <div>
                            <img src="'.$one_src.'">
                        </div>
                        {% endfor %}
                    </div>
                ';
                
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>",
                    'value' => $val
                );
            }
        );
        
        $list['google_map'] = array(
            'name' => 'Google map',
            'description' => 'Map for certain location',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $id = $val['id'];
                $map_id = $id."_map";
                
                $zoom = @$val['zoom'] ? : "{{ post.map_zoom }}";
                $coordinates = @$val['coordinates'] ? : "{{ post.map_location.coordinates }}";
                
                $template = '
                    <div>
                        <div id="'.$map_id.'" style="width:100%;height:400px"></div>
                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                        <script>
                            google.load("maps","3",{
                                other_params: "sensor=false",
                                callback: function(){
                                    $(function(){
                                        var mapOptions = {
                                            zoom: '.$zoom.',
                                            center: new google.maps.LatLng('.$coordinates.'),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        };
                                        var map = new google.maps.Map(document.getElementById("'.$map_id.'"),mapOptions);
                                    });
                                }
                            });
                        </script>    
                    </div>
                ';
                
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }
        );
        return $list;
    }
}