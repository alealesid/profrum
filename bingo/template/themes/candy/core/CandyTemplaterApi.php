<?php

include __DIR__."/LiquidHelpers.php";

class CandyTemplaterApi extends TemplaterApi {
    public function __construct() {
        parent::__construct(false);
        
        $this->settingsPath = realpath(__DIR__."/..")."/settings.json";
        $this->templatePath = realpath(__DIR__."/..")."/view";
        // $this->uploadDir = realpath(__DIR__."/..")."/upload";
        
        $this->base_url = dirname(INDEX_URL);
        $this->base_dir = dirname(INDEX_DIR);
        
        $this->includeModules(array("core",__DIR__."/templater_modules/candy"));
    }
    
    static $instance = false;
    static function get() {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    function publish() {
        $files = $_REQUEST['files'];
        foreach ($files as $path=>$text) {
            $path = __DIR__."/../publish".$path;

            $mark = "data:image/png;base64,";
            if (strpos($text,$mark)===0)
                $text = base64_decode(substr($text,strlen($mark)));
            
            $res = file_put_contents($path,$text);
        }
        copy(__DIR__."/../settings.json",__DIR__."/../publish/settings.json");
    }
    
    function getComponents() {
        $me = $this;
        $list = parent::getComponents();
        /*$list['callable'] = array(
            'update' => function ($val) {
                ob_start();
                if (@$val['function']) {
                    $val['function']();
                }
                $html = ob_get_clean();
                return array(
                    'html' => '<div>'.$html."</div>"
                );
            }        
        );*/
        
        $list['post_title'] = array(
            'name' => 'Post Title',
            'description' => 'Post or page title',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                return array(
                    'html' => '<div>'.$me->liquid('<h2 class="page-title">{{ post.title }}</h2>',$dataSource)."</div>"
                );
            }
        );
        
        $list['post_content'] = array(
            'name' => 'Post Content',
            'description' => 'Post or page content',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                return array(
                    'html' => '<div>'.$me->liquid('{{ post.content }}',$dataSource)."</div>"
                );
            }
        );        
        
        $list['post_comments'] = array(
            'name' => 'Post Comments',
            'description' => 'Post comments and comment form',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $template = '
                    <div class="comments">
                    {% for comment in post.comments %}
                        <div class="comment" id="comment-{{ comment.id }}">
                            {% if comment.type=="pingback" or comment.type=="trackback" %}
                            {% else %}
                                {{ comment.author }} says on {{ comment.date }}:
                                {% if not comment.approved %}
                                    <em class="comment-awaiting-moderation">Your comment is awaiting moderation.</em>
                                {% endif %}
                            
                                <div class="comment-body">
                                    {{ comment.content }}
                                </div>
                            
                            {% endif %}
                        </div>
                    {% endfor %}
                    </div>                
                    {{ post.comment_form | raw }}
                ';
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }            
        );
        
        $list['portfolio_list'] = array(
            'name' => 'Portfolio list',
            'description' => 'Portfolio works with tags',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                
                $columns = (int)$val['columns'];
                if (!$columns) $columns = 4;
                
                $settings = $me->getSettings();
                $width = ((int)$settings['theme']['sheet']['width']);
                $thumbWidth = (int)(($width - 10*($columns-1)) / $columns);
                
                $template = '
                    <div class="tag-filter">
                        <a href="#" class="tag-all active">All tags</a>
                        {% assign tags = "portfolio" | tags %}
                        {% for tag in tags %}
                            <a href="#" class="tag-{{ tag }}">{{ tag }}</a>
                        {% endfor %}
                    </div>
                    {% assign posts = "post_type=portfolio" | posts %}
                    <div class="portfolio">
                        {% for work in posts %}
                            <div style="width:'.$thumbWidth.'px;height:'.$thumbWidth.'px" class="work tag-all{% for tag in work.tags %} tag-{{ tag }}{% endfor %}">
                            <img src="{{ work.preview_image | image_url: \''.$thumbWidth."x".$thumbWidth.'\' }}">
                            <a class="enlarge" href="{{ work.permalink }}">Enlarge</a>
                            <a class="visit" href="{{ work.url }}" target="_blank">Visit website</a>
                            <h3>{{ work.title }}</h3>
                            <p>{{ work.excerpt | truncatewords: 10 }}</p>
                        </div>
                        {% endfor %}
                    </div>
                ';
                return array(
                    'form' => '
                        <label>Number of columns</label>
                        <input name="columns" value="'.$columns.'">
                    ',
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>",
                    'value' => $val
                );
            }
        );
        
        $list['slider'] = array(
            'name' => 'Slider',
            'description' => 'Slider based on user data',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $template = '
                    <div id="gallery">
                        {% assign images = post.images %}
                        {% for one in images %}
                        <div>
                            <img src="{{ one.url }}">
                        </div>
                        {% endfor %}
                    </div>
                ';
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>",
                    'value' => $val
                );
            }
        );
        
        $list['google_map'] = array(
            'name' => 'Google map',
            'description' => 'Map for certain location',
            'category' => 'Content',
            'update' => function ($val,$dataSource) use ($me) {
                $id = $val['id'];
                $map_id = $id."_map";
                
                $template = '
                    <div>
                        <div id="'.$map_id.'" style="width:100%;height:400px"></div>
                        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                        <script>
                            google.load("maps","3",{
                                other_params: "sensor=false",
                                callback: function(){
                                    $(function(){
                                        var mapOptions = {
                                            zoom: {{ post.map_zoom }},
                                            center: new google.maps.LatLng({{ post.map_location.coordinates }}),
                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                        };
                                        var map = new google.maps.Map(document.getElementById("'.$map_id.'"),mapOptions);
                                    });
                                }
                            });
                        </script>    
                    </div>
                ';
                
                return array(
                    'html' => '<div>'.$me->liquid($template,$dataSource)."</div>"
                );
            }
        );
        return $list;
    }
}