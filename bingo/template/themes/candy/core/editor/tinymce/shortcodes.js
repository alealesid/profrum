(function() {  
    var $ = teacss.jQuery;
	var internalName = 'data-mce-blox-editable';
	var VK = tinymce.VK;
    var ui = teacss ? teacss.ui : {};

    function handleSelection(ed) {
        var dom = ed.dom;
        var selection = ed.selection;
        var caretContainerClass = 'mceCaretContainer';
        var invisibleChar = '\uFEFF';        
        
		// Returns the content editable state of a node "true/false" or null
		function getContentEditable(node) {
            return node.getAttribute ? (node.getAttribute(internalName) || null) : null;
		};
        
        // return location object for range position (startNode,offset)
        function getLocation(start,offset) {
            var location = {node:start,offset:offset};
            
            // check normal state
            var node = start;
			while (node) {
				var state = getContentEditable(node);
                if (state) {
                    var isLast = (start.nodeType==3 && offset==start.nodeValue.length) || (start.nodeType!=3 && offset==start.childNodes.length);
                    
                    if (state==="false") {
                        location.parent = node;
                        // before state
                        if (isLast) {
                            var up = start;
                            while (up.parentNode && up.parentNode.lastChild==up) up = up.parentNode;
                            if (up.nextSibling && getContentEditable(up.nextSibling)==="false") {
                                if (up.nextSibling.nodeName!="DIV") {
                                    location.parent = up.nextSibling;
                                    location.select = false;
                                    location.before = true;
                                    return location;
                                } else {
                                    return location;
                                }
                            }
                        }
                        if (start==node.firstChild && offset==0) location.before = true;
                        // after state
                        if (start==node.lastChild && offset==1) location.after = true;
                        location.select = !location.before && !location.after;
                    } else {
                        location.editable = true;
                        location.parent = node;
                        
                        // first state
                        if (offset==0) {
                            var up = start;
                            while (up.parentNode && up.parentNode.firstChild==up) {
                                if (up==node) break;
                                up = up.parentNode;
                            }
                            if (up==node) {
                                location.first = true;
                            }
                        }
                        // last state
                        if (isLast) {
                            var up = start;
                            while (up.parentNode && up.parentNode.lastChild==up) {
                                if (up==node) break;
                                up = up.parentNode;
                            }
                            if (up==node) {
                                location.last = true;
                            }
                        }
                        
                    }
                    return location;
                }
				node = node.parentNode;
			}
            return location;
        }
        
        // create caret container before or after shortcode
        function positionCaret(target,before,char) {
            var rng = selection.getRng(true);
            var caretContainer = ed.getDoc().createTextNode(String.fromCharCode(char));
			if (before) {
				target.parentNode.insertBefore(caretContainer, target);
			} else {
				dom.insertAfter(caretContainer, target);
			}
            rng.setStart(caretContainer, 1);
			rng.collapse(true);
			selection.setRng(rng);
			return caretContainer;            
        }
        
        function moveSelection() {
            var rng = selection.getRng(true);
			var isCollapsed = rng.collapsed;
            var locStart = getLocation(rng.startContainer,rng.startOffset);
            var locEnd = getLocation(rng.endContainer,rng.endOffset);
            
            $(".mceSelected",ed.getDoc()).removeClass("mceSelected");
            
            // Is any of the range endpoints editable
            if (locStart.editable || locEnd.editable) {
                var loc = locEnd.editable ? locEnd : locStart;
                if (!locStart.editable || !locEnd.editable || locStart.parent!=locEnd.parent) {
                    var rng = selection.getRng(true);
                    rng.setStart(loc.node,loc.offset);
                    rng.setEnd(loc.node,loc.offset);
                    selection.setRng(rng);
                }
                if (loc.parent.childNodes.length==0 || (loc.parent.childNodes.length==1 && loc.parent.firstChild.nodeName=="BR")) {
                    $(loc.parent).html('<p><br data-mce-bogus="1" /></p>');
                    var rng = selection.getRng(true);
                    rng.setStart(loc.parent.firstChild,0);
                    rng.setEnd(loc.parent.firstChild,0);
                    selection.setRng(rng);
                }
            }            
            
			// Is any of the range endpoints noneditable
			else if (locStart.select || locEnd.select) {
				if (isCollapsed) {
                    selection.setCursorLocation(locStart.parent.firstChild,1);
                    $(locStart.parent).addClass("mceSelected");
				} else {
                    var rng = selection.getRng(true);
                    // Expand selection to include start non editable element
                    if (locStart.parent) rng.setStartBefore(locStart.parent);
                    // Expand selection to include end non editable element
                    if (locEnd.parent) rng.setEndAfter(locEnd.parent);
                    selection.setRng(rng);
				}
			}
            
            // debugging
            setTimeout(function(){
                var rng = selection.getRng(true);
                var isCollapsed = rng.collapsed;
                console.debug(rng.startContainer,rng.startOffset,rng.endContainer,rng.endOffset);
            },1);
        }
		ed.onMouseUp.addToTop(moveSelection);
		ed.onKeyUp.addToTop(moveSelection);
        
        ed.onKeyDown.addToTop(function(ed,e){
            var keyCode = e.keyCode;
            var rng = selection.getRng(true);
            
            var locStart = getLocation(rng.startContainer,rng.startOffset);
            var locEnd = getLocation(rng.endContainer,rng.endOffset);
            
            var loc = locEnd.parent ? locEnd : locStart;
            
            if (loc.editable) {
                if (loc.first && (keyCode==VK.LEFT || keyCode==VK.TOP || keyCode==VK.BACKSPACE)) {
                    e.preventDefault();
                    return false;
                }
                if (loc.last && (keyCode==VK.RIGHT || keyCode==VK.BOTTOM || keyCode==VK.DELETE)) {
                    e.preventDefault();
                    return false;
                }
                return;
            }
            
            if (loc.parent && loc.select) {
                if (keyCode == VK.LEFT) {
                    selection.setCursorLocation(loc.parent.firstChild,0);
                } 
                if (keyCode == VK.UP && loc.parent.nodeName=="DIV") {
                    selection.setCursorLocation(loc.parent.firstChild,0);
                }
                if (keyCode == VK.RIGHT) {
                    selection.setCursorLocation(loc.parent.lastChild,1);
                }
                if (keyCode == VK.DOWN && loc.parent.nodeName=="DIV") {
                    selection.setCursorLocation(loc.parent.lastChild,1);
                }
                if (keyCode == VK.DELETE || keyCode == VK.BACKSPACE) {
                    dom.remove(loc.parent);
                }
                
                e.preventDefault();
                return false;
            }
            
            if (loc.parent && !loc.select) {
                if (   (keyCode == VK.LEFT && loc.after) 
                    || (keyCode == VK.RIGHT && loc.before)
                ) 
                {
                    selection.setCursorLocation(loc.parent.firstChild,1);
                    e.preventDefault();
                    return false;
                }
                
                if ((keyCode == VK.BACKSPACE && loc.after) || (keyCode == VK.DELETE && loc.before)) {
                    dom.remove(loc.parent);
                    e.preventDefault();
                    return false;
                }
                
                if (keyCode != VK.LEFT && keyCode != VK.RIGHT && keyCode != VK.DELETE && keyCode != VK.BACKSPACE) {
                    if (
                        (keyCode>=48  && keyCode<=90) ||
                        (keyCode>=106 && keyCode<=111) ||
                        (keyCode>=186 && keyCode<=192) ||
                        (keyCode>=219 && keyCode<=222)
                    ) {
                        positionCaret(loc.parent,loc.before,keyCode);
                        e.preventDefault();
                        return false;
                    }
                    if (keyCode==VK.ENTER) {
                        positionCaret(loc.parent,loc.before,32);
                    }
                }
            }
            
            return false;
        });
    }
    
    tinymce.create('tinymce.plugins.shortcodes', {  
        
        css: {},
        list: [],
        registered: {},
        
        getActions: function (options) {
            var action = "";
            if (options.actions) {
                actions = "<span class='mceActions'>";
                for (var key in options.actions) {
                    var label = options.actions[key].label || key;
                    actions += "<button class='"+key+"'>"+label+"</button>";
                }
                actions += "</span>";
            }
            return actions;
        },
        
        getBefore: function(options,atts) {
            var before = options.before;
            if (before.call) before = before(atts);
            var tag = options.inline ? "span" : "div";
            var id = "shortcode_"+this.list.length;
            this.list.push({
                options: options,
                atts: atts,
                id: id
            });
            var cls = options.beforeClass || ("mceShortcode mceShortcode_"+options.code);
            if (cls.call) cls = cls.call(this,atts);
            var place = options.inner ? '' : '_';
            var actions = options.inner ? this.getActions(options) : '';
            return "<"+tag+" id='"+id+"' class='"+cls+"' data-mce-blox-editable='false'>" + place + actions + before;
        },
        
        getAfter: function(options) {
            var tag = options.inline ? "span" : "div";
            var place = options.inner ? '' : '_';
            var actions = options.inner ? '' : this.getActions(options);
            return options.after + actions + place + "</"+tag+">";
        },
        
        registerShortcode: function (options) {
            var me = this;
            me.registered[options.code] = options;

            //replace shortcode before editor content set
			this.editor.onBeforeSetContent.add(function(ed, o) {
                
                var re = new RegExp("(<p>)?\\["+options.code+"([^\\]]*)\\](<\\/p>)?","g");
                o.content = o.content.replace(re,function (s,p1,atts,p2) {
                    p1 = p1 || ""; p2 = p2 || "";
                    
                    var hash = {};
                    var att_re = /(\w+)\s*=\s*("(\\"|[^"])*")/g;
                    var att;
                    while (att = att_re.exec(atts)) {
                        hash[att[1]] = $.parseJSON(att[2]);
                    }
                    return options.inline ? p1+me.getBefore(options,hash)+p2 : me.getBefore(options,hash);
                });
                
                re = new RegExp("(<p>)?\\[\\/"+options.code+"\\](<\\/p>)?","g");
                o.content = o.content.replace(re,function(s,p1,p2){
                    p1 = p1 || ""; p2 = p2 || "";
                    return options.inline ? p1+me.getAfter(options)+p2 : me.getAfter(options);
                });
			});
            
            // register button
            if (options.button) this.editor.addButton("button_"+options.code, {  
                title : options.button.title,
                image : options.button.image,  
                onclick : function() {
                    var html = me.getBefore(options,{}) + (options.defaultContent || "") + me.getAfter(options);
                    var start = me.editor.selection.getStart();
                    if ($(start).attr(internalName)==="false")
                        $(start).after(html);
                    else
                        me.editor.selection.setContent(html);
                }
            });  
        
            // extra css if needed
            this.editor.onInit.add(function() {
                if (options.css && !me.css[options.css]) {
                    me.css[options.css] = true;
                    var linkElm = me.editor.dom.create('link', {rel : 'stylesheet', href : options.css });
                    me.editor.getDoc().getElementsByTagName('head')[0].appendChild(linkElm);        
                }
            });
        },
            
        editAction: function (controlsCallback) {
            return function (ed,e) {
                var dlg = $(e.target).data("dialog");
                var one = this;
                
                var el = $("#"+one.id,ed.getDoc());
                var content = el.find(one.options.contentSelector||">*:not(.mceActions)").html();
                
                function update(atts,content) {
                    var before = one.options.before;
                    if (before.call) before = before(atts);
                    var after = one.options.after;
                    
                    el.children().eq(0).replaceWith(before+$("<div>"+content+"</div>").html()+after);
                }
                
                if (!dlg) {
                    dlg = ui.dialog({
                        width: 400,
                        title: "Edit button",
                        buttons: {
                            "Save": function () {
                                one.atts = dlg.form.getValue();
                                delete one.atts.content;
                                $(this).dialog("close");
                            },
                            "Cancel": function () {
                                update(one.atts,content);
                                $(this).dialog("close");
                            }
                        },
                        position: { my: "center", at: "center", of: ed.getContainer() }
                    });
                    dlg.form = ui.form(function(){
                        controlsCallback(dlg);
                    });
                    dlg.form.bind("change",function(){
                        var val = this.getValue();
                        update(val,val.content);
                    });
                    $(e.target).data("dialog",dlg);
                }
                dlg.form.setValue($.extend({},this.atts,{content:content}));
                dlg.open();
            }
        },
            
        removeAction: function () {
            return function (ed,e) {
                $("#"+this.id,ed.getDoc()).remove();
            }
        },
            
        initCommon: function () {
            var ed = this.editor;
            var me = this;
            
            // extra css if needed
            this.editor.onInit.add(function() {
                $("body",me.editor.getDoc()).attr("spellcheck","false");
            });
            
			ed.onPreInit.add(function() { handleSelection(ed); });
            ed.onClick.add(function(ed,e) { 
                if ($(e.target).is(".mceActions > button")) {
                    var action = $(e.target).attr("class");
                    var id = $(e.target).parents(".mceShortcode").attr("id");
                    var one = me.list[parseInt(id.substring("shortcode_".length))];
                    action = one.options.actions[action];
                    if (action && action.call) action.call(one,ed,e);
                }
            });
            
            ed.onPreProcess.add(function(ed, o) {
                $($(o.node).find(".mceShortcode").get().reverse()).each(function(){
                    var id = $(this).attr("id");
                    var one = me.list[parseInt(id.substring("shortcode_".length))];
                    var content = $(this).find(one.options.contentSelector||">*:not(.mceActions)").html();
                    var atts = [""];
                    for (var key in one.atts) atts.push(key+"="+JSON.stringify(one.atts[key]));
                    atts = atts.join(" ");
                    
                    var code = one.options.code;
                    $(this).replaceWith("["+code+atts+"]"+content+"[/"+code+"]");
                });
            });
        },
            
        init : function(ed, url) {  
            var me = this;
            this.url = url;
            this.editor = ed;
            
            this.initCommon();
            
            ui.text = ui.Control.extend({
                init: function (o) {
                    this._super(o);
                    this.element = $("<input type='text'>");
                    this.element.css({
                        width: o.width,
                        margin: o.margin
                    });
                    var me = this;
                    this.element.bind("keyup mouseup change",function(){
                        me.trigger("change");
                    });
                },
                getValue: function () {
                    return this.element.val();
                },
                setValue: function (val) {
                    return this.element.val(val);
                }
            });

            this.registerShortcode({
                code: 'columns',
                before: "<div>", after: "</div>",
                css: url + "/shortcodes.css",
                
                button: { title: "Columns", image: url + "/icons/button-columns.png"},
                actions: {
                    'add column': function (ed,e) {
                        var div = $(e.target).parent().parent().find("> div");
                        var col = me.registered.column;
                        div.find("br").remove();
                        div.append(
                            me.getBefore(col,{class:'oh'})+col.defaultContent+me.getAfter(col)
                        );
                    }
                },
                defaultContent: "<br>"
            });

            this.registerShortcode({
                code: 'column',
                before: "<div "+internalName+"='true'>",
                beforeClass: function (atts) { return "mceShortcode mceShortcode_column "+atts.class; },
                after: "</div>",
                inner: true,
                defaultContent: "<h1>Column title</h1><p>Column content</p>",
                actions: {
                    dec: function () {
                        var f = function (ed,e) {
                            var classes = ["of","ot","oh","tt","tf","one"];
                            
                            var col = $(e.target).parent().parent();
                            var cls = this.atts.class;
                            
                            var idx = classes.indexOf(cls);
                            
                            idx = (idx + classes.length + ($(e.target).is("button.inc") ? 1:-1 ) ) % classes.length;
                            this.atts.class = classes[idx];
                            col.removeClass(cls).addClass(classes[idx]);
                        }
                        f.label = "<";
                        return f;
                    }(),
                    inc: function () {
                        var f = function (ed,e) {
                            return this.options.actions.dec.call(this,ed,e);
                        }
                        f.label = ">";
                        return f;
                    }(),
                    clone: function (ed,e) {
                        var what = $(e.target).parent().parent();
                        var clone = what.clone();
                
                        var atts = {}, id = "shortcode_"+ me.list.length;
                
                        clone.attr("id",id);
                        
                        var atts = {};
                        for (var key in this.atts) 
                            atts[key] = this.atts[key];
                        
                        me.list.push({options:this.options,atts:atts,id:id});
                        what.after(clone);
                    },
                    remove: this.removeAction()
                }
            });

            this.registerShortcode({
                code: 'button',
                inline: true,
                before: function (atts) {
                    var bg = atts.bg || "#eee";
                    return "<a style='background:"+bg+";' class='button'>";
                },
                after: "</a>",
                actions: { 
                    edit: this.editAction(function (dlg) {
                        dlg.push(
                            ui.label("Button text:"),"<br>",
                            ui.text({width:360,name:"content"}),"<br>",
                            ui.label("Button background:"),"<br>",
                            ui.colorPicker({width:200,height:30,name:"bg"})
                        );
                    }),
                    remove: this.removeAction()
                },
                button: { title: "Button", image: url + "/icons/button-button.png" },
                defaultContent: "Button label"
            });
        },  
        createControl : function(n, cm) { return null; }
    });  

    tinymce.PluginManager.add('shortcodes', tinymce.plugins.shortcodes);  
})();