(function() {  
    tinymce.create('tinymce.plugins.shortcodes', {  
        init : function(ed, url) {  
            ed.addButton('youtube', {  
                title : 'Add a Youtube video',  
                image : url+'/icons/button-youtube.png',  
                onclick : function() {  
                     ed.selection.setContent('[youtube id="Enter video ID (eg. Wq4Y7ztznKc)" width="600" height="350"]');  
                }  
            });  
            ed.addButton('vimeo', {  
                title : 'Add a Vimeo video',  
                image : url+'/icons/button-vimeo.png',  
                onclick : function() {  
                     ed.selection.setContent('[vimeo id="Enter video ID (eg. 10145153)" width="600" height="350"]');  
                } 
            });  
             ed.addButton('soundcloud', {  
                 title : 'Add a SoundCloud widget',  
                 image : url+'/icons/soundcloud.png',  
                 onclick : function() {  
                      ed.selection.setContent('[soundcloud url="http://api.soundcloud.com/tracks/15565763" comments="true" auto_play="false" color="ff7700" width="100%" height="81"]');  
                 }  
             });  
            ed.addButton('button', {  
                title : 'Add a button',  
                image : url+'/icons/button-button.png',  
                onclick : function() {  
                     ed.selection.setContent('[button color="e.g. green, darkgreen, orange, blue, red, pink, darkgray, lightgray" size="large or small" gradient1="" gradient2="" link="" target=""]Text here[/button]');  
                }  
            });  
            ed.addButton('dropcap', {  
                title : 'Add a dropcap',  
                image : url+'/icons/button-dropcap.png',  
                onclick : function() {  
                     ed.selection.setContent('[dropcap]...[/dropcap]');  
                }  
            });  
            ed.addButton('highlight', {  
                title : 'Add a highlight',  
                image : url+'/icons/button-highlight.png',  
                onclick : function() {  
                     ed.selection.setContent('[highlight color="eg. yellow, black"]...[/highlight]');  
                }  
            });  
            ed.addButton('checklist', {  
                title : 'Add a checklist',  
                image : url+'/icons/button-checklist.png',  
                onclick : function() {  
                     ed.selection.setContent('[checklist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/checklist]');  
                }  
            });  
            ed.addButton('badlist', {  
                title : 'Add a badlist',  
                image : url+'/icons/button-badlist.png',  
                onclick : function() {  
                     ed.selection.setContent('[badlist]<ul>\r<li>Item #1</li>\r<li>Item #2</li>\r<li>Item #3</li>\r</ul>[/badlist]');  
                }  
            });              
            ed.addButton('tabs', {  
                title : 'Add tabs',  
                image : url+'/icons/button-tabs.png',  
                onclick : function() {  
                     ed.selection.setContent('[tabs tab1=\"Tab 1\" tab2=\"Tab 2\" tab3=\"Tab 3\"]<br /><br />[tab id=1]Tab content 1[/tab]<br />[tab id=2]Tab content 2[/tab]<br />[tab id=3]Tab content 3[/tab]<br /><br />[/tabs]');  
                }  
            });  
            ed.addButton('toggle', {  
                title : 'Add a toggle',  
                image : url+'/icons/button-toggle.png',  
                onclick : function() {  
                     ed.selection.setContent('[toggle title=""]...[/toggle]');  
                }  
            });   
            ed.addButton('one_half', {  
                title : 'Add a one_half column',  
                image : url+'/icons/button-12.png',  
                onclick : function() {  
                     ed.selection.setContent('[one_half last="no"]...[/one_half]');  
                }  
            }); 
            ed.addButton('one_third', {  
                title : 'Add a one_third column',  
                image : url+'/icons/button-13.png',  
                onclick : function() {  
                     ed.selection.setContent('[one_third last="no"]...[/one_third]');  
                }  
            });      
            ed.addButton('two_third', {  
                title : 'Add a two_third column',  
                image : url+'/icons/button-23.png',  
                onclick : function() {  
                     ed.selection.setContent('[two_third last="no"]...[/two_third]');  
                }  
            });              
            ed.addButton('one_fourth', {  
                title : 'Add a one_fourth column',  
                image : url+'/icons/button-14.png',  
                onclick : function() {  
                     ed.selection.setContent('[one_fourth last="no"]...[/one_fourth]');  
                }  
            });  
            ed.addButton('three_fourth', {  
                title : 'Add a three_fourth column',  
                image : url+'/icons/button-34.png',  
                onclick : function() {  
                     ed.selection.setContent('[three_fourth last="no"]...[/three_fourth]');   
                }  
            });  
            ed.addButton('slider', {  
                title : 'Add a slider',  
                image : url+'/icons/slider-icon.png',  
                onclick : function() {  
                     ed.selection.setContent('[slider][slide link=""]image link[/slide][slide link=""]image link here[/slide][/slider]');
                }  
            });  
            ed.addButton('testimonial', {  
                title : 'Add a testimonial',  
                image : url+'/icons/testimonial-icon.png',  
                onclick : function() {  
                     ed.selection.setContent('[testimonial name="John Die" company="My Company"]"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consec tetur, adipisci velit, sed quia non numquam eius modi tempora incidunt utis labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minimas veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur slores amet untras vel illum qui."[/testimonial]');
                }  
            });     
            ed.addButton('progress', {  
                title : 'Add a progress bar',  
                image : url+'/icons/button-progress.png',  
                onclick : function() {  
                     ed.selection.setContent('[progress percentage="60"]Web Design[/progress]');  
                }  
            });  
            ed.addButton('person', {  
                title : 'Add a person',  
                image : url+'/icons/person-button.png',  
                onclick : function() {  
                     ed.selection.setContent('[person name="John Doe" picture="" title="Developer" facebook="http://facebook.com" twitter="http://twitter.com" linkedin="http://linkedin.com" dribbble="http://dribbble.com"]Redantium, totam rem aperiam, eaque ipsa qu ab illo inventore veritatis et quasi architectos beatae vitae dicta sunt explicabo. Nemo enim.[/person]');  
                }  
            });    
            ed.addButton('alert', {  
                title : 'Add an alert message',  
                image : url+'/icons/alert-icon.png',  
                onclick : function() {  
                     ed.selection.setContent('[alert type="e.g. general, error, success, notice"]Your Message Goes Here.[/alert]');  
                }  
            });  
            ed.addButton('pricing_table', {  
                title : 'Add pricing table',  
                image : url+'/icons/pricing-icon.png',  
                onclick : function() {  
                     ed.selection.setContent('[pricing_table type="e.g. 1 or 2"][pricing_column title="Standard"][pricing_price]$10[/pricing_price][pricing_row]Feature 1[/pricing_row][pricing_footer]Signup[/pricing_footer][/pricing_column][/pricing_table]');  
                }  
            });  
            ed.addButton('recent_works', {  
                title : 'Add recent works slider',  
                image : url+'/icons/recent-works-icon.png',  
                onclick : function() {  
                     ed.selection.setContent('[recent_works][/recent_works]');  
                }  
            });  
            ed.addButton('tagline_box', {  
                title : 'Add tagline box',  
                image : url+'/icons/tagline-box.png',  
                onclick : function() {  
                     ed.selection.setContent('[tagline_box link="http://themeforest.net/user/ThemeFusion" button="Purchase Now" title="Avada is incredibly responsive, with a refreshingly clean design" description="And it has some awesome features, premium sliders, unlimited colors, advanced theme options and so much more!"][/tagline_box]');  
                }  
            });        
            ed.addButton('content_boxes', {  
                title : 'Add content boxes',  
                image : url+'/icons/content-boxes.png',  
                onclick : function() {  
                     ed.selection.setContent('[content_boxes]<br />[content_box title="Responsive Design" image="http://theme-fusion.com/avada/wp-content/uploads/2012/07/ico-02.gif" link="http://themeforest.net/user/ThemeFusion" linktext="Learn More"]Avada is fully responsive and can adapt to any screen size. Try resizing your browser window to see the adaptation.[/content_box]<br />[content_box title="Awesome Sliders" image="http://theme-fusion.com/avada/wp-content/uploads/2012/07/ico-02.gif" link="http://themeforest.net/user/ThemeFusion" linktext="Learn More"]Avada includes the awesome Layer Parallax Slider as well as the popular FlexSlider2. Both are super easy to use![/content_box]<br />[content_box title="Unlimited Colors"  image="http://theme-fusion.com/avada/wp-content/uploads/2012/07/ico-03.gif" link="http://themeforest.net/user/ThemeFusion" linktext="Learn More"]We included a backend color picker for unlimited color options. Anything can be changed, including the gradients![/content_box]<br />[content_box last="yes" title="500+ Google Fonts"  image="http://theme-fusion.com/avada/wp-content/uploads/2012/07/ico-04.gif" link="http://themeforest.net/user/ThemeFusion" linktext="Learn More"]Avada loves fonts, choose from over 500+ Google Fonts. You can change all headings and body copy with ease![/content_box]<br />[/content_boxes]');  
                }  
            });  
            ed.addButton('recent_posts', {  
                title : 'Add recent posts',  
                image : url+'/icons/recent-posts.png',  
                onclick : function() {  
                     ed.selection.setContent('[recent_posts thumbnail="yes" title="yes" meta="yes" excerpt="yes"][/recent_posts]');  
                }  
            });              
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('shortcodes', tinymce.plugins.shortcodes);  
})();