<?php

// Raw Shortcode
function my_formatter($content) {
	$new_content = '';
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
    
	foreach ($pieces as $piece) {
		if (preg_match($pattern_contents, $piece, $matches)) {
			$new_content .= $matches[1];
		} else {
			$new_content .= wptexturize(wpautop($piece));
		}
	}
	return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'my_formatter', 11);
add_filter('widget_text', 'my_formatter', 11);


// Youtube shortcode
add_shortcode('youtube', 'shortcode_youtube');
function shortcode_youtube($atts) {
    $atts = shortcode_atts(array(
        'id' => '',
        'width' => 600,
        'height' => 360
    ), $atts);
    return '<div class="video-shortcode"><iframe title="YouTube video player" width="' . $atts['width'] . '" height="' . $atts['height'] . '" src="http://www.youtube.com/embed/' . $atts['id'] . '?wmode=transparent&rel=0" frameborder="0" allowfullscreen></iframe></div>';
}

	

// Vimeo shortcode
add_shortcode('vimeo', 'shortcode_vimeo');
function shortcode_vimeo($atts) {
    $atts = shortcode_atts(array(
        'id' => '',
        'width' => 600,
        'height' => 360
    ), $atts);
    return '<div class="video-shortcode"><iframe src="http://player.vimeo.com/video/' . $atts['id'] . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '" frameborder="0"></iframe></div>';
}

	

// SoundCloud shortcode
add_shortcode('soundcloud', 'shortcode_soundcloud');
function shortcode_soundcloud($atts) {
    $atts = shortcode_atts(array(
        'url' => '',
        'width' => '100%',
        'height' => 81,
        'comments' => 'true',
        'auto_play' => 'true',
        'color' => 'ff7700',
    ), $atts);
    return '<object height="' . $atts['height'] . '" width="' . $atts['width'] . '"><param name="movie" value="http://player.soundcloud.com/player.swf?url=' . urlencode($atts['url']) . '&amp;show_comments=' . $atts['comments'] . '&amp;auto_play=' . $atts['auto_play'] . '&amp;color=' . $atts['color'] . '"></param><param name="allowscriptaccess" value="always"></param><embed allowscriptaccess="always" height="' . $atts['height'] . '" src="http://player.soundcloud.com/player.swf?url=' . urlencode($atts['url']) . '&amp;show_comments=' . $atts['comments'] . '&amp;auto_play=' . $atts['auto_play'] . '&amp;color=' . $atts['color'] . '" type="application/x-shockwave-flash" width="' . $atts['width'] . '"></embed></object>';
}

	

// Button shortcode
add_shortcode('button', 'shortcode_button');
function shortcode_button($atts, $content = null) {
    return '[raw]<a class="button ' . $atts['size'] . ' ' . $atts['color'] . '" href="' . $atts['link'] . '" target="' . $atts['target'] . '">' .do_shortcode($content). '</a>[/raw]';
}

// Dropcap shortcode
add_shortcode('dropcap', 'shortcode_dropcap');
function shortcode_dropcap( $atts, $content = null ) {  
    return '<span class="dropcap">' .do_shortcode($content). '</span>';  
}

// Highlight shortcode
add_shortcode('highlight', 'shortcode_highlight');
function shortcode_highlight($atts, $content = null) {
    $atts = shortcode_atts(array(
        'color' => 'yellow',
    ), $atts);

    if($atts['color'] == 'black') {
        return '<span class="highlight2">' .do_shortcode($content). '</span>';
    } else {
        return '<span class="highlight1">' .do_shortcode($content). '</span>';
    }
}

// Check list shortcode
add_shortcode('checklist', 'shortcode_checklist');
function shortcode_checklist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="arrow">', do_shortcode($content));
	$content = str_replace('<li>', '<li>', do_shortcode($content));
	return $content;
}

// Tabs shortcode
add_shortcode('tabs', 'shortcode_tabs');
function shortcode_tabs( $atts, $content = null ) {
	extract(shortcode_atts(array(
    ), $atts));

    $out .= '[raw]<div class="tabs">[/raw]';
	$out .= '[raw]<ul class="tabs-nav">';

	foreach ($atts as $key => $tab) {
		$out .= '<li><a href="#' . $key . '">' . $tab . '</a></li>';
	}
    $out .= '</ul>[/raw]';
	$out .= do_shortcode($content) .'[raw]</div>[/raw]';
	return $out;
}

add_shortcode('tab', 'shortcode_tab');
function shortcode_tab( $atts, $content = null ) {
	extract(shortcode_atts(array(
    ), $atts));

    $out .= '[raw]<div id="tab' . $atts['id'] . '" class="tab tab_content">[/raw]' . do_shortcode($content) .'</div>';
	return $out;
}
	
// Toggle shortcode
add_shortcode('toggle', 'shortcode_toggle');
function shortcode_toggle( $atts, $content = null ) {
	extract(shortcode_atts(array(
        'title' => '',
        'open' => 'no'
    ), $atts));

	if($open == 'yes'){
		$toggleclass = "active";
		$toggleclass2 = "default-open";
		$togglestyle = "display: block;";
	}

	$out .= '<h5 class="toggle '.$toggleclass.'"><a href="#">' .$title. '</a></h5>';
	$out .= '<div class="toggle-content '.$toggleclass2.'" style="'.$togglestyle.'">';
	$out .= do_shortcode($content);
	$out .= '</div>';

   return $out;
}

// Column one_half shortcode
add_shortcode('one_half', 'shortcode_one_half');
function shortcode_one_half($atts, $content = null) {
    $last = (@$atts['last'] && @$atts['last']!='no') ? ' last' : '';
    return "<div class='oh$last'>" .do_shortcode($content). '</div>'.($last ? '<div class="cl"></div>':'');
}

// Column one_third shortcode
add_shortcode('one_third', 'shortcode_one_third');
function shortcode_one_third($atts, $content = null) {
    $last = (@$atts['last'] && @$atts['last']!='no') ? ' last' : '';
    return "<div class='ot$last'>" .do_shortcode($content). '</div>'.($last ? '<div class="cl"></div>':'');
}

// Column two_third shortcode
add_shortcode('two_third', 'shortcode_two_third');
function shortcode_two_third($atts, $content = null) {
    $last = (@$atts['last'] && @$atts['last']!='no') ? ' last' : '';
    return "<div class='tt$last'>" .do_shortcode($content). '</div>'.($last ? '<div class="cl"></div>':'');
}

// Column one_fourth shortcode
add_shortcode('one_fourth', 'shortcode_one_fourth');
function shortcode_one_fourth($atts, $content = null) {
    $last = (@$atts['last'] && @$atts['last']!='no') ? ' last' : '';
    return "<div class='of$last'>" .do_shortcode($content). '</div>'.($last ? '<div class="cl"></div>':'');
}

// Column three_fourth shortcode
add_shortcode('three_fourth', 'shortcode_three_fourth');
function shortcode_three_fourth($atts, $content = null) {
    $last = (@$atts['last'] && @$atts['last']!='no') ? ' last' : '';
    return "<div class='tf$last'>" .do_shortcode($content). '</div>'.($last ? '<div class="cl"></div>':'');
}

// Icon
add_shortcode('icon', 'shortcode_icon');
function shortcode_icon($atts, $content = null) {
    $href = @$atts['href'] ? "href='".$atts['href']."'"  : '';
    $str = "";
    $str .= "<a class='icon-link' $href title='".@$atts['type']."'>";
    $str .= '<i class="icon icon-'.@$atts['type'].'"></i>';
    $str .= "</a>";
    return $str;
}

// Tagline box shortcode
add_shortcode('tagline_box', 'shortcode_tagline_box');
function shortcode_tagline_box($atts, $content = null) {
    $str = '';
    $str .= '<section class="reading-box">';
    
    if($atts['link'] && $atts['button']):
        $str .= '[raw]<a href="'.$atts['link'].'" class="continue button large green">'.$atts['button'].'</a>[/raw]';
    endif;

    if($atts['title']):
        $str .= '[raw]<h3>'.$atts['title'].'</h3>[/raw]';
    endif;

    if($atts['description']):
        $str.= '[raw]<p>'.$atts['description'].'</p>[/raw]';
    endif;

    $str .= '</section>';
    return $str;
}



// Pricing table
add_shortcode('pricing_table', 'shortcode_pricing_table');
function shortcode_pricing_table($atts, $content = null) {
    $str = '';
    $str .= '[raw]<ul class="plans">[/raw]';
    $str .= do_shortcode($content);
    $str .= '</ul>';
    return $str;
}

// Pricing Column
add_shortcode('pricing_column', 'shortcode_pricing_column');
function shortcode_pricing_column($atts, $content = null) {
    $str = '<li class="'.@$atts['class'].'">';
    $str .= '<table class="price-table">';
    $str .= do_shortcode($content);
    $str .= '</table>';
    $str .= '</li>';

    return $str;
}

// Pricing Row
add_shortcode('pricing_row', 'shortcode_pricing_row');
function shortcode_pricing_row($atts, $content = null) {
    $str = '';
    $str .= '<tr class="'.@$atts['class'].'"><td>';
    $str .= do_shortcode($content);
    $str .= '</td></tr>';
    return $str;
}
add_shortcode('pricing_price', 'shortcode_pricing_price');
function shortcode_pricing_price($atts, $content = null) {
    return shortcode_pricing_row(array('class'=>'price'),$content);
}
add_shortcode('pricing_footer', 'shortcode_pricing_footer');
function shortcode_pricing_footer($atts, $content = null) {
    return shortcode_pricing_row(array('class'=>'footer'),$content);
}


// Content box shortcode
add_shortcode('content_boxes', 'shortcode_content_boxes');
function shortcode_content_boxes($atts, $content = null) {
    $str = '';
    $str .= '<section class="columns">';
    $str .= do_shortcode($content);
    $str .= '</section>';
    return $str;
}
add_shortcode('content_box', 'shortcode_content_box');
function shortcode_content_box($atts, $content = null) {
    $str = '';
    if($atts['last'] == 'yes'):
        $str .= '<article class="col last">';
    else:
        $str .= '<article class="col">';
    endif;

    if($atts['image'] || $atts['title']):
        $str .=	'<div class="heading">';
        if($atts['icon']): 
            $str .= '[raw]<i class="icon icon-' . $atts['icon'] . '"></i>[/raw]';
        endif;

        if($atts['title']):
            $str .= '[raw]<h2>'.$atts['title'].'</h2>[/raw]';
        endif;
        $str .= '</div>';
    endif;

    $str .= do_shortcode($content);

    if($atts['link'] && $atts['linktext']):
        $str .= '[raw]<span class="more"><a href="'.$atts['link'].'">'.$atts['linktext'].'</a></span>[/raw]';
    endif;
    $str .= '</article>';
    return $str;
}



//////////////////////////////////////////////////////////////////

// Slider

//////////////////////////////////////////////////////////////////

add_shortcode('slider', 'shortcode_slider');

	function shortcode_slider($atts, $content = null) {

		$str = '';

		$str .= '<div class="flexslider">';

		$str .= '<ul class="slides">';

		$str .= do_shortcode($content);

		$str .= '</ul>';

		$str .= '</div>';



		return $str;

	}



//////////////////////////////////////////////////////////////////

// Slide

//////////////////////////////////////////////////////////////////

add_shortcode('slide', 'shortcode_slide');

	function shortcode_slide($atts, $content = null) {

		$str = '';

		if($atts['type'] == 'video') {

			$str .= '<li class="video">';

		} else {

			$str .= '<li class="image">';

		}

		if($atts['link']):

		$str .= '<a href="'.$atts['link'].'">';

		endif;

		if($atts['type'] == 'video') {

			$str .= $content;

		} else {

			$str .= '<img src="'.$content.'" alt="" />';

		}

		if($atts['link']):

		$str .= '</a>';

		endif;

		$str .= '</li>';



		return $str;

	}



//////////////////////////////////////////////////////////////////

// Testimonials

//////////////////////////////////////////////////////////////////

add_shortcode('testimonials', 'shortcode_testimonials');

	function shortcode_testimonials($atts, $content = null) {

		$str = '';

		$str .= '<div class="reviews">';

		$str .= do_Shortcode($content);

		$str .= '</div>';



		return $str;

	}



//////////////////////////////////////////////////////////////////

// Testimonial

//////////////////////////////////////////////////////////////////

add_shortcode('testimonial', 'shortcode_testimonial');

	function shortcode_testimonial($atts, $content = null) {

		$str = '';

		$str .= '<div class="review">';

		$str .= '<blockquote>';

		$str .= '<q>';

		$str .= do_Shortcode($content);

		$str .= '</q>';

		if($atts['name']):

			$str .= '<div>';

			$str .= '[raw]<strong>'.$atts['name'].'</strong>[/raw]';

			if($atts['company']):

				$str .= '[raw]<span>, '.$atts['company'].'</span>[/raw]';

			endif;

			$str .= '</div>';

		endif;

		$str .= '</blockquote>';

		$str .= '</div>';



		return $str;

	}



	

// Progress Bar
add_shortcode('progress', 'shortcode_progress');
function shortcode_progress($atts, $content = null) {
	$html = '';
	$html .= '<div class="progress-bar">';
	$html .= '<div class="progress-bar-content" data-percentage="'.$atts['percentage'].'" style="width: ' . $atts['percentage'] . '%">';
	$html .= '</div>';
	$html .= '[raw]<span class="progress-title">' . $content . ' ' . $atts['percentage'] . '%</span>[/raw]';
	$html .= '</div>';
	return $html;
}


// Person
add_shortcode('person', 'shortcode_person');
function shortcode_person($atts, $content = null) {
	$html = '';
	$html .= '<div class="person">';
	$html .= '<img class="person-img" src="' . $atts['picture'] . '" alt="' . $atts['name'] . '" />';
	if($atts['name'] || $atts['title'] || $atts['facebooklink'] || $atts['twitterlink'] || $atts['linkedinlink'] || $content) {
		$html .= '<div class="person-desc">';
        
            $html .= '<div class="person-links">';
                if($atts['facebook']) {
                    $html .= shortcode_icon(array('type'=>'facebook','href'=>$atts['facebook']));
                }
        
                if($atts['twitter']) {
                    $html .= shortcode_icon(array('type'=>'twitter','href'=>$atts['twitter']));
                }
        
                if($atts['linkedin']) {
                    $html .= shortcode_icon(array('type'=>'linkedin','href'=>$atts['linkedin']));
                }
            $html .= '</div>';        
        
            $html .= '<div class="person-name">' . $atts['name'] . '</div>';
            $html .= '<div class="person-title">' . $atts['title'] . '</div>';

			$html .= '<div class="person-content">' . $content . '</div>';
		$html .= '</div>';
	}
	$html .= '</div>';
	return $html;
}



//////////////////////////////////////////////////////////////////

// Recent Posts

//////////////////////////////////////////////////////////////////

add_shortcode('recent_posts', 'shortcode_recent_posts');

function shortcode_recent_posts($atts, $content = null) {

	$attachment = '';

	$html = '<div class="container">';

	$html .= '<section class="columns" style="width:100%">';

	$html .= '<div class="holder">';

	$recent_posts = new WP_Query('showposts=3');

	$count = 1;

	while($recent_posts->have_posts()): $recent_posts->the_post();

	if($count == 3) {

		$html .= '<article class="col last">';

	} else {

		$html .= '<article class="col">';

	}

	if($atts['thumbnail'] == "yes"):

	$post->ID = get_the_ID();

	$args = array(

	    'post_type' => 'attachment',

	    'numberposts' => '5',

	    'post_status' => null,

	    'post_parent' => $post->ID,

		'orderby' => 'menu_order',

		'order' => 'ASC',

		'exclude' => get_post_thumbnail_id()

	);

	$attachments = get_posts($args);

	if($attachments || has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true)):

	$html .= '<div class="flexslider floated-post-slideshow">';

		$html .= '<ul class="slides">';

			if(of_get_option('post_slideshow', 'yes') == 'yes'):

			if(get_post_meta($post->ID, 'pyre_video', true)):

			$html .= '<li class="video">';

				$html .= get_post_meta($post->ID, 'pyre_video', true);

			$html .= '</li>';

			endif;

			endif;

			if(has_post_thumbnail()):

			$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'recent-posts');

			$full_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');

			$attachment_data = wp_get_attachment_metadata(get_post_thumbnail_id());

			$html .= '[raw]<li>

				<a href="'.$full_image[0].'" rel="prettyPhoto["gallery"]"><img src="'.$attachment_image[0].'" alt="'.''.'" /></a>

			</li>[/raw]';

			endif;

			if(of_get_option('post_slideshow', 'yes') == 'yes'):

			foreach($attachments as $attachment):

			$attachment_image = wp_get_attachment_image_src($attachment->ID, 'recent-posts');

			$full_image = wp_get_attachment_image_src($attachment->ID, 'full');

			$attachment_data = wp_get_attachment_metadata($attachment->ID);

			$html .= '[raw]<li>

				<a href="'.$full_image[0].'" rel="prettyPhoto["gallery"]"><img src="'. $attachment_image[0].'" alt="'.$attachment->post_title.'" /></a>

			</li>[/raw]';

			endforeach;

			endif;

		$html .= '[raw]</ul>

	</div>[/raw]';

	endif;

	endif;

	if($atts['title'] == "yes"):

	$html .= '<h3><a href="'.get_permalink($post->ID).'">'.get_the_title().'</a></h3>';

	endif;

	if($atts['meta'] == "yes"):

	$html .= '<ul class="meta">';

	$html .= '<li><em class="date">'.get_the_time('F j, Y', $post->ID).'</em></li>';

	if(get_comments_number($post->ID) >= 1):

	$html .= '<li>'.get_comments_number($post->ID).' Comments</li>';

	endif;

	$html .= '</ul>';

	endif;

	if($atts['excerpt'] == "yes"):

	$html .= '<p>'.string_limit_words(get_the_excerpt(), 15).'</p>';

	endif;

	$html .= '</article>';

	$count++;

	endwhile;

	$html .= '</div>';

	$html .= '</section>';

	$html .= '</div>';



	return $html;

}



//////////////////////////////////////////////////////////////////

// Recent Works

//////////////////////////////////////////////////////////////////

add_shortcode('recent_works', 'shortcode_recent_works');

function shortcode_recent_works($atts, $content = null) {

	$html = '';

	$html .= '<div class="related-posts related-projects">';

	$html .= '<div id="carousel" class="es-carousel-wrapper">';

	$html .= '<div class="es-carousel">';

	$html .= '<ul>';

					$args = array(

						'post_type' => 'avada_portfolio',

						'paged' => 1,

						'posts_per_page' => 10,

					);

					$works = new WP_Query($args);

					while($works->have_posts()): $works->the_post();

					if(has_post_thumbnail()):

					$html .= '<li>';

						$html .= '<div class="image">';

							$html .= '<a href="'.get_permalink($post->ID).'">';

								$html .= get_the_post_thumbnail($post->ID, 'related-img');

								$html .= '<div class="image-extras">';

									$html .= '<div class="image-extras-content">';

										$html .= '<img src="'.get_bloginfo('template_directory').'/images/link-ico.png" alt="'.get_the_title().'"/>';

										$html .= '<h3>'.get_the_title().'</h3>';

									$html .= '[raw]</div>

								</div>

							</a>

						</div>

					</li>[/raw]';

					endif; endwhile;

				$html .= '[raw]</ul>

			</div>

		</div>

	</div>[/raw]';



	return $html;

}

// Alert Message
add_shortcode('alert', 'shortcode_alert');
function shortcode_alert($atts, $content = null) {
	$html = '';
	$html .= '[raw]<div class="alert '.$atts['type'].'">[/raw]';
		$html .= '[raw]<a href="#" class="close">Close</a>[/raw]';
		$html .= '[raw]<div class="msg">'.do_shortcode($content).'</div>[/raw]';
	$html .= '[raw]</div>[/raw]';
	return $html;
}



// Add buttons to tinyMCE
add_action('init', 'add_button');
function add_button() {  
    if (current_user_can('edit_posts') &&  current_user_can('edit_pages')) {  
        add_filter('mce_external_plugins', 'add_plugin');  
        add_filter('mce_buttons_3', 'register_button');  
    }  
}  

function register_button($buttons) {  
   array_push($buttons, "youtube", "vimeo", "soundcloud", "button", "dropcap", "highlight", "checklist", "tabs", "toggle", "one_half", "one_third", "two_third", "one_fourth", "three_fourth", "slider", "testimonial", "progress", "person", "alert", "pricing_table", "recent_works", "tagline_box", "content_boxes", "recent_posts");  
   return $buttons;  
}  

function add_plugin($plugin_array) { 
    $plugin_array['shortcodes'] = get_template_directory_uri().'/core/editor/tinymce/customcodes.js';
    return $plugin_array;  
}  