<?php

add_filter('mce_buttons_2', function ($buttons) {
    array_splice($buttons, 1,0, array('styleselect'));
    return $buttons;
});

add_filter('tiny_mce_before_init', function ($settings) {
    $settings['theme_advanced_blockformats'] = 'p,blockquote,h1,h2,h3,h4,h5,h6';
    
    $style_formats = array(
        array('title' => 'Section heading', 'selector' => 'h1,h2,h3,h4,h5,h6', 'classes'=>'section-heading')
    );
    $settings['style_formats'] = json_encode( $style_formats );

    $settings['extended_valid_elements'] = "div[*]";
    return $settings;
});


