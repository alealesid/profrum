require('./components/blog_posts/blog_posts.js');

Component.app.bind("common-controls",function(d,e){
    Component.app.stylePanel.items[0].push(
        ui.panel({label:"Theme",padding:"10px 5px"}).push(
            ui.fieldset("Blog styles").push(
                ui.blogList({ name: "blog.list" })
            )
        )
    );
});

Component.app.styles.push(
    require.dir + "/style/style.tea"
);