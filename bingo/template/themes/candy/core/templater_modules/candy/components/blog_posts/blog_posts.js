ui.blogList = ui.presetSwitcherCombo.extendOptions({
    width: "100%", margin: 0,
    label: "Blog posts", inlineEditor: true
});

ui.blogList.default = ui.panel.extendOptions({
    label: "Default",
    layout: { display: "block", width: "auto", margin: "10px 10px 0" },
    items: function () { 
        return [];
    }
});

ui.blogList.flat = ui.panel.extendOptions({
    label: "Flat",
    layout: { display: "block", width: "auto", margin: "10px 10px 0" },
    items: function () { 
        return [];
    }
});