<?php

TemplaterApi::addAction('getComponents',function($api,&$components) {
    $components['blog_posts'] = array(
        'name' => 'Blog Posts',
        'description' => 'Latest posts',
        'category' => 'Content',
        'update' => function ($val,$dataSource) use ($api) {
            $template = '
                {% assign posts = "post_type=post&post_count=10&post_status=publish" | posts %}
                {% for post in posts %}
                    <article class="post">
                        <div class="post-header">
                            <h3 class="post-title"><a href="{{ post.permalink }}">{{ post.title }}</a></h3>
                            <span class="date">Posted on {{ post.date }}</span>
                        </div>
                        <div class="post-content">
                            {{ post.excerpt }}
                        </div>
                        <div class="post-footer">
                            {% if post.author %}
                            <span class="author">
                                <i class="icon icon-user"></i>
                                Written by <span class="author-name">{{ post.author }}</span>
                            </span>
                            {% endif %}
                            <span class="comments">
                                <i class="icon icon-comments"></i>
                                <a href="{{ post.permalink }}#comments">
                                    {{ post.comment_count }} 
                                    {% if post.comment_count == 1 %}
                                        comment
                                    {% else %}
                                        comments 
                                    {% endif %}
                                </a>
                            </span>
                        </div>
                    </article>
                {% endfor %}
            ';
            return array(
                'html' => '<div>'.$api->liquid($template,$dataSource)."</div>"
            );
        }
    );
});