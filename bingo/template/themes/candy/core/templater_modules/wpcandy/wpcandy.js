require("./wpcandy.css");

ui.wordpressEditor = ui.control.extend({
    init: function (o) {
        this._super(o);
        this.element = $("<div>");
    },
    setValue: function () {
    }
})
    
ui.widgetControl = ui.control.extend({
    init: function (o) {
        this._super(o);
        this.element = $("<div>");
    }
})

ui.wordpressEditor = ui.control.extend({
    init: function (o) {
        this._super(o);
        this.element = $("<div>");
    },
    
    createEditor: function () {
        var me = this;
        var id = this.id = this.value.id;
        
        $('#wp-'+id+'-wrap').remove();
        this.element.attr({id:'wp-'+id+'-wrap',class:"wp-core-ui wp-editor-wrap tmce-active"});
        this.element.append($("#wp-dummy-wrap .wp-editor-tools")[0].outerHTML.replace(/dummy/g,id));
        
        var html = this.value.html || "";
        html = switchEditors.wpautop(html);
        
        this.element.append(
            $("<div>",{id:'wp-'+id+'-editor-container',class:"wp-editor-container"}).append(
                $("<textarea>",{id:id,rows:30,class:'wp-editor-area'}).val(html)
            )
        );

        // init quick tags, hacky way
        var qtInit = tinyMCEPreInit.qtInit[id] = tinymce.extend({},tinyMCEPreInit.qtInit.dummy,{
            id: id
        });
        var qt = new QTags(qtInit);
        var instances = QTags.instances;
        QTags.instances = {id:qt};
        QTags._buttonsInit();
        QTags.instances = instances;
        
        // init tinymce, hacky way
        var init = tinyMCEPreInit.mceInit[id] = tinymce.extend({},tinyMCEPreInit.mceInit.dummy,{
            elements: id,
            bodyClass: id,
            height: 600,
            // wpautop: false,
            oninit: function () {
                me.editor = tinyMCE.get(me.id);
            }
        });
        tinymce.init(init); 
    },
    
    setValue: function (val) {
        var me = this;
        this._super(val || {});
        if (!this.editor) {
            this.createEditor();
        } else {
            this.editor.setContent(switchEditors.wpautop(this.value.html || ""));
        }
    },
    
    getValue: function () {
        var val = this._super(val);
        this.editor.save();
        val.html = $("#"+this.id).val();
        return val;
    }
});

