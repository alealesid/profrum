<?php

define('TEMPLATER_PATH','/var/www/uxcandy_boomyjee/data/public_html/templater');
require TEMPLATER_PATH."/lib/server/api.php";

include __DIR__."/CandyTemplaterApi.php";

class Candy extends \CMS\Controllers\Admin\BasePrivate {
    
    static $template = false;
    static $api = false;
    
    static function tpl_view() {
        if (!self::$template) return;
        if (!self::$api) return;
        
        $api = self::$api;
        $assets = t_url('publish');
        ?>
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8" />
            <title>Templater View</title>
            <script>var base_url = ""</script>
            <link rel="stylesheet" type="text/css" href="<?=$assets.'/default.css'?>">
            <script src="<?=$assets.'/default.js'?>"> </script>
        </head>           
        <? 
            $api->view(self::$template,false,false) 
        ?>
        </html>
        <?        
    }
    
    function tpl_list() {
        $api = new CandyTemplaterApi;
        $templates = $api->getTemplates();
        $list = array();
        
        foreach ((array)$templates as $name=>$t) {
            $one = (object)$t;
            $one->id = $name;
            $list[] = $one;
        }
        
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => function () use (&$api,&$templates) {
                $list = @$_POST['list'] ? : array();
                foreach ($list as $one) {
                    $id = $one['id'];
                    $check = @$one['check'];
                    if ($check) {
                        unset($templates->$id);
                    }
                }
                $json = json_encode($templates);
                $api->saveTemplates($json);
                redirect('admin/candy/tpl-list');
            }
        );
        $this->data['page_actions']['admin/candy/tpl-edit'] = _t('Create new');
        $this->data['item_actions']['admin/candy/tpl-design'] = _t('design');
        $this->data['fields'] = array('id'=>_t('Name'));
        $this->data['list'] = $list;
        
        $this->data['title'] = _t('Customize');
        $this->view('cms/base-list');
    }
    
    function tpl_edit($id) {
        $api = new CandyTemplaterApi;
        $templates = $api->getTemplates();
        
        $form = new \Bingo\Form;
        $form->text('name',_t('Name'),array('required',function($val) use ($templates) {
            if (isset($templates->$val))
                throw new \ValidationException(_t('This name is already taken'));
            return $val;
        }),$tpl['name']);
        $form->submit(_t('Save template'));
        
        if ($form->validate()) {
            $name = $form->values['name'];
            $templates->$name = array();
            $json = json_encode($templates);
            $api->saveTemplates($json);
            redirect('admin/candy/tpl-list');
        }
        
        $this->data['title'] = _t('New template');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit');
    }
    
    function tpl_design($name) {
        
        theme_base();

        $SITE_ROOT = url('');
        $THEME_ROOT = t_url('');
        
        ?>
        <!doctype html>
        <html>
            
        <head>
            <meta charset="utf-8">
            <title>Customize bingoCandy</title>
                
            <script src="/~boomyjee/dayside/server/assets/teacss/teacss.js"></script>
    
            <script src="/~boomyjee/dayside/server/assets/teacss/teacss-ui.js"></script>
            <link  href="/~boomyjee/dayside/server/assets/teacss/teacss-ui.css" type="text/css" rel="stylesheet">
            
            <script src="/~boomyjee/dayside/client/dayside.js"></script>
            <link  href="/~boomyjee/dayside/client/dayside.css" type="text/css" rel="stylesheet">
    
            <script>
                var SITE_ROOT = "<?= $SITE_ROOT ?>";
                var THEME_ROOT = "<?= $THEME_ROOT ?>";
                require("/~boomyjee/templater/lib/client/app.js",function (templater){
                    templater({
                        ajax_url: SITE_ROOT + "admin/candy/tpl-ajax",
                        template: "<?=$name?>",
                        sidebarWidth: 380,
                        closeLink: SITE_ROOT + "admin",
                        modules: ["core","<?= $THEME_ROOT."core/templater_modules/candy/candy.js" ?>"]
                    });
                });
            </script>
        </head>
        <body>
        </body>
        </html>

        <?
    }    
    
    function tpl_ajax() {
        if (!isset($_REQUEST['_type'])) die();
        $api = new CandyTemplaterApi;    
        $api->run();
    }
}