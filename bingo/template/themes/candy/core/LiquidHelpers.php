<?php

class LiquidThemeFilters {
    public static function url($url) {
        return site_url($url);
    }
    
    public static function t_url($url) {
        return get_bloginfo("stylesheet_directory")."/".ltrim($url,"/");
    }
    
    public static function image_url($id,$size) {
        if (is_numeric($size)) $size = $size."x".$size;
        
        return false;
        
        $res = wp_get_attachment_image_src($id,$size);
        return $res[0];
    }
    
    public static function concat($x,$y) {
        return $x.$y;
    }
    
    public static function at($list,$idx) {
        return @$list[$idx];
    }
    
    public static function _D($x) {
        return _D($x);
    }
    
    public static function posts($s) {
        $params = array();
        parse_str($s,$params);
        
        $criteria = array();
        $count = 10;
        if (isset($params['post_count']))
            $count = $params['post_count'];
        
        if (isset($params['post_type']))
            $criteria['type'] = $params['post_type'];
        
        $list = \Blog\Models\Post::findBy($criteria,false,0,$count);
        return $list;
    }
    
    public static function tags($s) {
        
        return array();
        
        $taxonomies = array('post_tag');
        $post_types = explode(",",$s);
        
        global $wpdb;
        $query = $wpdb->prepare("SELECT t.*, COUNT(*) from $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id WHERE p.post_type IN('".join( "', '", $post_types )."') AND tt.taxonomy IN('".join( "', '", $taxonomies )."') GROUP BY t.term_id",false);
        $results = $wpdb->get_results( $query );
        
        $hash = array();
        foreach ($results as $r) $hash[] = $r->name;
        return $hash;
    }
}

class SiteOptions extends LiquidDrop {
    function __call($name,$args) {
        return get_field($name,'options');
    }
}

class SitePost extends LiquidDrop {
    public $post;
    
    function __construct($post) {
        $post = (array)$post;
        $post = (object)$post;
        $this->post = $post;
    }
    
    function excerpt() {
        $content = $this->post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        return "<p>".$content."</p>";
    }
    
    function content() {
        $content = $this->post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        return $content;
    }
    
    function comments() {
        $list = get_comments(array('post_id'=>$this->post->ID));
        $res = array();
        foreach ($list as $one) {
            $res_one = array();
            foreach ($one as $key=>$val)
                $res_one[str_replace("comment_","",$key)] = $val;
            $res[] = $res_one;
        }
        return $res;
    }
    
    function permalink() {
        return get_permalink($this->post->ID);
    }
    
    function author() {
        return get_the_author_meta( 'user_nicename' , $this->post->post_author );
    }
    
    function comment_form() {
        ob_start();
        comment_form(array(),$this->post->ID);
        return ob_get_clean();
    }
    
    function tags() {
        $results = wp_get_post_tags($this->post->ID);
        foreach ($results as $r) $hash[] = $r->name;
        return $hash;
    }
    
    function to_string() {
        return $this->post->ID;
    }
    
    function __call($name,$args) {
        if (isset($this->post->$name)) return $this->post->$name;
        $postname = "post_".$name;
        if (isset($this->post->$postname)) return $this->post->$postname;
        $res = get_field($name,$this->post->ID);
        if (is_object($res) && isset($res->post_type)) {
            $res = new SitePost($res);
        }
        return $res;
    }
}

class SiteWpMenu extends LiquidDrop {
    
    static $instance;
    static function getInstance() {
        if (!$instance) $instance = new SiteWpMenu;
        return $instance;
    }
    
    function __call($name,$args) {
        return wp_nav_menu(array(
            'menu' => $name,
            'container_class' => 'menu',
            'container_id' => $name,
            'menu_class' => 'container',
            'menu_id' => $name."-list",
            'echo' => 0
        ));
    }
}

class SiteWp extends LiquidDrop {
    function head() {
        ob_start();
        wp_head();
        return ob_get_clean();
    }

    function footer() {
        ob_start();
        wp_footer();
        return ob_get_clean();
    }
    
    function menu() {
        return SiteWpMenu::getInstance();
    }
}