<?php

include __DIR__."/core/Candy.php";

\Bingo\Action::add('admin_pre_header',
function () {
    \Admin::$menu[_t('System','cms')][_t('Appearance','blog')][_t('Customize','candy')] = 'admin/candy/tpl-list';
});

$this->connect('admin/candy/:action/*id',array('controller'=>'Candy','id'=>false));

\Bingo\Action::add('get_include',function($in,$template) {
    $api = CandyTemplaterApi::get();
    $templates = $api->getTemplates();
    if (isset($templates->$template)) {
        Candy::$template = $template;
        Candy::$api = $api;
        return __DIR__."/routing.php";
    }
    return $in;
},10,2);