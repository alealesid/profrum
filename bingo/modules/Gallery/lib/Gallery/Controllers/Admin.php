<?php

namespace Gallery\Controllers;
use \Bingo\FormFilter_DoctrineObject as ObjectFilter;

class Admin extends \CMS\Controllers\Admin\Base
{
    function __construct() {
        parent::__construct();
        bingo_domain('gallery');
        $this->user = \CMS\Models\User::checkLoggedIn();
        if (!$this->user && $this->route('action')!='gallery-upload') redirect('admin/login');
    }
    
    function gallery_options() {
        $options = \CMS\Models\Option::get('gallery_options');
        
        $form = new \Bingo\Form;
        $form->html("<div class='sizeInputs'>")
                ->text('W',_t('Max image size (pixels)'),'',$options['W']?:500)
                    ->html(" x ")
                ->text('H',false,'',$options['H']?:500)
             ->html("</div>");

        $form->html("<div class='sizeInputs'>")
                ->text('tW',_t('Thumbnail size (pixels)'),'',$options['tW']?:100)
                    ->html(" x ")
                ->text('tH',false,'',$options['tH']?:100)
             ->html("</div>");
        $form->submit(_t('Save options'));
        
        if ($form->validate()) {
            \CMS\Models\Option::set('gallery_options',$form->values);
            $this->data['message'] = _t("Gallery options saved");
        }
        
        $this->data['title'] = _t("Gallery options");
        $this->data['form'] = $form->get();
        $this->view('gallery/base-edit',$this->data);
    }
    function gallery_list() {
        $this->data['title'] = _t('Gallery list');
        $this->data['list'] = $this->treeSort(\Gallery\Models\Gallery::findAll());
        
        $this->data['page_actions']['admin/gallery-edit'] = _t('Create new');
        $this->data['item_actions']['admin/gallery-edit'] = _t('edit');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => function ($list) {
                foreach ($list as $key=>$data) {
                    if ($data['check']) {
                        $gallery = \Gallery\Models\Gallery::find($key);
                        if ($gallery) {
                            foreach ($gallery->images as $image) $image->delete(false);
                            $gallery->delete(false);
                        }
                    }
                }
                \Bingo\Bingo::getInstance()->em->flush();
                redirect("admin/gallery-list");
            }
        );
        $this->data['fields'] = array('id'=>_t('#'),'tree_title'=>_t('title'));
        $this->view('cms/base-list',$this->data);
    }
    function gallery_edit($id) {
        $this->data['title'] = _t('Gallery edit');
        $gallery = $this->findOrCreate('\Gallery\Models\Gallery',$id);
            
        $form = new \Bingo\Form('post',array('autocomplete'=>false));
        $form->fieldset(false);
        $form->text('title',_t('Title'),'required',$gallery->title);
        $form->select('parent',_t('Parent'),
            $this->treeSort(\Gallery\Models\Gallery::findAll(),'parent','title','id',true),
            '',$gallery->parent)->add_filter(new ObjectFilter('\Gallery\Models\Gallery'));
        $form->fieldset(_t("Gallery options"),false,array('id'=>'gallery_options'))->add_class("foldable");
        $form->select('options[flag]',false,array(_t('Use defaults')=>0,_t('Use own options')=>1),'',$gallery->options['flag']);

        $form->html("<div class='sizeInputs'>")
                ->text('options[W]',_t('Max image size (pixels)'),'',$gallery->options['W']?:500)->html(" x ")
                ->text('options[H]',false,'',$gallery->options['H']?:500)
             ->html("</div>");
        $form->html("<div class='sizeInputs'>")
                ->text('options[tW]',_t('Thumbnail size (pixels)'),'',$gallery->options['tW']?:100)->html(" x ")
                ->text('options[tH]',false,'',$gallery->options['tH']?:100)
             ->html("</div>");
        
        if (!$gallery->id)
            $form->submit(_t('Create gallery'));
        else 
            $form->submit(_t('Update gallery options'));
            
        if (isset($_POST['with_selected'])) {
            if ($_POST['with_selected']=="delete") {
                if (isset($_POST['images'])) foreach ($_POST['images'] as $id=>$image) {
                    if (isset($image['check'])) {
                        $obj = \Gallery\Models\Image::find($id);
                        if ($obj) {
                            $obj->delete(false);
                        }
                    }
                }
            }
            if ($_POST['with_selected']==0) {
                $i = 0;
                if (isset($_POST['images'])) foreach ($_POST['images'] as $id=>$image) {
                    $obj = \Gallery\Models\Image::find($id);
                    if ($obj) {
                        $obj->title = $image['title'];
                        $obj->description = $image['description'];
                        $obj->sortOrder = $i++;
                        $obj->save(false);
                    }
                }
            }
            $this->em->flush();            
            return redirect("admin/gallery-edit/$gallery->id");
        }

        if ($form->validate()) 
        {
            $form->fill($gallery);
            $gallery->save();
            return redirect("admin/gallery-edit/$gallery->id");
        }
        
        $this->data['title'] = _t('Gallery edit');
        $this->data['form'] = $form->get();
        $this->data['gallery'] = $gallery;
        $this->view('gallery/edit',$this->data);
    }
    
    function gallery_upload($id) {
        if ($id) $gallery = \Gallery\Models\Gallery::find($id);
        if (!$gallery) return;
        
        $gallery_dir = INDEX_DIR."/upload/Gallery/$gallery->id";
        
        if (!file_exists($gallery_dir)) {
            mkdir($gallery_dir);
            mkdir($gallery_dir."/thumbs");
        }
        
        $error = $_FILES['Filedata']['error'];
        if ($error) {
            echo json_encode(array('error'=>$error));
            return;
        }
            
        $filename = $_FILES['Filedata']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        $image = new \Gallery\Models\Image;
        $image->gallery = $gallery;
        $image->upload($_FILES['Filedata']['tmp_name'],uniqid().".".$ext);
        $image->save();
        
        echo json_encode(array(
            'id'      => $image->id,
            'url'     => $image->imageUrl(),
            'thumb'   => $image->thumbUrl(),
            'error'   => 0
        ));
    }
    
}
