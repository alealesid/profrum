<?php

namespace Gallery\Models;

/** 
* @Entity 
* @Table(name="image")
*/
class Image extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=1024) */
    public $title;
    /** @Column(type="text") */
    public $description;
    /** @Column(length=1024) */
    public $file;
    
    /**
     * @ManyToOne(targetEntity="Gallery", inversedBy="images")
     * @JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    public $gallery;
    
    /** @Column(type="integer") */
    public $sortOrder;
    
    function __construct() {
        $this->title = "";
        $this->description = "";
        $this->sortOrder = 10000;
    }
    
    function thumbUrl() {
        return url("upload/Gallery/{$this->gallery->id}/thumbs/$this->file");
    }

    function imageUrl() {
        return url("upload/Gallery/{$this->gallery->id}/$this->file");
    }

    function upload($tempFile,$name) {

        $targetPath = INDEX_DIR."/upload/Gallery/{$this->gallery->id}/";
        $targetFile =  str_replace('//','/',$targetPath) . $name;

        $pathInfo = pathinfo($targetFile);$i=0;
        while(file_exists($targetFile)) {
            $targetFile = "$pathInfo[dirname]/$pathInfo[filename]($i).$pathInfo[extension]"; $i+=1;
        }
        if (!move_uploaded_file($tempFile,$targetFile)) {
            throw new \Exception("File was not uploaded");
        }

        $newName = basename($targetFile);

        $this->file = $newName;
        $this->resizeImage();
        $this->resizeThumb();
        return $newName;
    }

    function getOptions() {
        $flag = ($this->gallery->options && $this->gallery->options['flag']);
        if ($flag) return $this->gallery->options;
        $opts = \CMS\Models\Option::get('gallery_options');
        if ($opts) {
            return $opts;
        } else {
            return array(
                'W' => 10000,
                'H' => 10000,
                'tW' => 100,
                'tH' => 100
            );
        }
    }

    function resizeImage() {
        $options = $this->getOptions();
        $targetFile = INDEX_DIR."/upload/Gallery/{$this->gallery->id}/{$this->file}";
        try {
            $file = \PhpThumb\Factory::create($targetFile,array('resizeUp'=>false));
            $file->resize($options['W'],$options['H']);
            $file->save($targetFile);
        } catch (\Exception $e) {
            trigger_error($e->getMessage());
            return "";
        }
    }

    function resizeThumb() {
        $options = $this->getOptions();
        $targetFile = INDEX_DIR."/upload/Gallery/{$this->gallery->id}/{$this->file}";
        try {
            $file = \PhpThumb\Factory::create($targetFile);
            $file->adaptiveResize($options['tW'],$options['tH']);
            $file->save(INDEX_DIR."/upload/Gallery/{$this->gallery->id}/thumbs/".$this->file);
        } catch (\Exception $e) {
            trigger_error($e->getMessage());
            return "";
        }
    }

    function delete($flush=true) {
        unlink(INDEX_DIR."/upload/Gallery/{$this->gallery->id}/".$this->file);
        unlink(INDEX_DIR."/upload/Gallery/{$this->gallery->id}/thumbs/".$this->file);
        parent::delete($flush);
    }
}
