<?php

namespace Gallery\Models;

/** 
* @Entity 
* @Table(name="gallery")
*/
class Gallery extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=1024) */
    public $title;
    
    /**
     * @OneToMany(targetEntity="Image", mappedBy="gallery")
     * @OrderBy({"sortOrder" = "ASC"})
     */
    public $images;
    
    /** @Column(type="array") */
    public $options;
    
    /**
     * @OneToMany(targetEntity="Gallery", mappedBy="parent")
     */
    public $children;    
    /**
     * @ManyToOne(targetEntity="Gallery", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     */
    public $parent;
}
