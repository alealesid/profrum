<?php

class Gallery extends \Bingo\Module {
    function __construct() {
        $this->addModelPath(dirname(__FILE__)."/Gallery/Models",true);
        
        $this->connect('admin/:action/:data',
            array('controller'=>'\Gallery\Controllers\Admin','data'=>false),
            array('action'=>'(gallery-list|gallery-edit|gallery-upload|gallery-options)')  
        );
        
        \Bingo\Action::add('admin_pre_header',function () {
            \Admin::$menu[_t('Gallery','gallery')][_t('Galleries','gallery')] = 'admin/gallery-list';
            \Admin::$menu[_t('Gallery','gallery')][_t('Options','gallery')] = 'admin/gallery-options';
        });
    }

    function view($id,$template='default',$perPage=false) {
        if (isset($_GET['gallery'])) {
            $gallery = \Gallery\Models\Gallery::find($_GET['gallery']);
        }
        else {
            $gallery = \Gallery\Models\Gallery::find($id);
        }

        if (!$gallery) return "";

        if ($perPage) {
            $page = @$_GET['p'];
            if ($page<=0) $page = 1;
            $query = \Gallery\Models\Image::findByQuery(array('gallery'=>$gallery),'sortOrder');
            if (isset($_GET['gallery']))
                $pattern = "?gallery={$_GET['gallery']}&p=%s";
            else
                $pattern = "?p=%s";
            $pagination = new \Bingo\Pagination($perPage,$page,
                \DoctrineExtensions\Paginate\Paginate::count($query),$pattern,$query);
            $images = $pagination->result();
            $pageCount = $pagination->getPageCount();
            $pagination = $pagination->get();
        } else {
            $images = \Gallery\Models\Image::findBy(array('gallery'=>$gallery),'sortOrder');
            $pagination = false;
            $page = 1;
            $pageCount = 0;
            $pattern = "";
        }

        return \Bingo\Bingo::getInstance()->controller->view("gallery/$template",
            array(
                'gallery' => $gallery,
                'images' => $images,
                'pagination' => $pagination,
                'pattern' => $pattern,
                'page' => $page,
                'pageCount' => $pageCount
            )
        ,true);
    }
}



