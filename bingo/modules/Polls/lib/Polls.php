<?php

class Polls extends \Bingo\Module {
    function __construct() {
        bingo_domain_register('polls',dirname(__FILE__)."/../locale");
        bingo_domain('polls');

        $this->addModelPath(dirname(__FILE__)."/Polls/Models",true);
        $this->connect('admin/:action/:data',array('controller'=>'\Polls\Controllers\Admin','data'=>false),
            array('action'=>'(poll-list|poll-edit)'));
        $this->connect('polls-callback/:id',array('controller'=>'\Polls\Controllers\Poll','action'=>'callback'));

        \Bingo\Action::add('admin_pre_header',function () {
            \Admin::$menu[_t('Polls','polls')][_t('Polls list','polls')] = 'admin/poll-list';
        });
    }
}