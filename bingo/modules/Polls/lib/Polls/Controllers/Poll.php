<?php

namespace Polls\Controllers;

class Poll extends \Bingo\Controller {
    function __construct() {
        theme_base();
    }
    function callback($id) {
        $poll = \Polls\Models\Poll::find($id);
        if (!$poll) return;
        $poll->vote(@$_POST['answer']);
        $this->view('polls/result',array('poll'=>$poll));
    }
}