<?php

namespace Polls\Controllers;

class Admin extends \CMS\Controllers\Admin\BasePrivate {
    function __construct() {
        parent::__construct();
        bingo_domain('polls');
    }
    function poll_list() {
        $this->data['title'] = _t('Polls list');
        $this->data['list'] = \Polls\Models\Poll::findBy(array(),'id DESC');

        $this->data['page_actions']['admin/poll-edit'] = _t('Create new');
        $this->data['item_actions']['admin/poll-edit'] = _t('edit');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Polls\Models\Poll','admin/poll-list')
        );
        $this->data['fields'] = array('id'=>_t('#'),'question'=>_t('question'));
        $this->view('cms/base-list',$this->data);
    }
    function poll_edit($id) {
        /** @var $poll \Polls\Models\Poll */
        $poll = \Polls\Models\Poll::findOrCreate($id);

        $varForm = \Bingo\Form::create()
            ->text('text',_t('Variant text'))
            ->text('count',_t('Answered'));

        $form = new \Bingo\Form;

        $form->fieldset();
        $form->text('question',_t('Question'),'',$poll->question);
        $form->select('disabled',_t('Test status'),array(_t('Enabled')=>false,_t('Disabled')=>true),'',$poll->disabled);

        $form->fieldset(_t('Variants'))->add_class('variants');
        $form->form_list('variants',$varForm,$poll->variants,true);

        $form->fieldset();
        $form->submit(_t('Save poll'))->add_class('single');

        if ($form->validate()) {
            $form->fill($poll);
            $poll->save(true);
            redirect("admin/poll-list");
        }
        $this->data['title'] = _t('Edit poll');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }
}