<?php

namespace Polls\Models;

/**
 * @Entity
 * @Table(name="polls")
 */
class Poll extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /** @Column(length=256) */
    public $question;

    /** @Column(type="boolean") */
    public $disabled;

    /** @Column(type="object") */
    public $variants;

    function __construct() {
        $this->variants = array();
    }

    function hasVoted() {
        return isset($_COOKIE['voted'.$this->id]);
    }

    function vote($key) {
        if ($key) {
            if (isset($this->variants[$key])) {
                setcookie("voted".$this->id,1,time()+3600*24*365,"/");
                $this->variants[$key]['count'] = @$this->variants[$key]['count'] + 1;
                $this->save();
            }
        }
    }

    function render() {
        $c = \Bingo\Bingo::getInstance()->controller ?: new \Polls\Controllers\Poll();
        if ($this->hasVoted())
            $c->view('polls/result',array('poll'=>$this));
        else
            $c->view('polls/form',array('poll'=>$this));
    }
}
