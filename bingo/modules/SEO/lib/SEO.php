<?php

class SEO extends \Bingo\Module {
    function __construct() {
        $keywords_mapping = array('fieldName'=>'meta_keywords','type'=>'text');
        $description_mapping = array('fieldName'=>'meta_description','type'=>'text');
        $title_mapping = array('fieldName'=>'meta_title','type'=>'text');

        \Blog\Models\Post::mapField($keywords_mapping);
        \Blog\Models\Post::mapField($description_mapping);
        \Blog\Models\Post::mapField($title_mapping);

        $meta_box = function($post,$form){
            $form->fieldset(_t('SEO'),false,array('before'=>'main'))->add_class('second-column');
            $form->textarea('meta_title',_t('Meta Title'),'',$post->getField('meta_title'));
            $form->textarea('meta_description',_t('Meta Description'),'',$post->getField('meta_description'));
            $form->textarea('meta_keywords',_t('Meta Keywords'),'',$post->getField('meta_keywords'));
        };

        \Bingo\Action::add('add_meta_box',$meta_box,10,2);

        if (class_exists('\Shop')) {
            \Shop\Models\Category::mapField($keywords_mapping);
            \Shop\Models\Category::mapField($description_mapping);
            \Shop\Models\Category::mapField($title_mapping);

            \Shop\Models\Product::mapField($keywords_mapping);
            \Shop\Models\Product::mapField($description_mapping);
            \Shop\Models\Product::mapField($title_mapping);

            \Bingo\Action::add('shop_category_meta_box',$meta_box,10,2);
            \Bingo\Action::add('shop_product_meta_box',$meta_box,10,2);

            \Bingo\Action::add('shop_export_settings',function($settings){
                $settings['categories']['fields']['meta_keywords'] = 50;
                $settings['categories']['fields']['meta_description'] = 50;
                $settings['categories']['fields']['meta_title'] = 50;
                $settings['products']['fields']['meta_keywords'] = 50;
                $settings['products']['fields']['meta_description'] = 50;
                $settings['products']['fields']['meta_title'] = 50;
                return $settings;
            });

            $this->connect("sitemap.xml",array('function'=>array($this,'sitemap')));
        }
    }

    function sitemap() {
        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        // Shop sitemap
        $cats = \Shop\Models\Category::findBy(array('disabled'=>false));
        foreach ($cats as $cat) {
            $output .= '<url>';
            $output .= '<loc>' . str_replace('&', '&amp;', "http://".$_SERVER['HTTP_HOST'].$cat->get_url()) . '</loc>';
            $output .= '<changefreq>weekly</changefreq>';
            $output .= '<priority>0.7</priority>';
            $output .= '</url>';
        }

        $products = \Shop\Models\Product::findBy(array('disabled'=>false));
        foreach ($products as $product) {
            if ($product->category && $product->category->getField('disabled')) continue;
            $output .= '<url>';
            $output .= '<loc>' . str_replace('&', '&amp;', "http://".$_SERVER['HTTP_HOST'].$product->get_url()) . '</loc>';
            $output .= '<changefreq>weekly</changefreq>';
            $output .= '<priority>0.7</priority>';
            $output .= '</url>';
        }

        $output .= '</urlset>';
        header('Content-Type: application/xml');
        echo $output;
    }
}