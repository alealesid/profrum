<?php

class Acl extends \Bingo\Module {
    function __construct() {
        bingo_domain_register('acl',dirname(__FILE__)."/../locale");
        
        $this->connect("admin/user-edit/:data",array('data'=>false,
            'controller'=>'\Acl\Controllers\Admin','action'=>'user-edit','priority'=>1));
        
        $this->connect("admin/*data",array('data'=>false,'function'=>function($route){
            $data = $route['data'];
            if ($data=='login' || $data=='logout') return true;
            $c = new \Acl\Controllers\Admin;
            \Bingo\Routing::$controller = $c;
            return $c->index($data);
        },'priority'=>-1000));
        
        \Bingo\Action::add('admin_pre_header',function () {
            $user = \Bingo\Routing::$controller->user;
            
            if (!$user) return;
            if (\CMS\Configuration::$super_admin==$user->login) return;
            
            $f = function (&$list,&$key,&$val) use (&$f,$user) {
                if (!is_array($val)) {
                    $rval = str_replace("admin/","",$val);
                    if (!\Acl::hasAccess($user,$rval,false)) {
                        if ($list) unset($list[$key]);
                    }
                } else {
                    foreach ($val as $skey=>&$sval) {
                        $f($val,$skey,$sval);
                        if (empty($val)) {
                            if ($list) unset($list[$key]);
                        }
                    }
                }
            };
            $dummy=false;
            $f($dummy,$dummy,\Admin::$menu);
        },100000,1);        
    }
    
    static $mappers = array();
    static function hasAccess($user,$admin_url,$check_post=false) {
        if (!isset(self::$mappers[$user->id])) {
            self::$mappers[$user->id] = $mapper = \Net_URL_Mapper::getInstance('acl_'.$user->id);
            $permissions = \CMS\Models\Option::get('permissions_'.$user->id);
            if ($permissions) {
                foreach ($permissions as $p) {
                    $mapper->connect($p['uri'],array('post'=>@$p['post'],'type'=>@$p['type']));
                }
            }
        }
        $mapper = self::$mappers[$user->id];
        $match = $mapper->match($admin_url);
        
        if ($match && $match['type']=='deny') return false;
        if (!$match) return false;
        if ($check_post && !empty($_POST) && !$match['post']) return false;
        return true;
    }
}