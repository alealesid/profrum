<?php

namespace Acl\Controllers;

class Admin extends \CMS\Controllers\Admin\BasePrivate
{
    function user_edit($id) {
        $user = \CMS\Models\User::findOrCreate($id);
    
        $uriForm = new \Bingo\Form;
        $uriForm->text('uri',_t('URI','acl'));
        $uriForm->select('type',_t('Type','acl'),array(_t('Allow','acl')=>'allow',_t('Deny','acl')=>'deny'));
        $uriForm->checkbox('post',_t('Allow post?','acl'));
    
        $form = new \Bingo\Form;
        $form->fieldset(_t("Info",'acl'))
                ->text('login',_t('Login','acl'),'',$user->login)
                ->text('password',_t('Password','acl'),'','')
                ->text('email',_t('E-mail','acl'),'',$user->email);
    
    
        $form->fieldset(_t('Permissions','acl'));
    
        $permissions = $id ? \CMS\Models\Option::get('permissions_'.$id) : false;
        $form->form_list('permissions',$uriForm,$permissions,true)->add_class('menu_editor');
    
        $form->fieldset()
                ->submit(_t('Save user'))->add_class("inline single");
    
        if ($form->validate()) {
            $data = $form->getValue();
            $user->login = $data['login'];
            if ($data['password']) $user->password = md5($data['password']);
            $user->email = $data['email'];
            $user->save();
            \CMS\Models\Option::set('permissions_'.$user->id,$data['permissions']);
            return redirect("admin/user-list");
        }
    
        $this->data['title'] = _t('User edit','acl');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit');
    }
    
    function index($data) {
        $user = \CMS\Models\User::checkLoggedIn();
        if (!$user) return true;
        if (\CMS\Configuration::$super_admin==$user->login) return true;
        
        if (\Acl::hasAccess($user,$data,true)) {
            return true;
        }
    
        $this->data['title'] = _t('Access denied','acl');
        $this->data['message'] = _t('Access denied','acl');
        $this->view('cms/base');
    }
}