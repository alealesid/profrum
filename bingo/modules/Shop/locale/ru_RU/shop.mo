��    �      T      �
      �
     �
     �
     �
     �
     �
     �
  1   �
                '     9     =     B     P     _     g  
   }     �     �     �     �     �     �     �     �     �     �     �            
        (     1     M     Z     a  
   m     x       
   �     �     �     �     �     �                    -     9     M  +   R  
   ~     �     �     �     �     �  
   �     �     �  
   �  	   �     �     �  
             $  
   2     =     U     X     n     }     �     �  
   �  C   �     �          	          !  	   *     4     :  /   V     �     �     �     �     �     �     �     �     �     �               .     ;     I     R     [     g     o     t     �  
   �     �     �     �     �     �     �     �     �        
     
             7  ,   <     i     r     z     �     �     �     �     �     �     �     �     �     	                (     -     ;  6   ?  
   v  "   �  
   �  	   �     �     �     �     �     �     �     �     �     �                              %     +     1     9     ?     G  n  K     �     �     �     �  '   �  &     U   8  
   �  M   �  !   �     	               9     T  ,   c     �     �     �  #   �  #   �       
   +     6     L     h     �  
   �     �  
   �     �     �  8   �          )     8     I     c  7   j  %   �  '   �  %   �  -     #   D  !   h     �     �  /   �     �  ,   �  +   +  H   W     �     �     �     �     �          *     E     N  ,   c  
   �  
   �  #   �     �     �     �       4   1     f  *   m     �     �  
   �  $   �     �  �        �     �  +   �                    *      9  S   Z     �     �     �     �  /   �  /   #      S   
   o      z   !   �   ,   �      �      �      !     2!     ?!     T!     k!     z!  %   �!     �!     �!     �!  !   "  
   &"     1"     O"     `"     |"     �"     �"  !   �"  !   �"  T   #     V#  b   k#     �#     �#     �#     $     8$     E$     e$  %   t$  %   �$  9  �$     �?     @     )@  
   E@     P@     e@     l@     �@  |   �@     A  C   A     [A     hA     xA     �A  
   �A  
   �A     �A     �A     �A     �A     �A     �A     �A     �A     B  	   B     )B     8B  
   AB     LB     _B     rB    {summ}  # - Add product Add product from Icecat Add to category Additional info: district, subway, door-code, etc Address Address : street, number, flat All manufacturers Any Cart Cart is empty Cart is empty. Catalog Catalog export/import Categories Category Category depth Category description Category image Change City Client name Comment list Comment text Comments Common Common options Content Create new Currency Currency signs (pre & post) Custom title Delete Description Don't show E-mail E-mail for orders Edit order Edit plugin settings Edit product Edit product category Edit shop options Email for confirmation letter Excerpt Export to excel Extra categories Filter form Filter product list Flat How should we call you? <em>(required)</em> Icecat add Icecat login Icecat password Images Import from excel Import/export In storage Info Items List title Log me in Login Main category Make order Manufacturer Manufacturer: My profile New order from you shop No No (generic category) Not in storage Options Order Order comments Order list Order some goods or give us a call - we will find something for you Order without logging in Orders Parent category Payment Payments Permalink Phone Phone number to contact you Phone number to contact you <em>(required)</em> Pickup Picture Preorder available Price Price ascending Price descending Proceed order Product Product EAN Product categories Product category list Product description Product list Product order Products Quantity Recalculate Routing Save Save category Save options Save order Save product Save profile Search Send Comment Shipping Shipping address Shipping price Shipping title Shop Shop owner Shop title Shop url prefix (e.g. shop) Show Shows shop product categories as nested list Sort by: Special Special category Special price Status Storage state Success Successfully added Successfully saved Thanks for your order. Title Title ascending Title descending Total Visible Y.M. Yandex.Market Yes Yes (special category, like top-sale, best-offer, etc) Your cart  Your comment was sent to moderator Your email Your name Your order was accepted all author content created date disabled edit email enabled id image modified no id order price product title visible yes Project-Id-Version: shop\nReport-Msgid-Bugs-To: \nPOT-Creation-Date: 2014-11-30 18:46+0300\nPO-Revision-Date: 2014-11-30 18:46+0300\nLast-Translator: boomyjee <boomyjee@gmail.com>\nLanguage-Team: boomyjee\nLanguage: ru_RU\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nX-Poedit-KeywordsList: _t;_e\nX-Poedit-Basepath: .\nX-Poedit-SourceCharset: UTF-8\nPlural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11) ? 0 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);\nX-Generator: Poedit 1.5.5\nX-Poedit-SearchPath-0: c:\\Users\\boomyjee\\Downloads\\turbomed\\po\\src\n {сумма} № - Добавить товар Добавить товар из Icecat Добавить в категорию Дополнительно: район, ближайшее метро, домофон Адрес Адрес: улица, номер дома, подъезд, квартира Все производители Все Корзина Корзина пуста Корзина пуста. Каталог Импорт/экспорт каталога Категории Категория Глубина дерева Описание категории Картинка категории Изменить Город Имя клиента Список отзывов Текст отзыва Отзывы Общие Общие настройки Текст Создать Валюта Обозначения валюты (до и после) Заголовок Удалить Описание Не показывать E-mail Электронная почта для заказов Редактировать заказ Редактировать плагин Редактировать товар Редактировать категорию Настройки магазина Электронная почта Кратко Экспорт в эксель Дополнительные категории Форма поиска Обновить список товаров Фиксированная доставка Как нам вас называть? <em>(обязательно)</em> Добавить Icecat Логин Icecat Пароль Icecat Картинки Импорт из экселя Импорт/экспорт Есть на складе Инфо Содержимое Заголовок для категории Войти Логин Основная категория Сделать заказ Производитель Производитель: Мой профиль Новый заказ в вашем магазине Нет Нет (обычная категория) Нет на складе Настройки Заказ Примечания к заказу Список заказов Закажите что-нибудь или позвоните нам - мы обязательно подберем нужный товар для вас. Анонимный заказ Заказы Родительская категория Оплата Оплата Ссылка Телефон Телефон для связи Телефонный номер для связи <em>(обязательно)</em> Самовывоз Изображение Под заказ Цена Цене: от меньшей к Большей Цене: от Большей к меньшей Оформить заказ Товар EAN товара Категории товаров Список категорий товара Описание товара Список товаров Порядок товаров Товары Количество Пересчитать Роутинг Сохранение Сохранить категорию Сохранить Сохранить заказ Сохранить товар Сохранить профиль Поиск Отправить отзыв Доставка Адрес доставки Цена доставки Тип доставки Магазин Владелец магазина Название магазина Относительная ссылка на магазин (например, shop) Показывать Показывает категории товара в виде вложенного списка Сортировать по: Особое Особая категория Специальная цена Статус Состояние склада Успешно Успешное сохранение Успешное сохранение Спасибо за ваш заказ."
#~ msgid "Extensions"
#~ msgstr "Расширения"
#~ msgid "Basic"
#~ msgstr "Основное"
#~ msgid "Flat price"
#~ msgstr "Стоимость доставки курьером"
#~ msgid "type"
#~ msgstr "тип"
#~ msgid "state"
#~ msgstr "статус"
#~ msgid "Enabled"
#~ msgstr "Включена"
#~ msgid "Disabled"
#~ msgstr "Выключена"
#~ msgid "Item order"
#~ msgstr "Порядок"
#~ msgid "Cash on delivery"
#~ msgstr "Оплата при доставке"
#~ msgid "WebMoney"
#~ msgstr "WebMoney"
#~ msgid "Wire transfer"
#~ msgstr "Банковский перевод"
#~ msgid "Yandex.money"
#~ msgstr "Яндекс.деньги"
#~ msgid "Mail delivery"
#~ msgstr "Доставка почтой"
#~ msgid "Approximate price"
#~ msgstr "Примерная цена"
#~ msgid "approx."
#~ msgstr "прибл."
#~ msgid "Phone numbers"
#~ msgstr "Номера телефонов"
#~ msgid "Similar products"
#~ msgstr "Похожие товары"
#~ msgid "Order details"
#~ msgstr "Информация о заказе"
#~ msgid "Client phone"
#~ msgstr "Номер телефона"
#~ msgid "Client address"
#~ msgstr "Адрес клиента"
#~ msgid "details"
#~ msgstr "инфо"
#~ msgid "Delivery price"
#~ msgstr "Цена доставки"
#~ msgid "Send to printer"
#~ msgstr "Отправить на печать"
#~ msgid "Name"
#~ msgstr "Название"
#~ msgid "permalink"
#~ msgstr "ссылка"
#~ msgid "Show on main"
#~ msgstr "Показывать на главной"
#~ msgid "Thumb dimensions (pixels)"
#~ msgstr "Размер миниатюры (пикс.)"
#~ msgid "Product image dimensions (pixels)"
#~ msgstr "Размер изображения товара (пикселей)"
#~ msgid "Galleries"
#~ msgstr "Галереи"
#~ msgid "Gallery options saved"
#~ msgstr "Настройки галереи сохранены"
#~ msgid "Gallery options"
#~ msgstr "Настройки галереи"
#~ msgid "Gallery list"
#~ msgstr "Список галерей"
#~ msgid "Gallery edit"
#~ msgstr "Редактировать галерею"
#~ msgid "Use defaults"
#~ msgstr "По умолчанию"
#~ msgid "Create gallery"
#~ msgstr "Создать галерею"
#~ msgid "Update gallery options"
#~ msgstr "Обновить настройки галереи"
#~ msgid "upload images"
#~ msgstr "загрузить изображения"
#~ msgid "Gallery create"
#~ msgstr "Создать галерею"
#~ msgid "..with selected"
#~ msgstr "... с выбранными"
#~ msgid "Delete selected"
#~ msgstr "Удалить выбранные"
#~ msgid "Save images sorting and options"
#~ msgstr "Сохранить сортировку и описания"
#~ msgid "thumb"
#~ msgstr "миниатюра"
#~ msgid "Core"
#~ msgstr "Основное"
#~ msgid "Models"
#~ msgstr "Модели"
#~ msgid "Model"
#~ msgstr "Модель"
#~ msgid "Textures"
#~ msgstr "Текстуры"
#~ msgid "Texture"
#~ msgstr "Текстура"
#~ msgid "Brushes"
#~ msgstr "Кисти"
#~ msgid "Brush"
#~ msgstr "Кисть"
#~ msgid "Wall shapes"
#~ msgstr "Формы стен"
#~ msgid "Wall shape"
#~ msgstr "Форма стены"
#~ msgid "Saves"
#~ msgstr "Сохранения"
#~ msgid "Users"
#~ msgstr "Пользователи"
#~ msgid "Welcome to Galaxy admin"
#~ msgstr "Добро пожаловать в панель управления"
#~ msgid "You're not logged in ActiveCollab as admin"
#~ msgstr "Вы не вошли как администратор ActiveCollab"
#~ msgid "Save list order"
#~ msgstr "Сохранить порядок"
#~ msgid "owner id"
#~ msgstr "id владельца"
#~ msgid "Parent (valid only for common items)"
#~ msgstr "Категория, валидно только для общих элементов"
#~ msgid "Owner"
#~ msgstr "Владелец"
#~ msgid "Url"
#~ msgstr "Ссылка"
#~ msgid "Item edit"
#~ msgstr "Редактировать элемент"
#~ msgid "Schema update"
#~ msgstr "Обновление схемы"
#~ msgid "For All"
#~ msgstr "Для всех"
#~ msgid "Menu"
#~ msgstr "Меню"
#~ msgid "Songs"
#~ msgstr "Песни"
#~ msgid "Guestbook"
#~ msgstr "Гостевая книга"
#~ msgid "Hostname"
#~ msgstr "Адрес"
#~ msgid "Terminals"
#~ msgstr "Терминалы"
#~ msgid "Cameras"
#~ msgstr "Камеры"
#~ msgid "Is advertising"
#~ msgstr "Это реклама?"
#~ msgid "Just menu"
#~ msgstr "Просто меню"
#~ msgid "Advertising"
#~ msgstr "Реклама"
#~ msgid "Button background"
#~ msgstr "Фон кнопки"
#~ msgid "Background"
#~ msgstr "Фон экрана"
#~ msgid "parent"
#~ msgstr "родитель"
#~ msgid "Browse for image"
#~ msgstr "Загрузить картинку"
#~ msgid "Just dish"
#~ msgstr "Просто блюдо"
#~ msgid "Dish edit"
#~ msgstr "Изменить блюдо"
#~ msgid "Performer"
#~ msgstr "Исполнитель"
#~ msgid "Save song"
#~ msgstr "Сохранить песню"
#~ msgid "Song edit"
#~ msgstr "Изменить песню"
#~ msgid "XLS to import"
#~ msgstr "XLS для импорта"
#~ msgid "XLS Type"
#~ msgstr "Тип XLS"
#~ msgid "File to upload"
#~ msgstr "Файл для загрузки"
#~ msgid "Imported successfully"
#~ msgstr "Импорт прошел успешно"
#~ msgid "Songs import"
#~ msgstr "Импорт песен"
#~ msgid "New"
#~ msgstr "Новый"
#~ msgid "Finished"
#~ msgstr "Закрыт в кассе"
#~ msgid "terminal"
#~ msgstr "терминал"
#~ msgid "total"
#~ msgstr "итого"
#~ msgid "Terminal"
#~ msgstr "Терминал"
#~ msgid "Created"
#~ msgstr "Дата создания"
#~ msgid "moderated"
#~ msgstr "модерирован"
#~ msgid "not allowed"
#~ msgstr "не разрешен"
#~ msgid "Moderation"
#~ msgstr "Модерация"
#~ msgid "Not allowed"
#~ msgstr "Не разрешен"
#~ msgid "Allowed"
#~ msgstr "Разрешен"
#~ msgid "Logout"
#~ msgstr "Выйти"
#~ msgid "Goto website"
#~ msgstr "Перейти к сайту"
#~ msgid "Schema"
#~ msgstr "Схема БД"
#~ msgid "Post List"
#~ msgstr "Записи"
#~ msgid "User List"
#~ msgstr "Пользователи"
#~ msgid "User Create"
#~ msgstr "Создать"
#~ msgid "Admin panel"
#~ msgstr "Панель управления"
#~ msgid "404"
#~ msgstr "404"
#~ msgid "404 - not found"
#~ msgstr "404 - не найдено"
#~ msgid "Post"
#~ msgstr "Запись"
#~ msgid "Post delete"
#~ msgstr "Удалить запись"
#~ msgid "Empty"
#~ msgstr "Пусто"
#~ msgid "Index post"
#~ msgstr "Заглавная страница"
#~ msgid "Control panel"
#~ msgstr "Панель управления"
#~ msgid "Login, please"
#~ msgstr "Вход"
#~ msgid "actions"
#~ msgstr "действия"
#~ msgid "No posts yet"
#~ msgstr "Пока нет записей"
#~ msgid "login"
#~ msgstr "логин Заголовок Имени: от А до Я Имени: от Я до А Итого Показывать Я.М. Яндекс.Маркет Да Да (особая категория, например Распродажа или Выгодное Предложение) Корзина Ваш отзыв был отправлен на модерацию Ваш email Ваше имя Ваш заказ принят все автор текст создан дата выключено ред. email включено id картинка изменен нет id порядок цена товар заголовок видимость да 