<?php

class Shop extends \Bingo\Module {

    static public $paymentMethods = array();
    static public $shippingMethods = array();

    static function addShippingMethod() {
        foreach (func_get_args() as $one) self::$shippingMethods[] = $one;
    }
    
    static function addPaymentMethod() {
        foreach (func_get_args() as $one) self::$paymentMethods[] = $one;
    }

    function __construct() {
        parent::__construct();

        bingo_domain_register('shop',dirname(__FILE__)."/../locale");
        bingo_domain('shop');

        $this->addModelPath(dirname(__FILE__).'/Shop/Models');
        
        $this->connect('admin/shop/:action/:data',
            array('controller'=>'\Shop\Controllers\Admin','data'=>false),
            array('action'=>'(category-list|category-edit|product-list|product-edit'.
                  '|comment-list|comment-edit|options|shipping|payment|order-list|order-edit|export)')
        );
        
        \Bingo\Action::add('admin_pre_header',function () {
            \Admin::$menu[_t('Shop','shop')][_t('Options','shop')] = 'admin/shop/options';
            \Admin::$menu[_t('Shop','shop')][_t('Orders','shop')] = 'admin/shop/order-list';
            \Admin::$menu[_t('Shop','shop')][_t('Comments','shop')] = 'admin/shop/comment-list';
            \Admin::$menu[_t('Shop','shop')][_t('Payment','shop')] = 'admin/shop/payment';
            \Admin::$menu[_t('Shop','shop')][_t('Shipping','shop')] = 'admin/shop/shipping';

            \Admin::$menu[_t('Shop','shop')][_t('Catalog','shop')][_t('Categories','shop')] = 'admin/shop/category-list';
            \Admin::$menu[_t('Shop','shop')][_t('Catalog','shop')][_t('Products','shop')] = 'admin/shop/product-list';
            \Admin::$menu[_t('Shop','shop')][_t('Catalog','shop')][_t('Import/export','shop')] = 'admin/shop/export';
        });
        
        \Bingo\Action::add('template_data',function($data){
            $data['shop'] = new ShopTemplateData();
            $data['applicationMode'] = \Bingo\Configuration::$applicationMode;
            return $data;
        });
        
        $this->connect(':action/:data',
            array('controller'=>'\Shop\Controllers\Catalog','data'=>false),
            array('action'=>'(cart|cart-callback|order|login|logout|profile|search)')
        );
		
        $this->connect('*route',array('function'=>function($route){
            if (!@$route['route']) return true;
            $slugs = explode("/",trim($route['route'],"/"));
            
            $options = \CMS\Models\Option::get("shop_options");
            $prefix = @$options['prefix'];
            
            if ($prefix) {
                if (@$slugs[0]!=$prefix) return true;
                array_shift($slugs);
            }
            
            if (count($slugs)==0) {
                theme_base();
                if (\Bingo\Template::get_include("shop/index")) {
                    $c = new \Shop\Controllers\Catalog;
                    \Bingo\Routing::$controller = $c;
                    $c->index();
                    return false;
                } else {
                    return true;
                }
            }

            $last = array_pop($slugs);
            
            $parent = null;
            foreach ($slugs as $slug) {
                $cat = \Shop\Models\Category::findOneBy(array('permalink'=>$slug,'parent'=>$parent));
                if (!$cat) return true;
                $parent = $cat;
            }
            
            if ($cat = \Shop\Models\Category::findOneBy(array('permalink'=>$last,'parent'=>$parent))) {
                $c = new \Shop\Controllers\Catalog;
                    \Bingo\Routing::$controller = $c;
                if ($cat->disabled) return true;
                $c->category($cat->id);
                return false;
            }

            if (
                ($product = \Shop\Models\Product::findOneBy(array('permalink'=>$last,'category'=>$parent))) ||
                ($product = \Shop\Models\Product::findOneBy(array('id'=>$last,'category'=>$parent,'permalink'=>'')))
            ) {
                if ($product->disabled) return true;
                if ($product->category && $product->category->getField('disabled')) return true;
                $c = new \Shop\Controllers\Catalog;
                    \Bingo\Routing::$controller = $c;
                $c->product($product->id);
                return false;
            }
            return true;
        },'priority'=>100));

        \Layout\Widget::register("\Shop\Widgets\Categories");
    }
}

require_once "Liquid/Liquid.class.php";
class ShopTemplateData extends \LiquidDrop {
    function featured_products() {
        return \Bingo\Bingo::getInstance()->em->createQuery("
            SELECT p FROM \Shop\Models\Product p
            JOIN p.extra_categories c WHERE c.permalink='featured'
        ")->getResult();
    }
    function top_categories() {
        $res = array();
        $cats = \Shop\Models\Category::findBy(array('parent'=>null,'disabled'=>false),'item_order');
        foreach ($cats as $cat) {
            $res[] = array(
                'url' => $cat->get_url(),
                'title' => $cat->title
            );
        }
        return $res;
    }
    function cart() {
        $cart = \Shop\Models\Cart::getInstance();
        return array(
            'count' => $cart->getCount(),
            'total_price' => $cart->getTotal()
        );
    }
    function user() {
        $user = \Shop\Models\User::checkLoggedIn();
        if ($user) 
            return array('name'=>$user->name);
        return false;
    }
}
