<?php

namespace Shop;

class Plugin {
    
    public $constructed = false;
    public $loaded = false;
    public $id = false;
    public $data = array();
    
    function __construct($args=array()) {
        $this->price = 0;
        foreach ($args as $key=>$val) $this->$key = $val;
        $this->constructed = true;
    }
    
    function getPrice() {
        return $this->price;
    }
    
    public function __get($name) {
        if ($this->constructed) $this->load();
        if (isset($this->data[$name]))
            return $this->data[$name];
        else
            return null;
    }
    public function __set($name,$value) {
        $this->data[$name] = $value;
    }    
    
    public function load() {
        if ($this->loaded) return;
        $this->loaded = true;
        $class = get_class($this);
        $options = \CMS\Models\Option::get('shop_plugins');
        $this->data = @$options[$class."_".$this->id] ? : array();
    }

    public function save() {
        $class = get_class($this);
        $options = \CMS\Models\Option::get('shop_plugins');
        $options[$class."_".$this->id] = $this->data;
        \CMS\Models\Option::set('shop_plugins',$options);
    }

    function getAdminForm() {
        $form = new \Bingo\Form;
        $form->fieldset(_t('Common options','shop'));
        $form->text('title',_t('Title','shop'),'',$this->title);
        $form->text('description',_t('Description','shop'),'',$this->description);
        return $form;
    }
    
    function getJS() {
        return "{}";
    }
}
