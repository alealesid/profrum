<?php

namespace Shop\Widgets;

class Categories extends \Layout\Widget {

    function getLabel() {
        return _t('Product categories','shop');
    }

    function getDescription() {
        return _t('Shows shop product categories as nested list','shop');
    }

    function getForm() {
        return \Bingo\Form::create()
                ->text('title',_t('Custom title','shop'))
                ->text('depth',_t('Category depth','shop'));
    }

    function render_category(&$cat,&$category,&$parents,$depth) {
        if ($cat->disabled) return;
        $class = "level".($depth+1);
        if ($category==$cat)
            $class .= " active";
        else { if (in_array($cat->id,$parents))
            foreach ($parents as $p=>$parent)
                if ($cat->id==$parent)
                    $class .= " active_parent active_parent_".($p+1);
        }
        echo "<li class='$class'>";
        echo "<a href='".str_replace(" ","%20",$cat->get_url())."'>".$cat->title."</a>";
        if (count($cat->children_sorted)) {
            echo "<ul>";
            foreach ($cat->children_sorted as $sub) {
                $this->render_category($sub,$category,$parents,$depth+1);
            }
            echo "</ul>";
        }
        echo "</li>";
    }

    function render($data) {
        $cats = \Shop\Models\Category::findBy(array(),'item_order');
        $cats_with_id = array();
        foreach ($cats as $cat) {
            $cat->children_sorted = array();
            $cats_with_id[$cat->id] = $cat;
        }
        foreach ($cats as $cat) {
            if ($cat->parent) {
                $cats_with_id[$cat->parent->id]->children_sorted[] = $cat;
            }
        }

        $title = (isset($data['title']) && $data['title']) ? $data['title'] : "<h3>"._t('Categories','shop')."</h3>";
        echo "<div class='shop_categories_widget'>";
        if (trim($title)) echo $title;

        $controller = @\Bingo\Bingo::getInstance()->controller;
        $category = $controller ? @$controller->data['category'] : false;
        if (!$category && $controller) {
            $product = @$controller->data['product'];
            if ($product) $category = $product->category;
        }

        $parents = array();
        $parent = $category ? $category->parent: false;
        while ($parent) {
            $parents[] = $parent->id;
            $parent = $parent->parent;
        }
        
        echo "<ul>";
        foreach ($cats as $cat) {
            if (!$cat->parent)
                $this->render_category($cat,$category,$parents,0);
        }
        echo "</ul>";
        echo "</div>";
    }
}


