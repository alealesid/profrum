<?php

namespace Shop\Controllers;

class Catalog extends \Bingo\Controller
{
    function __construct() {
        parent::__construct();
        $this->user = \Shop\Models\User::checkLoggedIn();
        $this->data = array();
        $this->data['user'] = $this->user;

        theme_base();
        bingo_domain('shop');
    }
    
    function index() {
        $options = \CMS\Models\Option::get('shop_options');
        
        $this->data['title'] = @$options['title'] ? : _t("Shop");
        $this->view("shop/index",$this->data);
    }
    
    function category($id) {
        $cat = \Shop\Models\Category::find($id);
        $this->data['category'] = $cat;
        $this->data['children'] = \Shop\Models\Category::findBy(array('parent'=>$cat,'disabled'=>false));
        $this->data['title'] = $cat->title;

        $manufacturers = $this->em->createQuery("
            SELECT DISTINCT(p.manufacturer) as manufacturer
            FROM \Shop\Models\Product p
            WHERE p.disabled=0 AND p.category={$cat->id}
        ")->getResult();
        $manufacturerOptions = array(_t('All manufacturers')=>'');
        foreach ($manufacturers as $i=>$m) {
            $name = $m['manufacturer'];
            if ($name)
                $manufacturerOptions[$name] = $i;
        }

        $form = new \Bingo\Form('get',array('id'=>'sorting_form'));
        $form->html('<span>');
        $form->select('manufacturer',_t('Manufacturer:'),$manufacturerOptions,'','');
        $form->html('</span>');
        $form->html('<span>');
        $form->select('sort',_t('Sort by:'),array(
            _t('Title ascending') => "title-asc",
            _t('Title descending') =>"title-desc",
            _t('Price ascending') => "price-asc",
            _t('Price descending') =>"price-desc"
        ),'');
        $form->html('</span>');

        $order = (object)array('field'=>'title','val'=>-1);
        if ($form->validate()) {
            $value = @explode('-',@$form->values['sort']);
            if (
                count($value)==2 &&
                in_array($value[0],array('title','price')) &&
                in_array($value[1],array('asc','desc'))
            ) {
                $order->field = $value[0];
                $order->val = $value[1]=='asc' ? -1:1;
            }
        }
        
        $m = @$_GET['manufacturer'];
        if ($m!==false) $m = @$manufacturers[$m]['manufacturer'];
        
        $dql = "
            SELECT p as product, partial c2.{id,product_order,permalink,disabled,title}
            FROM \Shop\Models\Product p
            LEFT JOIN p.category c1
            LEFT JOIN p.extra_categories c2
            WHERE (c1=:cat OR c2=:cat) AND p.disabled = 0
        ";
        
        if ($m) $dql .= " AND p.manufacturer = :m";
        $query = $this->em->createQuery($dql)->setParameter("cat",$cat);
        if ($m) $query->setParameter("m",$m);

        $res = $query->getResult();
        
        $products = array();
        foreach ($res as $one) {
            $product = $one['product'];
            $product->final_order = (int)$product->item_order;
            if (!$product->final_order) {
                foreach ($product->extra_categories as $excat) {
                    if ($excat->product_order > $product->final_order) {
                        $product->final_order = $excat->product_order;
                        $product->order_permalink = $excat->permalink;
                    }
                }
            }
            $products[] = $product;
        }

        usort($products,function($a,$b) use ($order) {
            if ($a->final_order != $b->final_order)
                return $a->final_order < $b->final_order ? 1:-1;
            
            if ($a->special_price && !$b->special_price) return -1;
            if (!$a->special_price && $b->special_price) return 1;
            
            $f = $order->field;
            return $a->$f < $b->$f ? $order->val : -$order->val;
        });
        
        $this->data['products'] = $products;
        $this->data['sorting_form'] = $form->get();
        $this->view('shop/category',$this->data);
    }

    function search() {
        $s = @$_GET['s'];
        if (!$s) redirect('');
        $this->data['title'] = _t('Search');
        $this->data['category'] = false;
        $this->data['products'] = $this->em->createQuery('
                SELECT p FROM \Shop\Models\Product p
                INNER JOIN p.category c
                WHERE (p.id=:id
                  OR p.title LIKE :search
                ) AND p.disabled = FALSE AND c.disabled = FALSE
                ORDER BY p.special_price DESC, p.title
            ')
            ->setParameter('id',$s)
            ->setParameter('search','%'.$s.'%')
            ->setMaxResults(20)
            ->getResult();

        $this->data['search_string'] = $s;
        $this->view('shop/search',$this->data);
    }
    
    function product($id) {
        $product = \Shop\Models\Product::find($id);
        if ($product->disabled) return true;
        
        $comment_form = new \Form;
        $comment_form->text('author',_t('Your name'),'required');
        $comment_form->text('author_email',_t('Your email'),'');
        $comment_form->textarea('content',_t('Comment text'),'required');
        $comment_form->submit(_t("Send Comment"));
        
        if ($comment_form->validate()) {
            $comment = new \Shop\Models\Comment;
            $comment_form->fill($comment);
            $comment->product = $product;
            $comment->save();
            
            if (isset($_REQUEST['ajax'])) {
                echo 'ok';
                return;
            } else {
                set_flash('info',_t('Your comment was sent to moderator'));
                redirect($_SERVER['REQUEST_URI']);
            }
        }
        
        $comments = \Shop\Models\Comment::findBy(array('product'=>$product,'visible'=>true),'date DESC');
        
        $this->data['comments'] = $comments;
        $this->data['product'] = $product;
        $this->data['title'] = $product->title;
        $this->data['comment_form'] = $comment_form->get();
        $this->view('shop/product',$this->data);
    }

    function cart($action) {
        if ($action=='callback') { $this->cart_callback();return; }

        $cart = \Shop\Models\Cart::getInstance();
        if (isset($_POST['quantity'])) {
            $cart->add((int)$_POST['product'],(int)$_POST['quantity']);
            $cart->save();
            redirect('cart');
            return;
        }
        if (isset($_POST['action'])) {
            $product_data = @$_POST['products'];
            if ($product_data) {
                foreach ($product_data as $id=>$data) {
                    $qty = (int)$data['quantity'];
                    $cart->set($id,array('quantity'=>$qty));
                }
                $cart->save();
                if ($_POST['action']=='recalc')
                    redirect('cart');
                else
                    redirect('order');
                return;
            }
        }

        $this->data['title'] = _t("Cart");
        $this->data['cart'] = $cart;
        $this->view('shop/cart',$this->data);
    }

    function cart_callback() {
        if (isset($_POST['quantity'])) {
            $id = (int)$_POST['product'];
            $qty = (int)$_POST['quantity'];
            
            $cart = \Shop\Models\Cart::getInstance();
            $cart->add($id,$qty);
            
            $atts = array();
            foreach ($_POST as $key=>$val) {
                if (!in_array($key,array('product','id','quantity'))) $atts[$key] = $val;
            }
            if (!empty($atts)) $cart->set($id,$atts);
                    
            $cart->save();
        }
        $this->view('shop/cart_callback',$this->data);
    }

    function order() {
        $shipping = \Shop::$shippingMethods;
        $shipping_options = array();
        foreach ($shipping as $id=>$s) $shipping_options[$s->title."<em class='desc'>".$s->description."</em>"] = $id;

        $payment = \Shop::$paymentMethods;
        $payment_options = array();
        foreach ($payment as $id=>$p) $payment_options[$p->title."<em class='desc'>".$p->description."</em>"] = $id;

        $form = new \Bingo\Form('post',array('autocomplete'=>'off'));
        $form
            ->text('name',_t('How should we call you? <em>(required)</em>'),'required',$this->user ? $this->user->name : "")
            ->text('phone',_t('Phone number to contact you <em>(required)</em>'),'required',$this->user ? $this->user->phone : "")
            ->text('email',_t('Email for confirmation letter'),'',$this->user ? $this->user->email : "")

            ->fieldset(_t("Shipping address"))->add_class("address")
            ->text('city',_t('City'),'',$this->user ? $this->user->city : "")
            ->text('address',_t('Address : street, number, flat'),'',$this->user ? $this->user->address : "")
            ->textarea('address_extra',_t('Additional info: district, subway, door-code, etc'),'',$this->user ? $this->user->address_extra: "")
            ->close_fieldset()

            ->html("<div class='radio'>")
                ->radio('shipping',_t('Shipping'),$shipping_options,'',reset($shipping_options))
                ->radio('payment', _t('Payment'), $payment_options,'',reset($payment_options))
            ->html("</div>")
            ->submit(_t('Proceed order'))->add_class("order");

        $this->data['cart'] = $cart = \Shop\Models\Cart::getInstance();
        if (empty($cart->items)) redirect('cart');
        if ($form->validate()) {
            if (count($cart->items)) {
                $order = new \Shop\Models\Order();
                $order->data = array();
                foreach (array('name','phone','email','city','address','address_extra','shipping','payment') as $field) {
                    $order->data[$field] = $form->values[$field];
                }
                $order->data['items'] = array();
                
                foreach ($cart->items as $item) {
                    $id = $item->id;
                    $product = $item->product;
                    $qty = $item->quantity;
                    
                    $one = array(
                        'id' => $id,
                        'qty' => $qty,
                        'thumb' => @$product->images[0],
                        'title' => $product->title,
                        'price' => $product->price,
                        'special' => $product->get_price_value()
                    );
                    
                    foreach ($item as $key=>$val) {
                        if ($key!='product' && $key!='quantity' && !isset($one[$key])) {
                            $one[$key] = $val;
                        }
                    }
                    $order->data['items'][] = $one;
                }
                $order->data['shipping_price'] = 0;
                foreach ($shipping as $id=>$s) {
                    if ($id==$form->values['shipping']) {
                        $order->data['shipping_price'] = $s->getPrice();
                        $order->data['shipping_title'] = $s->title;
                    }
                }

                if ($this->user)
                    $order->user = $this->user;

                $order->save();
                $cart->setEmpty();
                $cart->save();
                $order->sendEmail();
            }
            $this->data['title'] = _t("Order");
            $this->view('shop/order_success',$this->data);
            return;
        }

        $this->data['title'] = _t("Order");
        $this->data['shipping'] = $shipping;
        $this->data['payment'] = $payment;
        $this->data['form'] = $form->get();
                    
        $options = \CMS\Models\Option::get('shop_options');
        $this->data['currencyPre'] = @$options['currencyPre'];
        $this->data['currencyPost'] = @$options['currencyPost'];                    
        $this->data['shipping_price'] = count($shipping) ? reset($shipping)->getPrice() : 0;
        
        $this->data['total'] = $cart->getTotal();
        $this->view('shop/order',$this->data);
    }

    function login() {
        $token = @$_POST['token'];
        $redirect = @$_GET['redirect'];

        if ($this->user) { redirect($redirect);return; }
        if ($token) {
            $user = \Shop\Models\User::login_token($token);
            if ($user) {
                redirect($redirect);
                return;
            }
        }
        $this->data['title'] = _t("Login");
        $this->view('shop/login',$this->data);
    }

    function logout() {
        if ($this->user) $this->user->logout();
        redirect('login');
    }

    function profile() {
        if (!$this->user) { redirect('login?redirect=profile');return; }

        $form = new \Bingo\Form;
        $form
            ->text('name',_t('How should we call you? <em>(required)</em>'),'required',$this->user->name)
            ->text('phone',_t('Phone number to contact you'),'',$this->user->phone)
            ->text('email',_t('Email for confirmation letter'),'',$this->user->email)

            ->fieldset(_t("Shipping address"))->add_class("address")
            ->text('city',_t('City'),'',$this->user->city)
            ->text('address',_t('Address : street, number, flat'),'',$this->user->address)
            ->textarea('address_extra',_t('Additional info: district, subway, door-code, etc'),'',$this->user->address_extra)
            ->close_fieldset()
            ->submit(_t('Save profile'));

        if ($form->validate()) {
            $form->fill($this->user);
            $this->user->save();
            redirect('profile');
        }

        $this->data['title'] = _t("My profile");
        $this->data['form'] = $form->get();
        $this->view('shop/profile',$this->data);
    }
    
}