<?php

namespace Shop\Controllers;
use \Bingo\FormFilter_DoctrineObject as ObjectFilter;

class Admin extends \CMS\Controllers\Admin\BasePrivate
{
    function __construct() {
        parent::__construct();
        bingo_domain('shop');
        if (!$this->user && $this->route('action')!='gallery-upload') redirect('admin/login');
    }

    function options() {
        $options = \CMS\Models\Option::get("shop_options");
        $form = new \Bingo\Form;
        $form
            ->fieldset(_t('Common options'))
            ->text('title',_t('Shop title'),'',@$options['title'])
            ->text('owner',_t('Shop owner'),'',@$options['owner'])
            ->text('email',_t('E-mail for orders'),'',@$options['email'])
            ->fieldset(_t('Routing'))
            ->text('prefix',_t('Shop url prefix (e.g. shop)'),'',@$options['prefix'])
            ->fieldset(_t('Currency'))
            ->html("<div class='sizeInputs'>")
                ->text('currencyPre',_t('Currency signs (pre & post)'),'',@$options['currencyPre'] ? : "")
                    ->html(_t(" {summ} "))
                ->text('currencyPost',false,'',@$options['currencyPost'] ? : "$")
            ->html("</div>")
            ->fieldset()
            ->submit(_t('Save options'))->add_class("single");

        if ($form->validate()) {
            \CMS\Models\Option::set("shop_options",$_POST);
            return redirect("admin/shop/options");
        }

        $this->data['title'] = _t('Edit shop options');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce',$this->data);
        return false;
    }
    
    function category_list() {
        $this->data['title'] = _t('Product category list');
        
        $this->data['list'] = $this->treeSort(\Shop\Models\Category::findAll(),'parent','title','item_order');
        
        $this->data['page_actions']['admin/shop/category-edit'] = _t('Create new');
        $this->data['list_actions'][0] = array(
            'form' => \Bingo\Form::create()->text('item_order',false)->checkbox('disabled',false),
            'function' => $this->action_edit('\Shop\Models\Category','admin/shop/category-list/',false)
        );
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Shop\Models\Category','admin/shop/category-list')
        );
        $this->data['fields'] = array(
            'id'=>_t('#'),
            'title'=>_t('title'),
            'item_order'=>_t('order'),
            'disabled'=>_t('disabled')
        );
        $this->data['field_filters']['title'] = function ($val,$obj) {
            return
                str_repeat("&mdash;&mdash;",$obj->tree_depth)
                ." <a href='".url('admin/shop/category-edit/'.$obj->id)."'>$obj->title</a>"
                . (($obj->permalink) ? " ($obj->permalink)" : "");
        };

        $this->view('cms/base-list',$this->data);
    }
    function category_edit($id) {
        $cat = $this->findOrCreate('\Shop\Models\Category',$id);
        $form = new \Bingo\Form;
        $form
            ->fieldset(_t('Common'))
            ->text('title',_t('Title'),'',$cat->title)
            ->text('permalink',_t('Permalink'),'',$cat->permalink)
            ->select('disabled',_t('Don\'t show'),array(_t('Show')=>false,_t('Don\'t show')=>true),'',$cat->disabled)
            ->fieldset(_t('Special'))
            ->select('special',_t('Special category'),array(_t('No (generic category)')=>false,_t('Yes (special category, like top-sale, best-offer, etc)')=>true),'',$cat->special)
            ->text('product_order',_t('Product order'),'numeric',(int)$cat->product_order)
            ->select('parent',_t('Parent category'),
                $this->treeSort(\Shop\Models\Category::findAll(),'parent','title','item_order',true),'',$cat->parent)
                ->add_filter(new ObjectFilter('\Shop\Models\Category'))
            ->fieldset(_t('Content'))
            ->text('image',_t('Category image'),'',$cat->image)->add_class('browse_file')
            ->textarea('content',_t('Category description'),'',$cat->content,array('rows'=>30))->add_class("tinymce")
            ->textarea('excerpt',_t('Excerpt'),'',$cat->excerpt,' rows=5"')
            ->fieldset()
            ->submit(_t('Save category'))->addClass('single');
        

        \Bingo\Action::run('shop_category_meta_box',array($cat,$form));

        if ($form->validate()) {
            $form->fill($cat);
            $cat->save(true);
            return redirect("admin/shop/category-list");
        }
        
        $this->data['title'] = _t('Edit product category');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce',$this->data);        
    }
    
    function product_list() {
        $this->data['title'] = _t('Product list');

        $cat_options = $this->treeSort(\Shop\Models\Category::findAll(),'parent','title','item_order',true,array(_t('Any')=>0));
        $filter_form = new \CMS\FilterForm;
        $filter_form->fieldset(_t('Filter form'));
        $filter_form->select('category',_t('Category'),$cat_options,'')->add_filter(new ObjectFilter('\Shop\Models\Category'));
        $filter_form->text('title',_t('Title'));
        $filter_form->text('id',_t('#'));
        $filter_form->text('manufacturer',_t('Manufacturer'));
        $filter_form->button(false,_t('Filter product list'),'submit');

        $this->data['filter_form'] = $filter_form;
        $this->data['filter_form_external'] = true;
        $this->data['filter_autosubmit'] = false;

        \Bingo\Action::run('shop_admin_product_list_filter_form',array($filter_form));
        $criteria = array();
        if ($filter_form->validate()) {
            $values = $filter_form->values;
            if ($values['category']) $criteria['category'] = $values['category'];
            if ($values['title']) $criteria['title'] = 'LIKE %'.$values['title'].'%';
            if ($values['manufacturer']) $criteria['manufacturer'] = 'LIKE %'.$values['manufacturer'].'%';
            if ($values['id']) $criteria['id'] = $values['id'];
        }
        $criteria = \Bingo\Action::filter('shop_admin_product_list_criteria',array($criteria,$filter_form));
        $query = \Shop\Models\Product::findByQuery($criteria,'title');

        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();

        $this->data['page_actions']['admin/shop/product-edit'] = _t('Create new');

        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Shop\Models\Product','admin/shop/product-list')
        );
        $this->data['list_actions'][0] = array(
            'form' => \Bingo\Form::create()->text('price',false,''),
            'function' => $this->action_edit('\Shop\Models\Product','admin/shop/product-list',false)
        );
        
        $this->data['fields'] = array('images'=>_t('image'),'title'=>_t('title'),'price'=>_t('price'));
        $this->data['field_filters']['images'] = function ($val,$obj) {
            if ($val && count($val)) {
                return "<img style='max-height:100px;max-width:100px' src='".$obj->get_image_url(100,100)."'>";
            } else {
                return "";
            }
        };
        $this->data['field_filters']['title'] = function ($val,$obj) {
            $out = _t('#')."{$obj->id}<br>";
            $out .= "<a href='".url('admin/shop/product-edit/'.$obj->id)."'>$val</a><br>";
            if ($obj->category)
                $out .=  $obj->category->getField('title')."<br>";
            if ($obj->manufacturer)
                $out .=  _t('Manufacturer','shop').': '.$obj->manufacturer."<br>";
            return $out;
        };
        \Bingo\Action::run('shop_admin_product_list_view',array($this));
        $this->view('shop/base-list',$this->data);
    }
    function product_edit($id) {
        $product = $this->findOrCreate('\Shop\Models\Product',$id);
        $cat_options = $this->treeSort(\Shop\Models\Category::findAll(),'parent','title','item_order',true);
        $cat_options_ex = $cat_options; unset($cat_options_ex[array_search(0,$cat_options)]);
        
        $imageForm = new \Bingo\Form;
        $imageForm
            ->text("url",false,'')->add_class("browse_file image");

        $form = new \Bingo\Form;
        
        $form->fieldset(_t('Save'))->add_class('second-column');
        $form->submit(_t('Save product'))->add_class("single");
        
        $form->fieldset(_t('Status'))->add_class('second-column');
        $form
            ->text('price',_t('Price'),'',$product->price)
            ->text('special_price',_t('Special price'),'',$product->special_price)
            ->text('manufacturer',_t('Manufacturer'),'',$product->manufacturer)
            ->select('disabled',_t('Don\'t show'),array(_t('Show')=>false,_t('Don\'t show')=>true),'',$product->disabled)
            ->select('storage_state',_t('Storage state'),$product->get_storage_states(),'',$product->storage_state)
        ;
        
        $form->fieldset(_t('Categories'))->add_class('second-column');
        $form
            ->select('category',_t('Main category'),$cat_options,'',$product->category)
                ->add_filter(new ObjectFilter('\Shop\Models\Category'))
            ->select('extra_categories',_t('Extra categories'),$cat_options_ex,'',
                    $product->extra_categories,array('multiple'=>true,'size'=>8))
                ->add_filter(new ObjectFilter('\Shop\Models\Category'))
        ;
        
        $extra_category_hash = array();
        if ($product->extra_categories) foreach ($product->extra_categories as $cat) {
            $extra_category_hash[$cat->id] = 1;
        }
        
        foreach ($cat_options_ex as $cat) {
            if ($cat->special)
                $form->checkbox('special_categories['.$cat->id.']',$cat->title,'',isset($extra_category_hash[$cat->id]));
        }
        
        $form->fieldset(false,false,array('name'=>'main'))->add_class('first-column');
        $form
            ->html("<div class='value'>{value}&nbsp;</div>",array('label'=>_t('#'),'value'=>$product->id))
            ->text('title',_t('Title'),'required',$product->title)->add_class('item-title')
            ->text('list_title',_t('List title'),'',$product->list_title)
            ->text('permalink',_t('Permalink'),'',$product->permalink)->add_class('item-permalink')
            ->textarea('content',_t('Product description'),'',$product->content,array('rows'=>30))->add_class("tinymce")
            ->textarea('excerpt',_t('Excerpt'),'',$product->excerpt,' rows=5"')
            ->fieldset(_t('Images'),true,array('name'=>'images'))->add_class("images")
                ->form_list('images',$imageForm,$product->images)
            ->close_fieldset()
        ;
        $form->hidden('referer',@$_SERVER['HTTP_REFERER']);

        \Bingo\Action::run('shop_product_meta_box',array($product,$form));

        if ($form->validate()) { 
            $form->fill($product);
            unset($product->images["{id}"]);
            $product->images = array_values($product->images);
            
            $special_hash = array();
            if ($product->extra_categories) foreach ($product->extra_categories as $key=>$cat) {
                if ($cat->special) {
                    if (!@$_POST['special_categories'][$cat->id]) {
                        unset($product->extra_categories[$key]);
                    } else {
                        $special_hash[$cat->id] = 1;
                    }
                }
            }
            foreach (@$_POST['special_categories']?:array() as $id=>$flag) {
                if (!isset($special_hash[$id])) {
                    $product->extra_categories[] = \Shop\Models\Category::find($id);
                }
            }
            
            \Bingo\Action::run('shop_save_product',array($product,$form->values,$form));
            $product->save(true);
            \Bingo\Action::run('shop_product_saved',array($product,$form->values,$form));
            
            if ($form->values['referer']) {
                redirect($form->values['referer']);
            } else {
                set_flash('info',_t('Successfully saved'));
                return redirect("admin/shop/product-edit/".$product->id);
            }
        }
        
        $this->data['title'] = _t('Edit product');
        $this->data['form'] = $form->get();
        $this->view('shop/base-edit-tinymce',$this->data);        
    }

    function export_fields($settings) {

        error_reporting(0);
        require_once 'PHPExcel/PHPExcel.php';

        $excel = new \PHPExcel();
        $sheet_index = 0;
        foreach ($settings as $sheet_label=>$sheet_info) {

            if ($sheet_index==0) {
                $sheet = $excel->getActiveSheet();
            } else {
                $sheet = $excel->createSheet($sheet_index);
            }
            $sheet_index++;
            $sheet->setTitle($sheet_label);

            $data = @$sheet_info['data'];
            $fields = @$sheet_info['fields'];
            $filters = @$sheet_info['filters'];

            $col = 0;
            foreach ($fields as $field=>$width) {
                $sheet->setCellValueByColumnAndRow($col,1,$field);
                $sheet->getStyleByColumnAndRow($col,1)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $sheet->getStyleByColumnAndRow($col,1)->getFill()->getStartColor()->setARGB("FFFFAAAA");
                if ($width) {
                    if ($width=="auto") {
                        $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
                    } else {
                        $sheet->getColumnDimensionByColumn($col)->setWidth($width);
                    }
                }
                $col++;
            }

            $row = 1;
            foreach ($data as $item) {
                $row++;
                $col = 0;
                foreach ($fields as $field=>$width) {
                    $val = $item->$field;
                    if ($val instanceof \DoctrineExtensions\ActiveEntity\ActiveEntity)
                        $val = $val->id;

                    if ($val instanceof \Doctrine\ORM\PersistentCollection) {
                        $res = array();
                        foreach ($val as $sub) $res[] = $sub->getField('id');
                        $val = join(',',$res);
                    }
                    if (isset($filters[$field])) {
                        $func = $filters[$field];
                        $val = call_user_func($func,$val);
                    }
                    if (\is_bool($val)) {
                        $val = (int)$val;
                    }
                    $sheet->setCellValueByColumnAndRow($col,$row,$val);
                    $col++;
                }
            }
        }
        $excel->setActiveSheetIndex(0);
        $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="export.xlsx"');

        $writer->save('php://output');
        $excel->disconnectWorksheets();
        unset($excel);
        die();
    }
    function import_fields($file,$settings) {
        require_once 'PHPExcel/PHPExcel.php';

        $reader = \PHPExcel_IOFactory::createReader('Excel2007');
        $reader->setReadDataOnly(true);

        /* @var $reader \PHPExcel */
        $excel = $reader->load($file);

        $sheet_index = 0;
        $import_result = array();
        foreach ($settings as $sheet_info) {

            /* @var $sheet \PHPExcel_Worksheet */
            $sheet = $excel->getSheet($sheet_index);
            $sheet_index++;

            $fields = @$sheet_info['fields'];
            $filters = @$sheet_info['in_filters'];
            $class = @$sheet_info['class'];
            $post_process = @$sheet_info['post_process'];

            $import_result[$class] = array();
            $maxRow = $sheet->getHighestRow();

            for ($row=2;$row<=$maxRow;$row++) {
                $col = 0;
                $obj = new $class;
                foreach ($fields as $field=>$width) {
                    $val = $sheet->getCellByColumnAndRow($col,$row)->getValue();
                    if (isset($filters[$field])) {
                        $func = $filters[$field];
                        $val = call_user_func($func,$val,$obj);
                    }
                    $obj->$field = $val;

                    $metadata = $this->em->getClassMetaData(get_class($obj));
                    $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
                    $obj->save(false);
                    $col++;
                }
                $import_result[$class][$obj->id] = $obj;
            }
            if ($post_process) call_user_func($post_process,$import_result[$class]);
            $this->em->flush();
        }
        $excel->disconnectWorksheets();
        unset($excel);
    }

    function export() {
        $form = new \Bingo\Form("post",array("enctype"=>"multipart/form-data"));
        $form
            ->fieldset()
            ->button('action',_t('Export to excel'),'submit','export')->add_class('single')
            ->fieldset()
            ->html("<input type='file' name='file'>")
            ->button('action',_t('Import from excel'),'submit','import');

        if ($form->validate()) {

            $settings = array(
                'categories' => array(
                    'fields' => array('id'=>10,'title'=>50,'parent'=>10,'permalink'=>30,
                                      'item_order'=>15,'disabled'=>15,'excerpt'=>50,'content'=>50),
                    'data' => false,
                    'class' => '\Shop\Models\Category',
                    'post_process' => function ($list) {
                        foreach ($list as $obj) {
                            $obj->parent = $obj->parent ? $list[$obj->parent] : null;
                            $obj->save(false);
                        }
                    },
                    'in_filters' => array(
                        'content' => function ($val) { return $val ?:""; },
                        'excerpt' => function ($val) { return $val ?:""; }
                    )
                ),
                'products' => array(
                    'fields' => array('id'=>10,'title'=>50,'list_title'=>50,'permalink'=>30,'manufacturer'=>30,'price'=>20,'item_order'=>15,
                                      'disabled'=>15,'storage_state'=>10,'category'=>10,
                                      'extra_categories'=>30,'images'=>50,'excerpt'=>50,'content'=>50),
                    'data' => false,
                    'class' => '\Shop\Models\Product',
                    'filters' => array(
                        'images' => function ($images) {
                            $urls = array();
                            foreach ($images as $image) $urls[] = $image['url'];
                            return join(',',$urls);
                        }
                    ),
                    'in_filters' => array(
                        'category' => function ($val) { return \Shop\Models\Category::find($val); },
                        'content' => function ($val) { return $val ?:""; },
                        'excerpt' => function ($val) { return $val ?:""; },
                        'list_title' => function ($val) { return $val ?:""; },
                        'extra_categories' => function ($val,$obj) {
                            $val = \explode(',',$val);
                            $res = array();
                            foreach ($val as $id) {
                                $obj = \Shop\Models\Category::find($id);
                                if ($obj) $res[] = $obj;
                            }
                            return $res;
                        },
                        'images' => function ($val) {
                            $val = \explode(',',$val);
                            $res = array();
                            foreach ($val as $url) $res[] = array('url'=>$url);
                            return $res;
                        }
                    )
                )
            );
            $settings = \Bingo\Action::filter('shop_export_settings',array($settings));

            if ($form->values['action']=='export') {
                $settings['categories']['data'] = \Shop\Models\Category::findAll();
                $settings['products']['data'] = \Shop\Models\Product::findAll();
                $this->export_fields($settings);
            } else {
                $file_data = @$_FILES['file'];
                if ($file_data && $file_data['tmp_name']) {
                    $path = $file_data['tmp_name'];

                    $this->em->createQuery('DELETE FROM \Shop\Models\Product')->getResult();
                    $this->em->createQuery('DELETE FROM \Shop\Models\Category')->getResult();

                    $this->import_fields($path,$settings);
                }
            }
        }
        $this->data['title'] = _t('Catalog export/import');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }

    function payment($name) {
        if ($name) return $this->plugin_edit('payment',$name);

        $this->data['title'] = _t('Payments');
        $this->data['list'] = \Shop::$paymentMethods;

        $this->data['fields'] = array('id'=>_t('id'),'title'=>_t('title'));
        $this->data['field_filters']['id'] = function ($val,$obj) {
            if ($obj->id)
                $edit = anchor('admin/shop/payment/'.$obj->id,$obj->id);
            else
                $edit = "["._t('no id')."]";
            return $edit;
        };
        $this->view('cms/base-list',$this->data);
    }
    function shipping($name) {
        if ($name) return $this->plugin_edit('shipping',$name);

        $this->data['title'] = _t('Shipping');
        $this->data['list'] = \Shop::$shippingMethods;

        $this->data['fields'] = array('id'=>_t('id'),'title'=>_t('title'));
        $this->data['field_filters']['id'] = function ($val,$obj) {
            if ($obj->id)
                $edit = anchor('admin/shop/shipping/'.$obj->id,$obj->id);
            else
                $edit = "["._t('no id')."]";
            return $edit;
        };
        $this->view('cms/base-list',$this->data);
    }

    function plugin_edit($type,$id) {
        $obj = false;
        $list = $type=='shipping' ? \Shop::$shippingMethods : \Shop::$paymentMethods;
        foreach ($list as $one) {
            if ($id==$one->id) {
                $obj = $one;
                break;
            }
        }
        if (!$obj) redirect('admin/shop/'.$type);
        
        $form = $obj->getAdminForm();
        $form->fieldset('');
        $form->submit(_t('Save options'))->addClass("single");

        if ($form->validate()) {
            $form->fill($obj);
            $obj->save();
            redirect('admin/shop/'.$type);
            return;
        }

        $this->data['title'] = _t('Edit plugin settings');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce',$this->data);
    }
    
    function comment_list() {
        $query = \Shop\Models\Comment::findByQuery(array(),'id DESC');
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);

        $this->data['list'] = $pagination->result();
        $this->data['pagination'] = $pagination->get(5);

        $this->data['page_actions']['admin/shop/comment-edit'] = _t('Create new');
        $this->data['list_actions']['change'] = array(
            'title' => _t('Change'),
            'form' => \Form::create()->select('visible',_t('Visible'),array(_t('Yes')=>1,_t('No')=>0),'',1),
            'function' => function ($list,$action) use ($type) {
                $form = $action['form'];
                if ($form->validate()) {
                    $values = $form->getValue();
                    foreach ($list as $id=>$data) {
                        if ($data['check']) {
                            $obj = \Shop\Models\Comment::find($id);
                            $obj->visible = $values['visible'];
                        }
                    }
                    \Bingo::$em->flush();
                }
                redirect('admin/shop/comment-list/'.$type);
            }
        );
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Shop\Models\Comment','admin/shop/comment-list')
        );

        $this->data['fields'] = array(
            'id'=>_t('#'),
            'date'=>_t('date'),
            'product' => _t('product'),
            'author' => _t('author'),
            'email' => _t('email'),
            'content' => _t('content'),
            'visible' => _t('visible')
        );
        $this->data['field_filters']['product'] = function ($val) {
            if ($val) {
                $val->getField('id');
                return anchor('admin/shop/product-edit/'.$val->id,$val->title);
            }
            return "";
        };
        $this->data['field_filters']['content'] = function ($val,$obj) {
            return "<h3>$obj->title</h3>".nl2br(htmlspecialchars($obj->content));
        };
        $this->data['field_filters']['visible'] = function ($val) {
            return $val ? _t('yes','shop'):_t('-','shop');
        };
        
        $this->data['title'] = _t('Comment list');
        $this->view('cms/base-list',$this->data);        
    }
    
    function order_list() {
        $query = \Shop\Models\Order::findByQuery(array(),'id DESC');
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query);

        $this->data['list'] = $pagination->result();
        $this->data['pagination'] = $pagination->get(5);

        $this->data['page_actions']['admin/shop/order-edit'] = _t('Create new');
        $this->data['item_actions']['admin/shop/order-edit'] = _t('edit');

        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Shop\Models\Order','admin/shop/order-list')
        );

        $this->data['fields'] = array(
            'id'=>_t('#'),
            'created'=>_t('created'),
            'modified' => _t('modified')
        );
        $this->data['title'] = _t('Order list');
        $this->view('cms/base-list',$this->data);
    }
    function order_edit($id) {
        $order = $this->findOrCreate('\Shop\Models\Order',$id);

        $itemForm = new \Bingo\Form;
        $itemForm
                ->text('qty',_t('Quantity'),'')
                ->text('title',_t('Title'),'')
                ->text('special',_t('Price'),'');
        
        \Bingo\Action::run('shop_order_item_meta_box',array($order,$itemForm));

        $form = new \Bingo\Form;
        $form
            ->fieldset(_t('Info'))
            ->html("<div class='value'>{value}</div>",array('label'=>_t('#'),'value'=>$order->id))
            ->text("data[name]",_t('Client name'),'',@$order->data['name'])
            ->text("data[phone]",_t('Phone'),'',@$order->data['phone'])
            ->text('data[shipping_title]',_t('Shipping title'),'',@$order->data['shipping_title'])
            ->text('data[shipping_price]',_t('Shipping price'),'',@$order->data['shipping_price'])
            ->html("<div class='value'>{value}</div>",array('label'=>_t('Total'),'value'=>\Shop\Models\Product::get_price_static(@$order->getTotal())));

        if (@$order->data['email'])
            $form->html("<div class='value'>{value}</div>",array('label'=>_t('E-mail'),'value'=>$order->data['email']));
        if (@$order->data['city'])
            $form->html("<div class='value'>{value}</div>",array('label'=>_t('City'),'value'=>$order->data['city']));
        if (@$order->data['address'])
            $form->html("<div class='value'>{value}</div>",array('label'=>_t('Address'),'value'=>$order->data['address']));
        if (@$order->data['address_extra'])
            $form->html("<div class='value'>{value}</div>",array('label'=>_t('Comment'),'value'=>$order->data['address_extra']));
        
        $form
            ->fieldset(_t('Items'))->add_class('order_items')
            ->form_list('data[items]',$itemForm,@$order->data['items'],true)
            ->fieldset(_t('Order comments'))
            ->textarea('comments',false,'',$order->comments,array('rows'=>15))->add_class('tinymce')
            ->fieldset()
            ->submit(_t('Save order'))->add_class("single");
        
        \Bingo\Action::run('shop_order_meta_box',array($order,$form));

        if ($form->validate()) {
            $form->fill($order);
            $order->save();
            set_flash('info',_t('Successfully saved'));
            return redirect("admin/shop/order-edit/$id");
        }

        $this->data['title'] = _t('Edit order');
        $this->data['form'] = $form->get();
        $this->view('shop/base-edit-tinymce',$this->data);          
    }
}