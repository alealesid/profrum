<?php

namespace Shop\Models;

/** 
* @Entity 
* @Table(name="shop_categories")
*/
class Category extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=1024) */
    public $title;
    /** @Column(type="text") */
    public $content;
    /** @Column(type="text") */
    public $excerpt;
    /** @Column(length=256) */
    public $permalink;
    /** @Column(type="boolean") */
    public $disabled;

    /** @Column(length=1024) */
    public $image;

    /**
     * @OneToMany(targetEntity="Category", mappedBy="parent")
     */
    public $children;    
    /**
     * @ManyToOne(targetEntity="Category", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id",onDelete="SET NULL")
     */
    public $parent;
    
    /**
     * @OneToMany(targetEntity="Product", mappedBy="parent")
     */
    public $products;    
    
    /** @Column(type="integer") */
    public $item_order;
    
    /** @Column(type="boolean") */
    public $special;
    
    /** @Column(type="integer") */
    public $product_order;
    
    /** @Column(type="array") */
    public $data;
    
    function __construct() {
        $this->data = array();
        $this->item_order = 0;
    }
    function get_url() {
        $p = $this;
        $chain = "";
        $count = 0;
        
        $options = \CMS\Models\Option::get("shop_options");
        $prefix = @$options['prefix'];
        
        while ($p) {
            if (!$p->permalink) return false;
            $chain = $p->getField('permalink')."/".$chain;
            $p = $p->parent;
            $count++;
            if ($count>20) break;
        }
        return url(($prefix ? $prefix."/" : "").trim($chain,"/"));
    }

    function get_image_url($w=150,$h=150) {
        if (!$this->image) return false;
        $file = $this->image;
        if (!file_exists(INDEX_DIR."/upload/CMS/files/$file")) return false;
        return \Bingo\ImageResizer::get_url($file,$w,$h,array(255,255,255));
    }
    
    function toLiquid() {
        $this->url = $this->get_url();
        return $this;
    }
}
