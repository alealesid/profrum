<?php

namespace Shop\Models;

/**
* @Entity
* @Table(name="shop_comments")
*/
class Comment extends \ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=100) */
    public $title;
    /** @Column(length=100) */
    public $author;
    /** @Column(length=100) */
    public $author_email;
    /** @Column(length=100) */
    public $author_url;
    /** @Column(type="text") */
    public $content;
    /** @Column(type="datetime") */
    public $date;
    /** @Column(type="boolean") */
    public $visible;
    /** @Column(type="object") */
    public $data;
    
    
    /**
     * @ManyToOne(targetEntity="Product",inversedBy="comments")
     * @JoinColumn(name="product_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $product;
    
    /**
     * @OneToMany(targetEntity="Comment", mappedBy="parent")
     */
    public $children;
    
    /**
     * @ManyToOne(targetEntity="Comment", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id",onDelete="SET NULL")
     */
    public $parent;    

    function __construct() {
        $this->date = new \DateTime("now");
        $this->title = "";
        $this->author = "";
        $this->author_email = "";
        $this->author_url = "";
        $this->visible = false;        
        $this->data = array();
    }
}
