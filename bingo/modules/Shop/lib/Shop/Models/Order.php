<?php

namespace Shop\Models;

/** 
* @Entity 
* @Table(name="shop_orders")
*/
class Order extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(type="array") */
    public $data;
    /** @Column(type="datetime") */
    public $created;
    /** @Column(type="datetime") */
    public $modified;
    /** @Column(type="text") */
    public $comments;
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    function __construct() {
        $this->created = new \DateTime();
        $this->comments = "";
    }

    function save($flush=true) {
        $this->modified = new \DateTime();
        parent::save($flush);
    }

    function getTotal() {
        $sum = $this->data['shipping_price'];
        foreach ($this->data['items'] as $i) {
            $sum += $i['special'] * $i['qty'];
        }
        return $sum;
    }

    function sendEmail() {
        $options = \CMS\Models\Option::get("shop_options");
        $c = new \Bingo\Controller();
        $c->data = $this->data;
        $c->data['order'] = $this;
        
        if (@$options['email']) {
            if (\Bingo\Template::get_include('shop/order_email')) {
                $html = $c->view('shop/order_email',$c->data,true);
                $mail = new \Mail('utf-8');
                $mail->setFrom(@$options['email']);
                $mail->addTo(@$options['email']);
                $mail->setSubject(_t("New order from you shop"));
                $mail->setBodyHtml($html);
                try {
                    $mail->send();
                } catch (\Exception $e) {
                }
            }
            
            if (isset($this->data['email'])) {
                if (\Bingo\Template::get_include('shop/order_client_email')) {
                    $html = $c->view('shop/order_client_email',$c->data,true);
                    $mail = new \Mail('utf-8');
                    $mail->setFrom(@$options['email']);
                    $mail->addTo($this->data['email']);
                    $mail->setSubject(_t("Your order was accepted"));
                    $mail->setBodyHtml($html);
                    try {
                        $mail->send();
                    } catch (\Exception $e) {
                    }
                }
            }       
        }
    }
}
