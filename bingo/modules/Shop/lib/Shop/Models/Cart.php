<?php

namespace Shop\Models;

class Cart {

    private static $instance;

    static public function getInstance() {
        if (!Cart::$instance) Cart::$instance = new Cart();
        return Cart::$instance;
    }

    public $items;

    protected function __construct() {
        $data = new \Session\SessionNamespace('Shop');
        $data = $data->cart;
        $this->items = array();
        
        if ($data) {
            $ids = array();
            foreach ($data as $item) $ids[] = $item['id'];
            if (!empty($ids)) Product::findBy(array('id'=>$ids));
            
            foreach ($data as $item) {
                $id = $item['id'];
                $product = $id ? Product::find($id) : null;
                if ($product) {
                    $this->items[$id] = (object)$item;
                    $this->items[$id]->product = $product;
                }
            }
        }
    }

    function add($product,$quantity=1) {
        if (is_int($product)) $product = Product::find($product);
        if (!$product) return;
        
        $id = $product->id;
        if (!isset($this->items[$id])) {
            $this->items[$id] = (object)array(
                'id' => $id,
                'product' => $product,
                'quantity' => 0
            );
        }
        $this->items[$id]->quantity += $quantity;
    }
    
    function set($product,$data) {
        if (is_int($product)) $product = Product::find($product);
        if (!$product) return;
        $id = $product->id;
        
        $this->add($product,0);
        $qty = (int)@$data['quantity'];
        if (isset($data['quantity']) && !$data['quantity']) {
            unset($this->items[$id]);
        } else {
            foreach ($data as $key=>$val) $this->items[$id]->$key = $val;
        }
    }
    
    function setEmpty() {
        $this->items = array();
    }

    function save() {
        $data = array();
        foreach ($this->items as $item) {
            $one = (array)$item;
            unset($one['product']);
            $data[] = $one;
        }
        $session = new \Session\SessionNamespace('Shop');
        $session->cart = $data;
    }

    function getCount() {
        $sum = 0;
        foreach ($this->items as $item) $sum += $item->quantity;
        return $sum;
    }

    function getTotal() {
        $sum = 0;
        foreach ($this->items as $item) {
            $product = $item->product;
            $qty = $item->quantity;
            $price = $product->special_price ? : $product->price;
            $sum += ($price * $qty);
        }
        return $sum;
    }
}
