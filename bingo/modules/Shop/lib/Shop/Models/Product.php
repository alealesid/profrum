<?php

namespace Shop\Models;

/** 
* @Entity 
* @Table(name="shop_products")
*/
class Product extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=1024) */
    public $title;
    /** @Column(length=1024) */
    public $list_title;
    /** @Column(length=1024) */
    public $manufacturer;

    /** @Column(type="text") */
    public $content;
    /** @Column(type="text") */
    public $excerpt;
    /** @Column(length=256) */
    public $permalink;
    
    /**
     * @ManyToOne(targetEntity="Category", inversedBy="products")
     * @JoinColumn(name="category_id", referencedColumnName="id",onDelete="SET NULL")
     */
    public $category;
    
    /**
     * @ManyToMany(targetEntity="Category")
     * @JoinTable(name="shop_products_to_categories",
     *      joinColumns={@JoinColumn(name="product_id", referencedColumnName="id",onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="id",onDelete="CASCADE")}
     *      )
     */    
    public $extra_categories;
    
    /**
     * @OneToMany(targetEntity="Comment", mappedBy="post")
     * @OrderBy({"date" = "DESC"})
     */
    protected $comments;

    /** @Column(type="integer") */
    public $item_order;
    
    /** @Column(type="array") */
    public $images;

    /** @Column(type="decimal") */
    public $price;

    /** @Column(type="decimal") */
    public $special_price;

    /** @Column(type="boolean") */
    public $disabled;

    public static function get_storage_states() {
        return array(
            _t('In storage','shop') => 0,
            _t('Not in storage','shop') => 1,
            _t('Preorder available','shop') => 2
        );
    }
    public function get_storage_state() {
        return array_search($this->storage_state,Product::get_storage_states());
    }
    
    /** @Column(type="integer") */
    public $storage_state;
    
    /** @Column(type="array") */
    public $data;

    static function findByQueryBuilder(array $criteria,$orderBy=false) {
        if (@$criteria['category']) {
            $cat = $criteria['category'];
            unset($criteria['category']);
            $qb = parent::findByQueryBuilder($criteria,$orderBy);

            $qb->leftJoin('obj.extra_categories','cat');
            $expr = $qb->expr()->orx();
            $expr->add("obj.category = :cat1");
            $expr->add("cat = :cat2");
            $qb->andWhere($expr);
            $qb->setParameter('cat1',$cat);
            $qb->setParameter('cat2',$cat);

            return $qb;
        } else {
            return parent::findByQueryBuilder($criteria,$orderBy);
        }
    }
    
    function __construct() {
        $this->data = array();
        $this->item_order = 0;
        $this->disabled = false;
        $this->list_title = "";
    }

    function get_url() {
        if (!$this->category) return "#";
        return $this->category->get_url()."/".($this->permalink?:$this->id);
    }

    function get_image_url($w=500,$h=500,$number=0) {
        if (count($this->images)<=$number) return false;
        $file = $this->images[$number]['url'];
        if (!file_exists(INDEX_DIR."/upload/CMS/files/$file")) return false;
        return \Bingo\ImageResizer::get_url($file,$w,$h,array(255,255,255));
    }

    static function get_price_static($p) {
        $opts = \CMS\Models\Option::get('shop_options');
        $pre = @$opts['currencyPre'];
        $post = @$opts['currencyPost'];
        return $pre.$p.$post;
    }

    function get_price_value() {
        return $this->special_price ?:$this->price;
    }

    function get_price($price=false) {
        $p = ($price===false) ? $this->get_price_value() : $price;
        return Product::get_price_static($p);
    }
    
    function toLiquid() {
        $this->url = $this->get_url();
        $this->storage = $this->get_storage_state();
        return $this;
    }
}
