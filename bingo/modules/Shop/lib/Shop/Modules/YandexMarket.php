<?php

namespace Shop\Modules;

class YandexMarket extends \Bingo\Module {
    function __construct() {
        \Shop\Models\Product::mapField(array('fieldName'=>'yandex','type'=>'boolean'));

        \Bingo\Action::add('shop_admin_product_list_view',function ($controller) {
            $controller->data['fields']['yandex'] = _t('Y.M.');
            $controller->data['list_actions'][0]['form']->checkbox('yandex',false);
        });

        \Bingo\Action::add('shop_admin_product_list_filter_form',function ($filter_form) {
            $filter_form->select('yandex',_t('Yandex.Market'),array(
                _t('all','shop') => 0,
                _t('enabled','shop') => 1,
                _t('disabled','shop') => -1
            ),'',false,array('after'=>'id'));
        });

        \Bingo\Action::add('shop_product_meta_box',function($post,$form){
            $form->hidden('yandex',0);
        },10,2);

        \Bingo\Action::add('shop_admin_product_list_criteria',function ($criteria,$filter_form) {
            if (@$filter_form->values['yandex'])
                $criteria['yandex'] = ($filter_form->values['yandex'] > 0);
            return $criteria;
        },10,2);

        $this->connect('shop.yml',array('function'=>array($this,'generate')));
    }

    function generate() {

        $options = \CMS\Models\Option::get("shop_options");
        $em = \Bingo\Bingo::getInstance()->em;

        $this->base = 'http://'.$_SERVER['HTTP_HOST'];

        $output = "";
        $output .= '<?xml version="1.0" encoding="utf-8"?>'."\n";
        $output .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">'."\n";
        $output .= '<yml_catalog date="' . date("Y-m-d H:m") . '">'."\n";
        $output .= '<shop>'  . "\n";
        $output .= '<name>' . @$options['title'] . '</name>'."\n";
        $output .= '<company>' . $options['owner'] . '</company>'."\n";
        $output .= '<url>' . $this->base.url('') . '</url>'. "\n";

        // Перечесляем валюту магазина
        // TODO: Добавить возможность настраивать проценты.
        $output .= '<currencies>'."\n";
        $output .= '<currency id="RUR" rate="1"/>'."\n";
        $output .= '<currency id="USD" rate="CBRF" plus="3"/>'."\n";
        $output .= '<currency id="EUR" rate="CBRF" plus="3"/>'."\n";
        $output .= '</currencies>'."\n";

        // Категории товаров
        $output .= '<categories>'."\n";
        $output .= $this->getCat(null);
        $output .= '</categories>'."\n";

        // Товарные позиции
        $output .= '<offers>'."\n";
        $products = \Shop\Models\Product::findBy(array('yandex'=>true,'disabled'=>false));
        
        $flat = false;
        foreach (\Shop::$shippingMethods as $one) {
            if (!$flat && $one instanceof \Shop\Shipping\Flat) $flat = $one;
            if ($one->id=='flat') $flat = $one;
        }
        
        foreach ($products as $product) {
            if ($product->category->getField('disabled')) continue;

            $output .= '<offer id="'.$product->id.'" available="true" bid="13">'."\n";
            $output .= '<url>' . $this->base . $product->get_url() . '</url>'."\n";
            $output .= '<price>' .  $product->get_price_value() . '</price>'."\n";
            $output .= '<currencyId>RUR</currencyId>'."\n";

            // Определяем категорию для товара
            $output .= '<categoryId>'. $product->category->id .'</categoryId>'."\n";

            // Определеяме изображение
            $image = $product->get_image_url();
            if ($image)
                $output .= '<picture>' . $this->base. $image . '</picture>'."\n";

            
            $shipping_price = $flat ? $flat->getPrice(array($product)) : 200;
            
            $output .= '<delivery> true </delivery>'."\n";
            $output .= '<local_delivery_cost>'.$shipping_price.'</local_delivery_cost>';
            $output .= '<name>'.htmlspecialchars($product->title).'</name>'."\n";
            $output .= '<description>' . htmlspecialchars(strip_tags($product->content)) . '</description>';
            $output .= '<manufacturer_warranty>true</manufacturer_warranty>';
            $output .= '</offer>'."\n";
        }

        $output .= '</offers>'."\n";
        $output .= '</shop>'."\n";
        $output .= '</yml_catalog>'."\n";

        header('Content-Type: application/xml');
        echo $output;
    }

    // Возвращает массив категорий
    protected function getCat($parent) {
        $categories =  \Shop\Models\Category::findBy(array('parent'=>$parent,'disabled'=>false));
        $out = '';
        foreach ($categories as $category) {
            $out .= '<category id="'.$category->id.'"';
            if($parent) $out .= ' parentId="'.$parent->id.'"';
            $out .='>'.$category->title.'</category>'."\n";
            if($e =  $this->getCat($category)) $out .= $e;
        }
        return $out;
    }
}