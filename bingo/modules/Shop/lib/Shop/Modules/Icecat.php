<?php

namespace Shop\Modules;

use \Bingo\FormFilter_DoctrineObject as ObjectFilter;

class Icecat extends \Bingo\Module {
    function __construct() {
        parent::__construct();
        $this->connect('admin/shop/icecat',array('controller'=>'\Shop\Modules\IcecatController','action'=>'index'));
        
        \Bingo\Action::add('admin_pre_header',function () {
            \Admin::$menu[_t('Shop','shop')][_t('Catalog','shop')][_t('Icecat add','shop')] = 'admin/shop/icecat';
        });        
    }
}

class IcecatController extends \CMS\Controllers\Admin\BasePrivate {
    function index() {
        $form = new \Bingo\Form();
        
        $form->text('icecat_login',_t('Icecat login'),'required');
        $form->password('icecat_password',_t('Icecat password'),'required');
        $form->text('ean',_t('Product EAN'),'required');
        $form->submit('Search');
        
        if ($form->validate()) {
            
            $username = $form->values['icecat_login'];
            $password = $form->values['icecat_password'];
            $ean = $form->values['ean'];
            
            $context = stream_context_create(array(
                'http' => array(
                    'header'  => "Authorization: Basic " . base64_encode($username.":".$password)
                )
            ));
            $data = file_get_contents('http://data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc='.$ean.';lang=en;output=productxml', false, $context);

            //$form->html("<pre>".htmlspecialchars($data)."</pre>");
            
            $xml = new \SimpleXMLElement($data);
            
            $product = $xml->xpath("//Product");
            
            $productDescription = $xml->xpath("//ProductDescription");
            $picture = $xml->xpath("//ProductPicture");
            $supplier = $xml->xpath("//Supplier");
            
            $images = array();
            $title = "";
            $description = "";
            $manufacturer = "";
            $error = false;
            foreach($product as $item) {
                $atts = $item->attributes();
                if ($atts['Title']!=null) $title = $atts['Title'];
                if ($atts['HighPic'] != null) $images[] = $atts['HighPic'];
                if ($atts['ErrorMessage']!=null) $error = $atts['ErrorMessage'];
            }
            foreach ($picture as $item) {
                $atts = $item->attributes();
                if (!$atts['logo'] && $atts['Pic']!=null) $images[] = $atts['Pic'];
            }
            foreach ($supplier as $item) {
                $atts = $item->attributes();
                if ($atts['Name']!=null) $manufacturer = $atts['Name'];
            }
			foreach($productDescription as $item) {
				$productValues = $item->attributes();
				if($productValues['URL'] != null) {
					$specs .= "<p id='manufacturerlink'><a href='".$productValues['URL']."'>Productinformation from manufacturer</a></p>";
				}
				if($productValues['LongDesc'] != null)
				{
                    $description = utf8_decode(str_replace('\n', '</p><p>', $productValues['LongDesc']));
					$description = str_replace('<b>', '<strong>', $description);
					$description = str_replace('<B>', '<strong>', $description);
					$description = str_replace('</b>', '</strong>', $description);
					$specs .= "<p id='manudescription'>".$description."</p>";
				}
            }
            
            $form->html("<hr>");
            
            if ($error) {
                $form->html($error);
            } else {
                $cat_options = $this->treeSort(\Shop\Models\Category::findAll(),'parent','title','item_order',true);
                $form->select('category',_t('Add to category'),$cat_options)
                    ->add_filter(new ObjectFilter('\Shop\Models\Category'));
                $form->html("<button type='submit' value='1' name='add'>"._t("Add product")."</button>");
                
                $form->html("<hr>");
                $form->html("<h1>".$title."</h1>");
                $form->html("<p><b>"._t('Manufacturer').":</b> ".$manufacturer."</p>");
                $img_html = "<div>";
                foreach ($images as $img) {
                    $img_html .= '<span style="display:inline-block"><span style="vertical-align: middle; display: table-cell;
                        text-align: center; width: 100px; height: 100px; 
                        border: 1px solid rgb(204, 204, 204); margin: 0px; padding: 0px;">
                        <a href="'.$img.'" class="colorbox" rel="prodgal">
                            <img style="display: block; margin: auto; max-width:100px; max-height:100px;" src="'.$img.'">
                        </a>
                    </span></span>';
                }
                $img_html .= "</div>";
                $form->html($img_html);
                $form->html($description);
                $form->validate();
                
                if (@$_POST['add']) {
                    $product = new \Shop\Models\Product;
                    $product->category = \Shop\Models\Category::find($_POST['category']);
                    $product->title = $title;
                    $product->content = $description;
                    $product->excerpt = "";
                    $product->permalink = "";
                    $product->price = 0;
                    $product->special_price = 0;
                    $product->storage_state = 0;
                    $product->images = array();
                    $product->manufacturer = $manufacturer;
                    $product->save();
                    
                    foreach ($images as $img) {
                        $name = basename($img);
                        $rel = "icecat/".$product->id;
                        $dir = INDEX_DIR . "/upload/CMS/files/".$rel;
                        $path = $dir."/".$name;
                        if (!file_exists($dir)) mkdir($dir,0777,true);
                        file_put_contents($path, file_get_contents($img));                    
                        $product->images[] = array('url'=>$rel."/".$name);
                    }
                    
                    $product->save();
                    $this->data['message'] = _t('Successfully added');
                }
            }
        }
        
        $this->data['form'] = $form->get();
        $this->data['title'] = _t('Add product from Icecat');
        $this->view("cms/base-edit");
    }
}