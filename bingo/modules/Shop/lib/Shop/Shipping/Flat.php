<?php

namespace Shop\Shipping;

class Flat extends \Shop\Plugin {
    
    public $exact = true;
    public $individual_price = false;
    public $individual_price_title;
    
    function __construct($args) {
        $this->title = _t('Flat','shop');
        
        parent::__construct($args);
        if ($this->individual_price) {
            $that = $this;
            \Bingo\Action::add('shop_product_meta_box',function($product,$form) use ($that){
                $title = $that->individual_price_title ?:_t('Shipping price','shop');
                $form->text('data[flat_price]',$title,'',@$product->data['flat_price'],array('after'=>'price'));
            },10,2);
        }
    }
    
    function getPrice($products=false) {
        if ($this->individual_price) {
            $price = 0;
            if (!$products) {
                $cart = \Shop\Models\Cart::getInstance();
                $products = array();
                foreach ($cart->items as $item) $products[] = $item->product;
            }
            foreach ($products as $product) {
                if (@$product->data['flat_price'] != '')
                    $price = max($price, $product->data['flat_price']);
                else
                    $price = max($price, $this->price);
            }
            return $price;          
        } else {
            return $this->price;
        }
    }
    
    function getJS() {
        if (!$this->exact) {
            return "{
                getShippingPrice:function(total) {
                    return '&asymp; ' + this.get_price($this->price);
                },
                getTotalPrice:function(total) {
                    return '&asymp; ' + this.get_price(total+$this->price);
                }
            }";            
        }
        return "{}";
    }
    
    function getAdminForm() {
        $form = parent::getAdminForm();
        $form->text('price',_t('Price','shop'),'',$this->price);
        return $form;
    }    
}
