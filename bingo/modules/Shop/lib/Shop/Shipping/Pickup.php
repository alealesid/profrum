<?php

namespace Shop\Shipping;

class Pickup extends \Shop\Plugin {
    function __construct($args) {
        $this->title = _t('Pickup','shop');
        parent::__construct($args);
    }
    
    function getPrice() {
        return 0;
    }
}
