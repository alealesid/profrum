<?php

class FileManager extends \Bingo\Module {
    function __construct() {
        $this->connect('file_manager/*url',array(
            'priority' => 100,
            'function' => function ($route) {
                $url = $route['url'];
                $path = dirname(__FILE__)."/kcFinder/".$url;
                if (is_file($path)) {
                    $ext = pathinfo($path,PATHINFO_EXTENSION);
                    if ($ext=='php') {
                        include $path;
                        die();
                    }
                    if ($ext=='css') header('Content-type: text/css');
                    if ($ext=='js') header('Content-type: application/javascript');
                    if ($ext=='png') header('Content-type: image/png');
                    if ($ext=='gif') header('Content-type: image/gif');
                    readfile($path);
                    die();
                }
                return true;
            }
        ));
    }

    static function view($path,$config=array()) {
        global $_CONFIG;
        $_CONFIG = array();
        $_CONFIG['disabled'] = false;
        $_CONFIG['uploadURL'] = url($path);
        $_CONFIG['uploadDir'] = INDEX_DIR."/".$path;
        
        if (session_id()=='') {
            $session_data = new \Session\SessionNamespace("kcFinder");
            if (!isset($session_data->data)) $session_data->data = array();
            $_CONFIG['_sessionVar'] = &$session_data->data;
        }
        
        $_CONFIG['types'] = array('files'=>"");
        if (isset($_GET['type'])) {
            $type = preg_replace("/[^A-Za-z0-9_-]/","_",$_GET['type']);
            $_CONFIG['types'][$type] = "";
        }
        
        if (isset($_GET['path'])) {
            $path = $_GET['path'];
            $dir = dirname($path);
            
            $_CONFIG['dir'] = $_GET['dir'] = $dir;
            $_CONFIG['selected_file'] = $_GET['selected_file'] = basename($path);
        }
        
        $_CONFIG = array_merge($_CONFIG,$config);
        require 'kcFinder/browse.php';
    }
}