<?php

class Layout extends \Bingo\Module {
    function __construct() {
        bingo_domain_register('layout',dirname(__FILE__)."/../locale");
        $this->connect('admin/:action/:data',array('controller'=>'\Layout\Controllers\Admin','data'=>false),
            array('action'=>'(layouts)'));

        \Bingo\Action::add('admin_pre_header',
        function () {
            \Admin::$menu[_t('System','cms')][_t('Appearance','blog')][_t('Layouts','layout')] = 'admin/layouts';
        });

    }
}

function snippet($name,$layout='default') {
    return \Layout\Widget::getSnippet($name,$layout);
}