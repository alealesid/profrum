<?php

namespace Layout\Controllers;

class Admin extends \CMS\Controllers\Admin\BasePrivate
{
    function __construct() {
        parent::__construct();
        bingo_domain('layout');
    }

    function layouts() {
        $form = new \Bingo\Form;
        $widgets = \CMS\Models\Option::get('widgets');
        $widget_types = &\Layout\Widget::$widget_types;

        $form->fieldset();
        $form->submit(_t("Save layouts"))->add_class("single");
        $form->close_fieldset();

        $placesCount = 0;
        foreach (\Layout\Widget::$layouts as $layout_name=>$layout) $placesCount += count($layout['widgetPlaces']);

        if ($placesCount) {
            $form->fieldset(_t("Available widgets"),true,array("id"=>"available_widgets"))->add_class('widgets available foldable');
            $form->html("<div>"._t("Drag one of this widgets to your widget places.")."</div>");
            foreach ($widget_types as $type_name=>$type) {
                $this->insertWidget($form,$type,$type_name);
            }
            $form->close_fieldset();
        }

        foreach (\Layout\Widget::$layouts as $layout_name=>$layout) {
            $form->fieldset($layout['label'],true,array("id"=>$layout_name))->add_class("layout foldable");
            foreach ($layout['widgetPlaces'] as $place_name=>$place) {
                $form->fieldset($place['label'],true,array("id"=>$place_name))->add_class('widgets foldable');
                if (isset($widgets[$layout_name]) && isset($widgets[$layout_name][$place_name]))
                    foreach ($widgets[$layout_name][$place_name] as $w=>$wdata) {
                        $widget_type = @$wdata['widget_type'];
                        if (isset($widget_types[$widget_type])) {
                            $type = $widget_types[$widget_type];
                            $this->insertWidget($form,$type,$widget_type,"{name}",$wdata);
                    }
                }
                $form->close_fieldset();
            }
            $snippetForm = $layout['snippetForm'];
            if (is_callable($snippetForm)) $snippetForm = $snippetForm();
            if (count($snippetForm->elements)) {
                $form->fieldset(_t("Settings"),true,array("id"=>$layout_name."_settings"))->add_class("foldable snippets");
                $root = clone $snippetForm->root;
                
                $rename = function ($ins) use (&$rename,$layout_name,$form,$widgets) {
                    if ($ins->name) {
                        $ins->value = @$widgets[$layout_name]['snippets'][$ins->name];
                        $ins->name = str_replace("{name}",$ins->name,"widgets[${layout_name}][snippets][{name}]");
                    }
                    $form->elements[] = $ins;
                    foreach ($ins->children as $el) {
                        $rename($el);
                    }
                };
                $rename($root);
                foreach ($root->children as $el) $form->add($el);
                
                $form->close_fieldset();
            }
            $form->close_fieldset();
        }

        if ($form->validate()) {
            $vals = $_POST['widgets'];
            $from_form = @$form->values['widgets'];
           
            if (!$vals) $vals = array();
            foreach ($vals as $layout=>$layout_list) {
                foreach ($layout_list as $place=>$place_list) {
                    if ($place!='snippets')
                        $vals[$layout][$place] = array_values($place_list);
                    else {
                        $vals[$layout][$place] = $from_form[$layout][$place];
                    }
                }
            }
            
            \CMS\Models\Option::set('widgets',$vals);
            set_flash('info',_t('Layouts saved'));
            redirect('admin/layouts');
        }

        $this->data['title'] = _t("Widgets");
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce',$this->data);
    }

    function insertWidget($form,$type,$type_name,$name_mask="{name}",$wdata=false) {
        $form->fieldset($type->getLabel(),true)->add_class("item");
        $form->html("<div class='description'>".nl2br($type->getDescription())."</div>");
        $form->html("<div class='elements'>");
        foreach ($type->getForm()->elements as $el) {
            $ins = clone($el);
            if ($wdata) {
                $val = @$wdata[$el->name];
                if ($ins->name)
                    $ins->name = str_replace("{name}",$el->name,$name_mask);
                $ins->value = $val;
            }
            $form->add($ins);
        }
        $form->hidden(str_replace("{name}","widget_type",$name_mask),$type_name);
        $form->html("<a href='#' class='array_element_del'>"._t("Delete")."</a>");
        $form->html("</div>");
        $form->close_fieldset();
    }
}