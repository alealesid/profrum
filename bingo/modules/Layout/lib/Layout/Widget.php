<?php

namespace Layout;

class Widget {

    static $widget_types = array();
    static $layouts = array();

    static function register($type) {
        Widget::$widget_types[$type] = new $type;
    }

    static function registerLayout($name,$args) {
        $defaults = array(
            'label' => _t('Default layout','core'),
            'widgetPlaces' => array(),
            'snippetForm' => new \Bingo\Form
        );
        $place_defaults = array(
            'label' => _t('Widget place','core'),
            'pre' => '',
            'post' => ''
        );

        Widget::$layouts[$name] = array_merge($defaults,$args);
        foreach (Widget::$layouts[$name]['widgetPlaces'] as $place_name=>$place) {
            Widget::$layouts[$name]['widgetPlaces'][$place_name] = array_merge(
                $place_defaults,
                $place
            );
        }
    }

    static function getSnippet($name,$layout='default') {
        $widgets = \CMS\Models\Option::get('widgets');
        return @$widgets[$layout]['snippets'][$name];
    }

    static function renderPlace($place,$layout='default') {
        $widgets = \CMS\Models\Option::get('widgets');
        if (isset(Widget::$layouts[$layout]['widgetPlaces'][$place]) && isset($widgets[$layout][$place])) {
            foreach ($widgets[$layout][$place] as $w) {
                $type = @Widget::$widget_types[$w['widget_type']];
                if ($type) {
                    $type->render($w);
                }
            }
        }
    }

    function getLabel() {
        return _t('My widget','core');
    }

    function getDescription() {
        return _t("My widget description",'core');
    }

    function getForm() {
        return new \Bingo\Form;
    }

    function render($data) {}
}