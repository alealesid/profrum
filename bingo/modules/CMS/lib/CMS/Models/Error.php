<?php

namespace CMS\Models;

/**
 * @Entity
 * @Table(name="cms_errors")
 */
class Error extends \ActiveEntity {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /** @Column(type="datetime") */
    public $date;

    /** @Column(length=64) */
    public $type;
    
    /** @Column(length=128) */
    public $group_name;

    /** @Column(type="text") */
    public $message;
    
    /** @Column(type="text") */
    public $debug_backtrace;
    
    /** @Column(type="text") */
    public $ip;    
    
    static function errors_with_same_message($ids) {
        if (!is_array($ids)) $ids = array($ids);
        $errors = array();
        if (!empty($ids)) {
            $errors = self::$entityManager->createQuery("
                SELECT e
                FROM \CMS\Models\Error e
                WHERE e.message IN ( SELECT i.message FROM \CMS\Models\Error i WHERE i.id in (".implode(',' ,$ids).") )
            ")->getResult();            
        }
        return $errors;
    }
    
    static function errors_with_same_group($ids) {
        if (!is_array($ids)) $ids = array($ids);
        $errors = array();
        if (!empty($ids)) {
            $errors = self::$entityManager->createQuery("
                SELECT e
                FROM \CMS\Models\Error e
                WHERE e.group_name IN ( SELECT i.group_name FROM \CMS\Models\Error i WHERE i.id in (".implode(',' ,$ids).") )
            ")->getResult();            
        }
        return $errors;
    }
    
    static function store_to_database($errno, $errstr, $errfile, $errline, $debug_backtrace = array()) {
        $type = array_key_exists($errno, $errors = array(
            2047 => 'E_ALL',
            1024 => 'E_USER_NOTICE',
            512 => 'E_USER_WARNING',
            256 => 'E_USER_ERROR',
            128 => 'E_COMPILE_WARNING',
            64 => 'E_COMPILE_ERROR',
            32 => 'E_CORE_WARNING',
            16 => 'E_CORE_ERROR',
            8 => 'E_NOTICE',
            4 => 'E_PARSE',
            2 => 'E_WARNING',
            1 => 'E_ERROR'))
        ? $errors[$errno]
        : $errno;

        $message = $errstr;
        
        if(in_array($type,array(
            'EXCEPTION',
            'E_COMPILE_WARNING',
            'E_COMPILE_ERROR',
            'E_CORE_WARNING',
            'E_CORE_ERROR',
            'E_NOTICE',
            'E_PARSE',
            'E_WARNING',
            'E_ERROR'
        ))) {
            $message = "<b>[".$type."]</b> ".$message." in ".$errfile." line ".$errline."";
        }

        if($debug_backtrace !== false) {
            $debug_backtrace['server'] = $_SERVER;
            $debug_backtrace['post'] = $_POST;
            $debug_backtrace['files'] = $_FILES;
            $debug_backtrace['get'] = $_GET;
            if(\Session::isStarted()) {
                $debug_backtrace['session'] = $_SESSION;
            }
            
            $debug_backtrace = self::clean_trace($debug_backtrace);
            $debug_backtrace = json_encode($debug_backtrace);
        }
        
        $error = new self;
        $error->message = $message;
        $error->group_name = '';
        $error->date = new \DateTime("now");
        $error->type = $type;
        $error->debug_backtrace = $debug_backtrace;
        $error->ip = $_SERVER['REMOTE_ADDR'];
        if(!self::$entityManager->isOpen()) {
            \Bingo\Bingo::getInstance()->setupDoctrine();
        }
        $error->save();
    }

    static function clean_trace($branch){
        if(is_object($branch)){
            // object
            $objName = get_class($branch);
            if(strtolower($objName) == 'stdclass') {
                $vars = get_object_vars($branch);
                $branch = array();
                foreach($vars as $key => $value) {
                    $branch[$key] = self::clean_trace($value);
                }
                return $branch;
            } else {
                return $objName;
            }
        }elseif(is_array($branch)){
            // array
            foreach($branch as $i=>$v) {
                $branch[$i] = self::clean_trace($v);
            }
            return $branch;
        }elseif(is_resource($branch)){
            // resource
            $branch = (string)$branch.' ('.get_resource_type($branch).')';
            return $branch;
        }elseif(is_string($branch)){
            // string (ensure it is UTF-8, see: https://bugs.php.net/bug.php?id=47130)
            $branch = utf8_encode($branch);
            return $branch;
        } else {
            return $branch;
        }
    }
}