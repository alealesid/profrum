<?php

namespace CMS\Models;

/** 
* @Entity 
* @Table(name="cms_options")
*/
class Option extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /** @Id @Column(type="string") */
    public $id;
    /** @Column(type="object") */
    public $value;
    
    private static $cache = array();

    public static function clearCache() {
        Option::$cache = array();
    }
    
    public static function set($key,$val,$flush=true) {
        $opt = Option::find($key);
        if ($val!=null) {
            if (!$opt) {
                $opt = new Option; 
                $opt->id=$key;
            }    
            $opt->value = $val;
            $opt->save($flush);
            Option::$cache[$key] = $val;
        } else {
            if ($opt) {
                $opt->delete($flush);
            }
            unset(Option::$cache[$key]);
        }
    }
    
    public static function get($key,$cache=true) {
        if ($cache && isset(Option::$cache[$key])) {
            return Option::$cache[$key];
        } else {
            $opt = Option::find($key);
            if ($opt) {
                Option::$cache[$key] = $opt->value;
                return $opt->value;
            } else {
                unset(Option::$cache[$key]);
                return null;
            }
        }
    }    
}