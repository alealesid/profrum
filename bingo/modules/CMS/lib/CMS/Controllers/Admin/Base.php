<?php
namespace CMS\Controllers\Admin;

class Base extends \Bingo\Controller {
    function __construct() {
        parent::__construct();
        template_base('admin');
        bingo_domain('cms');
        $this->user = false;
    }

    function findOrCreate($class,$id) {
        $obj = false;
        if ($id) $obj = $class::find($id);
        if ($obj) return $obj;
        return new $class;
    }
    
    /**
     * @deprecated
     * @param $list
     * @param string $parent_field
     * @param string $title_field
     * @param bool $order_field
     * @param bool $forSelect
     * @param bool $selectInit
     * @return array
     */
    function treeSort($list,$parent_field='parent',$title_field='title',$order_field=false,$forSelect=false,$selectInit=false) {
        if (!function_exists('CMS\Controllers\Admin\tree_prep')) {
            function tree_prep($id) {
                $id = (string)$id;
                return str_repeat('-',6-strlen($id))."{$id}";
            }
        }
        foreach ($list as $g) { unset($g->tree_index); }
        $tree_idx = function($g) use (&$tree_idx,&$parent_field,&$order_field) {
            if (isset($g->tree_index)) {
                return $g->tree_index;
            } else {
                $order = $order_field ? tree_prep($g->$order_field) : '';
                if ($g->$parent_field==null) {
                    $g->tree_index = $order.tree_prep($g->id);
                    $g->tree_depth = 0;
                } else {
                    $g->$parent_field->getField('id');
                    $g->tree_index = $g->tree_depth = 0;
                    $parent_idx = $tree_idx($g->$parent_field);
                    $g->tree_index = $parent_idx.$order.tree_prep($g->id);
                    $g->tree_depth = $g->$parent_field->tree_depth+1;
                }
                return $g->tree_index;
            }
        };

        foreach ($list as $key=>$p) {
            $tree_idx($p);
            $p->tree_title = str_repeat('&emsp;',$p->tree_depth).' '.$p->$title_field;
        }
        usort($list,function($a,$b){return $a->tree_index>$b->tree_index;});
        if ($forSelect) return $this->selectOptions($list,'tree_title',$selectInit ?:array(_t('None','cms')=>0));
        return $list;
    }

    function selectOptions($object_list,$title_field,$init) {
        $ret = $init;
        foreach ($object_list as $obj) {
            $key = $obj->$title_field;
            while (isset($ret[$key])) $key .= " ";
            $ret[$key] = $obj;
        }
        return $ret;
    }

    function getPage() {
        if (isset($_GET['p'])) $page = (int)$_GET['p']; else $page = 1;if ($page<=1) $page = 1;
        return $page;
    }

    function action_delete($class,$redirect='') {
        $that = $this;
        return function ($list) use ($class,$redirect,$that) {
            $keys = array();
            foreach ($list as $key=>$data) {
                if (@$data['check']) $keys[] = $key;
            }
            if (!empty($keys)) {
                $qb = $that->em->createQueryBuilder();
                $qb->delete($class,"obj");
                $qb->andWhere($qb->expr()->in('obj.id',$keys));
                $qb->getQuery()->getResult();
            }

            if ($redirect!==false){
                if ($redirect==='') $redirect = $_SERVER['REQUEST_URI'];
                redirect(str_replace(base_url(),"",$redirect));
                die();
            }
        };
    }
    function action_edit($class,$redirect=false,$onlyChecked=true) {
        return function ($list,$action) use ($class,$redirect,$onlyChecked){
            $form = @$action['form'];
            foreach ($list as $id=>$data) {
                $data_list = isset($_POST['filter']) ? $_POST : $data;
                if ($onlyChecked==false || @$data['check']) {
                    $obj = $class::find($id);
                    if ($obj) {
                        if ($form) foreach ($form->elements as $el) {
                            $f = $el->name;
                            if ($el instanceof \Bingo\FormElement_Checkbox) {
                                $obj->$f = isset($data_list[$f]);
                            } else {
                                if (isset($_POST[$f])) {
                                    $obj->$f = $data_list[$f];
                                }
                            }
                        }
                        $obj->save(false);
                    }
                }
            }
            \Bingo\Bingo::getInstance()->em->flush();
            if ($redirect!==false){
                if ($redirect==='') $redirect = $_SERVER['REQUEST_URI'];
                redirect(str_replace(base_url(),"",$redirect));
                die();
            }
        };
    }

    function action_copy($class,$redirect=false) {
        return function ($list,$action) use ($class,$redirect){
            $form = @$action['form'];
            if ($form) $form->validate();
            foreach ($list as $id=>$data) {
                if (@$data['check']) {
                    $obj = $class::find($id);
                    if ($obj) {
                        $new = clone $obj;
                        if ($form) foreach ($form->elements as $el) {
                            $f = $el->name;
                            if ($el instanceof \Bingo\FormElement_Checkbox) {
                                $new->$f = isset($form->values[$f]);
                            } else {
                                if (isset($form->values[$f])) {
                                    $new->$f = $form->values[$f];
                                }
                            }
                        }
                        $new->save(false);
                    }
                }
            }
            \Bingo\Bingo::getInstance()->em->flush();
            if ($redirect!==false){
                if ($redirect==='') $redirect = $_SERVER['REQUEST_URI'];
                redirect(str_replace(base_url(),"",$redirect));
                die();
            }
        };
    }
}