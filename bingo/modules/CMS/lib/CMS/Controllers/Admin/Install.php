<?php

namespace CMS\Controllers\Admin;

class Install extends Base
{
    function index() {
        $this->data['form'] = false;
        if (\Bingo\Configuration::$applicationMode!="development") {
            $this->data['message'] =  "Switch to development mode, please";
        } else {
            $super_admin = \CMS\Configuration::$super_admin;
            if ($super_admin) {

                $admin = false;
                $user_class = \CMS\Configuration::$user_class;
                try {
                    $admin = $user_class::findOneBy(array("login"=>$super_admin));
                } catch (\Doctrine\DBAL\DBALException $e) {}

                if ($admin) {
                    $this->data['message'] =
                            "Super admin alredy exists. Use Schema Update to update database. Not install.";
                } else {
                    $form = new \Bingo\Form;
                    $form->password("password","Please enter super admin password");
                    $form->submit("Set Password");

                    if ($form->validate()) {
                        $data = $form->getValue();
                        
                        $schema = new Schema();
                        $schema->schema_update(true);

                        $admin = new $user_class;
                        $admin->login = $super_admin;
                        $admin->email = "";
                        $admin->secret = "";
                        $admin->password = md5($data['password']);
                        $admin->save();

                        $this->data['message'] = "Superadmin created successfully";

                    } else {
                        $this->data['form'] = $form->get();
                    }
                }
            } else {
                $this->data['message'] = "No SUPER_ADMIN defined";
            }
        }

        $this->data['disable_menu'] = true;
        $this->data['title'] = _t('Install');
        $this->view('cms/base-edit');
    }
}