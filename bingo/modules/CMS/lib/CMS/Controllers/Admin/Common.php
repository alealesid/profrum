<?php
namespace CMS\Controllers\Admin;

class Common extends BasePrivate {
    function index() {
        $this->data['title'] = _t("Admin panel");
        $this->data['message'] = _t("Welcome to admin panel");

        template_base('admin');
        $this->view('cms/base');
    }

    function files() {
        $config = array();
        $config['_tinyMCEPath'] = t_url('cms/script/tiny_mce');
        \FileManager::view('upload/CMS',$config);
    }
}