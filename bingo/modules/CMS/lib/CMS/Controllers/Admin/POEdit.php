<?php

namespace CMS\Controllers\Admin;

class POEdit extends BasePrivate
{
    function get_po($domain,$locale) {
        global $domain_files;
        $po = $domain_files[$domain]."/$locale/{$domain}.po";
        if (file_exists($po)) return realpath($po);
        return false;
    }
    
    function po_list() {
        global $domain_files;
        
        $list = array();
        foreach ($domain_files as $domain=>$dir) {
            foreach (glob($dir."/*") as $locale_dir) {
                $locale = basename($locale_dir);
                if ($this->get_po($domain,$locale)) {
                    $list[] = (object)array(
                        'id' => $domain."___".$locale,
                        'domain' => $domain,
                        'locale' => $locale
                    );
                }
            }
        }
        
        $this->data['page_actions']['admin/po-create'] = _t('Create New');
        
        $this->data['list'] = $list;
        $this->data['title'] = _t("PO file list");
        $this->data['item_actions']['admin/po-edit'] = _t('edit');
        $this->data['fields'] = array('domain'=>_t('domain'),'locale'=>_t('locale'));
        $this->view('cms/base-list',$this->data);
    }
    
    function po_create() {
        $form = new \Form;
        
        global $domain_files;
        $domain_options = array();
        foreach ($domain_files as $domain=>$dir) {
            $domain_options[$domain] = $domain;
        }
        
        $form->select('domain',_t('Domain'),$domain_options,'');
        $form->text('locale',_t('Locale'),'required');
        $form->submit(_t('Create PO File'));
        
        if ($form->validate()) {
            $domain = $form->values['domain'];
            $locale = $form->values['locale'];
            $dir = $domain_files[$domain];
            
            $locale_dir = $dir."/$locale";
            $po = $locale_dir."/$domain.po";
            
            if (!file_exists($locale_dir)) mkdir($locale_dir);
            file_put_contents($po,"");
            redirect("admin/po-edit/{$domain}___{$locale}");
        }
        
        $this->data['title'] = _t('Create PO File');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit');
    }
    
    function po_edit($id) {
        list($domain,$locale) = explode("___",$id);
        
        $po = $this->get_po($domain,$locale);
        if (!$po) return;
        
        $poParser = new \Sepia\PoParser();
        $poParser->parseFile($po);
        
        $entries = &$poParser->entries;
        
        // parse headers from empty msgid as PoParser fails to do that
        $headers = $poParser->getHeaders()?:array();
        if (empty($headers) && isset($entries[""])) {
            $str = $entries[""]["msgstr"];
            unset($str[0]);
            unset($entries[""]);
            foreach ($str as $s) $headers[] = '"'.$s.'"';
        }
        
        // exclude X-Poedit-SearchPath from headers and save it into folders
        $folders = array();
        foreach ($headers as $h=>$header) {
            $needle = '"X-Poedit-SearchPath';
            if (substr($header, 0, strlen($needle)) === $needle) {
                list($key,$val) = explode(":",$header,2);
                $folders[] = str_replace("\\n","",str_replace('"',"",$val));
                unset($headers[$h]);
            }
        }
        $headers = array_values($headers);
        
        // override $folders is $_POST['folders'] exists
        if (isset($_POST['folders'])) {
            $folders = explode("\n",$_POST['folders']);
        }
        foreach ($folders as $f=>$folder) $folders[$f] = trim($folder);
        
        // exclude non-existing files from folder
        foreach ($folders as $key=>$folder) {
            if (!$folder || !file_exists($folder)) unset($folders[$key]);
        }
        
        // if folders are empty add default value
        if (empty($folders)) {
            $folders[] = dirname(dirname(dirname($po)))."/lib";
        }
        
        // write back folders to headers array
        foreach (array_values($folders) as $f=>$folder) {
            $headers[] = "\"X-Poedit-SearchPath-$f:$folder\"";
        }

        
        $form = new \Form;
        
        $form->fieldset(_t('Update strings'));
        $form->textarea('folders',_t('Search Folder'),'',implode("\n",$folders));
        $form->submit(_t('Scan for translations'),'scan',1);
        
        // if scan button pressed
        if (isset($_POST['scan'])) {
            
            $old_entries = $entries;
            $entries = array();
            
            foreach ($folders as $f=>$folder) {
                $folder = trim($folder);
                if (!file_exists($folder)) {
                    unset($folders[$f]);
                } else {
                    // scan file for _t occurences
                    $Directory = new \RecursiveDirectoryIterator($folder);
                    $Iterator = new \RecursiveIteratorIterator($Directory);
                    $Regex = new \RegexIterator($Iterator, '/^.+\.php$/i', \RecursiveRegexIterator::GET_MATCH);

                    $lines = array();
                    foreach ($Regex as $path) {
                        $path = $path[0];
                        
                        $handle =  fopen($path, "r");
                        if ($handle) {
                            $lineno = 0;
                            while (($text = fgets($handle)) !== false) {
                                $lineno++;
                                
                                preg_match_all('/_t\(("([^"]+)"|\'([^\']+)\')/',$text,$matches);
                                if (!empty($matches[0])) {
                                    foreach ($matches[2] as $i=>$line) {
                                        if (!$line) $line = $matches[3][$i];
                                        if ($line) {
                                            if (!isset($lines[$line])) $lines[$line] = array();
                                            $lines[$line][] = $path.":".$lineno;
                                        }
                                    }
                                }
                            }
                        }
                        fclose($handle);
                    }
                    
                    foreach ($lines as $mid=>$ref) {
                        $msg = "";
                        if (isset($old_entries[$mid])) {
                            $msg = implode("\n",$old_entries[$mid]['msgstr']);
                        }
                        $poParser->updateEntry($mid,$msg,array(),array(),array(), $createNew = true);
                        $entries[$mid]['reference'] = $ref;
                    }
                }
            }
            
            foreach ($old_entries as $msgid=>$entry) {
                if (!isset($entries[$msgid])) {
                    $entry['obsolete'] = 1;
                    $entries[$msgid] = $entry;
                }
            }
            
            if (is_writable($po)) {
                $poParser->setHeaders($headers);
                $poParser->writeFile($po);
                set_flash('info',_t('Scan successfully completed'));
            } else {
                set_flash('info',_t('File is write protected'));
            }
            
            redirect('admin/po-edit/'.$id);
            return;
        }
        
        $form->fieldset(_t('Strings'));
        
        
        $fields = array(0=>array(),1=>array());
        foreach ($entries as $one) {
            $msgid = @$one['msgid'][0];
            $msgstr = implode("\n",$one['msgstr']);
            $obsolete = isset($one['obsolete']) && $one['obsolete'];
            
            if ($msgid && !$obsolete) {
                $fields[$msgstr?1:0][$msgid] = $msgstr;
            }
        }
        
        $i=0;
        foreach (array_merge($fields[0],$fields[1]) as $msgid=>$msgstr) {
            $form->hidden("msg[$i][id]",$msgid);
            $form->textarea("msg[$i][str]",$msgid,'',$msgstr);
            $i++;
        }
        
        $form->fieldset();
        $form->submit(_t('Save PO File'))->addClass('single');
        
        // general save pressed
        if ($form->validate()) {
            $messages = @$_POST['msg']?:array();
            
            foreach ($messages as $one) {
                $poParser->updateEntry($one['id'],$one['str']);
            }
            
            if (is_writable($po)) {
                $poParser->setHeaders($headers);
                $poParser->writeFile($po);
                set_flash('info',_t('Successfully saved'));
            } else {
                set_flash('info',_t('File is write protected'));
            }
            redirect('admin/po-edit/'.$id);
        }
        
        $this->data['title'] = _t('Edit PO File');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit');
    }
}