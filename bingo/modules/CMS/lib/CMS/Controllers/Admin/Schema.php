<?php
namespace CMS\Controllers\Admin;

use Doctrine\ORM\Tools\Console\MetadataFilter;

class Schema extends Base {
    function schema_update($silent=false) {
        if (\Bingo\Configuration::$applicationMode!='development') return true;
        
        if (!$silent) {
            $user_class = \CMS\Configuration::$user_class;
            $this->user = $user_class::checkLoggedIn();
            if (!$this->user) redirect('admin/login');
        }
        
        $this->data['title'] = _t('Schema');
        $this->em = $em = \Bingo\Bingo::getInstance()->em;

        $tool = new \Doctrine\ORM\Tools\SchemaTool($this->em);

        $metadatas = $em->getMetadataFactory()->getAllMetadata();
        $sm = $em->getConnection()->getSchemaManager();
        $platform = $em->getConnection()->getDatabasePlatform();

        $sequences = array();
        if($platform->supportsSequences()) {
            $sequences = $sm->listSequences();
        }

        $tableNames = $sm->listTableNames();
        foreach ($tableNames as $key=>$name) {
            foreach (\CMS\Configuration::$ignored_tables as $regex) {
                if (preg_match($regex,$name)) {
                    unset($tableNames[$key]);
                    break;
                }
            }
        }
        $tables = array();
        foreach ($tableNames AS $tableName) {
            $tables[] = $sm->listTableDetails($tableName);
        }

        $fromSchema = new \Doctrine\DBAL\Schema\Schema($tables, $sequences, $sm->createSchemaConfig());
        $toSchema = $tool->getSchemaFromMetadata($metadatas);

        $comparator = new \Doctrine\DBAL\Schema\Comparator();
        $schemaDiff = $comparator->compare($fromSchema, $toSchema);

        $updateSchemaSql = $schemaDiff->toSql($platform);

        $conn = $this->em->getConnection();
        foreach ($updateSchemaSql as $sql) {
            $conn->executeQuery($sql);
        }

        if ($silent) return;

        $this->data['title'] = _t('Schema update');
        $this->data['message'] = _t("Schema successfully updated");
        $this->view('cms/base');
    }
    
    function generate_proxies() {
        $user_class = \CMS\Configuration::$user_class;
        $this->user = $user_class::checkLoggedIn();
        if (!$this->user) redirect('admin/login');
        
        $em = $this->em;
        $metadatas = $em->getMetadataFactory()->getAllMetadata();
        $destPath = $em->getConfiguration()->getProxyDir();

        if (!is_dir($destPath)) {
            mkdir($destPath, 0777, true);
        }
        $destPath = realpath($destPath);
        $classList = "";
        if (count($metadatas)) {
            foreach ($metadatas as $metadata) {
                $classList .= "<br>".$metadata->name;
            }            
            $em->getProxyFactory()->generateProxyClasses($metadatas, $destPath);
        }
        
        $this->data['title'] = _t('Generate proxies');
        $this->data['message'] = _t("Proxies were successfully generated for ".count($metadatas)." class(es)<br>".$classList);
        $this->view('cms/base');        
    }
}