<?php
namespace CMS\Controllers\Admin;

class Auth extends Base {
    function __call($action,$args) {
        $parent = new \Auth\Controllers\Auth(\CMS\Configuration::$user_class,'admin');
        $parent->view_config(array('login'=>'cms/base-edit'));
        $parent->data['disable_menu'] = true;
        $parent->$action();
    }
    function logout() {
        $user_class = \CMS\Configuration::$user_class;
        $this->user = $user_class::checkLoggedIn();
        if ($this->user) $this->user->logout();
        redirect('admin/login');
    }
}
