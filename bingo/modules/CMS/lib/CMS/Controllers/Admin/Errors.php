<?php

namespace CMS\Controllers\Admin;

class Errors extends BasePrivate {
    function error_list(){
        ini_set("memory_limit", "4000M");
        $this->data['fields'] = array(
            'date' => _t('date'),
            'group_name' => _t('group'),
            'message' => _t('message'),
            'ip' => _t('ip'),
            'details' => _t('details')
        );
        
        $res = $this->em->createQuery("
            SELECT DISTINCT(e.group_name) as group_name
            FROM \CMS\Models\Error e
        ")->getResult();
        
        $group_options = array(_t('All')=>'',_t('Empty group')=>'_EMPTY_');
        $group_options_set = array(_t('Empty group')=>'_EMPTY_');
        foreach ($res as $one) {
            if ($one['group_name']) {
                $group_options[$one['group_name']] = $one['group_name'];
                $group_options_set[$one['group_name']] = $one['group_name'];
            }
        }
        if (@$_GET['group_name'] && $_GET['group_name']!='_EMPTY_')
            $group_options[$_GET['group_name']] = $_GET['group_name'];
        
        
        $filter_form = new \CMS\FilterForm;
        $filter_form->select('group_name','',$group_options);
        $this->data['filter_form'] = $filter_form;
        $this->data['filter_autosubmit'] = true;
        
        $this->data['field_filters']['details'] = function ($val,$obj){
            return "<a target='blank' href='".url('admin/error-view/'.$obj->id)."'>"._t('details','cms')."</a>";
        };
        
        $criteria = array();
        if ($filter_form->validate()) {
            $group_name = $filter_form->values['group_name'];
            if ($group_name) {
                $criteria['group_name'] = ($group_name=='_EMPTY_') ? '' : $group_name;
            }
        }
        
        $query = \CMS\Models\Error::findByQuery($criteria, 'date DESC');
        $pagination = new \Bingo\Pagination(20,$this->getPage(),false,false,$query); 
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        
        
        $this->data['field_filters']['date'] = function($val, $obj) {
            if (!$val) return '';
            return $val->format("d.m.Y H:i:s");
        };
        
        $this->data['page_actions']['admin/clear-errors'] = _t('Delete all');
        $this->data['list_actions']['group'] = array(
            'title' => _t('Set group'),
            'form' => \Form::create()
                ->select('group_select','',$group_options_set)
                ->text('group_name','')
            ,
            'function' => function ($list) {
                foreach ($list as $one) {
                    if (@$one['check']) {
                        $obj = \CMS\Models\Error::find($one['id']);
                        $obj->group_name = $_POST['group_name'] ?: $_POST['group_select'];
                        $obj->save(false);
                    }
                }
                \Bingo::$em->flush();
                redirect(str_replace(base_url(),"",$_SERVER['REQUEST_URI']));
            }
        );
        
        $that = $this;
        $this->data['list_actions']['group_same_message'] = array(
            'title' => _t('Set group with same message as checked'),
            'form' => \Form::create()
                ->select('group_select_2','',$group_options_set)
                ->text('group_name_2','')
            ,
            'function' => function ($list) use ($that) {
                $ids = $that->getSelectedIds($list);
                if (!empty($ids)) {
                    $errors = \CMS\Models\Error::errors_with_same_message($ids);
                    foreach ($errors as $one) {
                        $one->group_name = $_POST['group_name_2'] ?: $_POST['group_select_2'];
                        $one->save(false);
                    }
                    $that->em->flush();
                }
                redirect(str_replace(base_url(),"",$_SERVER['REQUEST_URI']));
            }
        );
        
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete selected'),
            'function' => $this->action_delete('\CMS\Models\Error')
        );
        $controller = $this;
        $this->data['list_actions']['del_same_message'] = array(
            'title' => _t('Delete all with same message as checked'),
            'function' => function($list) use ($controller) { 
                $controller->delete_same_message_errors($list); 
                redirect(str_replace(base_url(),"",$_SERVER['REQUEST_URI']));
            }
        );
        $this->data['list_actions']['del_same_group'] = array(
            'title' => _t('Delete all with same group as checked'),
            'function' => function($list) use ($controller) { 
                $controller->delete_same_group_errors($list); 
                redirect(str_replace(base_url(),"",$_SERVER['REQUEST_URI']));
            }
        );
        
        
        $this->data['list_actions']['export'] = array(
            'title' => _t('Export checked'),
            'function' => function($list) use ($controller) { 
                $controller->iframe_download(
                    'admin/export-errors?'.http_build_query($_POST)
                );
            }
        );
        $this->data['list_actions']['export_same_message'] = array(
            'title' => _t('Export all with same message as checked'),
            'function' => function($list) use ($controller) { 
                $controller->iframe_download(
                    'admin/export-same-message-errors?'.http_build_query($_POST)
                );
            }
        );        
        $this->data['list_actions']['export_same_group'] = array(
            'title' => _t('Export all with same group as checked'),
            'function' => function($list) use ($controller) { 
                $controller->iframe_download(
                    'admin/export-same-group-errors?'.http_build_query($_POST)
                );
            }
        );        
        
        $this->data['content_class'] = 'Error';
        $this->data['title'] = _t('Error log');
        $this->view('cms/base-list');
    }
    
    function iframe_download($url) {
        // invisible iframe for download
        ?>
            <iframe src="<?=url($url)?>" style="position:absolute;z-index:-1;opacity:0">
            </iframe>
        <?
    }
    
    function delete_same_message_errors($list) {
        $ids = $this->getSelectedIds($list);
        if (!empty($ids)) {
            $errors = \CMS\Models\Error::errors_with_same_message($ids);
            foreach ($errors as $one) 
                $one->delete(false);
            $this->em->flush();
        }
    }
    
    function delete_same_group_errors($list) {
        $ids = $this->getSelectedIds($list);
        if (!empty($ids)) {
            $errors = \CMS\Models\Error::errors_with_same_group($ids);
            foreach ($errors as $one) 
                $one->delete(false);
            $this->em->flush();
        }
    }    
    
    function export_errors() {
        $list = $_GET['list'];
        $ids = $this->getSelectedIds($list);
        if (!empty($ids)) {
            $errors = \CMS\Models\Error::findBy(array('id' => $ids), 'date DESC');
            $this->output_as_html($errors);
        }
    }
    
    function error_view($id) {
        $errors = \CMS\Models\Error::findBy(array('id'=>$id));
        $this->output_as_html($errors,$download=false);
    }
    
    function export_same_message_errors(){
        $list = $_GET['list'];
        $ids = $this->getSelectedIds($list);
        if (!empty($ids)) {
            $errors = \CMS\Models\Error::errors_with_same_message($ids);
            $this->output_as_html($errors);
        }
    }
    
    function export_same_group_errors(){
        $list = $_GET['list'];
        $ids = $this->getSelectedIds($list);
        if (!empty($ids)) {
            $errors = \CMS\Models\Error::errors_with_same_group($ids);
            $this->output_as_html($errors);
        }
    }
    
    
    function getSelectedIds($list) {
        $ids = array();
        foreach ($list as $one) {
            if (isset($one['check']))
                $ids[] = $one['id'];
        }
        return $ids;
    }    
    
    function output_csv_errors($errors) {
        $csv = '';
        foreach ($errors as $one) {
            $csv .= $one->date->format('d.m.Y H:i:s') . ';';
            $csv .= $one->message . ';';
            $csv .= json_encode($one->debug_backtrace) . ';';
            $csv .= $one->ip . ';';
            $csv .= $one->username . ';';
            $csv .= $one->user_agent . ";\r\n";
        }
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for i.e.
        header("Content-Type: text/csv");
        header("Content-Transfer-Encoding: Text");
        header("Content-Length:".strlen($csv));
        header("Content-Disposition: attachment; filename=".date('Y-m-d_H-i-s')."errors.csv");
        echo $csv;
    }
    
    function output_as_html($errors,$download=true) {
        $data = array('errors' => $errors);
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public"); // needed for i.e.
        header("Content-Type: text");
        header("Content-Transfer-Encoding: Text");
        if ($download) {
            header("Content-Disposition: attachment; filename=".date('Y-m-d_H-i-s')."errors.htm");
        }
        
        ?>
            <head>
            <meta charset="utf-8" />
            <style>
                table {
                    width: 100%;
                    border-collapse: collapse;
                }
                td {
                    padding: 5px;
                    border: 1px solid #eee;
                }

                pre {
                    white-space: pre-wrap;
                    display: block;
                }

            </style>
            </head>
            <body>
                <table>
                    <? foreach($errors as $one): ?>
                        <tr>
                            <td><?=$one->date->format('d.m.Y H:i:s')?></td>
                            <td><?=$one->message?></td>
                            <td><?=$one->ip?></td>
                            <td><?=$one->username?></td>
                            <td><?=$one->user_agent?></td>
                        </tr>
                    <tr>
                        <td colspan="5"><pre><?=htmlspecialchars(print_r(json_decode($one->debug_backtrace), true))?></pre></td>            
                    </tr>
                    <? endforeach ?>
                </table>
            </body>
        <?
    }
    
    function clear_errors(){
        $this->em->createQuery("DELETE FROM \CMS\Models\Error")->getResult();
        redirect('admin/error-list');
    }
}