<?php
namespace CMS\Controllers\Admin;

class BasePrivate extends Base
{
    function __construct() {
        parent::__construct();
        $user_class = \CMS\Configuration::$user_class;
        $this->user = $user_class::checkLoggedIn();
        if (!$this->user) { redirect('admin/login?redirect='.\Bingo\Bingo::getInstance()->request); die(); }
        $redirect = \Bingo\Action::filter('admin_check',array(false,$this->user));
        if ($redirect!==false) redirect($redirect);
    }
}
