<?php

namespace CMS;

class Configuration {
    static public $super_admin = 'admin';
    static public $user_class = '\CMS\Models\User';
    static public $theme_path = "themes";
    static public $ignored_tables = array();
    static public $log_errors = false;
}