<?php

namespace CMS;

class FilterForm extends \Bingo\Form {
    static function create($method="get",$atts=array()) {
        return new FilterForm();
    }
    function __construct() {
        parent::__construct("GET",array('autocomplete'=>'off'));
    }
    function validate() {
        foreach ($this->elements as $el) {
            $name = $el->name;
            if ($name && isset($_GET[$name])) 
                return parent::validate();
        }
        return false;
    }
}