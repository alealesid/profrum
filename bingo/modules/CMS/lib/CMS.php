<?php

include "helpers/theme.php";

class Admin {
    public static $menu = array();
}

class CMS extends \Bingo\Module {
    
    static $shortcodes = array();
    
    static function add_shortcode($key,$f) {
        self::$shortcodes[$key] = $f;
    }
    
    static function do_shortcode($content,&$args=null) {
        $shortcode_tags = CMS::$shortcodes;
        $tagnames = array_keys($shortcode_tags);
        $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
        $pattern = '(.?)\[('.$tagregexp.')\b(.*?)(?:(\/))?\](?:(.+?)\[\/\2\])?(.?)';
        return preg_replace_callback('/'.$pattern.'/s', function ($n) use (&$args) {
            $shortcode_tags = CMS::$shortcodes;
            if ( $n[1] == '[' && $n[6] == ']' ) return substr($n[0], 1, -1);
            $tag = $n[2];
            $text = $n[3];
            
            $atts = array();
            $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
            $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
            if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
                foreach ($match as $m) {
                    if (!empty($m[1]))
                        $atts[strtolower($m[1])] = stripcslashes($m[2]);
                    elseif (!empty($m[3]))
                        $atts[strtolower($m[3])] = stripcslashes($m[4]);
                    elseif (!empty($m[5]))
                        $atts[strtolower($m[5])] = stripcslashes($m[6]);
                    elseif (isset($m[7]) and strlen($m[7]))
                        $atts[] = stripcslashes($m[7]);
                    elseif (isset($m[8]))
                        $atts[] = stripcslashes($m[8]);
                }
            } else {
                $atts = ltrim($text);
            }

            if ( isset( $n[5] ) ) {
                return $n[1] . call_user_func( $shortcode_tags[$tag], $atts, $n[5], $tag, $args ) . $n[6];
            } else {
                return $n[1] . call_user_func( $shortcode_tags[$tag], $atts, NULL,  $tag, $args ) . $n[6];
            }
        }, $content);
    }        

    function __construct() {
        bingo_domain_register('cms',dirname(__FILE__)."/../locale");
        $this->addModelPath(dirname(__FILE__)."/CMS/Models",true);
        $this->initRouting();
        $this->initAdminMenu();
        
        \Bingo\Action::add('global_error',function($errno, $errstr, $errfile, $errline,$trace){
            if (\CMS\Configuration::$log_errors) {
                \CMS\Models\Error::store_to_database($errno, $errstr, $errfile, $errline, $trace);
            }
        },10,10);

        \Bingo\Action::add('global_exception',function($e){
            if ($e instanceof PDOException) {
                if ($e->getCode()=='42S02' && strpos($e->getMessage(),'cms_options')!==false) {
                    redirect('install');
                }
            }
        });
    }

    function initRouting() {
        $this->connect('install',
            array('controller'=>'\CMS\Controllers\Admin\Install','action'=>'index'));
        $this->connect('admin/:action',
            array('controller'=>'\CMS\Controllers\Admin\Common','action'=>'index'),
            array('action'=>'(index)'));
        $this->connect('admin/:action',
            array('controller'=>'\CMS\Controllers\Admin\Schema','priority'=>-1001),
            array('action'=>'(schema-update|generate-proxies)'));
        $this->connect('admin/files/browse.php',
            array('controller'=>'\CMS\Controllers\Admin\Common','data'=>false,'action'=>'files'));
        $this->connect('admin/:action',
            array('controller'=>'\CMS\Controllers\Admin\Auth'),
            array('action'=>'(login|logout)'));
        $this->connect('admin/:action/:data',
            array('controller'=>'\CMS\Controllers\Admin\Users','data'=>false),
            array('action'=>'(user-list|user-edit|user-profile)'));
        $this->connect('admin/:action/:data',
            array('controller'=>'\CMS\Controllers\Admin\POEdit','data'=>false),
            array('action'=>'(po-list|po-edit|po-create)'));
        $this->connect('admin/:action/:data',
            array('controller'=>'\CMS\Controllers\Admin\Errors','data'=>false),
            array('action'=>'(error-list|error-view|clear-errors|export-errors|export-same-message-errors|export-same-group-errors)'));
    }

    function initAdminMenu() {
        \Bingo\Action::add('admin_pre_header',
        function () {
            $user = \Bingo\Routing::$controller->user;
            if ($user) {
                $applicationMode = \Bingo\Configuration::$applicationMode;
                if ($applicationMode=="development") {
                    \Admin::$menu[_t('Development','cms')][_t('Schema update','cms')] = 'admin/schema-update';
                    \Admin::$menu[_t('Development','cms')][_t('Generate proxies','cms')] = 'admin/generate-proxies';
                    \Admin::$menu[_t('Development','cms')][_t('Translations','cms')] = 'admin/po-list';
                }
                \Admin::$menu[_t('System','cms')][_t('Users','cms')] = 'admin/user-list';
                if (\CMS\Configuration::$log_errors) {
                    \Admin::$menu[_t('System','cms')][_t('Errors','cms')] = 'admin/error-list';
                }
            }
        });
    }
}
