<?php

function theme_base() {
	$theme = false;
    if (count(\Bingo\Configuration::$connections))
        $theme = \CMS\Models\Option::get('theme');
    if (!$theme) $theme = "default";
    template_base(\CMS\Configuration::$theme_path."/".$theme);
}
