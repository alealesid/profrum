<?php

class Meta extends \Bingo\Module {
    function __construct() {
        $this->addModelPath(dirname(__FILE__).'/Meta/Models');
    }
};