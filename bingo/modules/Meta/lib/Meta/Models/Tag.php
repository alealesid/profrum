<?php

namespace Meta\Models;

/**
 * @Entity
 * @Table(name="meta_tags")
 */
class Tag extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /**
     * @Column(type="integer")
     */
    public $owner_id;
    /** @Column(type="string") */
    public $type;
    /** @Column(type="string") */
    public $value;
    
    static function getTags($owner,$type=false) {
        if (is_array($owner)) {
            $owners = $owner;
            if (!count($owners)) return false;
            if (!$type) $type = get_class($owners[0]);
            
            $ids = array();
            $by_id = array();
            $all_tags = array();
            foreach ($owners as $owner) {
                $ids[] = $owner->id;
                $by_id[$owner->id] = $owner;
            }
            $tags = \Meta\Models\Tag::findBy(array('owner_id'=>$ids,'type'=>$type),'value');
            foreach ($tags as $tag) {
                $owner = $by_id[$tag->owner_id];
                if (!@$owner->_tags) $owner->_tags = array();
                $owner->_tags[] = $tag;
                $all_tags[$tag->value] = 1;
            }
            return array_keys($all_tags);
        } else {
            if ($owner) {
                if (!$type) $type = get_class($owner);
                $type = get_class($owner);
                if (!@$owner->_tags) {
                    $owner->_tags = \Meta\Models\Tag::findBy(array('owner_id'=>$owner->id,'type'=>$type),'value');
                }
                return array_map(function($t){return $t->value;},$owner->_tags);
            } else {
                if (!$type) return false;
                $res = \Meta\Models\Tag::findBy(array('type'=>$type),'value');
                return array_values(array_unique(array_map(function($t){return $t->value;},$res)));
            }
        }
    }

    static function setTags($owner,$tags,$flush=true,$type=false) {
        if (!$type) $type = get_class($owner);
        
        $old = $owner->getTags();
        $new = array();
        // delete removed
        foreach ($owner->_tags as $key=>$tag) {
            if (!in_array($tag->value,$tags)) {
                $tag->delete(false);
                unset($owner->_tags[$key]);
            }
        }
        // add new
        foreach ($tags as $tag_s) {
            if (!in_array($tag_s,$old)) {
                $new = new \Meta\Models\Tag();
                $new->type = $type;
                $new->owner_id = $owner->id;
                $new->value = $tag_s;
                $new->save(false);
                $owner->_tags[] = $new;
            }
        }
        if ($flush) \Bingo\Bingo::getInstance()->em->flush();
    }
}