<?php

class Forms extends \Bingo\Module {
    function __construct() {
        bingo_domain_register('forms',dirname(__FILE__)."/../locale");
        bingo_domain('forms');

        $this->addModelPath(dirname(__FILE__).'/Forms/Models');

        $this->connect('admin/:action/:data',
            array('controller'=>'\Forms\Controllers\Admin','data'=>false),
            array('action'=>'(form-list|form-edit|form-track-list|form-track-edit)')
        );

        \Bingo\Action::add('admin_pre_header',function () {
                \Admin::$menu[_t('Forms','forms')][_t('Forms','forms')] = 'admin/form-list';
                \Admin::$menu[_t('Forms','forms')][_t('Tracking','forms')] = 'admin/form-track-list';
        });
    }
}



