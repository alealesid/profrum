<?php

namespace Forms\Models;

/**
* @Entity
* @Table(name="forms_tracking")
*/
class Track extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(type="array") */
    public $data;
    /** @Column(type="datetime") */
    public $date;
    /**
     * @ManyToOne(targetEntity="Form")
     * @JoinColumn(name="form_id", referencedColumnName="id",onDelete="SET NULL")
     */
    public $form;

    function __construct() {
        $this->date = new \DateTime();
        $this->data = array();
    }

    function fill($form,$form_model) {
        $this->data['values'] = array();
        $this->data['elements'] = $form_model->elements;
        foreach ($form->values as $key=>$val) {
            $el = $form->findElement($key);
            $this->data['values'][$key] = $val;
        }
        foreach ($form->elements as $el) {
            if ($el instanceof \Bingo\FormElement_File) {
                if (isset($_FILES[$el->name])) {
                    $file_id = uniqid();
                    $path = INDEX_DIR.'/upload/Forms/'. $file_id;
                    if (move_uploaded_file($_FILES[$el->name]['tmp_name'],$path)) {
                        $this->data['values'][$el->name] = array(
                            'id' => $file_id,
                            'name' => basename($_FILES[$el->name]['name'])
                        );
                    }
                }
            }
        }
    }
}