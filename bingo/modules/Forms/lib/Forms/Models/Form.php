<?php

namespace Forms\Models;

/**
* @Entity
* @Table(name="forms")
*/
class Form extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(type="array") */
    public $elements;
    /** @Column(length="100") */
    public $title;
    /** @Column(length="100") */
    public $redirect;
    /** @Column(length="100") */
    public $email;
    /** @Column(length="100") */
    public $required_text;

    function __construct() {
        $this->required_text = " *";
        $this->email = "";
    }


    static function get_element_types() {
        return array(
            _t("Single line of text") => 'text',
            _t("Multiple lines of text") => 'textarea',
            _t("Combo box select") => 'select',
            _t("Checkbox") => 'checkbox',
            _t("New fieldset") => 'fieldset',
            _t("End fieldset") => 'close_fieldset',
            _t("Text only (no input)") => 'text_only',
            _t("Submit button") => 'submit'
        );
    }

    function get_form() {
        $f = new \Bingo\Form;
        $i = 0;
        foreach ($this->elements as $el) {
            $data = explode('#',$el['label']);
            $label = \array_shift($data);
            $required = @$el['required'];
            $rules = '';

            if ($required) {
                $label .= "<em>".$this->required_text."</em>";
                $rules = 'required';
            }
            
            $i++;

            switch ($el['type']) {
                case 'text':
                    $f->text("el$i",$label,$rules);
                    break;
                case 'textarea':
                    $f->textarea("el$i",$label,$rules);
                    break;
                case 'select':
                    $f->select("el$i",$label,array_flip($data),$rules);
                    break;
                case 'checkbox':
                    $f->checkbox("el$i",$label,$rules);
                    break;
                case 'fieldset':
                    $f->fieldset($label);
                    break;
                case 'close_fieldset':
                    $f->close_fieldset();
                    break;
                case 'text_only':
                    $f->html("<p>$label</p>");
                    break;
                case 'submit':
                    $f->html("<div class='button_holder'>");
                    $f->submit($label);
                    $f->html("</div>");
                    break;
            }
        }
        $f->hidden('form_id',$this->id);
        return $f;
    }

    function get() {
        $form = $this->get_form();
        if (@$_POST['form_id']==$this->id)
            if ($form->validate()) {
                $track = new Track();
                $track->fill($form,$this);
                $track->form = $this;
                $track->save();

                $redirect = $this->redirect ?:$_SERVER['REQUEST_URI'];
                set_flash('info',_t('Successfully sent','forms'));
                set_flash('form_id',$this->id);
                redirect(str_replace(base_url(),"",$redirect));
                die();
            }
        return $form->get();
    }
}
