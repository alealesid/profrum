<?php

namespace Forms\Controllers;
use \Bingo\FormFilter_DoctrineObject as ObjectFilter;

class Admin extends \CMS\Controllers\Admin\BasePrivate
{
    function __construct() {
        parent::__construct();
        bingo_domain('forms');
    }

    function form_list() {
        $this->data['title'] = _t('Form list');

        $this->data['list'] = \Forms\Models\Form::findAll();

        $this->data['page_actions']['admin/form-edit'] = _t('Create new');
        $this->data['item_actions']['admin/form-edit'] = _t('edit');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Forms\Models\Form','admin/form-list')
        );
        $this->data['fields'] = array('id'=>_t('#'),'title'=>_t('title'));
        $this->view('cms/base-list',$this->data);
    }

    function form_edit($id) {
        $data = $this->findOrCreate('\Forms\Models\Form',$id);

        $elTypes = \Forms\Models\Form::get_element_types();
        $elForm = new \Bingo\Form;
        $elForm
            ->html("<div class='drag_helper'></div>")
            ->text('label',_t('Label'))
            ->select('type',_t('Type'),$elTypes)
            ->checkbox('required',_t('Required'));

        $form = new \Bingo\Form;
        $form
            ->fieldset()
            ->text('title',_t('Title'),'',$data->title)
            //->text('email',_t('E-mail to send form data'),'',$data->email)
            ->text('redirect',_t('Redirect after'),'',$data->redirect)
            ->fieldset(_t('Texts'))
            ->text('required_text',_t('"Required" mark in field label'),'',$data->required_text)
            ->fieldset(_t("Elements"))
            ->form_list('elements',$elForm,$data->elements,true)
            ->fieldset()
            ->submit(_t('Save form'))->add_class("single");

        if ($form->validate()) {
            $form->fill($data);
            $data->save(true);
            return redirect("admin/form-list");
        }

        $this->data['title'] = _t('Edit form');
        $this->data['form'] = $form->get();
        $this->view('forms/base-edit',$this->data);
    }

    function form_track_list() {
        $this->data['title'] = _t('Form tracking');
        $this->data['list'] = \Forms\Models\Track::findAll();

        $this->data['item_actions']['admin/form-track-edit'] = _t('view');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('\Forms\Models\Track','admin/form-track-list')
        );
        $this->data['fields'] = array('id'=>_t('#'),'date'=>_t('date'),'form'=>_t('form'));
        $this->data['field_filters']['form'] = function ($val) { return $val ? $val->getField('title'):""; };
        $this->view('cms/base-list',$this->data);
    }

    function form_track_edit($id) {
        $track = $this->findOrCreate('\Forms\Models\Track',$id);
        $form_model = new \Forms\Models\Form;
        $form_model->elements = $track->data['elements'];
        $values = $track->data['values'];

        $form = $form_model->get_form();
        foreach ($form->elements as $el) {
            if ($el->name && isset($values[$el->name])) $el->value = $values[$el->name];
        }

        $this->data['title'] = _t('View form track record');
        $this->data['form'] = $form->get();
        $this->view('forms/base-edit',$this->data);
    }
}