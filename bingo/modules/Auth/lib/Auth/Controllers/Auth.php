<?php
namespace Auth\Controllers;

class Auth extends \Bingo\Controller
{
    public $userClass;
    public $afterLogin;

    function __construct($userClass,$afterLogin) {
        parent::__construct();
        bingo_domain('auth');

        $this->userClass = $userClass;
        $this->afterLogin = $afterLogin;

        $userClass = $this->userClass;
        $this->user = $userClass::checkLoggedIn();
    }

    function login() {
        if ($this->user) redirect($this->afterLogin);
        $that = $this;
        $form = new \Bingo\Form;
        $form
            ->text('login',_t('Login'),'')
            ->password('password',_t('Password'),function($password) use ($that) {
                $userClass = $that->userClass;
                if (!$userClass::login($_POST['login'],$password))
                    throw new \ValidationException(_t("Wrong login/password combination"));
                return "";
            })
            ->submit(_t('Login'));

        if ($form->validate()) {
            if (isset($_GET['redirect']))
                redirect($_GET['redirect']);
            else
                redirect($this->afterLogin);
        }

        $this->data['title'] = _t("Login");
        $this->data['form'] = $form->get();
        $this->view('login');
    }
    function logout() {
        if ($this->user) $this->user->logout();
        $login_url = \Bingo\Routing::generate(array('controller'=>get_class($this),'action'=>'login'));
        redirect($login_url);
    }
}