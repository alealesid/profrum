<?php

namespace Auth\Models;

/** @MappedSuperclass */
class User extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=256) */
    public $login;
    /** @Column(length=256) */
    public $password;
    /** @Column(length=512) */
    public $email = "";
    /** @Column(length=100) */
    public $secret = "";


    public static $expireTime = 86400;

    public static function testLogin($login,$password) {
        $md5pass =  md5($password);
        $class = get_called_class();

        return $class::findOneBy(array('login'=>$login,'password'=>$md5pass));
    }

    public static function login($login,$password,$sessionExpire=false) {
        $class = get_called_class();
        $user = $class::testLogin($login,$password);
        if ($user) {
            $class::generateSecret($user,$sessionExpire);
        }
        return $user;
    }

    public static function loginUser($user,$sessionExpire=false) {
        $class = get_called_class();
        if ($user) {
            $class::generateSecret($user,$sessionExpire);
        }
        return $user;
    }

    public function logout() {
        $class = get_called_class();
        $class_name = str_replace("\\","",$class);
        setcookie("bingo_auth_$class_name", "", time()-60*60,"/");
    }

    public static function checkLoggedIn($cookie=false) {

        $class = get_called_class();
        $class_name = str_replace("\\","",$class);
        
        if (!$cookie) $cookie = @$_COOKIE["bingo_auth_$class_name"];
        if (!$cookie) return false;

        $cookie_var = explode("-", $cookie);
        
        $ck_expire   = array_pop($cookie_var);
        $ck_password = array_pop($cookie_var);
        $ck_username = implode('-' ,$cookie_var);
        
        $user = $class::findOneBy(array('login'=>$ck_username,'password'=>$ck_password));
        if ($user) {
            if ($ck_expire!="1") $class::generateSecret($user);
            return $user;
        }
        return false;
    }

    protected static function generateSecret($user,$sessionExpire=false) {
        $md5str = md5( time() );
        $class = get_called_class();
        $class_name = str_replace("\\","",$class);
        
        if ($sessionExpire) {
            $cookie_val = "$user->login-$user->password-1";
            setcookie("bingo_auth_$class_name", $cookie_val, 0,"/");
            
        } else {
            $cookie_val = "$user->login-$user->password-0";
            setcookie("bingo_auth_$class_name", $cookie_val, time() + $class::$expireTime,"/");
        }
    }
}