<?php

function add_include_path($path) {
    set_include_path(get_include_path(). PATH_SEPARATOR . $path );
}

function _D_clean($var,$maxDepth=1) {
    $return = null;
    $isObj = is_object($var);

    if ($isObj && in_array('Doctrine\Common\Collections\Collection', class_implements($var))) {
        $var = $var->toArray();
    }
    
    if (is_array($var)) {
        $return = array();
        foreach ($var as $k => $v) {
            $return[$k] = _D_clean($v, $maxDepth);
        }
    }    
    else if ($maxDepth) {
        if ($isObj) {
            if ($var instanceof \DateTime) {
                $return = $var->format('c');
            } else {
                $reflClass = new \ReflectionClass(get_class($var));
                $return = new \stdclass();

                if (get_class($var)=='stdClass') {
                    $nextDepth =  $maxDepth;
                } else {
                    $nextDepth = $maxDepth - 1;
                    $return->{'__CLASS__'} = get_class($var);
                }
                
                $excludeProperties = array();

                if ($var instanceof \Doctrine\ORM\Proxy\Proxy) {
                    $excludeProperties = array('__initializer__','__cloner__','__isInitialized__','lazyPropertiesDefaults');
                }

                if ($var instanceof \Doctrine\ORM\Proxy\Proxy && !$var->__isInitialized()) {
                    foreach ($reflClass->getProperties() as $reflProperty) {
                        $name  = $reflProperty->getName();
                        if ( ! in_array($name, $excludeProperties)) {
                            $reflProperty->setAccessible(true);
                            $return->$name = _D_clean($reflProperty->getValue($var), $maxDepth - 1);
                        }
                    }
                } else {
                    foreach ((array)$var as $name=>$sub) {
                        if ( ! in_array($name, $excludeProperties)) {
                            if (ord($name[0])!=0) {
                                $return->$name = _D_clean($sub, $nextDepth);
                            }
                        }
                    }
                }
            }
        } else {
            $return = $var;
        }
    } else {
        $return = is_object($var) ? get_class($var) 
            : (is_array($var) ? 'Array(' . count($var) . ')' : $var);
    }
    return $return;
}

function _D() {
    echo "<meta charset='utf-8' />";
    echo "<pre style='position:relative;z-index:100000;background:white;border:1px solid #eee;'>";
    
    $trace = debug_backtrace();
    $loc = $trace[0];
    echo $loc['file']." (".$loc['line'].")\n";
    
    $args = func_get_args();
    foreach ($args as $i=>$val) {
        $val = _D_clean($val);
        $s = print_r($val,true);
        if ($i>0) echo "\n";
        echo htmlspecialchars($s);
    }
    echo "</pre>";
}

function file_exists_ip($filename) {
    if(function_exists("get_include_path")) {
        $include_path = get_include_path();
    } elseif(false !== ($ip = ini_get("include_path"))) {
        $include_path = $ip;
    } else {return false;}

    if(false !== strpos($include_path, PATH_SEPARATOR)) {
        if(false !== ($temp = explode(PATH_SEPARATOR, $include_path)) && count($temp) > 0) {
            for($n = 0; $n < count($temp); $n++) {
                if(false !== @file_exists($temp[$n] .'/'. $filename)) {
                    return realpath($temp[$n] .'/'. $filename);
                }
            }
            return false;
        } else {return false;}
    } elseif(!empty($include_path)) {
        if(false !== @file_exists($include_path)) {
            return realpath($include_path);
        } else {return false;}
    } else {return false;}
}

spl_autoload_register(
    function ($className) {
        $path = str_replace("\\", "/", $className.".php");
        $path = ltrim($path,'/');

        if ($path = file_exists_ip($path)) {
            require_once($path);
        }
    }
);

if (!defined('INDEX_DIR')) {
    $qs = $_SERVER['QUERY_STRING'];
    $qs = urldecode($qs);
    if (strlen($qs)>0 && $qs[0]!="/") $qs = "/&".$qs;
    $req = urldecode($_SERVER['REQUEST_URI']);
    if (isset($_SERVER['PATH_TRANSLATED'])) {
        define('INDEX_DIR',dirname($_SERVER['PATH_TRANSLATED']));
        define('INDEX_URL',
            rtrim(
                substr($req,0,strlen($req)-strlen($qs)),
                "/"
            )
        );
    } else {
        define('INDEX_DIR',realpath(dirname($_SERVER['SCRIPT_FILENAME'])));
        define('INDEX_URL',
            rtrim(
                substr($req,0,strlen($req)-strlen($qs)),
                "/"
            )
        );
    }
}

set_include_path(".");
add_include_path(dirname(__FILE__)."/lib");

\Bingo\Bingo::getInstance();