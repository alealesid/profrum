<?php

namespace Bingo;

class ImageResizer {
    
    static function get_url($file,$w,$h,$outer=false,$adaptive=true) {
        $file = INDEX_DIR."/upload/CMS/files/".$file;
        return self::get_file_url($file,$w,$h,$outer,$adaptive);
    }
    
    static function get_file_url($file,$w,$h,$outer=false,$adaptive=true) {
        $file_cut = $file;
        if (substr($file_cut,0,strlen(INDEX_DIR))==INDEX_DIR) {
            $file_cut = substr($file_cut,strlen(INDEX_DIR)+1);
        }
        $cached = $w."x".$h.($adaptive ? "_a":"").($outer ? "_o":"")."_".str_replace(array("/","\\"),"_",$file_cut);
        if (!file_exists(INDEX_DIR."/cache/images")) mkdir(INDEX_DIR."/cache/images",0777,true);
        
        $process = false;
        if (file_exists(INDEX_DIR."/cache/images/$cached")) {
            if (file_exists($file)) {
                if (filemtime($file)>filemtime(INDEX_DIR."/cache/images/$cached")) {
                    $process = true;
                }
            }
        } else {
            $process = true;
        }
        
        if ($process) {
            if ($outer) {
                $xpos = 0;
                $ypos = 0;

                $info = getimagesize($file);
                $info = array(
                    'width'  => $info[0],
                    'height' => $info[1],
                    'bits'   => $info['bits'],
                    'mime'   => $info['mime']
                );
                if (!$info['width'] || !$info['height']) return false;

                $scale = min($w / $info['width'], $h / $info['height']);

                $new_width = (int)($info['width'] * $scale);
                $new_height = (int)($info['height'] * $scale);
                $xpos = (int)(($w - $new_width) / 2);
                $ypos = (int)(($h - $new_height) / 2);

                $mime = $info['mime'];
                if ($mime == 'image/gif') {
                    $image = imagecreatefromgif($file);
                } elseif ($mime == 'image/png') {
                    $image = imagecreatefrompng($file);
                } elseif ($mime == 'image/jpeg') {
                    $image = imagecreatefromjpeg($file);
                }

                $image_old = $image;
                $image = imagecreatetruecolor($w, $h);

                $background = imagecolorallocate($image, $outer[0], $outer[1], $outer[2]);
                imagefilledrectangle($image, 0, 0, $w, $h, $background);

                imagecopyresampled($image, $image_old, $xpos, $ypos, 0, 0, $new_width, $new_height, $info['width'], $info['height']);
                imagedestroy($image_old);

                $info = pathinfo($file);
                $extension = $info['extension'];

                $file = INDEX_DIR."/cache/images/$cached";
                if ($extension == ('jpeg' || 'jpg')) {
                    imagejpeg($image, $file, 70);
                } elseif($extension == 'png') {
                    imagepng($image, $file, 0);
                } elseif($extension == 'gif') {
                    imagegif($image, $file);
                }
                imagedestroy($image);

            } else {
                try {
                    $file = \PhpThumb\Factory::create($file,array('resizeUp'=>false));
                    if ($adaptive)
                        $file->adaptiveResize($w,$h);
                    else
                        $file->resize($w,$h);
                    $file->save(INDEX_DIR."/cache/images/$cached");
                } catch (\Exception $e) {
                    trigger_error($e->getMessage());
                    return "";
                }
            }
        }
        return url("cache/images/$cached");
    }
}