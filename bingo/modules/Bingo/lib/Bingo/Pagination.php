<?php
namespace Bingo;

class Pagination {
    
    public $perPage;
    public $current;
    public $total;
    public $pattern;
    public $query;

    public static $template = array(
        '<li><a href="{pattern}">{page}</a></li>',
        '<li class="current">{page}</li>',
        '<li> ... </li>'
    );

    /**
     * @param $perPage
     * @param $current
     * @param $total
     * @param $pattern
     * @param bool|mixed $query
     */
    function __construct($perPage,$current,$total,$pattern,$query=false) {
        if (!$pattern) {
            $purl = parse_url($_SERVER['REQUEST_URI']);
            parse_str(@$purl['query'],$params);
            unset($params['p']);
            $pattern0 = trim($purl['path'].'?'.http_build_query($params),'?');
            $params['p'] = "__s__";
            $pattern = $purl['path'].'?'.http_build_query($params);
            $pattern = str_replace('__s__',"{page}",$pattern);
            $pattern = array($pattern0,$pattern);
        } else {
            if (!is_array($pattern)) $pattern = array($pattern,$pattern);
        }
        $this->perPage = $perPage;
        $this->current = $current;
        if ($total===false && $query) $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($query);
        $this->total = $total;
        $this->pattern = $pattern;
        $this->query = $query;
    }
    
    public function getPageCount() {
        $pages = (int)($this->total / $this->perPage);
        if ($pages * $this->perPage!=$this->total) $pages++;
        return $pages;
    }
    
    public function get($around=0) {
        $pages = $this->getPageCount();
        if ($pages<2) return "";
        $pages_html = "<ul>";
        $prev = 0;
        for ($i=1;$i<=$pages;$i++) {

            if ($around!=0 && abs($i-$this->current)>$around && $pages!=$i && 1!=$i) continue;

            if ($i-$prev>1) $pages_html .=  Pagination::$template[2];
            $prev = $i;

            $pattern = ($i==1) ? $this->pattern[0] : $this->pattern[1];
            $pattern = str_replace("{page}",$i,$pattern);

            $search = array('{page}','{pattern}');
            $replace = array($i,$pattern);

            if ($this->current==$i)
                $pages_html .= str_replace($search,$replace,Pagination::$template[1]);
            else
                $pages_html .= str_replace($search,$replace,Pagination::$template[0]);
        }
        $pages_html .= "</ul>";
        return $pages_html;
    }
    
    public function result() {
        if (!$this->query) return array();
        return $this->query
                    ->setFirstResult(($this->current-1)*$this->perPage)
                    ->setMaxResults($this->perPage)
                    ->getResult();
    }
}