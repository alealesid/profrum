<?php

namespace Bingo;

require_once('helpers/validation.php');
require_once('Bingo/FormElement.php');

class Form {
    public $elements = array();
    public $nameHash = array();
    private $end = false;
    public $current;
    public $root;
    
    public $values = array();
    
    static protected $_extensions;
    static function extend($name,$f) {
        if ($f) {
            self::$_extensions[$name] = $f;
        } else {
            unset(self::$_extensions[$name]);
        }
    }
    
    public function __call($name,$args) {
        if (isset(self::$_extensions[$name])) {
            $args = array_merge(array($this),$args);
            return call_user_func_array(self::$_extensions[$name],$args);
        } else {
            throw new \Exception('Method '.$name.'is not implemented');
        }
    }
    
    /**
    * Create form class
    * @var $method html form method
    */
    public function __construct($method="post",$atts=array()) {
        if ($method=='multipart') {
            $method = "post";
            $atts['enctype']="multipart/form-data";
        }
        if (is_string($atts)) parse_str($atts,$atts);
        $this->atts['method'] = $method;
        $this->atts = array_merge($this->atts,$atts);
        $this->root = new FormElement;
        $this->current = $this->root;
    }
    
    /**
    * Create form class (factory method)
    * @var $method html form method
    */
    public static function create($method="post",$atts=array()) {
        return new Form($method,$atts);
    }
    
    /**
    * Add element to the form
    * @var $el element to add
    */
    public function add(FormElement $el) {
        $current = false;
        if ($el->before) {
            $other = $this->findElement($el->before);
            if ($other) $current = $other->parent;
        }
        else if ($el->after) {
            $other = $this->findElement($el->after);
            if ($other) $current = $other->parent;
        }
        if (!$current) $current = $this->current;
        $current->add($el);
        
        $this->end = $el;
        if ($el->name) $this->nameHash[$el->name] = $el;
        $this->elements[] = $el;
    }

    /**
    * Add elements to the form (via clone) and rename them via mask ({name}) if given
    * @var $source element to add (Form of array of elements)
    */
    public function addElements($source,$mask="{name}") {
        $list = is_array($source) ? $source : $source->elements;
        foreach ($list as $el) {
            $ins = clone($el);
            $ins->name = str_replace("{name}",$ins->name,$mask);
            $this->add($ins);
        }
    }

    /**
    * Text input
    */
    public function text($name,$label=false,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Text(compact('name','label','rules','value','atts')));
        return $this;
    }
    /**
    * Text Range input
    */
    public function text_range($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_TextRange(compact('name','label','rules','value','atts')));
        return $this;
    }
    /**
    * Text input with DateTime conversion
    */
    public function text_date($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Text(compact('name','label','rules','value','atts')));
        $this->add_filter(new FormFilter_DateTime("d.m.Y"))->add_class("date");
        return $this;
    }
    /**
    * Password input
    */
    public function password($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Password(compact('name','label','rules','value','atts')));
        return $this;
    }
    /**
    * Textarea
    * @param array|bool $atts
    */
    public function textarea($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_TextArea(compact('name','label','rules','value','atts')));
        return $this;
    }
    /**
    * Hidden input
    */
    public function hidden($name,$value,$atts=false) {
        $this->add(new FormElement_Hidden(compact('name','value','atts')));
        return $this;
    }
    /**
    * Submit button
    */
    public function submit($caption,$name=false,$value=false,$atts=false) {                            
        $type = 'submit';
        $this->add(new FormElement_Button(compact('name','caption','type','value','atts')));
        return $this;
    }
    /**
    * Generic button, can be submit one
    */
    public function button($name,$caption,$type="button",$value=false,$atts=false) {
        $this->add(new FormElement_Button(compact('name','caption','type','value','atts')));
        return $this;
    }
    /**
     * Select input
     * @param $name
     * @param $label
     * @param $options array
     * @param string $rules
     * @param mixed $value
     * @param bool|array $atts
     * @return Form
     */
    public function select($name,$label,$options,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Select(compact('name','label','options','rules','value','atts')));
        return $this;
    }
    /**
    * Radio input
    * @var $options array radio options in $options[$key] = $caption format
    */
    public function radio($name,$label,$options,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_RadioSelect(compact('name','label','options','rules','value','atts')));
        return $this;
    }
    /**
    * Checkbox
    */
    public function checkbox($name,$label,$rules='',$value=false,$atts=false) {
        $this->add(new FormElement_Checkbox(compact('name','label','rules','value','atts')));
        return $this;
    }
    /**
    * Fieldset, close automatically
    */
    public function fieldset($label=false,$nested=false,$atts=false) {
        if (!$nested) $this->close_fieldset();
        $args = compact('label','atts');
        $args['form'] = $this;
        $fset = new FormElement_Fieldset($args);
        $this->add($fset);
        $this->current = $fset;
        return $this;
    }
    /**
    * Close fieldset manually
    */
    public function close_fieldset() {
        if ($this->current != $this->root && $this->current instanceof FormElement_Fieldset)
            $this->current = $this->current->parent;
        return $this;
    }     
    
    public function tab($label=false,$value=false) {
        return $this->fieldset($label,false,array('value'=>$value));
    }
    
    public function close_tab() {
        return $this->close_fieldset();
    }
    
    public function tabs($name=false,$value=false,$atts=false) {
        $args = compact('name','value','atts');
        $tabs = new FormElement_Tabs($args);
        $this->add($tabs);
        $this->current = $tabs;
        return $this;
    }
    
    public function close_tabs() {
        if ($this->current != $this->root && $this->current instanceof FormElement_Tabs)
            $this->current = $this->current->parent;
        return $this;
    }
    
    /**
    * Custom html in form
    */
    public function html($html,$atts=array()) {
        $this->add(new FormElement_HTML(compact('html','value','atts')));
        return $this;
    }
    /**
     * Array value repsented as name[N]['value'] based on another form
     * @param $name
     * @param $form
     * @param $value
     * @param bool $as_table
     * @param bool|array $atts
     * @return Form
     */
    public function form_list($name,$form,$rules,$value,$as_table=false,$atts=false) {
        $this->add(new FormElement_Array(compact('name','form','rules','value','as_table','atts')));
        return $this;
    }

    /**
    * Add class for last added element                                  
    */
    public function add_class($class) {
        if (!$this->end)
            throw(new \Exception('You have to create an element before add_class'));
        $this->end->class .= " ".$class;
        return $this;
    }
    
    public function addClass($class) {
        return $this->add_class($class);
    }
    
    /**
    * Add value filter for last added element                                  
    */
    public function add_filter($filter) {
        if (!$this->end)
            throw(new \Exception('You have to create an element before add_filter'));
        $this->end->filter = $filter;
        return $this;
    }
    
    /**
    * Add doctrine object value filter for last added element                                  
    */
    public function object_filter($class,$primary='id') {
        $this->add_filter(new FormFilter_DoctrineObject($class,$primary));
        return $this;
    }   
    
    /**
    * Add doctrine olist value filter for last added element                                  
    */
    public function list_filter($class,$primary='id') {
        $this->add_filter(new FormFilter_DoctrineList($class,$primary));
        return $this;
    }        
    
    /**
    * Fill given stdObject with form value (post or get)
    */
    function fill($obj,$keys=false) {
        foreach ($this->getValue() as $key=>$val)  {
            if (!$keys || ($keys && in_array($key,$keys))) {
                if ($key[0]!="_") {
                    if ($obj->$key instanceof \Doctrine\ORM\PersistentCollection) {
                        $list = $obj->$key;
                        $val = is_array($val) ? $val : array();
                        // remove old
                        foreach ($list as $old) {
                            if (!in_array($old,$val)) {
                                $list->removeElement($old);
                            }
                        }
                        // add new
                        foreach ($val as $new) {
                            if (!$list->contains($new)) {
                                $list->add($new);
                            }
                        }
                    } else {
                        $obj->$key = $val;
                    }
                }
            }
        }
    }
    /**
    * Validate form
    */
    public function validate() {
        if ($this->atts['method']=='post' && empty($_POST)) return false;
        if ($this->atts['method']=='get' && empty($_GET)) return false;
        
        $this->nameHash = array();
        foreach ($this->elements as $el) {
            $name = $el->name;
            if ($name) $this->nameHash[$name] = $el;
        }
        return $this->validate_value($this->atts['method']=='post'?$_POST:$_GET);
    }
    /**
    * Validate single value
    */
    public function validate_value($list,$addErrorClass=true) {
        $valid = true;

        foreach ($this->nameHash as $key=>$el) {
            if (!$key) continue;
            
            $chain = explode("[",$key);
            $val = $list;
            foreach ($chain as $idx=>$subkey) $chain[$idx] = str_replace("]","",$subkey);
            foreach ($chain as $subkey) {
                if (isset($val[$subkey]))
                    $val = $val[$subkey];
                else {
                    $val = false;
                    break;
                }
            }
            if (!$el->validate($val)) {
                $valid = false;
            }

            $arr =& $this->values;
            $last_key = array_pop($chain);
            foreach ($chain as $key) {
                if (!isset($arr[$key])) $arr[$key] = array();
                $arr =& $arr[$key];
            }
            $arr[$last_key] = $el->value;
        }
        return $valid;
    }
    /**
    * Get form data (post or get)
    */
    public function getValue() {
        return $this->values;
    }
    /**
    * Set error for give element (by element name)
    */
    public function setError($key,$error) {
        $el = $this->nameHash[$key];
        if ($el) {
            $el->error = $error;
            $el->class .= " error";
        }
    }
    /**
    * Print elements HTML (without <form>)
    */
    public function print_elements() {
        $this->root->render_children();
    }
    /**
    * Find element by its name
    */
    public function findElement($name) {
        foreach ($this->elements as $el) if ($el->name==$name) return $el;
        return false;
    }
    /**
    * Remove element by its name
    */
    public function removeElement($name) {
        foreach ($this->elements as $key=>$el) if ($el->name==$name) {
            $el->parent->children = array_filter($el->parent->children,function($e) use ($el){
                return ($e!=$el);
            });
            unset($this->elements[$key]);
        }
    }
    /**
    * Get only element HTML without <form>
    */
    public function get_elements($start_indent=0) {
        ob_start();
        $this->indent = $start_indent;
        $this->print_elements($this->indent);
        return ob_get_clean();
    }
    
    /**
    * Get form HTML
    */
    public function get($start_indent=0) {
        ob_start();
        $atts = '';
        foreach ($this->atts as $key=>$val) $atts .= "$key = '$val' ";
        echo "<form $atts>";
        $this->print_elements();
        echo "</form>";
        return ob_get_clean();
    }
}