<?php

namespace Bingo;

class Routing {

    static $mappers = array();
    static $route = false;
    static $controller = false;
    static $uri = false;

    /**
    * Add routing handler
    * @var $path string route uri, like :controller/:action/*some_data
    * @var $defaults default values for route variables not found in uri
    * @var $rules regex check for route variables, like array($var=>$regex,...)
    * @var $key route key for route manipulation (if not given - generated)
    * @return route key
    */
    static public function connect($path, $defaults = array(), $rules = array(),$key=false) {
        $n = count(Routing::$mappers);
        $mapper = \Net_URL_Mapper::getInstance("mapper".$n);
        $prefix = base_url();
        $mapper->connect($path,$defaults,$rules);

        if (!$key) $key = "mapper_".$n."_".uniqid("",true);
        Routing::$mappers[$key] = $mapper;
        return $key;
    }

    /**
    * Disconnect module routes
    * @var $key route key, if not given all routes will be disconnected
    */
    static public function disconnect($key=false) {
        if (!$key)
            Routing::$mappers = array();
        else
            unset(Routing::$mappers[$key]);
    }

    /**
    * Handles given uri (executes function or controller method)
    * @var $uri uri
    */
    static public function route($uri) {
        Bingo::getInstance()->request = self::$uri = $uri;
        
        $routes = array();
        foreach (Routing::$mappers as $mapper)
        {
            try {
                $route = $mapper->match($uri);
                if ($route) {
                    $paths = $mapper->getPaths();
                    $route['_path'] = reset($paths)->getPath();
                    $routes[] = $route;
                }
            }
            catch (\Exception $e) {}
        }

        usort($routes,function($r1,$r2){
            $p1 = (isset($r1['priority']))?$r1['priority']:10;
            $p2 = (isset($r2['priority']))?$r2['priority']:10;
            return $p1>$p2;
        });
        
        if (count($routes)) {
            foreach ($routes as $route) {
                Routing::$route = $route;

                if (isset($route['function'])) {
                    if (call_user_func($route['function'],$route))
                        continue;
                    else
                        return;
                }

                if (isset($route['controller']))
                {
                    $controller_class = $route['controller'];
                    $action = $route['action'];

                    $controller = new $controller_class();
                    Routing::$controller = $controller;
                    $controller->route = $route;

                    $args = array();
                    foreach ($route as $key=>$val) $args[$key] = $val;

                    unset( $args['controller'] );
                    unset( $args['action'] );
                    unset( $args['priority']);
                    unset( $args['_path']);

                    if (call_user_func_array(array($controller,$action),$args))
                        continue;
                    else
                        return;
                }
            }
        }
        header("HTTP/1.0 404 Not Found");
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        echo 'Not found';
    }

    static function generate($route) {
        foreach (Routing::$mappers as $mapper) {
            $url = $mapper->generate($route);
            if ($url) return trim($url,"/");
        }
        return false;
    }
}
