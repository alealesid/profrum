<?php

namespace Bingo;

class Configuration {
    static public $locale = 'en_EN';
    static public $connections = array();
    static public $modules = array();
    static public $modulePaths;
    static public $super_admin = 'admin';
    static public $applicationMode = 'production';

    static function addDbConnection($host,$dbname,$user,$password) {
        return self::addMySQLConnection($host,$dbname,$user,$password);
    }
    
    static function addMySQLConnection($host,$dbname,$user,$password) {
        $driver = 'pdo_mysql';
        $driverOptions = array(1002=>'SET NAMES utf8');
        Configuration::$connections[] = compact('host','dbname','user','password','driver','driverOptions');
        return count(Configuration::$connections)-1;
    }
    
    static function addSQLiteConnection($path) {
        Configuration::$connections[] = array('path'=>$path,'driver'=>'pdo_sqlite');
        return count(Configuration::$connections)-1;
    }

    static function addModulePath($path) {
        Configuration::$modulePaths[] = $path;
    }

    static function addModules() {
        $list = func_get_args();
        foreach (Configuration::$modulePaths as $path) {
            foreach ($list as $module_name) {
                if (is_dir($path."/".$module_name."/lib")) {
                    add_include_path($path."/".$module_name."/lib");
                }
            }
        }
        Configuration::$modules = array_merge(Configuration::$modules,$list);
    }
}

Configuration::$modulePaths = array(realpath(dirname(__FILE__)."/../../../"));