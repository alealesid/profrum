<?php
namespace Bingo;

class Controller extends Template
{
    /** @var \Doctrine\ORM\EntityManager */
    public $em;

    function __construct() {
        $this->em = Bingo::getInstance()->em;
    }

    /**
    * Check if given var exists on route
    * @var $var var name
    */
    function route_isset($var) {
       return isset(Routing::$route[$var]);
    }

    /**
    * Returns route variable if exists, false otherwise
    * @var string $var var name
    */
    function route($var) {
       if ($this->route_isset($var)) return Routing::$route[$var];
       return false;
    }

    function __call($method,$params) {
       $method_new = str_replace(array('-','/','.'),'_',$method);
       
       if ($method_new!=$method)
            return call_user_func_array(array($this,$method_new),$params);
       else {
            $class = get_class($this);
            throw new \Exception("Controller '$class' has no '$method' action.");
       }
    }
}