<?php

namespace Bingo;

require_once('Bingo/helpers/url.php');
require_once('Bingo/helpers/locale.php');
require_once('Bingo/helpers/minify.php');

require_once('Action.php');

class Bingo {

    /** @var \Doctrine\ORM\EntityManager */
    public $em;
    public $ems = array();

    /** 
    * Get only instance of Bingo singleton
    * @return \Bingo\Bingo
    */
    static function getInstance() {
        static $instance = null;
        if ($instance == NULL) $instance = new Bingo();
        return $instance;
    }
    private function __clone() {}
    private function __construct() {}

    /**
    * Sets up directories and connections parameters for doctrine
    */
    public function setupDoctrine() {
        $cache = new \Doctrine\Common\Cache\ArrayCache;
        $config = new \Doctrine\ORM\Configuration;
        
        $this->annotationDriver = $config->newDefaultAnnotationDriver(array());
        
        $config->setMetadataDriverImpl($this->annotationDriver);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir(INDEX_DIR.'/cache/proxies');
        $config->setProxyNamespace('Bingo\Proxies');
        if (\Bingo\Configuration::$applicationMode=='development') {
            $config->setAutoGenerateProxyClasses(true);
        } else {
            $config->setAutoGenerateProxyClasses(false);
        }        
        
        $this->doctrineConfig = $config;
        
        if (count(Configuration::$connections)) {
            $this->setActiveConnection(0);
        }
    }
    
    /**
    * Switch active db connection
    */
    public function setActiveConnection($index) {
        $connectionOptions = @Configuration::$connections[$index];
        if (!$connectionOptions) return;
        
        $hash = json_encode($connectionOptions);
        if (!isset($this->ems[$hash]) || !$this->ems[$hash]->isOpen()) {
            $this->ems[$hash] = EntityManager::create($connectionOptions, $this->doctrineConfig);
        }
        $em = $this->ems[$hash];
        \Bingo::$em = $this->em = $em;
        \DoctrineExtensions\ActiveEntity\ActiveEntity::setEntityManager($em);
    }
    
    /**
    * Sets up base config, locale and url base
    */
    public function setupSystems()  {
        if (!isset($this->base_url)) {
            $req = urldecode($_SERVER['REQUEST_URI']);
            $qs = urldecode($_SERVER['QUERY_STRING']);
            $this->base_url = substr($req,0,
                strlen($req)-strlen($qs));
            $this->base_url = rtrim($this->base_url,"?");
            $this->base_url = rtrim($this->base_url,"/");
        }

        bingo_locale(Configuration::$locale);
        bingo_domain("none");
        bingo_domain_register('bingo',dirname(__FILE__)."/../../locale");

        \Bingo\Template::addIncludePath(false,INDEX_DIR."/template",url("template"));
    }
    /**
    * Module loading
    */
    public function setupModules()  {
        $accepted = Configuration::$modules;
        foreach ($accepted as $subdir) {
            $module = \Bingo\Module::getInstance("\\".$subdir);
        }
    }

    /**
    * System start
    */
    public function setup() {
        set_error_handler(array('\Bingo\Bingo','handle_regular_error'));
        register_shutdown_function(array('\Bingo\Bingo','handle_shutdown_error'));        
        set_exception_handler(array('\Bingo\Bingo','handle_exception'));
        
        $this->setupSystems();
        $this->setupDoctrine();
        $this->setupModules();
    }

    /**
    * Handle current request uri
    */
    public function route() {
        $url = urldecode($_SERVER['REQUEST_URI']);
        if (($pos = strpos($url, '?')) !== false) {
            $url = substr($url, 0, $pos);
        }        
        if ($this->base_url && strpos($url,$this->base_url)===0)
            $url = substr($url,strlen($this->base_url));
        unset($_GET[$url]);        
        $url = trim($url,"/");
        Routing::route($url);
    }
    
    static public function handle_error($errno, $errstr, $errfile, $errline, $trace) {
        ini_set('display_errors','0');

        $echo_message = \Bingo\Configuration::$applicationMode=='development';
        if($echo_message) {
            if(!\Session::isStarted()) {
                \Session::start();
            }
            echo "<meta charset='utf-8' />";
            echo "<pre style='position:relative;z-index:100000;background:white;border:1px solid #eee;padding:5px;'>";
            echo htmlspecialchars($errstr.' in '.$errfile.' on '.$errline);
            echo "</pre>";
        }

        \Bingo\Action::run('global_error',array($errno,$errstr,$errfile,$errline,$trace));
    }
    
    static public function handle_regular_error($errno,$errstr,$errfile,$errline) {
        $error_reporting = ini_get('error_reporting');
        if (!($error_reporting & $errno)) return;
        self::handle_error($errno, $errstr, $errfile, $errline, debug_backtrace());
    }
    
    static public function handle_shutdown_error() {
        $error = error_get_last();
        if($error == NULL || !in_array($error["type"], array(E_ERROR, E_USER_ERROR, E_PARSE))) return;
        self::handle_error($error["type"],  $error["message"], $error["file"], $error["line"], debug_backtrace());
    }
    
    static public function handle_exception($e) {
        \Bingo\Action::run('global_exception',array($e));
        self::handle_error('EXCEPTION', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTrace());
    }

    /**
    * Handles default user requested uri
    * @var $route Flag whether to do routing
    */
    public function run($route=true) {
        $this->setup();
        if ($route) $this->route();
    }
}
