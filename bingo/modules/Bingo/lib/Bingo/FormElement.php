<?php

namespace Bingo;

abstract class FormFilter {
    abstract function to($str);
    abstract function from($val);
}
class FormFilter_DateTime {
    public $dateFormat;
    function __construct($dateFormat="d.m.Y")  { $this->dateFormat = $dateFormat; }
    function to($str) {
        if (is_array($str)) {
            $out = array();
            foreach ($str as $key=>$val) $out[$key] = $this->to($val);
            return $out;
        }
        return $str ? \DateTime::createFromFormat($this->dateFormat,$str) : null;
    }
    function from($val) {
        if ($val)
            return $val->format($this->dateFormat);
        return null;
    }
}
class FormFilter_DoctrineObject {
    public $class;
    public $primary;
    function __construct($class,$primary='id') { $this->class = $class; $this->primary = $primary; }
    function to($str) { 
        $class = $this->class;
        if (is_array($str)) {
            $res = array();
            foreach ($str as $id) {
                $obj = $class::findOneBy(array($this->primary=>$id));
                if ($obj) $res[] = $obj;
            }
            return $res;
        } else {
            return $class::findOneBy(array($this->primary=>$str));
        }
    }
    function from($val) { 
        if (is_array($val) || $val instanceof \Doctrine\ORM\PersistentCollection) {
            $res = array();
            foreach ($val as $it)
                $res[] = ($it) ? $it->getField($this->primary) : false;
            if (count($res)==0) $res[] = false;
            return $res;
        } else {
            return ($val) ? $val->getField($this->primary) : false;
        }
    }
}

class FormFilter_DoctrineList {
    public $class;
    public $primary;
    function __construct($class,$primary='id') { $this->class = $class; $this->primary = $primary; }
    function to($str) { 
        $class = $this->class;
        if (is_array($str)) {
            $res = array();
            foreach ($str as $i=>$obj_data) {
                $obj = $class::find($obj_data[$this->primary]);
                if (!$obj) $obj = new $class;
                foreach ($obj_data as $key=>$val) {
                    if ($key!=$this->primary) {
                        $obj->$key = $val;
                    }
                }
                $res[$i] = $obj;
            }
            return $res;
        }
    }
    function from($val) {
        if (is_array($val) || $val instanceof \Doctrine\ORM\PersistentCollection) {
            $res = array();
            foreach ($val as $i=>$it)
                $res[$i] = (array)$it;
            return $res;
        }
        return array();
    }
}

class FormElement {
    public $args;
    public $need_close = false;
    public $filter = false;
    public $parent = false;
    public $children = array();
    
    public function __construct($args=array()) {
        $this->args = $args;
        
        $atts = isset($args['atts'])?$args['atts']:false;

        if ($atts) {
            if (is_array($atts)) {
                $this->args = array_merge($atts,$this->args);
            }
            else {
                $att_array = array();
                parse_str($atts,$att_array);
                $this->args = array_merge($att_array,$this->args);
            }
        }
    }
    
    public function __clone() {
        $children = array();
        foreach ($this->children as $el) {
            $children[] = clone $el;
        }
        $this->children = $children;
    }
    
    public function add($el) {
        if ($el->parent) {
            $el->parent->children = array_filter($el->parent->children,function($e) use ($el){
                return ($e!=$el);
            });
        }
        $el->parent = $this;
        
        if ($el->before || $el->after) {
            $before = $el->before;
            $after = $el->after;
            foreach ($this->children as $key=>$element) {
                if ($before && $element->name==$before) {
                    array_splice($this->children,$key,0,array($el));
                    break;
                }
                if ($after && $element->name==$after) {
                    array_splice($this->children,$key+1,0,array($el));
                    break;
                }
            }
        } else {
            $this->children[] = $el;
        }        
    }

    public function __get($name) {
        if (isset($this->args[$name]))
            return $this->args[$name];
        else
            return null;
    }
    public function __set($name,$value) {
        $this->args[$name] = $value;
    }
    
    public function attr($key,$att=false) {
        if (!$att) $att = $key;
        if ($this->$key===null) return '';
        return "$att='{$this->out($key)}'";
    }
    public function out($key) {
        if ($this->$key===null) return '';
        if ($this->filter && $key=='value')
            return $this->filter->from($this->$key);
        else {           
            if (is_string($this->$key))
                return htmlspecialchars($this->$key,ENT_QUOTES);
            else
                return $this->$key;
        }
    }
    
    public function render_label() {
        if ($this->label)
            echo "<label {$this->attr('id','for')}>{$this->label}</label>";
    }
    public function render_children() {
        foreach ($this->children as $one) 
            $one->render();
    }
    public function render_error() {
        if ($this->error)
            echo "<span class='error'>{$this->error}</span>";
    }
    public function render_element() {}
    public function render() {
        $this->render_label();
        $this->render_element();
        $this->render_error();
    }
    
    public function validate($val,$addErrorClass=true) {
        $el = $this;
        
        if ($el->filter) {
            $val = $el->filter->to($val);
        } else {
            if (is_string($val))
                $val = htmlspecialchars_decode($val,ENT_QUOTES);
        }

        $rules = $el->rules;

        if (is_string($rules)) $rules = explode('|',$rules);
        if (!is_array($rules)) $rules = array($rules);
        
        $valid = true;
        foreach ($rules as $rule) {
            if ($rule=='') continue;
            try {
                $val = call_user_func($rule,$val,$el,$this);
            } catch (\ValidationException $e) {
                $valid = false;
                $el->error = $e->getMessage();
                if ($addErrorClass) $el->class .= " error";
                if (isset($e->value)) {
                    $val = $e->value;
                }
                break;
            }
        }
        $el->value = $val;
        return $valid;
    }
}
class FormElement_Text extends FormElement {
    function render_element() {
        echo "<input type='text' {$this->attr('id')} {$this->attr('name')} {$this->attr('value')} {$this->attr('class')} {$this->attr('placeholder')}  />";
    }
}

class FormElement_TextRange extends FormElement {
    function render_element() {
        $value = $this->value;
        $this->value = $value['from'];
        echo "<span class='range_input'>";
        echo "<input type='text' {$this->attr('id')} name='{$this->out('name')}[from]' value='{$this->out('value')}' {$this->attr('class')} />";
        $this->value = $value['to'];
        echo "<input type='text' {$this->attr('id')} name='{$this->out('name')}[to]' value='{$this->out('value')}' {$this->attr('class')} />";
        $this->value = $value;
        echo "</span>";
    }
}

class FormElement_Password extends FormElement {
    function render_element() {
        echo "<input type='password' {$this->attr('id')} {$this->attr('name')} {$this->attr('value')} {$this->attr('class')} {$this->attr('placeholder')} />";
    }
}
class FormElement_TextArea extends FormElement {
    function render_element() {
        echo "<textarea {$this->attr('id')} {$this->attr('name')} {$this->attr('rows')} {$this->attr('class')}>{$this->out("value")}</textarea>";
    }
}
class FormElement_Hidden extends FormElement {
    function render_label() {}
    function render_element() {
        echo "<input type='hidden' {$this->attr('name')} {$this->attr('value')} />";
    }
}
class FormElement_Select extends FormElement {
    function render_element() {
        $name = $this->name;

        if ($this->multiple) $this->name .= "[]";
        echo "<select {$this->attr('id')} {$this->attr('name')} {$this->attr('size')} {$this->attr('class')} {$this->attr('multiple')} >";
        $this->name = $name;

        $options = $this->options;
        $value = $this->value;
        $out_value = $this->out('value');

        $optgroup_value = $this->optgroup_value;
        $was_group = false;

        foreach ($options as $key=>$val) {
            if ($optgroup_value && $optgroup_value==$val) {
                if ($was_group) echo "</optgroup>";
                echo "<optgroup label='".$key."'>";
                continue;
            }

            if ($this->filter) {
                $val = $this->filter->from($val);
            }
            else {
                $val = htmlspecialchars($val,ENT_QUOTES);
            }
            if ($this->multiple) {
                if ($out_value && in_array($val,$out_value))
                    $sel = "selected";
                else
                    $sel = "";
            } else {
                if ($val==$out_value)
                    $sel = "selected";
                else
                    $sel = "";
            }
            
            $disabled = "";
            if ($key && $key[0]=="!") {
                $key = substr($key,1);
                $disabled = "disabled";
            }
            
            echo "<option value='$val' $sel $disabled>$key</option>";
        }
        if ($was_group) echo "</optgroup>";
        echo "</select>";
    }
}

class FormElement_RadioSelect extends FormElement {
    function render_element() {
        $name = $this->name;

        $options = $this->options;
        $value = $this->value;
        $out_value = $this->out('value');

        echo "<div {$this->attr('class')}>";
        foreach ($options as $key=>$val) {
            if ($this->filter) {
                $val = $this->filter->from($val);
            }
            else {
                $val = htmlspecialchars($val,ENT_QUOTES);
            }
            if ($this->multiple) {
                if (in_array($val,$out_value))
                    $sel = "checked";
                else
                    $sel = "";
            } else {
                if ($val==$out_value)
                    $sel = "checked";
                else
                    $sel = "";
            }
            echo "<label><input type='radio' {$this->attr('name')} value='$val' $sel >$key</label>";
        }
        echo "</div>";
    }
}

class FormElement_Checkbox extends FormElement {
    function render() {
        $check = ($this->value) ? "checked" : '';
        echo "<label {$this->attr('class')}><input type='checkbox' {$this->attr('id')} {$this->attr('name')} $check/>{$this->label}</label>";
        $this->render_error();
    }
    
    function validate($val,$addCls=true) {
        $ret = parent::validate($val,$addCls);
        $this->value = (bool)$this->value;
        return $ret;
    }
}
class FormElement_Button extends FormElement {
    function render_label() {}
    function render_element() {
        echo "<button {$this->attr('id')} {$this->attr('value')} {$this->attr('type')} {$this->attr('name')} {$this->attr('class')}>{$this->out("caption")}</button>";
    }         
}
class FormElement_HTML extends FormElement {
    function render() {
        $this->render_label();
        if (!($this->value===false))
            echo str_replace('{value}',$this->value,$this->html);
        else
            echo $this->html;
        $this->render_error();
    }
}
class FormElement_Fieldset extends FormElement {
    function render() {
        if (!count($this->children) && !$this->class) return;
        echo "<fieldset {$this->attr('class')} {$this->attr('id')}>";
        if ($this->label) 
            echo "<span class='legend'>{$this->out("label")}</span>";
        echo "<div class='fieldset-content'>";
        $this->render_children();
        echo "</div></fieldset>";
    }
}

class FormElement_Tabs extends FormElement {
    
    static $id = 1;
    
    function render() {
        echo "<div class='tabs'>";
        if ($this->name) {
            echo "<input type='hidden' {$this->attr('name')} {$this->attr('value')} />";
        }
        $sid = self::$id;
        echo "<ul>";
        foreach ($this->children as $child) {
            if ($child instanceof FormElement_Fieldset) {
                echo "<li><a href='#tab_".self::$id."'>".$child->out('label')."</a></li>";
                self::$id++;
            }
        }
        echo "</ul>";
        
        self::$id = $sid;
        foreach ($this->children as $child) {
            if ($child instanceof FormElement_Fieldset) {
                echo "<div id='tab_".self::$id."' data-value='".$child->out('value')."'>";
                $child->render_children();
                echo "</div>";
                self::$id++;
            }
        }
        echo "</div>";
    }
}

class FormElement_Array extends FormElement {
    
    public $form_errors = array();

    public function __construct($args) {
        parent::__construct($args);
        $self = $this;
        
        $form = $self->form;
        foreach ($form->elements as $el) {
            $el->original_name = $el->name;
            $el->original_class = $el->class;
            $el->original_value = $el->value;
        }    
    }
    
    public function validate($val,$addErrorClass=true) {
        unset($val['{id}']);
        $form = $this->form;
        $errors = false;
        $ret = array();
        
        $prev = $this->value;
        if ($this->filter)
            $prev = $this->filter->from($prev);
        
        foreach ($val as $i=>$sub) {
            $retsub = array();

            foreach ($form->elements as $el) {
                if ($el->original_name) {
                    $el->name = "{$this->name}[$i][{$el->original_name}]";
                    $el->value = @$prev[$i][$el->original_name];
                }
            }

            $form->validate_value($sub,false);

            foreach ($form->nameHash as $name=>$el) {
                if ($el->error) $errors = true;
                $this->form_errors[$i][$name] = $el->error;
                $el->error = false;
                $retsub[$name] = $el->value;
            }
            $ret[$i] = $retsub;
        }
        $val = $ret;
        
        if (!$errors && $this->filter) {
            $val = $this->filter->to($val);
        }
        $this->value = $val;
        if (!$errors)  {
            return parent::validate($val,$addErrorClass);
        } else {
            return false;
        }
    }

    function render() {
        $this->render_label();
        $form = $this->form;

        $as_table = $this->as_table;

        if ($as_table) {
            echo "<table {$this->attr('class')}>";
            echo "<tr>";
            foreach ($form->elements as $el) {
                echo "<th class='item_{$el->original_name}'>$el->label</th>";
                $el->label = false;
            }
            if (!$this->locked)
                echo "<th></th>";
            echo "</tr>";

            $container = "tr";
            $pre = "<td>";
            $post = "</td>";
            $pre_del = "<td class='array_element_del_td'>";
            $post_del = "</td>";
        } else {
            $container = "fieldset";
            $pre = $post = "";
            $pre_del = $post_del = "";
        }

        $array = $this->value;
        if ($this->filter)
            $array = $this->filter->from($array);
        
        if ($array) foreach ($array as $i=>$item) {
            if ('{id}'===$i) continue;
            echo "<$container class='item'>";
            foreach ($form->elements as $el) {
                $key = $el->original_name;
                if ($key) {
                    $el->value = isset($item[$key]) ? $item[$key] : null;
                    $el->name = "{$this->name}[$i][{$key}]";
                    $el->error = @$this->form_errors[$i][$key];
                    if ($el->error)
                        $el->class = $el->original_class . " error";
                    else
                        $el->class = $el->original_class;
                }
            }
            
            foreach ($form->root->children as $el) {
                echo $pre;
                $el->render();
                echo $post;
            }
            
            if (!$this->locked)
                echo $pre_del."<a class='array_element_del' href=#>"._t('Remove','bingo')."</a>".$post_del;
            echo "</$container>";
        }
        if (!$this->locked) {
            echo "<$container class='array_element_template item' style='display:none'>";
            foreach ($form->elements as $el) {
                $key = $el->original_name;
                if ($key) {
                    $el->value = $el->original_value;
                    $el->name = "{$this->name}[{id}][{$key}]";
                }
                $el->error = false;
            }
            foreach ($form->root->children as $el) {
                echo $pre;
                $el->render();
                echo $post;
            }
            echo $pre_del."<a class='array_element_del' href=#>"._t('Remove','bingo')."</a>".$post_del;
            echo "</$container>";
        }

        if ($as_table) echo "</table>";

        if (!$this->locked)
            echo "<a class='array_element_add' href=#>"._t('Add element','bingo')."</a>";

        $this->render_error();
    }
}