<?php

namespace Bingo\Acl;

interface AssertInterface
{
    /**
     * Returns true if and only if the assertion conditions are met
     *
     * This method is passed the ACL, Role, Resource, and privilege to which the authorization query applies. If the
     * $role, $resource, or $privilege parameters are null, it means that the query applies to all Roles, Resources, or
     * privileges, respectively.
     *
     * @param  \Bingo\Acl                    $acl
     * @param  \Bingo\Acl\RoleInterface     $role
     * @param  \Bingo\Acl\ResourceInterface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(\Bingo\Acl $acl, \Bingo\Acl\RoleInterface $role = null, \Bingo\Acl\ResourceInterface $resource = null,
                           $privilege = null);
}
