<?php

namespace Bingo;

require_once('Net/URL/Mapper.php');

class Module {
    
    static $modules = array();
    
    /**
    * Get module instance by name
    */
    static function getInstance($module) 
    {
        if (isset(\Bingo\Module::$modules[$module]))
            return \Bingo\Module::$modules[$module];
            
        $class_name = $module;
        \Bingo\Module::$modules[$module] = new $class_name;
        return \Bingo\Module::$modules[$module];
    }

    /**
    * Base module constructor
    * Inits include paths
    */
    function __construct() 
    {
        $class = get_called_class();
        $this->module = explode("\\",$class);
        $this->module = $this->module[0];
    }

    /**
    * Add path where doctrine will search for annotated models
    */
    public function addModelPath($path) {
        \Bingo\Bingo::getInstance()->annotationDriver->addPaths(array($path));
    }

    /**
    * Add routing handler
    * @var $path string route uri, like :controller/:action/*some_data
    * @var $defaults default values for route variables not found in uri
    * @var $rules regex check for route variables, like array($var=>$regex,...)
    * @var $key route key for route manipulation (if not given - generated)
    * @return route key
    */
    static public function connect($path, $defaults = array(), $rules = array(),$key=false) {
        return Routing::connect($path,$defaults,$rules,$key);
    }

    /**
    * Disconnect module routes
    * @var $key route key, if not given all routes will be disconnected
    */
    static public function disconnect($key=false) {
        return Routing::disconnect($key);
    }
}