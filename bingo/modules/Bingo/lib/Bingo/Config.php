<?php
namespace Bingo;

class Config {
    /**
    * Gets default Config singleton instance
    */
    static function getInstance() {
        static $instance = null;
        if ($instance == NULL) $instance = new Config();
        return $instance;
    }
    private function __construct() {}
    
    /**
    * Gets value by given config array name and key
    * @var $name previously loaded config array
    * @var $key key in array
    */
    public static function &get($name,$key=null) {
        if ($key) {
            $list =& Config::getInstance()->$name; 
            if (isset($list[$key]))
                return $list[$key];
            else {
                $val = null;
                return $val;
            }
        }
        else
            return Config::getInstance()->$name;
    }
    
    /**
    * Load config array from php file
    * @var $name array name
    * @var $file config file path
    * @var $var php array variable name in given file
    */
    public static function loadFile($name,$file,$var='config') {
        $$var = array();
        include $file;
        Config::getInstance()->$name =& $$var;
    }
    /**
    * Save config array to php file, useful for saving config values through gui
    * @var $name string array name
    * @var $file string config file path
    * @var $var string php array variable name in given file to save in
    * @return bool
    */
    public static function saveFile($name,$file,$var='config') {
        $config = Config::getInstance();
        $content = '<?php' . PHP_EOL . "$var = " . var_export($config->$name, true) . ';';
        return is_numeric(file_put_contents($file, $content));
    }
}