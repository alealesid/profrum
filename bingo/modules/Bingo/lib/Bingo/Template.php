<?php

namespace Bingo {

class Template {
    public static $baseDirectories = array();
    public static $base = "";

    public $data = array();

	private $_view = false;
    private $_hash = array();

    static function addIncludePath($prefix,$path,$url) {
        self::$baseDirectories[] = array('path'=>$path,'url'=>$url,'prefix'=>$prefix);
    }

    static function setBase($path) {
        self::$base = $path;
    }

    static function getUrl($test) {
        if (@$test[0]=='/') {
            $test = substr($test,1);
        }
        else {
            if (self::$base) $test = self::$base . '/' . $test;
        }
        foreach (self::$baseDirectories as $dir) {
            $sub = $test;
            if ($dir['prefix']) {
                if (strpos($sub,$dir['prefix']."/")===0) {
                    $sub = substr($sub,strlen($dir['prefix'])+1);
                } else {
                    continue;
                }
            }
            $path =  $dir['path'].'/'.$sub;
            if (file_exists($path)) {
                return $dir['url']."/".$sub;
            }
        }
        return false;
    }

    static function getPath($test) {
        if (@$test[0]=='/') {
            $test = substr($test,1);
        }
        else {
            if (self::$base) $test = self::$base . '/' . $test;
        }

        foreach (self::$baseDirectories as $dir) {
            $sub = $test;
            if ($dir['prefix']) {
                if (strpos($sub,$dir['prefix']."/")===0) {
                    $sub = substr($sub,strlen($dir['prefix'])+1);
                } else {
                    continue;
                }
            }
            $path =  $dir['path'].'/'.$sub;

			if (file_exists($path)) {
                return $path;
            }
        }
        return false;
    }

    public function view_config($hash) {
        $this->_hash = $hash;
    }

    /**
    * Renders template haml or php template depending on which is available in TEMPLATE_PATH
    * @var $template string template name in relation to TEMPLATE_BASE
    * @var $data array variables array avaialable in template file
    * @var $ret bool return result? echoes otherwise
    */
    public function view($template,$data=array(),$ret=false) {
        \Bingo\Bingo::getInstance()->controller = $this;
        $data = $data ? : $this->data;
        $data = \Bingo\Action::filter('template_data',array($data));

        if (isset($this->_hash[$template])) $template = $this->_hash[$template];
        
        $data['_template'] = $template;

        extract($data);

        if ($this->_view) {
            require $this->get_include($template);
            return true;
        }

        $this->_view = true;

        require_once("Bingo/helpers/template.php");
        
        \Blocks::$instance->reset();
        $block = startblock('bingo',false);

        // for templates that can be compiled to php
        if ($include = $this->get_include($template)) {
            require $include;
        } else {
            // for others
            if ($tpl = Template::getPath($template.".liquid")) {
                
                defined('LIQUID_INCLUDE_PREFIX') or define('LIQUID_INCLUDE_PREFIX', '');
                
                require_once "Liquid/Liquid.class.php";
                require_once "LiquidExtensions/Tag/LiquidTagExtends.php";
                require_once "LiquidExtensions/Tag/LiquidTagBlock.php";
                
                $root = substr($tpl,0,-strlen($template.".liquid"));

                $liquid = new \LiquidTemplate($root);
                
                if (!file_exists(INDEX_DIR.'/cache/liquid')) mkdir(INDEX_DIR.'/cache/liquid');
                $cache = new \LiquidCacheFile(array('cache_dir'=>INDEX_DIR.'/cache/liquid'));
                $liquid->setCache($cache);
                
                $liquid->registerTag('extends', 'LiquidTagExtends');
                $liquid->registerTag('block', 'LiquidTagBlock');
                
                \Bingo\Action::run('liquid_created',array(&$liquid));
                
                $res = $liquid->parse(file_get_contents($tpl))->render($data);
                $res = \Bingo\Action::filter("template_view",array($res));
                if ($ret) return $res;
                echo $res;
                return;
                
            } else {
                throw(new \Exception("Template $template not found"));
            }
        }

        endblock();
                   
        if (count(\Blocks::$instance->stack)>0) {
            ob_end_clean();
            $end = end(\Blocks::$instance->stack);
            $name = $end[0];
            throw new \Exception("You have unclosed block '$name'");
        }
                   
        $buffer = \Blocks::$instance->get($block);
        $buffer = \Bingo\Action::filter("template_view",array($buffer));

        $this->_view = false;
        if ($ret)
            return $buffer;
        else {
            echo $buffer;
        }
        return true;
    }

    /**
    * Gets include filename for given template
    */
    static public function get_include($template) {
        if ($tpl = Template::getPath($template.".php")) {
            return $tpl;
        }
        elseif ($tpl = Template::getPath($template.".haml"))
        {
            require_once("PHamlP/haml/HamlParser.php");
            $haml = new \HamlParser(array('style'=>'compressed','ugly'=>false));

            $filename = trim(str_replace(INDEX_DIR,"",$tpl),"/");
            $dir = str_replace(array('/',"\\"),"_",dirname($filename));
            $file = basename($template);
            $mtime = filemtime(INDEX_DIR."/".$filename);

            if ($mtime > @filemtime($filename_new))  {
                if (!file_exists(INDEX_DIR.'/cache/haml')) mkdir(INDEX_DIR.'/cache/haml');
                $filename_new = INDEX_DIR."/cache/haml/{$dir}_{$file}.php";
                file_put_contents($filename_new, $haml->parse(INDEX_DIR."/".$filename));
            }
            return $filename_new;
        }
        else {
            $res = \Bingo\Action::filter('get_include',array(false,$template));
            return $res;
        }
        return false;
    }
}

}
namespace {
    function partial($template) {
        $res = \Bingo\Template::get_include($template);
        if (!$res) 
            throw new \Exception(str_replace("{template}",$template,_t("Template {template} is not found","bingo")));
        return $res;
    }

    function template_base($path) {
        \Bingo\Template::setBase($path);
    }

    function t_url($sub) {
        return \Bingo\Template::getUrl($sub);
    }
}
