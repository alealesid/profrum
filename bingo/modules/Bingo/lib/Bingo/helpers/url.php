<?php
function base_url() {
    return \Bingo\Bingo::getInstance()->base_url;
}

function url($sub) {
    return base_url().'/'.\Bingo\Action::filter('url',array(ltrim($sub,'/')));
}

function anchor($sub,$text,$target=false) {
    $target = $target ? 'target="'.$target.'"' : '';
    return '<a '.$target.' href="'.url($sub).'">'.$text.'</a>';
}

function show_error($err) {
    echo $err;
}

function redirect($uri = '', $method = 'location', $http_response_code = 302)
{
    if (!preg_match('#^https?://#i', $uri)) $uri = url($uri);
    switch($method) {
        case 'refresh':
            header("Refresh:0;url=".$uri);
            break;
        default:
            header("Location: ".$uri, TRUE, $http_response_code);
            break;
    }
    exit;
}

function set_flash($name,$value=false) {
    $data = new \Session\SessionNamespace('flash');
    $data->$name = $value;
}

function get_flash($name,$remove=true) {
    $data = new \Session\SessionNamespace('flash');
    if (!$name || @$data->$name==false) return null;
    $msg = $data->$name;
    if ($remove) {
        if (is_int($remove)) {
            $data->setExpirationSeconds($remove,$name);
        } else {
            unset($data->$name);
        }
    }
    return $msg;
}