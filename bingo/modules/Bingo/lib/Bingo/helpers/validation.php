<?php

class ValidationException extends Exception {}

function valid_email($str) {
    if ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str))
        throw new ValidationException(_t("Has to be a valid e-mail",'bingo'));
    return $str;
}

function valid_regex($regex,$error_message) {
    return function ($val) use ($regex,$error_message) {
        if (!preg_match($regex,$val))
            throw new ValidationException($error_message);
        return $val;
    };
}

function numeric($str) {
    if (!is_numeric($str))
        throw new ValidationException(_t("Valid numeric needed",'bingo'));
    return $str;
}

function positive($str) {
    if ((int)$str<=0)
        throw new ValidationException(_t("Positive value needed",'bingo'));
    return $str;
}

function required($str) {
    if (!$str && !($str==="0"))
        throw new ValidationException(_t("Field is required",'bingo'));
    return $str;
}

function unique_id($class) {
    return function ($val) use ($class) {
        $obj = $class::find($val);
        if ($obj)
            throw new ValidationException(_t("Unique id is required",'bingo'));
        return $val;
    };
}

function to_int($str) {
    return (int)$str;
}

function to_float($str) {
    return (float)$str;
}

function trim_br($val) {
    return preg_replace('{(<br(\s*/)?>|ANDnbsp;)+$}i', '', $val);
}

function obj_from_id($class) {
    return function ($val) use ($class) {
        return ($val)?$class::find($val):null;
    };
}

function unique_field($that,$field) {
    return function ($val) use ($that,$field) {
        $class = get_class($that);
        if ($val) {
            $other = $class::findOneBy(array($field=>$val));
            if ($other && $other!=$that)
                throw new ValidationException(_t("Unique field is required:",'bingo').' '.$field);
        }
        return $val;
    };
}

function password($old_password) {
    return function ($val) use ($old_password) {
        if ($old_password) {
            if ($val) 
                return md5($val);
            else   
                return $old_password;
        } else {
            return md5(required($val));
        }
    };
}

?>
