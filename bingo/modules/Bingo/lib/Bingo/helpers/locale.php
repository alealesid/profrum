<?php

require_once('gettext/gettext.inc');
require_once('php-mo.php');

function bingo_locale($value) {
    global $locale;
    $locale = $value;
    T_setlocale(LC_ALL,$value);
}

function bingo_get_locale() {
    global $locale;
    return $locale;
}

function bingo_domain_get($domain) {
    global $locale;
    global $domain_cache,$domain_files;

    $locale_dir = INDEX_DIR."/cache/locale";

    if (!$locale) return "none";
    if (!isset($domain_files[$domain])) return "none";

    if (!isset($domain_cache) || !$domain_cache) $domain_cache = array();
    if (!isset($domain_cache[$domain]))
    {
        $filename = $domain_files[$domain]."/$locale/{$domain}.mo";
        $po = $domain_files[$domain]."/$locale/{$domain}.po";
        
        $mtime = (int)@filemtime($filename);
        
        if (file_exists($po)) {
            $po_mtime = filemtime($po);
            if ($po_mtime > $mtime) {
                phpmo_convert($po);
                $mtime = filemtime($filename);
            }
        }
        
        if (!$mtime) return "none";
        $filename_new = "$locale_dir/$locale/LC_MESSAGES/{$domain}_{$mtime}.mo";
        
        if (!file_exists($filename_new)) {
            $files = glob("$locale_dir/$locale/LC_MESSAGES/{$domain}_*.mo");
            foreach ($files as $file) unlink($file);

            if (!file_exists($locale_dir."")) mkdir($locale_dir."");
            if (!file_exists($locale_dir."/$locale")) mkdir($locale_dir."/$locale");
            if (!file_exists($locale_dir."/$locale/LC_MESSAGES")) mkdir($locale_dir."/$locale/LC_MESSAGES");

            copy($filename,$filename_new);
        }
        $domain_cache[$domain] = "{$domain}_{$mtime}";
        T_bindtextdomain($domain_cache[$domain], $locale_dir);
        T_bind_textdomain_codeset($domain_cache[$domain], 'utf-8');
    }
    return $domain_cache[$domain];
}

function bingo_domain($domain) {
    T_textdomain(bingo_domain_get($domain));
}

function bingo_domain_register($domain,$file) {
    global $domain_files;
    if (!isset($domain_files) || !$domain_files) $domain_files = array();
    $domain_files[$domain] = $file;
}

function _t($text,$domain=false) {
    if ($domain) {
        return T_dgettext(bingo_domain_get($domain),$text);
    }
    else
        return T_($text);
}