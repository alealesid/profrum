<?php

define('SUPERBLOCK','super');

define('SUPER_BLOCK','super');
define('WIDGET_BLOCK','widget');
define('PRINT_BLOCK','print');
 
class Blocks {
    public $stack = array();
    public $blocks = array();
    
    function reset() {
        $this->stack = array();
        $this->blocks = array();
    }
    
    function start($name,$type=false) {
        $new = new stdClass();
        $new->name = $name;
        $new->type = $type;
        $new->items = array();
        $new->args = func_get_args();
        
        $count = count($this->stack);
        if ($count) {
            $end = end($this->stack);
            $end->items[] = ob_get_clean();
            $end->items[] = $new;
        }
        $this->stack[] = $new;
        
        if (!isset($this->blocks[$name])) $this->blocks[$name] = array();
        $this->blocks[$name][] = $new;

        ob_start();
        return $new;
    }
    
    function end() {
        $count = count($this->stack);
        if (!$count) throw new Exception("Invalid end of block");
        
        $end = $this->stack[count($this->stack)-1];
        $end->items[] = ob_get_clean();
        
        array_pop($this->stack);
        if (count($this->stack)) ob_start();
    }
    
    function get($block) {
        $name = $block->name;
        $blocks = $this->blocks[$name];
        
        if ($block->type==WIDGET_BLOCK) return "";
        
        if ($block->type==PRINT_BLOCK) {
            $list = array();
            foreach ($blocks as $one) {
                if ($one->type==WIDGET_BLOCK) $list = array($one);
            }
        } else {
            $first = $blocks[0];
            if ($first!=$block) return "";
            if ($first->type==SUPER_BLOCK)
                $list =  &$blocks;
            else
                $list = array(end($blocks));
        }
        
        $s = "";
        foreach ($list as $block) {
            foreach ($block->items as $item) {
                if (is_object($item)) {
                    $s .= $this->get($item);
                } else {
                    $s .= $item;
                }
            }
        }
        return $s;
    }
    
    static $instance;
}

Blocks::$instance = new Blocks();
function startblock($name,$type=false) {
    return call_user_func_array(array(Blocks::$instance,'start'),func_get_args());
}

function endblock() {
    return Blocks::$instance->end();
}

function superblock($name) {
    return startblock($name,SUPER_BLOCK);
}

function widgetblock($name) {
    return startblock($name,WIDGET_BLOCK);
}

function printblock($name) {
    return emptyblock($name,PRINT_BLOCK);
}

function emptyblock($name,$type=false) {
    $res = startblock($name,$type);
    endblock();
    return $res;
}

