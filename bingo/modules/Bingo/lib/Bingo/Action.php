<?php
namespace Bingo {
    /**
    * Class for creating hooks
    */
    class Action {
        static public $actions = array();

        /**
         * Clear all actions (hooks)
         */
        static function clearAll() {
            Action::$actions = array();
        }

        /**
        * Give handler for certain hook
        * @var $tag hook name
        * @var $function_to_add closure - handler (closure or function-name)
        * @var $priority int hook priority (lower executes earlier)
        * @var $accepted_args how many variables handler expects
        */
        static function add($tag,$function_to_add,$priority=10,$accepted_args=1) {
            Action::$actions[$tag][$priority][] = 
                array(
                    'function' => $function_to_add, 
                    'accepted_args' => $accepted_args
                );
        }

        /**
         * Run all handlers for hook (set hook place)
         * @param string $tag hook name
         * @param array $args hook arguments
         * @return bool|mixed last action return or false if none
         */
        static function run($tag, $args=array()) {
            if (!isset(Action::$actions[$tag])) return false;
            ksort( Action::$actions[ $tag ] );
            reset( Action::$actions[ $tag ] );
            do {
                foreach ( (array) current(Action::$actions[$tag]) as $the_ )
                    if ( !is_null($the_['function']) ) {
                        $res = call_user_func_array(
                            $the_['function'], 
                            array_slice($args, 0, (int) $the_['accepted_args'])
                        );
                        if ($res) return $res;
                    }
            } while ( next(Action::$actions[$tag]) !== false );
            return false;
        }

        /**
         * Run all handlers for filter
         * @param string $tag hook name
         * @param array $args hook arguments
         * @return mixed filtered result
         */
        static function filter($tag, $args=array()) {
            if (!isset(Action::$actions[$tag])) return $args[0];
            ksort( Action::$actions[ $tag ] );
            reset( Action::$actions[ $tag ] );
            do {
                foreach ( (array) current(Action::$actions[$tag]) as $the_ )
                    if ( !is_null($the_['function']) ) {
                        $res = call_user_func_array(
                            $the_['function'], 
                            array_slice($args, 0, (int) $the_['accepted_args'])
                        );
                        $args[0] = $res;
                    }
            } while ( next(Action::$actions[$tag]) !== false );
            return $args[0];
        }    
    }
}