<?php

namespace Bingo;

class MetadataFactory extends \Doctrine\ORM\Mapping\ClassMetadataFactory
{
    function __construct() {
        $this->setReflectionService(new ReflectionService);
    }
}

class ReflectionService extends \Doctrine\Common\Persistence\Mapping\RuntimeReflectionService {
    public function getAccessibleProperty($class, $property) {
        $r = new \ReflectionClass($class);
        if ($r->hasProperty($property))
            return parent::getAccessibleProperty($class, $property);
        else
            return new DynamicProperty($class,$property);
    }
}

class DynamicProperty {
    public $name = null;
    public $class = null;

    public function __construct($class, $name) {
        $this->class = $class;
        $this->name = $name;
    }
    public function setAccessible($flag) {}
    public function setValue($entity = null, $value = null) {
        $entity->setField($this->name,$value);
    }
    public function getValue($entity = null) {
        return $entity->getField($this->name);
    }
}

class EntityEvents
{
    private static $mappings = array();
    public static function mapField($class,$mapping) {
        EntityEvents::$mappings[$class][] = $mapping;
    }

    public function loadClassMetadata(\Doctrine\ORM\Event\LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if (isset(EntityEvents::$mappings[$classMetadata->name])) {
            foreach (EntityEvents::$mappings[$classMetadata->name] as $mapping) {
                $classMetadata->mapField($mapping);
            }
        }
    }
}

class EntityManager extends \Doctrine\ORM\EntityManager
{
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        $config->setClassMetadataFactoryName("\Bingo\MetadataFactory");
        $config->addCustomNumericFunction('RAND', 'DoctrineExtensions\Query\Mysql\Rand');
        parent::__construct($conn, $config, $eventManager);
        $eventManager->addEventListener(\Doctrine\ORM\Events::loadClassMetadata, new EntityEvents());
    }

    public static function create($conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager = null)
    {
        if (!$config->getMetadataDriverImpl()) {
            throw ORMException::missingMappingDriverImpl();
        }

        if (is_array($conn)) {
            $conn = \Doctrine\DBAL\DriverManager::getConnection($conn, $config, ($eventManager ?: new \Doctrine\Common\EventManager()));
        } else if ($conn instanceof \Doctrine\DBAL\Connection) {
            if ($eventManager !== null && $conn->getEventManager() !== $eventManager) {
                throw ORMException::mismatchedEventManager();
            }
        } else {
            throw new \InvalidArgumentException("Invalid argument: " . $conn);
        }

        return new EntityManager($conn, $config, $conn->getEventManager());
    }
}