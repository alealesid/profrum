<?php

class Bingo {
    static public $em;
    static function __callStatic($name,$args) {
        $obj = \Bingo\Bingo::getInstance();
        if (method_exists($obj,$name)) call_user_func_array(array($obj,$name),$args);
    }
}
