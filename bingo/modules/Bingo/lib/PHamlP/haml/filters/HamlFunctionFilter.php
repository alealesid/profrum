<?php

include_once "PHamlP/haml/filters/HamlBaseFilter.php";

class HamlFunctionFilter extends HamlBaseFilter {

    public function init() {}

    public function run($text) {
        $haml = new HamlParser();

        $lines = explode("\n",$text);
        $def = $lines[0]; unset($lines[0]);
        $body = join("\n",$lines);

        return "<?php function $def { ?>".$haml->toTree($body)->render()."<?php } ?>";
    }
}