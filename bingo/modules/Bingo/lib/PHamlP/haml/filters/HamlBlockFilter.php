<?php

include_once "PHamlP/haml/filters/HamlBaseFilter.php";

class HamlBlockFilter extends HamlBaseFilter {

    public function init() {}

    public function run($text) {
        $haml = new HamlParser();

        $lines = explode("\n",$text);
        $def = $lines[0]; unset($lines[0]);
        $body = join("\n",$lines);

        return "<?php startblock('$def'); ?>".$haml->toTree($body)->render()."<?php endblock(); ?>";
    }
}