<?php

include_once "PHamlP/haml/filters/HamlBaseFilter.php";

class HamlIncludeFilter extends HamlBaseFilter {

    public function init() {}

    public function run($text) {
        return '<?php include (\Bingo\Template::get_include("'.trim($text).'")) ?>';
    }
}