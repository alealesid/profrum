<?php

namespace DoctrineExtensions\ActiveEntity;
define('DATETIME',\Doctrine\DBAL\Types\Type::DATETIME);

class ActiveEntity
{
    protected static $entityManager;

    public static function setEntityManager($em)
    {
        self::$entityManager = $em;
    }

    protected static function getEntityManager()
    {
        if (!self::$entityManager) {
            throw new \Exception("No entity manager");
        }
        return self::$entityManager;
    }
    
    public function save($flush=true)
    {
        self::getEntityManager()->persist($this);
        if ($flush) self::getEntityManager()->flush($this);
    }

    public function delete($flush=true)
    {
        self::getEntityManager()->remove($this);
        if ($flush) self::getEntityManager()->flush($this);
    }
    
    public function getField($field) {
        return @$this->$field;
    }

    public function setField($field,$value) {
        $this->$field = $value;
    }
    
    public function &__get($key) {
        if (property_exists($this,$key)) return $this->$key;
        $false = false;
        return $false;
    }
    
    public function __set($key,$val) {
        $this->$key = $val;
    }    
    
    /**
     * @static
     * @param $id
     * @return static
     */
    public static function findOrCreate($id) {
        $class = get_called_class();
        $obj = false;
        if ($id) $obj = $class::find($id);
        if ($obj) return $obj;
        return new $class;
    }

    public static function mapField(array $mapping) {
        $class = get_called_class();
        \Bingo\EntityEvents::mapField($class,$mapping);
    }

    /**
     * @static
     * @param array $criteria
     * @param bool $orderBy
     * @return \Doctrine\ORM\Query
     */
    public static function findByQuery(array $criteria,$orderBy=false) {
        $cls = get_called_class();
        $qb = $cls::findByQueryBuilder($criteria,$orderBy);
        return $qb->getQuery();
    }
    
    /**
     * @static
     * @param array $criteria
     * @param bool $orderBy
     * @return \Doctrine\ORM\QueryBuilder
     */
    public static function findByQueryBuilder(array $criteria,$orderBy=false) {
        $qb = self::getEntityManager()->createQueryBuilder();
        $qb->select('obj');
        $qb->from(get_called_class(),'obj');
        
        $i = 0;
        $joins = array();
        foreach ($criteria as $key=>$val) {
            $obj = "obj";
            
            $hasValue = true;
            if (is_numeric($key)) {
                $key = $val;
                $hasValue = false;
            }
            
            if (strpos($key,".")!==false) {
                $chain = explode(".",$key);
                $last_key = array_pop($chain);
                $current = array();
                foreach ($chain as $part) {
                    $current[] = $part;
                    $join = "obj_".join("_",$current);
                    if (!isset($joins[$join])) {
                        $joins[$join] = true;
                        $joinName = $join;
                        $joinName[strrpos($join,"_")] = ".";
                        $qb->leftJoin($joinName,$join);
                    }
                }
                $key = $last_key;
                $obj = $join;
            }
            
            if (!$hasValue) {
                $qb->andWhere("$obj.$key");
            }
            else if ($val===null) {
                $qb->andWhere("$obj.$key IS NULL");
            } 
            else { 
                if (strpos($key,"?")!==false) {
                    $i++;
                    $qb->andWhere($obj.".".str_replace("?","?$i",$key));
                    $qb->setParameter($i,$val);
                }
                else if (is_array($val)) {
                    $from = isset($val['from']) ? $val['from'] : false;
                    $to = isset($val['to']) ? $val['to'] : false;

                    if ($from!==false || $to!==false) {
                        if ($from) {
                            $i++;
                            $qb->andWhere("$obj.$key >= ?$i");
                            if ($from instanceof \DateTime)
                                $qb->setParameter($i,$from,\Doctrine\DBAL\Types\Type::DATETIME);
                            else
                                $qb->setParameter($i,$from);
                        }
                        if ($to) {
                            $i++;
                            $qb->andWhere("$obj.$key <= ?$i");
                            if ($to instanceof \DateTime)
                                $qb->setParameter($i,$to,\Doctrine\DBAL\Types\Type::DATETIME);
                            else
                                $qb->setParameter($i,$to);
                        }
                    } else {
                        $expr = $qb->expr()->orx();
                        foreach ($val as $v) {
                            if ($v===null) {
                                $expr->add("$obj.$key IS NULL");
                            } else {
                                $i++;
                                $expr->add("$obj.$key = ?$i");
                                if ($v instanceof \DateTime)
                                    $qb->setParameter($i,$v,\Doctrine\DBAL\Types\Type::DATETIME);
                                else
                                    $qb->setParameter($i,$v);
                            }
                        }
                        if (count($val)) $qb->andWhere($expr);
                    }
                } else {
                    $i++;

                    if (is_string($val) && strpos($val,"LIKE ")===0) {
                        $qb->andWhere("$obj.$key LIKE ?$i");
                        $qb->setParameter($i,substr($val,5));
                    } else {
                        $qb->andWhere("$obj.$key = ?$i");
                        if ($val instanceof \DateTime)
                            $qb->setParameter($i,$val,\Doctrine\DBAL\Types\Type::DATETIME);
                        else {
                            $qb->setParameter($i,$val);
                        }
                    }
                }
            }
        }
        if ($orderBy) {
            if (!is_array($orderBy)) $orderBy = array($orderBy);
            foreach ($orderBy as &$ord) $ord = "obj.$ord";
            $qb->add('orderBy',join(",",$orderBy));
        }
        return $qb;
    }
    
    public static function findBy(array $criteria,$orderBy=false,$from=0,$max=false) {
        $class = get_called_class();
        $query = $class::findByQuery($criteria,$orderBy);
        if ($max) {
            $query->setFirstResult($from);
            $query->setMaxResults($max);
        }
        return $query->getResult();
    }

    public static function findOneBy(array $criteria,$orderBy=false) {
        $class = get_called_class();
        $res = $class::findBy($criteria,$orderBy,0,1);
        if (!count($res)) return null; else return $res[0];
    }

    public static function find($id) {
        if ($id===false || $id===null) return false;
        return self::getEntityManager()->getRepository(get_called_class())->find($id);
    }

    public static function findAll() {
        return self::getEntityManager()->getRepository(get_called_class())->findAll();
    }

    public static function __callStatic($method, $arguments)
    {
        return call_user_func_array(
            array(self::getEntityManager()->getRepository(get_called_class()), $method),
            $arguments
        );
    }

    /**
     * @static
     * @param array $criteria
     * @param string $parent_field
     * @param string $title_field
     * @param bool $order_field
     * @param bool $forSelect
     * @param bool|array $selectInit
     * @return array
     */
    public static function treeSort(
        $criteria=array(),
        $parent_field='parent',
        $title_field='title',
        $order_field=false,
        $forSelect=false,
        $selectInit=false
    ) {
        $class = get_called_class();
        if (@$criteria[0] instanceof ActiveEntity || empty($criteria))
            $list = $criteria;
        else
            $list = $class::findBy($criteria);

        if (!function_exists('DoctrineExtensions\ActiveEntity\tree_prep')) {
            function tree_prep($id) {
                $id = (string)$id;
                return str_repeat('-',20-strlen($id))."{$id}";
            }
        }
        foreach ($list as $g) { unset($g->tree_index); }
        $tree_idx = function($g) use (&$tree_idx,&$parent_field,&$order_field) {
            if (isset($g->tree_index)) {
                return $g->tree_index;
            } else {
                $order = $order_field ? tree_prep($g->$order_field) : '';
                if ($g->$parent_field==null) {
                    $g->tree_index = $order.tree_prep($g->id);
                    $g->tree_depth = 0;
                } else {
                    $g->$parent_field->getField('id');
                    $g->tree_index = $g->tree_depth = 0;
                    $parent_idx = $tree_idx($g->$parent_field);
                    $g->tree_index = $parent_idx.$order.tree_prep($g->id);
                    $g->tree_depth = $g->$parent_field->tree_depth+1;
                }
                return $g->tree_index;
            }
        };

        $min_depth = 5000;
        foreach ($list as $key=>$p) {
            $tree_idx($p);
            if ($min_depth > $p->tree_depth) $min_depth = $p->tree_depth;

        }
        foreach ($list as $key=>$p) {
            $p->tree_title = str_repeat('&emsp;',$p->tree_depth-$min_depth).' '.$p->$title_field;
        }
        usort($list,function($a,$b){return $a->tree_index>$b->tree_index;});
        if ($forSelect) return $class::selectOptions($list,'tree_title',$selectInit);
        return $list;
    }

    public static function selectOptions($criteria,$title_field,$init=false) {
        $init = $init!==false?$init:array(_t('None','bingo')=>0);
        $class = get_called_class();
        if (@$criteria[0] instanceof ActiveEntity)
            $list = $criteria;
        else
            $list = $class::findBy($criteria);

        $ret = $init;
        foreach ($list as $obj) $ret[$obj->$title_field] = $obj;
        return $ret;
    }
}
