<?php

class Blog extends \Bingo\Module {

    function __construct() {
        bingo_domain_register('blog',dirname(__FILE__)."/../locale");
        $this->addModelPath(dirname(__FILE__)."/Blog/Models",true);

        $this->connect('admin/:action/:data',array('controller'=>'\Blog\Controllers\Admin\Posts','data'=>false),
            array('action'=>'(post-list|post-edit|post-options|post-mass-edit)'));
        $this->connect('admin/:action/:data',array('controller'=>'\Blog\Controllers\Admin\Themes','data'=>false),
            array('action'=>'(theme-list)'));

        $this->connect("/",
            array('controller'=>'\Blog\Controllers\Blog','action'=>'index','priority'=>100)
        );

        $this->connect("rss",
            array('controller'=>'\Blog\Controllers\Blog','action'=>'rss')
        );

        $this->connect("search",
            array('controller'=>'\Blog\Controllers\Blog','action'=>'search')
        );

        $this->connect("*any",
            array('controller'=>'\Blog\Controllers\Blog','action'=>'error_404','priority'=>200)
        );

        $this->connect('*route',array('function'=>function($route){
            if (count(\Bingo\Configuration::$connections)==0) return true;
            if (!@$route['route']) return true;
            $slugs = explode("/",trim($route['route'],"/"));
            if (count($slugs)==0) return true;
            
            $parent = null;
            foreach ($slugs as $slug) {
                $post = \Blog\Models\Post::findOneBy(array('permalink'=>$slug,'parent'=>$parent,'hidden'=>false));
                if (!$post)
                    $post = \Blog\Models\Post::findOneBy(array('id'=>$slug,'parent'=>$parent,'hidden'=>false));
                if (!$post) {
                    if ($parent==null && $slug==\CMS\Models\Option::get('category_prefix')) {
                        $post = null;
                    } else {
                        return true;
                    }
                }
                $parent = $post;
            }
            $c = new \Blog\Controllers\Blog;
            \Bingo\Routing::$controller = \Bingo\Bingo::getInstance()->controller = $c;
            $c->post($post->id);
            return false;
        },'priority'=>100),array(),'blog_posts');

        \Blog\Configuration::registerPostType('page',
            array(
                'builtin'=> true,
                'label'=> _t('Pages','blog'),
                'singular_label' => _t('Page','blog'),
                'hierarchical' => 'page',
                'parent_label' => _t('Parent page','blog'),
                'show_tree' => true
            )
        );
        \Blog\Configuration::registerPostType('post',
            array(
                'builtin'=> true,
                'label'=> _t('Posts','blog'),
                'singular_label' => _t('Post','blog'),
                'hierarchical' => 'category',
                'parent_label' => _t('Category','blog')
            )
        );
        \Blog\Configuration::registerPostType('category',
            array(
                'builtin' => true,
                'label' => _t('Categories','blog'),
                'singular_label' => _t('Category','blog'),
                'hierarchical' => 'category',
                'parent_label' => _t('Parent category','blog'),
                'show_tree' => true
            )
        );

        \Bingo\Action::add('admin_pre_header',
        function () {
            $user = \Bingo\Routing::$controller->user;
            if ($user) {
                $themes = \Bingo\Template::getPath("/themes");
                $theme_dirs = $themes ? glob($themes."/*",GLOB_ONLYDIR) : array();
                if (count($theme_dirs))
                    \Admin::$menu[_t('System','cms')][_t('Appearance','blog')][_t('Themes','blog')] = 'admin/theme-list';
                
                \Admin::$menu[_t('Blog','blog')][_t('Options','blog')] = 'admin/post-options';
                foreach (\Blog\Configuration::$post_types as $type=>$args) {
                    if ($args['show_ui']) {
                        \Admin::$menu[$args['admin_menu']][$args['label']] = "admin/post-list/$type";
                    }
                }
            }
        });

        $theme = false;
        if (count(\Bingo\Configuration::$connections)) {
            try {
                $theme = \CMS\Models\Option::get('theme');
            }
            catch (\PDOException $e) {
                $theme = false;
            }
        }
        if (!$theme) $theme = "default";
        $file = \Bingo\Template::getPath("/themes/$theme/functions.php");
        if ($file) include $file;
    }
}