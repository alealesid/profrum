<?php

namespace Blog;

class Configuration {
    static $post_types = array();
    static function registerPostType($type,$args) {
        $defaults = array(
            'label'           => "$type list",
            'singular_label'  => "$type",
            'public'          => true,
            'show_ui'         => true,
            'builtin'         => false,
            'capability_type' => "post",
            'hierarchical'    => false,
            'parent_label'    => _t('Parent','blog'),
            'admin_menu'      => _t('Blog','blog'),
            'show_tree'       => false
        );
        Configuration::$post_types[$type] = array_merge($defaults,$args);
    }
}