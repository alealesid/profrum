<?php

namespace Blog\Models;

/**
* @Entity
* @Table(name="blog_comments")
*/
class Comment extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    /** @Column(length=100) */
    public $title;
    /** @Column(length=100) */
    public $author;
    /** @Column(length=100) */
    public $author_email;
    /** @Column(length=100) */
    public $author_url;
    /** @Column(type="text") */
    public $content;
    /** @Column(type="datetime") */
    public $date;
    /** @Column(type="object") */
    public $data;
    
    /**
     * @ManyToOne(targetEntity="Post", inversedBy="comments")
     * @JoinColumn(name="post_id", referencedColumnName="id",onDelete="CASCADE")
     */
    public $post;
    
    /**
     * @OneToMany(targetEntity="Comment", mappedBy="parent")
     */
    public $children;
    
    /**
     * @ManyToOne(targetEntity="Comment", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id",onDelete="SET NULL")
     */
    public $parent;    

    function __construct() {
        $this->date = new \DateTime("now");
    }
}
