<?php

namespace Blog\Models;

/** 
* @Entity 
* @Table(name="blog_posts")
*/
class Post extends \DoctrineExtensions\ActiveEntity\ActiveEntity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    /** @Column(length=1024) */
    protected $title;
    /** @Column(type="text") */
    protected $content;
    /** @Column(type="text") */
    protected $excerpt;
    /** @Column(length=256) */
    protected $permalink;
    
    /** @Column(type="datetime") */
    protected $date;
    
    /** @Column(type="datetime") */
    protected $modified;

    /**
     * @OneToMany(targetEntity="Comment", mappedBy="post")
     * @OrderBy({"date" = "DESC"})
     */
    protected $comments;

    /**
     * @OneToMany(targetEntity="Post", mappedBy="parent")
     * @OrderBy({"date" = "DESC"})
     */
    protected $children;    
    /**
     * @ManyToOne(targetEntity="Post", inversedBy="children")
     * @JoinColumn(name="parent_id", referencedColumnName="id",onDelete="SET NULL")
     */
    protected $parent;
    
    /** @Column(length=128) */
    protected $type;
    
    /** @Column(type="integer") */
    protected $item_order;
    
    
    /** @Column(type="array") */
    protected $data;
    
    /** @Column(type="boolean") */
    protected $hidden;
    

    function getTags() {
        return \Meta\Models\Tag::getTags($this);
    }

    function setTags($tags,$flush=true) {
        return \Meta\Models\Tag::setTags($this,$tags,$flush);
    }

    function __construct() {
        $this->type = "post";
        $this->data = array();
        $this->date = new \DateTime("now");
        $this->modified = $this->date;
        $this->item_order = 0;
        $this->content = "";
        $this->hidden = false;
        $this->excerpt = "";
    }

    function _closetags($html) {
        preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);

        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);

        for ($i=0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)){
                $html .= '</'.$openedtags[$i].'>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }

    function get_excerpt() {
        if ($this->excerpt) return $this->excerpt;
        $more = \explode("<!--more-->",$this->content);
        return $this->_closetags($more[0]);
    }

    function get_url() {
        $p = $this;
        $chain = "";
        $depth = 0;
        while ($p) {
            if ($depth++ > 20) break;
            $permalink = $p->getField('permalink') ?: $p->id;
            $chain = $permalink."/".$chain;            
            $p = $p->parent;
        }
        $ret = url(trim($chain,"/"));
        $ret = \Bingo\Action::filter('post_get_url',array($ret,$this));
        return $ret;
    }
    
    public function save($flush=true) {
        $this->modified = new \DateTime("now");
        if (@$this->_tags)
            foreach ($this->_tags as $tag) $tag->save(false);
        parent::save($flush);
    }
    
    function get_prev($parent) {
        $dql = "SELECT p FROM \Blog\Models\Post p 
                WHERE p.date < ?1 AND p.id<>?2 AND p.type=?3";
        if ($parent) $dql .= " AND p.parent =?4";
        $dql .= " ORDER BY p.date DESC";
                
        $query = Post::$entityManager
            ->createQuery($dql)
            ->setParameter(1,$this->date,\Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter(2,$this->id)
            ->setParameter(3,$this->type)
            ->setMaxResults(1);
            
        if ($parent) $query->setParameter(4,$this->parent);
        $res = $query->getResult();
        if (count($res)) return $res[0];
        return false;
    }
    function get_next($parent) {
        $dql = "SELECT p FROM \Blog\Models\Post p 
                WHERE p.date > ?1 AND p.id<>?2 AND p.type=?3";
        if ($parent) $dql .= " AND p.parent =?4";
        $dql .= " ORDER BY p.date ASC";
                
        $query = Post::$entityManager
            ->createQuery($dql)
            ->setParameter(1,$this->date,\Doctrine\DBAL\Types\Type::DATETIME)
            ->setParameter(2,$this->id)
            ->setParameter(3,$this->type)
            ->setMaxResults(1);
            
        if ($parent) $query->setParameter(4,$this->parent);
        $res = $query->getResult();
        if (count($res)) return $res[0];
        return false;
    }

    static function getList($type='post') {
        $post_type = \Blog\Configuration::$post_types[$type];
        if ($post_type['show_tree']) {
            $t = $type;
            $types = array($type);
            while (true) {
                $type = \Blog\Configuration::$post_types[$t]['hierarchical'];
                if (!$type || in_array($type,$types)) break;
                $types[] = $type;
            }            
            $qb = Post::$entityManager->createQueryBuilder();
            $qb->select('p');
            $qb->from('\Blog\Models\Post','p');
            $qb->andWhere($qb->expr()->in('p.type',$types));

            return $qb->getQuery()->getResult();
        } else {
            return \Blog\Models\Post::findByType($type);
        }
    }
    
    static function findByParentName($name,$orderBy,$from,$max) {
        if (!is_array($name)) $name = array($name);
        $cat = Post::findBy(array('permalink'=>$name));
        if (count($cat)) {
            return Post::findBy(array('parent'=>$cat),$orderBy,$from,$max);
        } else {
            return array();
        }
    }
    
    static function findByTag($tag) {
        if ($tag instanceof \Meta\Models\Tag) {
            $field = 'id';
            $value = $tag->id;
        } else {
            $field = 'value';
            $value = $tag;
        }
        return self::$entityManager
            ->createQuery("SELECT p FROM \Blog\Models\Post p
                           WHERE p.id in (select t.owner_id from \Meta\Models\Tag t 
                           WHERE t.$field=:value)")
            ->setParameter('value',$value)
            ->getResult();        
    }
    
    function toLiquid() {
        $res = array();
        foreach ($this as $key=>$val) {
            $res[$key] = $val;
        }
        foreach ($this->data as $key=>$val) {
            $res[$key] = $val;
        }
        
        $res['tags'] = $this->getTags();
        $res['comment_count'] = 0;
        $res['permalink'] = $this->get_url();
        return $res;
    }    
}
