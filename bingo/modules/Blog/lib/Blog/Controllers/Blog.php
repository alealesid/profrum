<?php

namespace Blog\Controllers;

class Blog extends \Bingo\Controller
{
    function __construct() {
        parent::__construct();
        theme_base();
        $this->data['is_home'] = false;
    }

    function _get_template($templates) {
        
        foreach ($templates as $t) {
            $ret = \Bingo\Template::get_include("blog/$t");
            if ($ret) return "blog/$t";
        }
        return false;
    }

    function error_404() {
        if ($this->_get_template(array('404'))) {
                header("HTTP/1.0 404 Not Found");
                header("HTTP/1.1 404 Not Found");
                header("Status: 404 Not Found");
                $this->view('blog/404',$this->data);
        } else
            return true;
    }

    function index() {
        $this->data['is_home'] = true;
        $this->data['post'] = false;
        $this->data['title'] = _t('Home','core');
        if (count(\Bingo\Configuration::$connections)) {
            $index_id = \CMS\Models\Option::get('index_page');
            if ($index_id) return $this->post($index_id);
        }
        $template_order = array('home');
        $tpl = $this->_get_template(array('home','index')); if (!$tpl) return true;
        $this->view($tpl,$this->data);
    }

    function paginate_children($post,$perPage=10) {
        $page = @$_GET['p'];
        if ($page<1) $page = 1;
        $query = \Blog\Models\Post::findByQuery(array('parent'=>$post),'date DESC');
        $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($query);
        $pagination = new \Bingo\Pagination($perPage,$page,$total,false,$query);
        return $pagination;
    }

    function post($id) {
        if ($id) $post = $this->em->find('Blog\Models\Post',$id);
        if (!$post) return true;
        
        \Bingo\Action::run('post_display',array($post,$this));

        $type = $post->type;
        $this->data['type'] = $type;
        $this->data['post'] = $post;
        $this->data[$type] = $post;
        $this->data['title'] = $post->title;
        $this->data['permalink'] = $post->permalink;

        if ($type=='category') {
            $post->posts = $post->children;
        }

        \Bingo\Action::add('post_content',array($this,'post_content_filter'),11,2);
        $post->content = \Bingo\Action::filter('post_content',array($post->content,$post));

        $template_order = array();
        $template_order[] = "$type-{$post->permalink}";
        $template_order[] = "$type-{$post->id}";
        if (isset($post->data['template']) && $post->data['template']) $template_order[] = 'template-'.$post->data['template'];
        if ($this->data['is_home']) $template_order[] = 'home';
        $template_order[] = $type;
        $template_order[] = 'index';

        $tpl = $this->_get_template($template_order); if (!$tpl) return true;
        
        $modified = clone($post->modified);
        $modified->setTimezone(new \DateTimeZone("GMT"));
        
        header("Last-Modified: ".$modified->format("D, d M Y H:i:s ")."GMT");
        $this->view($tpl,$this->data);
    }

    function post_content_filter($content,$post) {
        $shortcode_tags = \CMS::$shortcodes;
        if (empty($shortcode_tags)) return $content;
        return \CMS::do_shortcode($content,$post);
    }

    function rss() {
        $this->data['posts'] = $this->em
            ->createQuery(' SELECT p
                            FROM \Blog\Models\Post p
                            WHERE p.type<>?1 AND p.type<>?2
                            ORDER BY p.date DESC')
            ->setParameter(1,"page")
            ->setParameter(2,"category")
            ->setMaxResults(10)
            ->getResult();

        $last = $this->em
            ->createQuery(' SELECT p
                            FROM \Blog\Models\Post p
                            WHERE p.type<>?1 AND p.type<>?2
                            ORDER BY p.modified DESC')
            ->setParameter(1,"page")
            ->setParameter(2,"category")
            ->setMaxResults(1)
            ->getSingleResult();

        $this->data['last_modified'] = $last->modified;
        $tpl = $this->_get_template(array('rss')); if (!$tpl) return true;
        $this->view($tpl,$this->data);
    }

    function search() {
        $tpl = $this->_get_template(array('search')); if (!$tpl) return true;
        $s = @$_GET['s'];
        if ($s) {
            $query = $this->em->createQuery('
                  SELECT p
                  FROM \Blog\Models\Post p
                  WHERE p.title LIKE :s1
                  OR p.content LIKE :s2
                ')
                ->setParameter('s1',"%".$s."%")
                ->setParameter('s2',"%".$s."%");

            $list = $query->setMaxResults(20)->getResult();

        } else {
            $list = array();
        }

        $this->data['posts'] = $list;
        $this->data['s'] = $s;
        $this->data['title'] = _t('Search','blog');
        $this->view($tpl);
    }
}