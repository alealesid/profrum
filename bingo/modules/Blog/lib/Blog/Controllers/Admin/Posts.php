<?php

namespace Blog\Controllers\Admin;

class Posts extends \CMS\Controllers\Admin\BasePrivate
{
    function __construct() {
        parent::__construct();
        bingo_domain('blog');
    }
    
    function _get_post_parents($type,$init,$only_id=true) {
        if (!$type) $type = 'post';
        $post_type = \Blog\Configuration::$post_types[$type];
        $parents = false;
        if ($post_type['hierarchical']) {
            $parents = $init;
            $list = $this->treeSort(\Blog\Models\Post::getList($post_type['hierarchical']));
            foreach ($list as $el) {
                if ($el->permalink)
                    $parents[$el->tree_title." (".$el->permalink.")"] = $only_id ? $el->id : $el;
                else
                    $parents[$el->tree_title] = $only_id ? $el->id : $el;
            }
        }
        return $parents;
    }
    function post_list($type) {
        if (!$type) $type = 'post';

        $post_type = \Blog\Configuration::$post_types[$type];
        $parents = $this->_get_post_parents($type,array(_t("Don't change")=>-1, _t('None')=>0));

        $this->data['title'] = \Blog\Configuration::$post_types[$type]['label'];
        $this->data['page_actions']['admin/post-mass-edit/'.$type] = _t('Mass edit');
        $this->data['page_actions']['admin/post-edit/'.$type] = _t('Create new');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete('Blog\Models\Post','admin/post-list/'.$type)
        );
        $this->data['list_actions']['change'] = array(
            'title' => _t('Change'),
            'form' => \Bingo\Form::create()
                        ->select('parent',$post_type['parent_label'],$parents,obj_from_id('Blog\Models\Post'),-1),
            'function' => function ($list,$action) use ($type) {
                $form = $action['form'];
                if ($form->validate()) {
                    $values = $form->getValue();
                    foreach ($list as $id=>$data) {
                        if ($data['check']) {
                            $obj = \Blog\Models\Post::find($id);
                            if ($obj && $obj!=$values['parent']) {
                                $obj->parent = $values['parent'];
                                $obj->save(false);
                            }
                        }
                    }
                    \Bingo\Bingo::getInstance()->em->flush();
                }
                redirect('admin/post-list/'.$type);
            }
        );

        $this->data['fields'] = array('id'=>_t('#'),'title'=>_t('title'),'parent'=>_t('parent'));
        $this->data['field_filters']['parent'] = function ($val) { return $val ? $val->getField('title'):''; };
        $this->data['field_filters']['title'] = function ($val,$obj) {
            return "<a href='".url("admin/post-edit/$obj->id")."'>$obj->title</a><br><em>$obj->permalink</em>";
        };

        if ($post_type['show_tree']) {
            $list = \Blog\Models\Post::getList($type);
            $this->data['list'] = $this->treeSort($list,'parent','title','item_order');
            foreach ($list as $obj) $obj->parent_id = $obj->parent ? $obj->parent->getField('id') : 0;
            $this->data['tree_parent_field'] = 'parent_id';
            $this->data['tree_column'] = 'title';
            unset($this->data['fields']['parent']);
            $this->data['list_actions'][0] = array(
                'form' => \Bingo\Form::create()->text('item_order',false),
                'function' => $this->action_edit('\Blog\Models\Post','admin/post-list/'.$type,false)
            );
            $this->data['fields']['item_order'] = _t('order');
        } else {
            $filter_form = new \CMS\FilterForm;
            $parents_for_filter = $this->_get_post_parents($type,array(_t("All")=>-1, _t('None')=>0));
            $filter_form->select('parent',false,$parents_for_filter,'');
            $this->data['filter_form'] = $filter_form;
            $this->data['filter_autosubmit'] = true;

            $filter = array('type'=>$type);
            if ($filter_form->validate()) {
                if ($filter_form->values['parent']!=-1) {
                    $filter['parent'] = $filter_form->values['parent'];
                    if (!$filter['parent']) $filter['parent'] = null;
                }
            }
            $query = \Blog\Models\Post::findByQuery($filter,'date DESC');
            $query = \Bingo\Action::filter("posts_query",array($query,$filter));
            $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($query);
            $pagination = new \Bingo\Pagination(20,$this->getPage(),$total,false,$query);
            $this->data['pagination'] = $pagination->get();
            $this->data['list'] = $pagination->result();
            $this->data['fields']['date'] = _t('date-time');
            $this->data['field_filters']['date'] = function ($val) { return $val->format("d.m.Y H:i"); };
        }
        
        $this->data = \Bingo\Action::filter("posts_view_data",array($this->data));
        $this->view('cms/base-list',$this->data);
    }

    function post_mass_edit($type) {
        if (!$type) $type = 'post';

        $list = \Blog\Models\Post::treeSort(array('type'=>$type));
        $array = array();
        foreach ($list as $post) {
            $array[$post->id] = array(
                'title' => $post->title,
                'permalink' => $post->permalink,
                'parent' => $post->parent ? $post->parent->getField('id') : 0,
                'prefix' => str_repeat("&mdash;",$post->tree_depth)
            );
        }

        $post_type = \Blog\Configuration::$post_types[$type];
        $parents = $this->_get_post_parents($type,array(_t('None')=>0));

        $editForm = new \Bingo\Form('post','autocomplete=off');
        $editForm
            ->html("{value}",array('name'=>'prefix'))
            ->text('title',_t('Title'))
            ->text('permalink',_t('Permalink'));
        if ($parents)
            $editForm->select('parent',$post_type['parent_label'],$parents);

        $form = new \Bingo\Form;
        
        $form->html("<style>div.form th.item_parent {width:250px;}</style>");
        $form->form_list('posts',$editForm,$array,true);
        $form->submit(_t('Save posts'));

        $this->data['title'] = _t('Post mass edit');
        $this->data['form'] = $form->get();

        if (!empty($_POST)) {
            $posts = $_POST['posts'];
            foreach ($posts as $id=>$post) {
                $obj = \Blog\Models\Post::find($id);
                if (!$obj) {
                    $obj = new \Blog\Models\Post;
                    $obj->content = $obj->excerpt = "";
                    $obj->type = $type;
                }
                if (!@$post['title']) continue;

                $obj->title = $post['title'];
                $obj->permalink = $post['permalink'];
                $obj->parent = $post['parent'] ? \Blog\Models\Post::find($post['parent']) : null;
                $obj->save(false);
            }
            $this->em->flush();

            set_flash('info',_t('Successfully saved'));
            redirect('admin/post-mass-edit/'.$type);
        }

        $this->view('cms/base-edit-tinymce',$this->data);
    }

    function post_edit($id) {
        $this->data['title'] = _t('Post');

		$post = false;
        if ($id) $post = $this->em->find('Blog\Models\Post',$id);
        if (!$post) {
            $post = new \Blog\Models\Post;
            if ($id && !is_int($id))
                $post->type = $id;
            else
                $post->type = 'post';
        }

        if (\Bingo\Action::run('edit_post',array($post))) return;

        $this->data['page_actions']['admin/post-edit/'.$post->type] = _t('Create another');

        $post_type = \Blog\Configuration::$post_types[$post->type];
        $parents = $this->_get_post_parents($post->type,array(_t('None')=>0));
        if ($post->id) unset($parents[$post->id]);

        $form = new \Bingo\Form('post','autocomplete=off');
        
        $all_tags = \Meta\Models\Tag::getTags(false,get_class($post));
        $form->html("<script>var all_tags = ".json_encode($all_tags).";</script>");
        
        $form->fieldset(_t('Publish'))->add_class('second-column');
        $form->select('hidden',_t('Status'),array(
            _t('Published') => false,
            _t('Hidden') => true
        ),'',$post->hidden);
        $form->text('date',_t('Date-time'),'',$post->date)->add_filter(new \Bingo\FormFilter_DateTime('d.m.Y H:i'))->add_class('datetime');
        $form->submit(_t('Publish'));
        
        $form->fieldset(_t('Tags'))->add_class('second-column');
        $form->text('tags',false,'',join(", ",$post->getTags()))->add_class("post_tags");
        
        $parent = $post->parent ? $post->parent->getField('id') : 0;
        if ($parents) {
            $form->fieldset($post_type['parent_label'])->add_class('second-column');
            $form->select('parent',false,$parents,obj_from_id('Blog\Models\Post'),$parent);
        }

        {
            $files = glob(INDEX_DIR."/template/themes/default/core/template-*.php");
            $templates = array(0=>_t('No template'));
            if ($files) foreach ($files as $f) {
                if (is_file($f)) {
                    $t = str_replace("template-","",basename($f,'.php'));
                    $templates[$t] = $t;
                }
            }
            $templates = \Bingo\Action::filter('blog_templates',array($templates));
            if (count($templates)>1) {
                $form->fieldset(_t('Template'))->add_class('second-column');
                $form->select('template',false,$templates,'',
                    isset($post->data['template'])?$post->data['template']:false);
            } else {
                $post->template = false;
            }
        }
        
        $form->fieldset(false,false,array('name'=>'main'))->add_class('first-column');
        $form->text('title',_t('Title'),'',$post->title);
        $form->text('permalink',_t('Permalink'),'',$post->permalink);
        $form
            ->textarea('content',_t('Content'),'',$post->content,array('rows'=>30))->add_class("tinymce")
            ->textarea('excerpt',_t('Excerpt'),'',$post->excerpt,' rows=5"')->add_class("tinymce");

        \Bingo\Action::run('add_meta_box',array($post,$form,$this));
        
        if ($form->validate()) {
            $data = $form->getValue();
            $form->fill($post);
            $post->permalink = trim($post->permalink);
            $post->data['template'] = $post->template;

            \Bingo\Action::run('save_post',array($post,$data));
            $post->save();
            \Bingo\Action::run('post_saved',array($post,$data));
            
            $tags_data = explode(",",$data['tags']);
            $tags = array();
            foreach ($tags_data as $tag) {
                $tag = trim($tag);
                if ($tag) $tags[] = $tag;
            }
            $post->setTags($tags);
            
            set_flash('info',_t('Successfully saved'));
            return redirect("admin/post-edit/$post->id");
        } else {
            if (!empty($_POST))
                $this->data['error_message'] = _t('Save failed - there are validation errors below');
        }

        $this->data['title'] = $post_type['singular_label'];
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce',$this->data);
    }
    function post_options() {
        $form = \Bingo\Form::create("post",array('autocomplete'=>'off'))
            ->select('index_page',_t('Index page'),$this->_get_post_parents('page',array(_t('Empty')=>0)),
                '',\CMS\Models\Option::get('index_page'))
            ->text('category_prefix',_t('Category prefix'),'',\CMS\Models\Option::get('category_prefix'))
            ->submit(_t('Save options'));

        if ($form->validate()) {
            $this->data['message'] = _t('Successfully saved');
            $values = $form->getValue();
            \CMS\Models\Option::set('index_page',$values['index_page']);
            \CMS\Models\Option::set('category_prefix',$values['category_prefix']);
        }
        $this->data['title'] = _t('Blog options');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }
}
