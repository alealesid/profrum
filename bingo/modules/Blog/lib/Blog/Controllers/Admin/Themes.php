<?php

namespace Blog\Controllers\Admin;

class Themes extends \CMS\Controllers\Admin\BasePrivate
{
    function __construct() {
        parent::__construct();
        bingo_domain('blog');
    }

    function theme_list() {
        $this->data['themes'] = array();
        $themes = \Bingo\Template::getPath("/themes");
        $dirs = $themes ? glob($themes."/*",GLOB_ONLYDIR) : array();
        
        $form = new \Bingo\Form;

        foreach ($dirs as $dir) {
            $name = basename($dir);
            $form->fieldset($name)->add_class("theme");

            if ($name==\CMS\Models\Option::get('theme')) {
                $form->add_class('current');
            }

            if (file_exists($dir."/screenshot.png")) {
                $url = url(\CMS\Configuration::$theme_path."/$name/screenshot.png");
            } else {
                $url = t_url("cms/style/images/no_screenshot.png");
            }
            $form->html("<img src='$url'>");
            $form->button("theme",_t("Set theme current"),"submit",$name)->add_class("single");
        }

        if ($form->validate()) {
            \CMS\Models\Option::set('theme',$form->values['theme']);
            redirect('admin/theme-list');
        }

        $this->data['title'] = _t("Theme list");
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }

    function insertWidget($form,$type,$type_name,$name_mask="{name}",$wdata=false) {
        $form->fieldset($type->getLabel(),true)->add_class("item");
        $form->html("<div class='description'>".nl2br($type->getDescription())."</div>");
        $form->html("<div class='elements'>");
        foreach ($type->getForm()->elements as $el) {
            $ins = clone($el);
            if ($wdata) {
                $val = @$wdata[$el->name];
                if ($ins->name)
                    $ins->name = str_replace("{name}",$el->name,$name_mask);
                $ins->value = $val;
            }
            $form->add($ins);
        }
        $form->hidden(str_replace("{name}","widget_type",$name_mask),$type_name);
        $form->html("<a href='#' class='array_element_del'>"._t("Delete")."</a>");
        $form->html("</div>");
        $form->close_fieldset();
    }
}