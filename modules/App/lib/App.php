<?php

function _h($value) {
     if (is_string($value))
        return nl2br(htmlspecialchars($value,ENT_QUOTES));
    else
        return $value;
}

class App extends \Bingo\Module {
    function __construct() {
        parent::__construct();
        
        \Bingo\Config::loadFile('config',INDEX_DIR."/config.php");
        
        $this->addModelPath(__DIR__."/App/Models");

        $this->connect("/", ['controller' => 'App\Controllers\Main', 'action' => 'main']);
        $this->connect(":action", ['controller' => 'App\Controllers\Auth'],
            ['action'=>'(login|logout|signup|social-network-auth|forgot-password|change-password)']
        );
        $this->connect(":action/:id", ['controller' => 'App\Controllers\Journal', 'id' => false],
            ['action'=>'(journal|journal-list|journal-table-list)']
        );
        $this->connect(":action/:journal_id/:review_id", ['controller' => 'App\Controllers\JournalReview', 'journal_id' => false, 'review_id' => false],
            ['action'=>'(journal-review-edit|journal-review-list|journal-review-list-partial|journal-review-list-user-partial)']
        );
        $this->connect("journal-review/:review_id", ['controller' => 'App\Controllers\JournalReview', 'action' => 'journal-review', 'review_id' => false]);
        $this->connect(":action/:conference_id/:review_id", ['controller' => 'App\Controllers\ConferenceReview', 'conference_id' => false, 'review_id' => false],
            ['action'=>'(conference-review-edit|conference-review-list|conference-review-list-partial|conference-review-list-user-partial)']
        );
        $this->connect("conference-review/:review_id", ['controller' => 'App\Controllers\ConferenceReview', 'action' => 'conference-review', 'review_id' => false]);
        $this->connect(":action/:owner_type/:owner_id/:question_id", ['controller' => 'App\Controllers\Question', 'owner_type' => false, 'owner_id' => false, 'question_id' => false], 
            ['action'=>'(question-edit|question-list|question-list-partial)']
        );
        $this->connect(":action/:id", ['controller' => 'App\Controllers\Question', 'id' => false], 
            ['action'=>'(question-user-list-partial)']
        );
        $this->connect("question/:question_id", ['controller' => 'App\Controllers\Question', 'action' => 'question', 'question_id' => false]);
        $this->connect(":action/:owner_type/:owner_id/:comment_id", ['controller' => 'App\Controllers\Comment', 'owner_type' => false, 'owner_id' => false, 'comment_id' => false],
            ['action'=>'(comment-edit|comment-list)']
        );
        $this->connect(":action/:owner_type/:owner_id", ['controller' => 'App\Controllers\Like', 'owner_type' => false, 'owner_id' => false],
            ['action'=>'like']
        );
        $this->connect(":action/:id", ['controller' => 'App\Controllers\User', 'id' => false],
            ['action'=>'(profile|upload-avatar|remove-avatar)']
        );
        $this->connect('admin/app/:action/:id', ['controller' => 'App\Controllers\Admin\Users', 'id' => false],
            ['action'=>'(user-list|user-edit)']
        );
        $this->connect('admin/app/:action/:id', ['controller' => 'App\Controllers\Admin\Journal', 'id' => false],
            ['action'=>'(journal-list|journal-edit|journal-join)']
        );
        $this->connect('admin/app/:action/:id', ['controller' => 'App\Controllers\Admin\Category', 'id' => false],
            ['action'=>'(category-list|category-edit)']
        );
        $this->connect(":action/:id", ['controller' => 'App\Controllers\Conference', 'id' => false],
            ['action'=>'(conference|conference-list)']
        );
        $this->connect('admin/app/:action/:id', ['controller' => 'App\Controllers\Admin\Conference', 'id' => false],
            ['action'=>'(conference-list|conference-edit|conference-join)']
        );
        $this->connect("feedback/:action", ['controller' => 'App\Controllers\Feedback'],
            ['action' => '(journal|conference)']
        );
        $this->connect("admin/app/feedback/:action", ['controller' => 'App\Controllers\Admin\Feedback'],
            ['action' => '(journal-list|conference-list)']
        );
        $this->connect(":action/:id", ['controller' => 'App\Controllers\IMHOTop', 'id' => false],
            ['action'=>'(standart-imhotop-edit|nonstandart-imhotop-edit|imhotop-delete|standart-imhotop|nonstandart-imhotop)'] 

        );
        $this->connect(":action", ['controller' => 'App\Controllers\IMHOTop'], 
            ['action'=>'(standart-imhotop-top10|imhotop-list|my-nonstandart-imhotops-list|my-standart-imhotops-list)'] 
        );
        $this->connect("admin/app/imhotop/:action", ['controller' => 'App\Controllers\Admin\IMHOTop'],
            ['action' => '(imhotop-list)']
        ); 
        $this->connect("admin/app/:action", ['controller' => 'App\Controllers\Admin\StaticPage'],
            ['action' => '(faq-edit|about-edit|rules-edit|page404-edit|page500-edit)']
        ); 
        $this->connect(":action", ['controller' => 'App\Controllers\StaticPage'],
            [ 'action' => '(faq|rules|about|page404|page500)']
        );
        $this->connect("admin/app/review/:action/:type", ['controller' => 'App\Controllers\Admin\Review'],
            ['action' => '(review-list)']
        ); 
        
        $this->connect("admin/:action/:id", ['priority' => 1, 'controller' => 'App\Controllers\Admin\CMSUsers', 'id' => false], 
            ['action'=>'(user-edit)'] 
        );
        $this->connect("admin/app/question/:action", ['controller' => 'App\Controllers\Admin\Question'],
            ['action' => '(question-list)']
        );
        $this->connect("*error404", [ 'priority'=>999, 'controller' => 'App\Controllers\StaticPage', 'action'=>'page404' ]);
        
        $this->connect("autocomplete", ['controller' => 'App\Controllers\Autocomplete', 'action'=>'autocomplete']);

        \CMS\Configuration::$user_class = \App\Models\CMSUser::class;
        
        \Bingo\Action::add('admin_pre_header', function () {

            $user_class = \CMS\Configuration::$user_class;
            $user = $user_class::checkLoggedIn();
            
            \Admin::$menu[_t('Списки')][_t('Пользователи')] = 'admin/app/user-list';
            \Admin::$menu[_t('Списки')][_t('Журналы')] = 'admin/app/journal-list';
            \Admin::$menu[_t('Списки')][_t('Категории')] = 'admin/app/category-list';
            \Admin::$menu[_t('Списки')][_t('Конференции')] = 'admin/app/conference-list';
            \Admin::$menu[_t('Списки')][_t('Отзывы по журналам')] = 'admin/app/review/review-list/journal-review';
            \Admin::$menu[_t('Списки')][_t('Отзывы по конференциям')] = 'admin/app/review/review-list/conference-review';
            \Admin::$menu[_t('Списки')][_t('Вопросы')] = 'admin/app/question/question-list';
            \Admin::$menu[_t('Списки')][_t('Запросы на журналы')] = 'admin/app/feedback/journal-list';
            \Admin::$menu[_t('Списки')][_t('Запросы на конференции')] = 'admin/app/feedback/conference-list';
            \Admin::$menu[_t('Списки')][_t('Топы')] = 'admin/app/imhotop/imhotop-list';
            if ($user && $user->hasRole(\App\Models\CMSUser::ROLE_ADMIN)) { 
                \Admin::$menu[_t('Статичные страницы')][_t('FAQ')] = 'admin/app/faq-edit';
                \Admin::$menu[_t('Статичные страницы')][_t('О проекте')] = 'admin/app/about-edit';
                \Admin::$menu[_t('Статичные страницы')][_t('Правила')] = 'admin/app/rules-edit';
                \Admin::$menu[_t('Статичные страницы')][_t('Страница 404')] = 'admin/app/page404-edit';
                \Admin::$menu[_t('Статичные страницы')][_t('Страница 500')] = 'admin/app/page500-edit';
            }
        }, $priority = 1);

        \Bingo\Action::add('admin_header',function () {
            echo '<script src="'.url('assets/js/autocomplete.js').'"></script>';
        });

        \Form::extend('slider', function ($form, $name, $label=false, $rules='', $value=false, $min=0, $max=100, $atts=false) {
            $form->add(new \App\Forms\Elements\FormElement_Slider(compact('name','label','rules','value','min','max','atts')));
            return $form;
        });
        
        \Form::extend('attribute_text', function ($form, $name,$label=false,$rules='',$value=false,$atts=false) {
            $form->add(new \App\Forms\Elements\AttributeText(compact('name','label','rules','value','atts')));
            return $form;
        });

        \Bingo::$em->getEventManager()->addEventSubscriber(
            new \App\Models\EventSubscribers\CommentDeleteSubscriber()
        );
        
        \Bingo::$em->getEventManager()->addEventSubscriber( 
            new \App\Models\EventSubscribers\RefreshResearchAuthorRating() 
        ); 
    }
}