<?php

namespace App\Controllers;

class ConferenceReview extends Review {

    protected $review_class = \App\Models\ConferenceReview::class;
    protected $parent_class = \App\Models\Conference::class;
    protected $name = 'conference';
    
    public function conference_review_edit($conference_id, $review_id) {

        if (isset($_GET['redirect'])) {
            $transitUrl = urldecode($_GET['redirect']) . '?' . http_build_query([
                'redirect' => urlencode(\Bingo\Routing::$uri),
                'stash_token' => $this->stashFormData()
            ]);
            redirect($transitUrl);
        }

        $conference = \App\Models\Conference::find($conference_id);
        
        $review = \App\Models\ConferenceReview::findOrCreate($review_id);
        if (!$review->moderated) return true;
        if ($conference) $review->conference = $conference;
        $form = new \App\Forms\ConferenceReview('post', ['class' => $review->article_status], $review);
        if ($form->validate($this->getStashedFormData())) {  
            $form->fill($review);
            $review->user = $this->user ? $this->user : NULL;
            if (!$review->user) {
                $review->anonymous = true;
                $review->moderated = false;
            }
            $review->save();
        
            if ($review->user) { 
                set_flash('info',_t('Спасибо за ваш отзыв!')); 
                $redirect_uri = 'conference-review/'.$review->id;
            } else { 
                set_flash('info',_t('Cпасибо, ваш отзыв отправлен на модерацию')); 
                $redirect_uri = 'conference/'.$review->conference->id; 
            } 
            
            redirect($redirect_uri);
        }

        $this->data['title'] = _t('Отзыв к конференции');
        $this->data['review_form'] = $form; 
        $this->data['review'] = $review; 
        $this->data['conference'] = $conference; 
        $this->view('conference_review_edit');

    }
    
    public function conference_review($id) {
        $this->data['shareLink'] = url('conference-review/'.$id);
        $this->review($id, 'conference_review');
    }
    
    protected function conference_review_list_base($conference_id, $view) {  
        $criteria = [];
        if ($conference_id) {
            $conference = \App\Models\Conference::find($conference_id);
            if (!$conference) return true;
            $criteria['conference'] = $conference;
        }
        $this->review_list($criteria, $view);
    }
    
    public function conference_review_list($conference_id) { 
        $this->data['title'] = _t('Список отзывов');
        $this->conference_review_list_base($conference_id, 'review_list');
    }
    
    public function conference_review_list_partial($conference_id) { 
        $this->conference_review_list_base($conference_id, 'partial/review_list');
    }
    
    public function conference_review_list_user_partial($user_id) { 
        $criteria = [];
        if ($user_id) {
            $user = \App\Models\User::find($user_id);
            if (!$user) return true;
            $criteria['user'] = $user;
            if ($user != $this->user) $criteria['anonymous'] = false;
        }
        $this->review_list($criteria, 'partial/review_list', true);
    }
}