<?php

namespace App\Controllers;

class Question extends Base {
    
    public function question_edit($ownerType, $ownerId, $id) { 

        if (isset($_GET['redirect'])) {
            $transitUrl = urldecode($_GET['redirect']) . '?' . http_build_query([
                'redirect' => urlencode(\Bingo\Routing::$uri),
                'stash_token' => $this->stashFormData()
            ]);
            redirect($transitUrl);
        }
        
        $ownerTypes = \App\Models\Question::getOwnerTypes();
        if (!isset($ownerTypes[$ownerType])) return true;

        $ownerClass = $ownerTypes[$ownerType];
        $owner = $ownerClass::find($ownerId);
        if (!$owner) return true;
        
        $question = \App\Models\Question::findOneBy(['user' => $this->user, 'id' => $id]);
        if (!$question) {
            $question = new \App\Models\Question;
        }
        $question->setOwner($owner);
        
        $form = new \Form('post');
        $form->textarea('text', _t('Задайте свой вопрос'), 'required', $question->text, ['class'=>'form-control']);
        
        if ($form->validate($this->getStashedFormData())) {
            $form->fill($question);
            $question->user = $this->user ?: null;
            if (!$question->user) {
                $question->anonymous = true;
                $question->moderated = false;
            }
            $question->save();
        
            set_flash('info',_t('Изменения сохранены успешно'));
            
            redirect($question->getOwnerUrl());
        }
        
        $this->data['form'] = $form;
        $this->data['question'] = $question;
        $this->view('question_edit');
    }
    
    public function question($id) {

        $question = \App\Models\Question::find($id);
        if (!$question || ($question->anonymous && !$question->moderated)) return true;
        $this->data['title'] = ( $question->owner_class == \App\Models\Journal::class ? _t('Вопрос о журнале') : _t('Вопрос о конференции') ) . ' ' . $question->getOwner()->name;
        $this->data['comment_form'] = new \App\Forms\Comment('post', [], $question);
        $this->data['question'] = $question; 
        $this->view('question');
    }

    public function question_list($ownerType, $ownerId) { 
        $this->data['title'] = _t('Список вопросов');
        $this->question_owner_list($ownerType, $ownerId, 'question_list');
    }

    public function question_list_partial($ownerType, $ownerId) { 
        $this->question_owner_list($ownerType, $ownerId, 'partial/question_list');
    }
    
    private function question_owner_list($ownerType, $ownerId, $view) { 
        
        $ownerTypes = \App\Models\Question::getOwnerTypes();
        if (!isset($ownerTypes[$ownerType])) return true;
            $ownerClass = $ownerTypes[$ownerType];

        $qb = $this->em->createQueryBuilder()
            ->where('q.owner_class = :oclass')
            ->andWhere('q.moderated = :moderated')
            ->setParameters([
                ':oclass' => $ownerClass,
                ':moderated' => true
            ]);

        if ($ownerId) {
            $owner = $ownerClass::find($ownerId);
            if (!$owner) return true;
            $qb->andWhere('q.owner_id = :oid')
            ->setParameter(':oid', $ownerId);
        }

        return $this->question_query_list($qb, $view);
    }
    
    public function question_user_list_partial($userId) { 
        $user = \App\Models\User::find($userId);
        if (!$user) return true;

        $qb = $this->em->createQueryBuilder()
            ->where('q.user = :user')
            ->setParameter(':user', $user);

        return $this->question_query_list($qb, 'partial/question_list');
    }
    
    private function question_query_list($qb, $view) { 
        $qb->select('DISTINCT q')
            ->from(\App\Models\Question::class, 'q')
            ->orderBy('q.created', 'DESC');

        $filterForm = new \CMS\FilterForm();
        $filterForm->checkbox('answered', _t('с ответом'), '', false);
        $filterForm->checkbox('unanswered', _t('без ответа'), '', false);

        if ($filterForm->validate()) {
            if (!($filterForm->values['unanswered'] && $filterForm->values['answered'])) {
                $qb->leftJoin(\App\Models\Comment::class, 'c', 'WITH', 'c.owner_class = :owner_class AND c.owner_id = q.id');
                $qb->setParameter(':owner_class', \App\Models\Question::class);

                if ($filterForm->values['unanswered'])
                     $qb->andWhere('c.id IS NULL');
                if ($filterForm->values['answered'])
                    $qb->andWhere('c.id IS NOT NULL');  
            }
        }

        $query = $qb->getQuery();
        $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($query, $distinct = true);
        $pagination = new \Bingo\Pagination(5, $this->getPage(), $total, $pattern = false, $query);
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(5);
        $this->data['questions'] = $pagination->result();
        $this->view($view);
    }

    protected function stashFormData() {
        $sessionName = 'question'.uniqid();
        $stashFormSession = new \Session\SessionNamespace($sessionName);
        $stashFormSession->setExpirationSeconds(24*60*60);
        $stashFormSession->active = true;
        $stashFormSession->formData = $_POST;

        return $sessionName;
    }

    protected function getStashedFormData() {
        if (isset($_GET['stash_token'])) {
            $stashFormSession = new \Session\SessionNamespace($_GET['stash_token']);
            if ($stashFormSession->active) {
                $stashFormSession->active = false;
                return $stashFormSession->formData;
            }
        }
        
        return false;
    }
}