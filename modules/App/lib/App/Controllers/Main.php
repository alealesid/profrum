<?php

namespace App\Controllers;

class Main extends Base {
    
    public function main() {        
        
        $filterForm = new \CMS\FilterForm;
        $filterForm->attribute_text('name',false,false,false,[
            "class"=>"autocomplete form-control", 
            "url"=> url('autocomplete?entities=journal,conference'),
            "placeholder" => _t("Найти журнал или конференцию")
        ]);

        $search_params = [];
        $journals = [];
        $conferences = [];

        if ($filterForm->validate()) {
            $name = trim($filterForm->values['name']);
            if ($name) {
                $search_params['name'] = $name;

                $journals = \App\Models\Journal::findBy(['name' => 'LIKE %'.$name.'%'], 'positive_review_count DESC', 0, 6); 
                $conferences = \App\Models\Conference::findBy(['name' => 'LIKE %'.$name.'%'], 'positive_review_count DESC', 0, 6);
            }           
        }
        
        $this->data['search_params'] = $search_params;
        $this->data['journals'] = $journals;
        $this->data['conferences'] = $conferences;
        $this->data['filter_form'] = $filterForm;
        $this->data['journal_reviews'] = \App\Models\JournalReview::findBy(['moderated' => true], 'created DESC', 0, 3);
        $this->data['conference_reviews'] = \App\Models\ConferenceReview::findBy(['moderated' => true], 'created DESC', 0, 3);
        $this->data['journal_questions'] = \App\Models\Question::findBy(['owner_class'=>\App\Models\Journal::class], 'created DESC', 0, 3);
        $this->data['conference_questions'] = \App\Models\Question::findBy(['owner_class'=>\App\Models\Conference::class], 'created DESC', 0, 3);
        $this->data['title'] = _t('Главная - Professional Rumors');
        $this->view('main');

    }

    private function existsResearchWithName($name, $class) {
         return $this->em->createQueryBuilder()
            ->select('count(research)')
            ->from($class, 'research')
            ->where('research.name LIKE :name')
            ->setParameter('name', '%'.$name.'%')
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }
}