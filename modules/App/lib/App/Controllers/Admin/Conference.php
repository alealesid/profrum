<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Conference extends BasePrivate {
    
    function conference_list() {   

        $filterForm = new \CMS\FilterForm;
        $filterForm
            ->text('id')
            ->text('name')
            ->text('location')
            ->text_date('start', '')
            ->text_date('finish', '')
            ->text_date('created', '')
        ;
        
        $criteria = [];
        if ($filterForm->validate()) {
            $id = trim($filterForm->values['id']);
            if ($id) $criteria['id'] = $id; 
            
            $name = trim($filterForm->values['name']);
            if ($name) $criteria['name'] = "LIKE %".$name."%";
            
            $start = $filterForm->values['start'];
            if ($start) $criteria['start >= ?'] = $start;

            $finish = $filterForm->values['finish'];
            if ($finish) $criteria['finish >= ?'] = $finish;
            
            $location = trim($filterForm->values['location']);
            if ($location) $criteria['location'] = "LIKE %".$location."%";
            
            $created = $filterForm->values['created'];
            if ($created) $criteria['created >= ?'] = $created;
            
        }
        
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        
        $query = \App\Models\Conference::findByQuery($criteria, "$sort_by $sort_order");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();

        $this->data['fields'] = [
            'id' => _t('ID'), 
            'name' => _t('Название'),
            'start' =>  _t('Начало'),
            'finish' =>  _t('Конец'),
            'location' => _t('Место проведения'),
            'created' => _t('Дата добавления')
        ];

        $_h = function ($value) { return _h($value); };
        $this->data['field_filters'] = [
            'name' => $_h,
            'location' => $_h
        ]; 
        
        $this->data['sort_fields'] = ['id', 'name', 'location', 'start', 'finish', 'created'];        
        $this->data['page_actions']['admin/app/conference-edit'] = _t('Создать новую');
        $this->data['item_actions']['admin/app/conference-edit'] = _t('редактировать');

        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {  
            $this->data['list_actions'] = [
                'delete' => [
                    'title' => _t('Удалить конференции'),
                    'function' => $this->action_delete(\App\Models\Conference::class)
                ]
            ];
        }
        
        $this->data['title'] = _t('Список конференций');
        $this->view('cms/base-list');
    }
    
    function conference_edit($id) { 
             
        $conference = \App\Models\Conference::findOrCreate($id);
        $conferencesOptions = \App\Models\Category::selectOptions([], 'name', ['' => NULL]); 
        unset($conferencesOptions[$conference->name]);
        
        $form = new \Form('post');
        $form->fieldset(_t('Данные конференции'));
        $form->text('name', _t('Название'), 'required', $conference->name);
        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {  
            $form->html('<a href="'.url('admin/app/conference-join/'.$conference->id).'">'._t('Объединить с ...').'</a>');
        }
        $form->text('location', _t('Место проведения'), 'required', $conference->location);
        $form->text('site', _t('Официальный сайт'), '', $conference->site);
        $form->text_date('start', _t('Начало проведения'), 'required', $conference->start);
        $form->text_date('finish', _t('Конец проведения'), [ 'required', function($finish) use ($form) { 
            if ($finish->diff($form->values['start']) && (int)$finish->diff($form->values['start'])->format("%r%a") > 0) 
                throw new \ValidationException(_t('Конференция не могла закончится раньше, чем началась')); 
            return $finish; 
        } ] , $conference->finish); 
        $form->textarea('description', _t('Описание'), '', $conference->description, ['class' => 'tinymce', 'rows'=>7]);
        $form->checkbox('hidden', _t('Скрыть с сайта'), '', $conference->hidden);


        $allCategories = [];
        $categoryOptions = [];

        $queriedCategories = \App\Models\Category::findBy( [], 'name ASC' );
        foreach($queriedCategories as $category) {
            $allCategories[$category->id] = $category;
            $categoryOptions[$category->name] = $category->id;
        }

        $categoryForm = new \Form();
        $categoryForm->select('id',  _t('Категория'), $categoryOptions, ['required', function ($catId) use ($allCategories)  {
            if (!isset($allCategories[$catId])) 
                throw new \ValidationException(_t('Несуществующая категория'));
            return $catId;
        }]);
        
        $conferenceValues = array_map(function ($category) {
            return ['id'=>$category->id];
        }, $conference->categories->toArray());

        $form->form_list('categories', $categoryForm, '', $conferenceValues, true, $atts=false);
        
        $form->fieldset(_t('Регулярные конференции'));
        $form->attribute_text('prev', _t('Предыдущая конференция'), [function ($prevConference) use ($conference)  {
            if ($prevConference && $conference->id == $prevConference->id) 
                throw new \ValidationException(_t('Вы указали текущую конференцию как предыдущую'));
            return $prevConference;
        }], $conference->prev, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=conference'),])
            ->object_filter(\App\Models\Conference::class, 'name');
        $form->attribute_text('next', _t('Следующая конференция'), [function ($nextConference) use ($conference)  {
            if ($nextConference && $conference->id == $nextConference->id) 
                throw new \ValidationException(_t('Вы указали текущую конференцию как следующую'));
            return $nextConference;
        }], $conference->next, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=conference'),])
            ->object_filter(\App\Models\Conference::class, 'name');


        $form->submit(_t('Сохранить'));
        
        if ($form->validate()) {
            if (is_object($oldPrev = $conference->prev) && $oldPrev != $form->values['prev']) {
                $oldPrev->next = NULL;
                $oldPrev->save();
            }
            if (is_object($oldNext = $conference->next) && $oldNext != $form->values['next']) {
                $oldNext->prev = NULL;
                $oldNext->save();
            }

            $form->fill($conference);

            if ($conference->next) {
                $conference->next->prev = $conference;
                $conference->next->save();
            }            
            if ($conference->prev) {
                $conference->prev->next = $conference;
                $conference->prev->save();
            }

            $conference->categories = array_map(function ($category) use ($allCategories) {
                return $allCategories[$category['id']];
            }, array_unique($form->values['categories'], SORT_REGULAR));

            $conference->save();
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/conference-list/');
        }
        
        $this->data['title'] = _t('Редактирование конференции');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit-tinymce');
    }
    
    function conference_join($id) { 

        $this->allowForRoles([CMSUser::ROLE_SUPER_MODERATOR]);

        if (!($thisConference = \App\Models\Conference::find($id))) return true;
        
        $form = new \Form('post');
        $form->fieldset(_t('Объединить с ') . $thisConference->name);
        $form->html('<p>'. _t('Вы можете объединить два профиля конференций в один - все отзывы, вопросы и лайки по указанному ниже профилю будут присоединены к этому профилю') .'</p>');
        $form->attribute_text('join', _t('Введите имя конференции, которую следует присоединить к ') . _h($thisConference->name), [
            function ($conference) { 
                if (!$conference)
                    throw new \ValidationException(_t('Необходимо указать конференцию'));
                return $conference;
            },
            function ($conference) use ($thisConference) { 
                if ($conference->id == $thisConference->id)
                    throw new \ValidationException(_t('Вы пытаетесь журнал с самим собой'));
                return $conference;
            }
        ], false, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=conference'),])
            ->object_filter(\App\Models\Conference::class, 'name');
        $form->submit(_t('Объединить'));
        
        if ($form->validate()) {

            if ($joinConference = $form->values['join']) {
                $thisConference->joinResearch($joinConference);
                $joinConference->delete();
            }
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/conference-list/');
        }
        
        $this->data['title'] = _t('Слияние конференций');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit');
    }
}