<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Review extends BasePrivate {

    public function review_list($type) {

        $types = [
            'journal-review' => \App\Models\JournalReview::class, 
            'conference-review' => \App\Models\ConferenceReview::class
        ];
        if (!array_key_exists($type, $types)) return true;

        $class = $types[$type];
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        
        $query = $class::findByQuery([], "$sort_by $sort_order");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields'] = [
            'id' => _t('ID'), 
            'user' => _t('Пользователь'),
            'created' => _t('Создан'),
            'moderated' => _t('Опубл.'),
            'review_contents' => _t('Содержание')
        ];
        $this->data['sort_fields'] = ['id', 'created', 'user', 'moderated'];   
        $this->data['field_filters'] = [
            'user' => function ($user, $review) {
                if (!$user) {
                    return _t('(Аноним)');
                } elseif ($review->anonymous) {
                    return _h($user->name).'<br>'._t('(Анонимно)');
                } else {
                    return _h($user->name);
                }
            },
            'created' => function ($created, $review) use ($type) {
                return '<a href="'.url($type.'/'.$review->id).'">'.$created->format("d.m.Y H:i").'</a>';
            },
            'moderated' => function ($moderated) {
                return $moderated ? _t('Да') : _t('Нет');
            },
            'review_contents' => function ($null, $review) use ($class) {
                switch ($class) {
                    case \App\Models\JournalReview::class:
                        return $this->getJournalReviewPreview($review);
                    case \App\Models\ConferenceReview::class:
                        return $this->getConferenceReviewPreview($review);
                }
            }
        ];
        $this->data['list_actions'] = [
            'publish' => [
                'title' => _t('Опубликовать'),
                'function' => $this->changeModeratedStatus(true, $this->em, $class)
            ], 
            'unpublish' => [
                'title' => _t('Скрыть'),
                'function' => $this->changeModeratedStatus(false, $this->em, $class)
            ]
        ];

        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) { 
            $this->data['list_actions']['delete'] = [
                'title' => _t('Удалить выбранные'),
                'function' => $this->action_delete($class)
            ];
        }
        
        $this->data['title'] = _t('Список отзывов');
        $this->view('cms/base-list');
    }

    private function changeModeratedStatus($value, $em, $class) {
        return function ($list) use ($value, $em, $class) {
            $keys = [];
            foreach ($list as $key=>$data) {
                if (@$data['check']) $keys[] = $key;
            }
            if (!empty($keys)) {
                $qb = $em->createQueryBuilder();
                $qb->update($class, 'obj')
                    ->where($qb->expr()->in('obj.id',$keys))
                    ->set('obj.moderated', ':value')
                    ->setParameter('value', $value)
                    ->getQuery()
                    ->getResult();
            }
            
            redirect(\Bingo\Routing::$uri);
        };
    }

    private function getConferenceReviewPreview($review) {
        $typeLables = [
            \App\Models\Review::ATTITUDE_NEUTRAL => _t('Нейтральный'),
            \App\Models\Review::ATTITUDE_NEGATIVE => _t('Отрицательный'),
            \App\Models\Review::ATTITUDE_POSITIVE => _t('Положительный')
        ];
        $attitude = isset($typeLables[$review->attitude]) ? $typeLables[$review->attitude] : '';

        return '<table>'.
            '<tr><td><strong>'._t('Конференция').'</strong></td><td>'.$review->conference->name.'</td></tr>'.
            '<tr><td><strong>'.$attitude.'</strong></td><td></td></tr>'.
            '<tr><td>'._t('Насколько сложно стать участником').'</td><td>'.$review->difficult.'</td></tr>'.
            '<tr><td>'._t('Уровень докладов').'</td><td>'.$review->level.'</td><tr>'.
            '<tr><td>'._t('Практическая польза').'</td><td>'.$review->usefulness.'</td></tr>'.
            '<tr><td>'.'<tr><td>'._t('Кофе-брейки/фуршет').'</td><td>'.$review->coffeebreaks.'</td></tr>'.
            '<tr><td>'._t('Культурная программа'.'</td><td>').($review->has_culture ? $review->culture : _t('Нет')).'</td></tr>'.
            '<tr><td colspan="2">'._h($review->notes).'</td></tr>'.
            '</table>';
    }

    private function getJournalReviewPreview($review) {
        $typeLables = [
            \App\Models\Review::ATTITUDE_NEUTRAL => _t('Нейтральный'),
            \App\Models\Review::ATTITUDE_NEGATIVE => _t('Отрицательный'),
            \App\Models\Review::ATTITUDE_POSITIVE => _t('Положительный')
        ];
        $attitude = isset($typeLables[$review->attitude]) ? $typeLables[$review->attitude] : '';

        return '<table>'.
            '<tr><td><strong>'._t('Журнал').'</strong></td><td>'.$review->journal->name.'</td></tr>'.
            '<tr><td><strong>'.$attitude.'</strong></td><td></td></tr>'.
            '<tr><td>'._t('Дата подачи').'</td><td>'.$review->article_date->format('m Y').'</td></tr>'.
            '<tr><td>'._t('Статус').'</td><td>'.$review->getArticleStatusLabel().'</td></tr>'.
            ($review->has_critic ? (
            '<tr><td>'._t('Время до рецензии').'</td><td>'.$review->critic_duration_weeks._t('нед.').'</td></tr>'.
            '<tr><td>'._t('Полезность рецензий').'</td><td>'.$review->critic_usefulness.'</td></tr>'
            ) : '<tr><td>'._t('Без рецензирования').'</td><td></td></tr>').
            ($review->article_status == \App\Models\JournalReview::ARTICLE_STATUS_PUBLISHED? (
            '<tr><td>'._t('Время до публикации').'</td><td>'.$review->publish_duration_weeks._t('нед.').'</td></tr>'
            ) : '').
            ($review->special ? '<tr><td>'._t('Специальный выпуск').'</td><td></td></tr>' : '').
            '<tr><td colspan="2">'._h($review->notes).'</td></tr>'.
            '</table>';
    }
}