<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Journal extends BasePrivate {
    
    function journal_list() { 
                
        $filterForm = new \CMS\FilterForm;
        $filterForm
            ->text('id')
            ->text('name')
            ->text_date('created', '')
        ;
        
        $criteria = [];
        if ($filterForm->validate()) {
            $id = trim($filterForm->values['id']);
            if ($id) $criteria['id'] = $id; 
            
            $name = trim($filterForm->values['name']);
            if ($name) $criteria['name'] = "LIKE %".$name."%";

            $created = $filterForm->values['created'];
            if ($created) $criteria['created >= ?'] = $created;
            
        }
        
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        
        $query = \App\Models\Journal::findByQuery($criteria, "$sort_by $sort_order");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();

        $this->data['fields'] = [
            'id' => _t('ID'), 
            'name' => _t('Название'),
            'databases' => _t('Базы данных'),
            'created' => _t('Дата добавления')
        ];
 
        $_h = function ($value) { return _h($value); };
        $this->data['field_filters'] = [
            'name' => $_h,
            'databases' => function($val, $obj) {
                $info = '';
                if ($obj->included_to_scopus)
                    $info .= _t('Scopus').'<br>';
                if ($obj->included_to_russian_journals)
                    $info .= _t('Российские журналы');

                return $info;
            }
        ];
        
        $this->data['sort_fields'] = ['id', 'name', 'created'];        
        $this->data['page_actions']['admin/app/journal-edit'] = _t('Создать новый');
        $this->data['item_actions']['admin/app/journal-edit'] = _t('редактировать');

        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {   
            $this->data['list_actions'] = [
                'delete' => [
                    'title' => _t('Удалить журналы'),
                    'function' => $this->action_delete(\App\Models\Journal::class)
                ]
            ];
        }
        
        $this->data['title'] = _t('Список журналов');
        $this->view('cms/base-list');
    }
    
    public function journal_edit($id) {
         
        $journal = \App\Models\Journal::findOrCreate($id);

        $databaseValidator = function($val) {
            if (empty($_POST['included_to_scopus']) && empty($_POST['included_to_russian_journals']))
                throw new \ValidationException(_t('Выберите как минимум одну БД'));
            return $val;
        };
        
        $form = new \Form('post');
        $form->fieldset(_t('Данные журнала'));
        $form->text('name', _t('Название'), 'required', $journal->name);
        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {  
            $form->html('<a href="'.url('admin/app/journal-join/'.$journal->id).'">'._t('Объединить с ...').'</a>');
        }
        $form->text('site', _t('Официальный сайт'), '', $journal->site);
        $form->textarea('description', _t('Описание'), '', $journal->description, ['class' => 'tinymce','rows'=>7]);
        $form->checkbox('hidden', _t('Скрыть с сайта'), '', $journal->hidden);
        $form->checkbox('included_to_scopus', _t('Входит в БД Scopus'), $databaseValidator, $journal->included_to_scopus);
        $form->checkbox('included_to_russian_journals', _t('Входит в БД Российских журналов'), $databaseValidator, $journal->included_to_russian_journals);

        $allCategories = [];
        $categoryOptions = [];

        $queriedCategories = \App\Models\Category::findBy( [], 'name ASC' );
        foreach($queriedCategories as $category) {
            $allCategories[$category->id] = $category;
            $categoryOptions[$category->name] = $category->id;
        }

        $categoryForm = new \Form();
        $categoryForm->select('id',  _t('Категория'), $categoryOptions, ['required', function ($catId) {
            if (!\App\Models\Category::find($catId)) 
                throw new \ValidationException(_t('Несуществующая категория'));
            return $catId;
        }]);
        
        $journalValues = array_map(function ($category) {
            return ['id'=>$category->id];
        }, $journal->categories->toArray());

        $form->form_list('category_list', $categoryForm, '', $journalValues, true, $atts=false);
        $form->submit(_t('Сохранить'));
        
        if ($form->validate()) {
            $form->fill($journal);

            $journalCategoryIds = [];
            foreach ($journal->categories as $category)
                $journalCategoryIds[] = $category->id;

            $formCategoryIds = [];
            foreach ($form->values['category_list'] as $category) {
                $category = \App\Models\Category::find($category['id']);
                if (!in_array($category->id, $journalCategoryIds)) {
                    $journal->categories->add($category);
                }
                $formCategoryIds[] = $category->id;
            }

            foreach ($journal->categories as $category) {
                if (!in_array($category->id, $formCategoryIds))
                    $journal->categories->removeElement($category);
            }
            $journal->save();
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/journal-list/');
        }
        
        $this->data['title'] = _t('Редактирование журнала');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit-tinymce');
    }
    
    function journal_join($id) { 
        $this->allowForRoles([CMSUser::ROLE_SUPER_MODERATOR]);

        if (!($thisJournal = \App\Models\Journal::find($id))) return true;
        
        $form = new \Form('post');
        $form->fieldset(_t('Объединить с ') . $thisJournal->name);
        $form->html('<p>'. _t('Вы можете объединить два профиля журналов в один - все отзывы, вопросы и лайки по указанному ниже профилю будут присоединены к этому профилю') .'</p>');
        $form->attribute_text('join', _t('Введите имя журнала, который следует присоединить к '). _h($thisJournal->name), [
            function ($journal) { 
                if (!$journal)
                    throw new \ValidationException(_t('Необходимо указать журнал'));
                return $journal;
            },
            function ($journal) use ($thisJournal) { 
                if ($journal->id == $thisJournal->id)
                    throw new \ValidationException(_t('Вы пытаетесь журнал с самим собой'));
                return $journal;
            }
        ], false, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=journal'),])
            ->object_filter(\App\Models\Journal::class, 'name');
        $form->submit(_t('Объединить'));
        
        if ($form->validate()) {

            if ($joinJournal = $form->values['join']) {
                $thisJournal->joinResearch($joinJournal);
                $joinJournal->delete();
            }
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/journal-list/');
        }
        
        $this->data['title'] = _t('Слияние журналов');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit');
    }
}