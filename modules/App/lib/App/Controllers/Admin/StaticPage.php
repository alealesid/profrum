<?php

namespace App\Controllers\Admin;

class StaticPage extends BasePrivate {
    function page404_edit() {
        $this->static_page_edit('page404', _t('Страница 404')); 
    }

    function page500_edit() {
        $this->static_page_edit('page500', _t('Страница 500'));
    }

    function about_edit() {
        $this->static_page_edit('about_page', _t('Страница "О проекте"'));
    }

    function rules_edit() {
        $this->static_page_edit('rules_page', _t('Страница "Правила сообщества"'));
    }
    
    function faq_edit() {
        $this->static_page_edit('faq_page', _t('Страница FAQ'));
    }
    
    private function static_page_edit($pagename, $title) {
        $this->restrictIfNotAdmin();
        
        $pageSettings = \CMS\Models\Option::get($pagename) ?: ['title' => '', 'content' => ''];
        
        $form = new \Bingo\Form();
        $form->text('title', _t('Заголовок'), 'required', $pageSettings['title']);
        $form->textarea('content', _t('Контент'), 'required', $pageSettings['content'])->add_class('tinymce');
        $form->submit(_t('Сохранить'));

        if($form->validate()) {
            \CMS\Models\Option::set($pagename, $form->values);
            set_flash('info', _t('Страница успешно сохранена'));
            redirect(\Bingo\Routing::$uri);
        }

        $this->data['title'] = $title;
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit-tinymce');
    }
}