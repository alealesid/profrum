<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Feedback extends BasePrivate {
    
    public function journal_list() {
        
        $this->feedback_list(\App\Models\Feedback::TYPE_JOURNAL_NOT_FOUND, [
            'id' => _t('ID'), 
            'user' => _t('user'),
            'created' => _t('Добавлено'), 
            'name' => _t('Название'), 
            'site' => _t('Сайт'), 
            'notes' => _t('Комментарии')
        ]);
    }
    
    public function conference_list() {
        
        $this->feedback_list(\App\Models\Feedback::TYPE_CONFERENCE_NOT_FOUND, [
            'id' => _t('ID'), 
            'user' => _t('user'),
            'name' => _t('Название'), 
            'location' => _t('Место проведения'), 
            'site' => _t('Сайт'), 
            'notes' => _t('Комментарии')
        ]);
    }

    private function feedback_list($type, $fields) {        
        $query = \App\Models\Feedback::findByQuery(['type' => $type], "created DESC");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields'] = $fields;
        $this->data['sort_fields'] = ['id', 'created', 'user'];
        $this->data['field_filters'] = [
            'user' => function ($user) {
                if (!$user) return '';
                return _h($user->name);
            }
        ];
        $this->data['list_actions'] = [
            'delete' => [
                'title' => _t('Удалить выбранные'),
                'function' => $this->action_delete(\App\Models\Feedback::class)
            ]
        ];
        
        $this->data['title'] = _t('Список запросов');
        $this->view('cms/base-list');
    }
}