<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Category extends BasePrivate {
    
    function category_list() {

        $filterForm = new \CMS\FilterForm;
        $filterForm->text('name');
        
        $criteria = [];
        if ($filterForm->validate()) {
            $name = trim($filterForm->values['name']);
            if ($name) $criteria['name'] = "LIKE %".$name."%";
        }
        
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        
        $query = \App\Models\Category::findByQuery($criteria, "$sort_by $sort_order");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();

        $this->data['fields'] = [
            'id' => _t('ID'), 
            'name' => _t('Название')
        ];
        $_h = function ($value) { return _h($value); };
        $this->data['field_filters'] = [
            'name' => $_h
        ]; 
        
        $this->data['sort_fields'] = ['id', 'name']; 
        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {        
            $this->data['page_actions']['admin/app/category-edit'] = _t('Создать новый');
            $this->data['item_actions']['admin/app/category-edit'] = _t('редактировать');
            $this->data['list_actions'] = [
                'delete' => [
                    'title' => _t('Удалить категории'),
                    'function' => $this->action_delete(\App\Models\Category::class)
                ]
            ];
        }
        
        $this->data['title'] = _t('Список категорий');
        $this->view('cms/base-list');
    }
    
    function category_edit($id) { 
        
        $category = \App\Models\Category::findOrCreate($id);
        
        $form = new \Form('post');
        $form->fieldset(_t('Данные категории'));
        $form->text('name', _t('Название'), 'required', $category->name);
        $form->submit(_t('Сохранить'));
        
        if ($form->validate()) {
            $form->fill($category);
            $category->save();
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/category-list/');
        }
        
        $this->data['title'] = _t('Редактирование категории');
        $this->data['form'] = $form->get();
        
        $this->view('cms/base-edit');
    }
}