<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class CMSUsers extends BasePrivate {
    
    function user_list() {  

        $this->restrictIfNotAdmin();

        $this->data['title'] = _t("User list");
        $user_class = \CMS\Configuration::$user_class;
        $this->data['list'] = $user_class::findAll();
        $this->data['page_actions']['admin/user-edit'] = _t('Create new');
        $this->data['item_actions']['admin/user-edit'] = _t('edit');
        $this->data['list_actions']['delete'] = array(
            'title' => _t('Delete'),
            'function' => $this->action_delete($user_class,'admin/user-list')
        );
        $this->data['fields'] = array('id'=>_t('#'),'login'=>_t('login'));
        $this->view('cms/base-list',$this->data);
    }
    
    function user_edit($id) {  

        $this->restrictIfNotAdmin();

        $this->data['title'] = _t("User edit");
        $user = $this->findOrCreate(\CMS\Configuration::$user_class,$id);

        $rolesOptions = [
            _t('Администратор') => \App\Models\CMSUser::ROLE_ADMIN,            
            _t('Модератор 2 уровня') => \App\Models\CMSUser::ROLE_SUPER_MODERATOR, 
            _t('Модератор 1 уровня') => \App\Models\CMSUser::ROLE_MODERATOR 
        ];

        $rolesForm = new \Form();
        $rolesForm->select('role', _t('Роль'), $rolesOptions, function ($role) use ($rolesOptions) {
            if (!in_array($role, array_values($rolesOptions)))
                throw new \ValidationException(_t('Role doesn\'t exist'));
            return $role;
        });

        $form = new \Bingo\Form;
        $form->fieldset(_t("Info"))
                ->text('login',_t('Login'),'required',$user->login)
                ->text('password',_t('Password'),'required','')
                ->text('email',_t('E-mail'),'',$user->email);

        $form->fieldset(_t("Роли пользователей"))
                ->form_list('roles', $rolesForm, '', array_map(function ($role) {
                    return ['role' => $role];
                }, $user->roles), false);

        $form->fieldset()
                ->submit(_t('Save user'))->add_class("inline single");

        if ($form->validate()) {
            $data = $form->getValue();
            $user->login = $data['login'];
            if ($data['password']) $user->password = md5($data['password']);
            $user->email = $data['email'];
            $user->roles = array_reduce($data['roles'], function ($roles, $role) {
                $roleValue = $role['role'];
                if (!in_array($roleValue, $roles)) {
                    $roles[] = $roleValue;
                }
                return $roles;
            }, []);
            $user->save();
            return redirect("admin/user-list");
        }
        $this->data['title'] = _t("User edit");
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }
    
    function user_profile() { 

        $user = $this->user;
        $form = new \Bingo\Form;
        $cls = \CMS\Configuration::$user_class;

        $form->fieldset(_t("Change password"))
                ->text('old_password',_t('Old Password'),function($val) use ($user,$cls) {
                    $test = $cls::login($this->user->login,$val);
                    if (!$test)
                        throw new \ValidationException(_t('Wrong password'));
                    return $val;
                },'')
                ->text('new_password',_t('New Password'),'','');

        $form->fieldset()
                ->submit(_t('Save changes'))->add_class("inline single");

        if ($form->validate()) {
            $user->password = md5($form->values['new_password']);
            $user->save();
            
            $cls::loginUser($user);
            
            set_flash('info',_t('Password changed'));
            return redirect("admin/user-profile");
        }
        $this->data['title'] = _t("My profile");
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit',$this->data);
    }
}