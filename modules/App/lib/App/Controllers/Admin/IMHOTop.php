<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class IMHOTop extends BasePrivate {

    public function imhotop_list() { 
        
        $filterForm = new \CMS\FilterForm;
        $filterForm
            ->text('user')
            ->text('category')
            ->select('type', '', [
                '' => '',
                _t('Стандартный') => \App\Models\IMHOTop::TYPE_STANDART,
                _t('Нестандартный') => \App\Models\IMHOTop::TYPE_NONSTANDART
            ]);
        
        $qb = $this->em->createQueryBuilder()
            ->select('top')
            ->from(\App\Models\IMHOTop::class, 'top');

        if ($filterForm->validate()) {
            
            if ($user = trim($filterForm->values['user'])) {
                $qb->leftJoin('top.user', 'u', 'WITH', 'u.name LIKE :user');
                $qb->andWhere('u.name LIKE :user');
                $qb->setParameter('user', '%'.$user.'%');
            }

            if ($category = trim($filterForm->values['category'])) {
                $qb->leftJoin('top.category', 'c', 'WITH', 'c.name LIKE :category');
                $qb->andWhere('c.name LIKE :category');
                $qb->setParameter('category', $category);
            }

            if ($type = trim($filterForm->values['type'])) {
                $qb->andWhere('top.type = :type');
                $qb->setParameter('type', $type);
            }

        }
        
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';

        $qb->orderBy("top.$sort_by", $sort_order);

        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $qb->getQuery());
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields'] = [
            'id' => _t('ID'), 
            'type' => _t('Тип'),
            'user' => _t('Пользователь'),
            'category' => _t('Категория'),
            'items' => _t('Позиции')
        ];
        $this->data['sort_fields'] = ['id', 'category', 'user'];   
        $this->data['field_filters'] = [
            'type' => function ($val) {
                $typeLables = [
                    \App\Models\IMHOTop::TYPE_STANDART => _t('Стандартный'),
                    \App\Models\IMHOTop::TYPE_NONSTANDART => _t('Нестандартный')
                ];

                return isset($typeLables[$val]) ? $typeLables[$val] : '';
            },
            'user' => function ($user) {
                return _h($user->name);
            },
            'category' => function ($category, $imhotop) { 
                return $category ? 
                '<strong>'._h($category->name).'</strong>' : 
                _h($imhotop->name); 
            }, 
            'items' => function ($items) {
                $journals = array_map(function ($imhotopItem) {
                    return _h($imhotopItem->position.'. '.$imhotopItem->journal->name);
                }, $items->toArray());
                return join('<br>', $journals);
            }
        ];
        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {   
            $this->data['list_actions'] = [
                'delete' => [
                    'title' => _t('Удалить выбранные'),
                    'function' => $this->action_delete(\App\Models\IMHOTop::class)
                ]
            ];
        }
        
        $this->data['title'] = _t('Список ИМХО-топов');
        $this->view('cms/base-list');
    }
}