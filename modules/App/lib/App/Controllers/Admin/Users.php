<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Users extends BasePrivate {
    
    function user_list() { 
        
        $filterForm = new \CMS\FilterForm;
        $filterForm->text('id');
        $filterForm->text('name');
        $filterForm->text('email');
        $filterForm->text('work');
        $filterForm->text('position');         
        
        $criteria = [];
        if ($filterForm->validate()) {
            $id = $filterForm->values['id'];
            if ($id) $criteria['id'] = $id;
            $name = trim($filterForm->values['name']);
            if ($name) $criteria['name'] = "LIKE %".$name."%";
            $email = trim($filterForm->values['email']);
            if ($email) $criteria['email'] = "LIKE %".$email."%";
            $work = trim($filterForm->values['work']);
            if ($work) $criteria['work'] = "LIKE %".$work."%";
            $position = trim($filterForm->values['position']);
            if ($position) $criteria['position'] = "LIKE %".$position."%";
        }
        
        $_GET['sort_by'] = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $_GET['sort_order'] = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        $query = \App\Models\User::findByQuery($criteria, $_GET['sort_by']." ".$_GET['sort_order']);
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();

        $this->data['fields'] = [
            'id' => _t('ID'),
            'blocked' => _t('Доступ'),
            'name' => _t('Имя'),
            'email' => _t('Email'),
            'work' => _t('Место работы'),
            'position' => _t('Должность'),
            'degree' => _t('Учёная степень')
        ];
        $this->data['sort_fields'] = ['id', 'name', 'email', 'blocked', 'work', 'position', 'degree'];
        $_h = function ($value) { return _h($value); };
        $this->data['field_filters'] = [
            'name' => $_h,
            'work' => $_h,
            'position' => $_h,
            'degree' => function ($degree) {
                $degrees = \App\Models\User::getDegreeLabels();
                return $degrees[$degree];
            },
            'blocked' => function ($blocked) { 
                return $blocked ? _t('Заблокирован') : _t('Есть');
            }
        ]; 
          
        if ($this->user->hasRole(CMSUser::ROLE_ADMIN)) {     
            $this->data['page_actions']['admin/app/user-edit'] = _t('Создать нового');
            $this->data['item_actions']['admin/app/user-edit'] = _t('редактировать');
        }
        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) {   
            $this->data['list_actions'] = [ 
                'deny' => [ 
                    'title' => _t('Блокировать'), 
                    'function' => $this->setUsersBlocked(true)
                ], 
                'allow' => [ 
                    'title' => _t('Разблокировать'), 
                    'function' => $this->setUsersBlocked(false)
                ]
            ]; 
        }
        
        $this->data['title'] = _t('Список пользователей');
        $this->view('cms/base-list');
    }
    
    function user_edit($id) {  

        $this->restrictIfNotAdmin();
        
        $form = new \Form('post');
        $form->fieldset(_t('Данные пользователя'));
        
        $user = \App\Models\User::find($id);
        if ($user) {
            $passwordRules = [];
        } else {
            $user = new \App\Models\User;
            $passwordRules = ['required'];
        }
        
        $form->text('login', _t('Логин'), 'required', $user->login);
        $form->text('email', _t('Email'), ['required', 'valid_email'], $user->email);
        $form->text('name', _t('Имя'), 'required', $user->name);
        $form->text('new_password', _t('Пароль'), $passwordRules);
        $form->text('work', _t('Место работы (учёбы)'), [], $user->work);
        $form->text('position', _t('Должность'), [], $user->position);
        $degrees = array_flip(\App\Models\User::getDegreeLabels());
        $form->select('degree', _t('Учёная степень'), $degrees, [], false);
        $form->text('status', _t('Статус'), '', $user->status);
        $form->checkbox('blocked', _t('Заблокирован'), '', $user->blocked);
        $form->submit(_t('Сохранить'));
        
        if ($form->validate()) {
            if ($form->values['blocked']) {
                $user->logout();
            }
            $form->fill($user);
            if ($form->values['new_password']) {
                $user->setPassword($form->values['new_password']);
            }
            $user->save();
            
            set_flash('info',_t('Изменения сохранены успешно'));
            redirect('admin/app/user-edit/'.$user->id);
        }
        
        $this->data['title'] = _t('Редактирование пользователя');
        $this->data['form'] = $form->get();
        $this->view('cms/base-edit');
    }

    private function setUsersBlocked($blocked) {

        $em = $this->em;

        return function ($list) use ($blocked, $em) { 
            $keys = []; 
            foreach ($list as $key=>$data) { 
                if (@$data['check']) $keys[] = $key; 
            }
            if (!empty($keys)) { 
                foreach(\App\Models\User::findBy(['id' => $keys]) as $user) {
                    $user->blocked = $blocked;
                    $user->save();
                    if ($user->blocked) $user->logout();
                }
            }
            redirect(\Bingo\Routing::$uri); 
        }; 
    }
}