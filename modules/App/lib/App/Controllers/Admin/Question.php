<?php

namespace App\Controllers\Admin;

use \App\Models\CMSUser;

class Question extends BasePrivate {

    public function question_list() {
        $class = \App\Models\Question::class;
        $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'id';
        $sort_order = isset($_GET['sort_order']) ? $_GET['sort_order'] : 'DESC';
        
        $query = \App\Models\Question::findByQuery([], "$sort_by $sort_order");
        $pagination = new \Bingo\Pagination(20, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['pagination'] = $pagination->get(10);
        $this->data['list'] = $pagination->result();
        $this->data['fields'] = [
            'id' => _t('ID'), 
            'user' => _t('Пользователь'),
            'created' => _t('Создан'),
            'moderated' => _t('Опубл.'),
            'owner_name' => _t('Название'),
            'text' => _t('Содержание'),
        ];
        $this->data['sort_fields'] = ['id', 'created', 'user', 'moderated'];   
        $this->data['field_filters'] = [
            'user' => function ($user, $review) {
                if (!$user) {
                    return _t('(Аноним)');
                } elseif ($review->anonymous) {
                    return _h($user->name).'<br>'._t('(Анонимно)');
                } else {
                    return _h($user->name);
                }
            },
            'moderated' => function ($moderated) {
                return $moderated ? _t('Да') : _t('Нет');
            },
            'owner_name' => function ($val, $question) {
                return $question->getOwner()->name;
            },
        ];
        $this->data['list_actions'] = [
            'publish' => [
                'title' => _t('Опубликовать'),
                'function' => $this->changeModeratedStatus(true, $this->em, $class)
            ], 
            'unpublish' => [
                'title' => _t('Скрыть'),
                'function' => $this->changeModeratedStatus(false, $this->em, $class)
            ]
        ];

        if ($this->user->hasOneOfRoles([CMSUser::ROLE_ADMIN, CMSUser::ROLE_SUPER_MODERATOR])) { 
            $this->data['list_actions']['delete'] = [
                'title' => _t('Удалить выбранные'),
                'function' => $this->action_delete($class)
            ];
        }
        
        $this->data['title'] = _t('Список вопросов');
        $this->view('cms/base-list');
    }

    private function changeModeratedStatus($value, $em, $class) {
        return function ($list) use ($value, $em, $class) {
            $keys = [];
            foreach ($list as $key=>$data) {
                if (@$data['check']) $keys[] = $key;
            }
            if (!empty($keys)) {
                $qb = $em->createQueryBuilder();
                $qb->update($class, 'obj')
                    ->where($qb->expr()->in('obj.id',$keys))
                    ->set('obj.moderated', ':value')
                    ->setParameter('value', $value)
                    ->getQuery()
                    ->getResult();
            }
            
            redirect(\Bingo\Routing::$uri);
        };
    }
}