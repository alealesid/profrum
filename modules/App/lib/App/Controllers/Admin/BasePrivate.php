<?php

namespace App\Controllers\Admin;

class BasePrivate extends \CMS\Controllers\Admin\BasePrivate {

    public function allowForRoles($roles = []) {
        if ($this->user->hasRole(\App\Models\CMSUser::ROLE_ADMIN)) return true;

        foreach($roles as $role) {
            if ($this->user->hasRole($role)) return true;
        }

        set_flash('info',_t('Недостаточно прав для доступа'));
        redirect('admin');
    }

    public function restrictIfNotAdmin() {
        $this->allowForRoles([\App\Models\CMSUser::ROLE_ADMIN]);
    }

    public function action_delete($class,$redirect='') {
        $that = $this;
        return function ($list) use ($class,$redirect,$that) {
            $keys = array();
            foreach ($list as $key=>$data) {
                if (@$data['check']) $keys[] = $key;
            }
            if (!empty($keys)) {
                $itemsToDelete = $class::findBy( [ 'id' => $keys ]);
                foreach ($itemsToDelete as $itemToDelete) {
                    $itemToDelete->delete(false);
                }
                \Bingo::$em->flush();
            }

            if ($redirect!==false){
                if ($redirect==='') $redirect = $_SERVER['REQUEST_URI'];
                redirect(str_replace(base_url(),"",$redirect));
                die();
            }
        };
    }
}