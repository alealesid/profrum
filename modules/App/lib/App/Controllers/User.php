<?php

namespace App\Controllers;

class User extends Base {
    
    public function profile($id) {
        if ($id) {
            $this->userProfile($id);
        } else {
            $this->personalProfile();
        }
        
    }

    protected function userProfile($id) {
        $user = \App\Models\User::find($id);
        if (!$user) return true;
        $this->data['profile_user'] = $user;
        $this->view('user-profile');

    }

    protected function personalProfile() {
        if (!$this->user) redirect('login');

        $form = new \App\Forms\User('post', [], $this->user);
        if ($form->validate()) {
            $password = $form->values['password'];
            unset($form->values['password']);
            $form->fill($this->user);
            if ($password) {
                $this->user->password = md5($password);
            }
            $this->user->save();
        }

        $this->data['default_avatar'] = \App\Models\User::getDefaultAvatarImagePath();
        $this->data['form'] = $form;
        $this->view('profile');

    }

    public function upload_avatar() {
        if (@$_FILES['file']['size'] != 0) {
            $path = \App\Models\User::getPathToAvatars();           
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);            
            $tempFile = '_tmp_'.$this->user->name .'_'. uniqid() . '.' .$ext;            
            $path .= $tempFile;            
            try {                
                $file = \PhpThumb\Factory::create($_FILES["file"]["tmp_name"],array('resizeUp'=>false));                
                $file->resize(200,200);                
                $file->save(INDEX_DIR."/".$path);                               
                $error = false;            
            } catch (\Exception $e) {             
                $error = _t('File is not a valid image.');            
            }                        
            echo json_encode([                
                'error'=>$error,                
                'src'=> \App\Models\User::getPathToAvatars().$tempFile,              
            ]);            
            return;
        }
        return true;
    }

    public function remove_avatar() {
        if (isset($_POST['remove_avatar'])) {
            echo json_encode([ 'default_avatar_path' => \App\Models\User::getDefaultAvatarImagePath() ]);
            return;
        }
        return true;
    }
}