<?php

namespace App\Controllers;

class StaticPage extends Base {
    function page404() {
        $this->static_page_base('page404');
    }

    function page500() {
        $this->static_page_base('page500');
    }
    function about() {
        $this->static_page_base('about_page');
    }

    function rules() {
        $this->static_page_base('rules_page');
    }
    
    function faq() {
        $this->static_page_base('faq_page');
    }

    private function static_page_base($pagename) {
        $pageSettings = \CMS\Models\Option::get($pagename);        
        if (empty($pageSettings)) return true;

        $this->data['title'] = $pageSettings['title'];
        $this->data['content'] = $pageSettings['content'];
        $this->view('static_page');
    }
}