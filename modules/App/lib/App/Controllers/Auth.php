<?php

namespace App\Controllers;

use \App\Models\User;
use \App\Models\Identity;

class Auth extends Base {
    
    public function signup() {
        $user = $this->user;
        
        $form = new \App\Forms\User('post');
                
        if ($form->validate()) {
            $user = new User();
            $form->fill($user);
            $loginExists = \App\Models\User::findOneBy(['email' => $user->email]);
            $user->login = $user->email . ($loginExists ? uniqid() : '');
            $user->setPassword($form->values['password']);
            $user->save();

            $this->loginUserAndSaveLikes($user);

            redirect($this->getRedirectPath());
        }
        
        $this->data['title'] = _t('Signup');
        $this->data['signup_form'] = $form;

        $this->data['signup_url'] = $this->url_with_redirect('signup');
        $this->data['ulogin_redirect_url'] = "//".$_SERVER['HTTP_HOST']. urlencode($this->url_with_redirect('social-network-auth'));
        
        $this->view('signup');
    }

    public function social_network_auth() {
        if (empty($_POST['token'])) return;

        $uloginData = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $uloginUser = json_decode($uloginData, true);
        if (!$uloginUser) {
            trigger_error("uLogin API error");
            return;
        }

        $user = User::findOneBy(['identities.identity' => $uloginUser['identity']]);
        if ($user && !$user->blocked) {
            $this->loginUserAndSaveLikes($user);
        } else {
            $user = User::findOneBy(['email' => $uloginUser['email']]);
            if ($user->blocked) redirect('login'); 
            if (!$user) {
                $user = new User();
                $user->login = $uloginUser['nickname'];
                $user->setPassword(uniqid($user->login));
                $user->email = $uloginUser['email'];
                $user->secret = "";
                $user->name = $uloginUser['first_name'].' '.$uloginUser['last_name'];
                $user->work = "";
                $user->position = "";
                $user->degree = User::DEGREE_NONE;
                $user->status = "";
                if ($uloginUser['photo_big']) {
                    $user->avatar = $uloginUser['photo_big'];
                }
                $user->save();
            }

            $identity = new Identity;
            $identity->identity = $uloginUser['identity'];
            $identity->user = $user;
            $identity->save();

            $this->loginUserAndSaveLikes($user);
        }    
        redirect($this->getRedirectPath());
    }
    
    public function login() {
        if ($this->user) redirect('profile');

        $user = $this->user;
        $form = new \Form('post');
        $form->text('email', _t('Email'), ['required', 'valid_email'], false, ['class'=>'form-control']);
        $form->password('password', _t('Пароль'), ['required', function($password) use (&$user) {
            $user = User::findOneBy(['email'=> $_POST['email']]);
            if ($user) {
                $successLogin = User::testLogin($user->login, $password);
                if ($successLogin) return $password;
            }
            throw new \ValidationException(_t('Неправильный пароль'));
        }],false, ['class'=>'form-control']);
        $form->checkbox('remember_me', _t('Запомнить меня'), '', true);
        
        if ($form->validate()) {
            if ($user && !$user->blocked) {
                
                $this->loginUserAndSaveLikes($user, $form->values['remember_me']);
                                
                redirect($this->getRedirectPath());
            }
        }
        
        $this->data['title'] = _t('Log in');
        $this->data['login_form'] = $form;

        $this->data['signup_url'] = $this->url_with_redirect('signup');
        $this->data['login_url'] = $this->url_with_redirect('login');
        $this->data['ulogin_redirect_url'] = "//".$_SERVER['HTTP_HOST']. urlencode($this->url_with_redirect('social-network-auth'));
        
        $this->view('login');
    }
    
    function logout() {
        if ($this->user) {
            $this->user->logout();
        }
        redirect('login');
    }

    public function forgot_password () {

        if ($this->user) redirect('profile');
        
        $form = new \Form('post');
        $form->text('email', _t('Email'), ['required', 'valid_email', function($val)  {
            $user = \App\Models\User::findOneBy(['email' => $val]);
            if (!$user) {
                throw new \ValidationException(_t('Такой email не зарегистрирован'));
            }
            return $val;
        }], false, ['class'=>'form-control']);


        if ($form->validate()) {
            $user = \App\Models\User::findOneBy(['email' => $form->values['email']]);
            $now = new \DateTime('now');

            if (!isset($user->last_email_sent) || $now->diff($user->last_email_sent)->i > 30)  {

                $user->secret = md5(uniqid($user->id));
                $user->last_email_sent = $now; 
                $user->save();

                $config = \Bingo\Config::get('config', 'smtp');
                $mail = new \PHPMailer;
                $mail->IsSMTP();
                $mail->Username = $config['username'];
                $mail->Password = $config['password'];
                $mail->Host = $config['host'];
                $mail->Port = $config['port'];
                $mail->CharSet = 'utf-8';
                $mail->SMTPAuth = true;
                $mail->ClearReplyTos();

                $mail->SetFrom($config['from_address'], $config['from_title']);
                $mail->Subject = _t('Восстановление пароля');
                $passwordChangeLink = (isset($_SERVER['HTTPS'])?'https':'http').'://'.$_SERVER['HTTP_HOST'].url('change-password').'?'.http_build_query([
                    'secret' => urlencode($user->secret)
                ]);
                $template = $this->view('email/password_forgot', ['password_change_link' => $passwordChangeLink], true);
                $mail->MsgHTML($template);
                $mail->ClearAddresses();
                $mail->AddAddress($user->email, $user->name);

                $mail->Send();
            }

            set_flash('info', _t('Готово! На указанный вами почтовый ящик придёт письмо с инструкциями по восстановлению пароля'));
            redirect('login');
        }


        $this->data['title'] = _t('Забыли пароль?');
        $this->data['form'] = $form;
        
        $this->view('password_forgot');

    }

    public function change_password() {

        if ($this->user) redirect('profile');

        $form = new \Form('post');
        $request_data = $_POST;
        $form->password('password', _t('Пароль'), ['required', function ($password) use ($request_data) {
                if ($password && $password != $request_data['password_again'])
                    throw new \ValidationException(_t('Пароли не совпадают'));
                return $password;
        }], false, ['class'=>'form-control']);
        $form->password('password_again', _t('Пароль ещё раз'), 'required', false, ['class'=>'form-control']);

        if (isset($_GET['secret'])) {
            $secret = urldecode($_GET['secret']);
            $user = \App\Models\User::findOneBy(['secret' => $secret]);
            if (!$user) return true;

            if ($form->validate()) {
                $user->secret = '';
                $user->setPassword($form->values['password']);
                $user->save();

                $this->loginUserAndSaveLikes($user);

                set_flash('info', _t('Ваш пароль был изменён'));
                redirect('profile');
            }
        } else {
            return true;
        }

        $this->data['target_user'] = $user;
        $this->data['form'] = $form;
        $this->data['title'] = _t('Восстановление пароля');
        
        $this->view('password_change');

    }

    public function url_with_redirect($uri) {

        $redirect = false;
        if (isset($_GET['redirect'])) {
            $redirect = $_GET['redirect'];
        } else if (isset($_SERVER['HTTP_REFERER'])) {
            $redirect = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
            $redirect = str_replace(url(''), '', $redirect);
            $redirect = urlencode($redirect);
        }

        $params = isset($_GET) ? $_GET : [];
        if ($redirect) $params['redirect'] = $redirect;

        return url($uri) . (!empty($params) ? "?".http_build_query($params) : '' );
    }

    public function getRedirectPath() {
        $params = isset($_GET) ? $_GET : [];
        unset($params['redirect']);
        $path = isset($_GET['redirect']) ? str_replace(url(''), '', urldecode($_GET['redirect'])) : 'profile'; 
        return $path . (!empty($params) ? "?".http_build_query($params) : '' );
    }

    private function loginUserAndSaveLikes($user, $rememberMe = false) {

        $session = $this->anonymousUserSession;
        if (!empty($session->userLikes)) {
            
            foreach ($session->userLikes as $sessionLike) {
                
                $qb = $this->em->createQueryBuilder();

                $qb->select('anonymous as anonymousLike, userLike.id as existingLikeID')
                    ->from(\App\Models\Like::class, 'anonymous')
                    ->leftJoin(\App\Models\Like::class,'userLike','WITH', $qb->expr()->andX(
                        $qb->expr()->eq('userLike.owner_class', 'anonymous.owner_class'),
                        $qb->expr()->eq('userLike.owner_id', 'anonymous.owner_id'),
                        $qb->expr()->eq('userLike.user', ':user')
                    ))
                    ->where('anonymous.id = :id')
                    ->setParameters([
                        'id' => $sessionLike["id"],
                        'user' => $user
                    ]);

                $likeInfo = $qb->getQuery()
                    ->getOneOrNullResult();
                    
                if ($likeInfo) {    
                    if ($likeInfo['anonymousLike'] && !$likeInfo['existingLikeID']) {                        
                        $likeInfo['anonymousLike']->user = $user;
                        $likeInfo['anonymousLike']->save(false);
                    } else if ($likeInfo['anonymousLike'] && $likeInfo['existingLikeID']) {
                        $likeInfo['anonymousLike']->delete(false);                    
                    }
                }
                
            }
            $this->em->flush();
            $session->userLikes = [];
        }

        $user = User::loginUser($user, $rememberMe);

    }
}