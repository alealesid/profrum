<?php

namespace App\Controllers;

class Feedback extends Base {
    
    public function journal() { 
        if (!$this->user) redirect('login');
        
        $feedback = new \App\Models\Feedback();
        $form = new \Form('post', []);
        $form->text('name',false, 'required', false, ['class'=>'form-control', 'placeholder' => _t('Название журнала')]);
        $form->text('site',false, 'required', false, ['class'=>'form-control', 'placeholder' => _t('Сайт журнала или страница журнала в базах eLibrary, Scopus и др.')]);
        $form->textarea('notes', _t('Комментарии'), [function ($value, $formElement, $form) {
            if (count(explode(' ', $value)) > 250) {
                throw new \ValidationException(_t('Текст не должен превышать 250 слов'));
            }
            return $value;
        }], false, ['class'=>'form-control']);
        
        if ($form->validate()) {
            $form->fill($feedback);
            $feedback->type = \App\Models\Feedback::TYPE_JOURNAL_NOT_FOUND;
            $feedback->contents = $form->values;
            $feedback->user = $this->user;
            $feedback->save();
            set_flash('info',_t('Большое спасибо! Ваш запрос отправлен!'));
            redirect('journal-list');
        }
        
        $this->data['form'] = $form;
        $this->data['title'] = _t('Не нашёл журнал');
        $this->view('journal_feedback_edit');
    }
    
    public function conference() { 
        if (!$this->user) redirect('login');
        
        $feedback = new \App\Models\Feedback();
        $form = new \Form('post', []);
        $form->text('name', false, 'required', false, ['class'=>'form-control', 'placeholder' => _t('Название конференции')]);
        $form->text('site', false, 'required', false, ['class'=>'form-control', 'placeholder' => _t('Официальный сайт')]);
        $form->text('location', false, 'required', false, ['class'=>'form-control', 'placeholder' => _t('Место проведения')]);
        $form->textarea('notes', _t('Комментарии'), [function ($value, $formElement, $form) {
            if (count(explode(' ', $value)) > 250) {
                throw new \ValidationException(_t('Текст не должен превышать 250 слов'));
            }
            return $value;
        }], false, ['class'=>'form-control']);
        if ($form->validate()) {
            $form->fill($feedback);
            $feedback->type = \App\Models\Feedback::TYPE_CONFERENCE_NOT_FOUND;
            $feedback->contents = $form->values;
            $feedback->user = $this->user;
            $feedback->save();
            set_flash('info',_t('Большое спасибо! Ваш запрос отправлен!'));
            redirect('conference-list');
        }
        
        $this->data['form'] = $form;
        $this->data['title'] = _t('Не нашёл конференцию');
        $this->view('conference_feedback_edit');
    }
}