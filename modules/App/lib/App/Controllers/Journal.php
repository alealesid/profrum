<?php

namespace App\Controllers;

class Journal extends Base {

    function getPage() {
        if (isset($_GET['p'])) $page = (int)$_GET['p']; else $page = 1;if ($page<=1) $page = 1;
        return $page;
    }
    
    public function journal($id) {  
        
        $journal = \App\Models\Journal::find($id);
        if (!$journal) redirect('/');        
        if ($journal->hidden) return true;

        $imhotopItems = \App\Models\IMHOTopItem::findBy( [ 'journal' => $journal ] );
        $categories = [];
        
        foreach($imhotopItems as $imhotopItem) {
            if (!in_array($imhotopItem->imhotop->category, $categories))
                $categories[] = $imhotopItem->imhotop->category; 
        } 

        $IMHOInfo = []; 
        foreach ($categories as $category) {
            foreach(\App\Models\IMHOTop::getTop100Journals($category) as $j) { 
                if ($j->id == $journal->id) {
                    if ($j->top10_position <= 5) {
                        $top = 'Топ5';
                    }  else if ($j->top10_position <= 10) {
                        $top = 'Топ10';
                    } else if ($j->top10_position <= 25) {
                        $top = "Q1";
                    } else if ($j->top10_position <= 50) {
                        $top = "Q2";
                    } else if ($j->top10_position <= 75) {
                        $top = "Q3";
                    } else if ($j->top10_position <= 100) {
                        $top = "Q4";
                    }
                    $IMHOInfo[] = [ 
                        'category' => $category->name,
                        'category_id' => $category->id, 
                        'score' => $j->top10_rating, 
                        'top' => $top 
                    ]; 
                } 
            } 
        } 

        $this->data['anonymousUserSession'] = $this->anonymousUserSession;        
        $this->data['IMHOInfo'] = $IMHOInfo; 
        $this->data['journal'] = $journal;
        $this->data['title'] = $journal->name;
        $this->view('journal');
    }
    

    public function journal_list() {

        $categories = [];
        $categoriesOptions = [_t('Все') => 'none'];
        $queriedCategories = \App\Models\Category::findBy( [], 'name ASC' );
        foreach($queriedCategories as $category) {
            $categories[$category->id] = $category;
            $categoriesOptions[$category->name] = $category->id;
        }
        
        $filterForm = new \CMS\FilterForm;
        $filterForm->attribute_text('name',false,false,false,["id"=>"journals", "class"=>"autocomplete form-control", "url"=> url('autocomplete?entities=journal')]);
        $filterForm->select('category', false, $categoriesOptions, [function ($catId) use ($categories) {
            if ($catId!='none' && !isset($categories[$catId])) {
                throw new \ValidationException(_t('Такая категория не существует'));
            }
            return $catId;
        }], false, [ 'class' => 'custom-select' ]);
        $filterForm->checkbox('russian_journals', _t('российские журналы'));
        $filterForm->checkbox('scopus_journals', _t('журналы Scopus'));
        
        $qb = $this->em->createQueryBuilder()
            ->select('distinct j')
            ->from(\App\Models\Journal::class, 'j')
            ->andWhere('j.hidden = 0')
            ->orderBy("j.positive_review_count ", "desc")
            ->addOrderBy("j.neutral_review_count", "desc")
            ->addOrderBy("j.negative_review_count", "desc");

        $searchParams = [];
        if ($filterForm->validate()) {
            $name = trim($filterForm->values['name']);
            if ($name) {
                $searchParams['name'] = $name;
                $qb->andWhere('j.name LIKE :name')
                    ->setParameter('name', '%'.$name.'%'); 
            }
            $category = $filterForm->values['category'];
            $searchParams['category'] = $category;
            if ($category!='none') {
                $qb->leftJoin('j.categories', 'cat')
                    ->andWhere('cat.id = :category')
                    ->setParameter('category', $category);
            }
            $russianJournals = $filterForm->values['russian_journals'];
            if ($russianJournals) {
                $qb->andWhere('j.included_to_russian_journals = :included_to_russian_journals');
                $qb->setParameter('included_to_russian_journals', $russianJournals);
            }
            $scopusJournals = $filterForm->values['scopus_journals'];
            if ($scopusJournals) {
                $qb->andWhere('j.included_to_scopus = :included_to_scopus');
                $qb->setParameter('included_to_scopus', $scopusJournals);
            }
        }

        $perPage = 5;
        $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($qb->getQuery(), true);
 
        $pagination = new \Bingo\Pagination($perPage, $this->getPage(), $total, false, $qb->getQuery());
        
        $this->data['search_params'] = $searchParams;
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(4);
        $this->data['journals'] = $pagination->result();
        $this->data['reviews'] = \App\Models\JournalReview::findBy(['moderated' => true], 'created DESC', 0, 3);
        $this->data['questions'] = \App\Models\Question::findBy(['owner_class'=>\App\Models\Journal::class], 'created DESC', 0, 3);
        $this->data['title'] = _t('Журналы');
        $this->view('journal_list');

    }
    

    public function journal_table_list() {

        $sortBy = 'rating';
        $sortOrder = 'DESC'; 
        
        $filterForm = new \CMS\FilterForm;
        $categoryOptions = \App\Models\Category::selectOptions([], 'name', [_t('Все') => NULL]);
        $filterForm->select('category', false, $categoryOptions, '', NULL, ['class'=>'form-control custom-select'])
            ->object_filter(\App\Models\Category::class);
        $filterForm->hidden('sort_by', $sortBy);
        $filterForm->hidden('sort_order', $sortOrder);
        
        $qb = $this->em->createQueryBuilder()
            ->select('distinct j')
            ->from(\App\Models\Journal::class, 'j')
            ->andWhere('j.hidden = 0')
            ->leftJoin('j.reviews','r')
            ->andWhere('r.moderated = 1')
            ->groupBy('j.id');


        if ($filterForm->validate()) {
            $category = $filterForm->values['category'];
            if ($category) {
                $qb->leftJoin('j.categories', 'cat')
                    ->andWhere('cat.id = :category')
                    ->setParameter('category', $category);
            }

            $formSortBy = $filterForm->values['sort_by'];
            if ($formSortBy && in_array($formSortBy, ['likes', 'reviews', 'critic', 'usefulness', 'publish', 'rating'])) { 
                $sortBy = $formSortBy;
            }

            $formSortOrder = $filterForm->values['sort_order'];
            if ($formSortOrder && in_array($formSortOrder, ['ASC', 'DESC'])) { 
                $sortOrder = $formSortOrder;
            }
        }
        
        $qb->orderBy('sort_param', $sortOrder);

        switch ($sortBy) {
            case 'likes':
                $qb->addSelect('count(l) as hidden sort_param')
                    ->leftJoin(\App\Models\Like::class, 'l', 'WITH', 'l.owner_class = :owner_class AND l.owner_id = j.id')
                    ->setParameter('owner_class', \App\Models\Journal::class);
                break;
            case 'reviews':
                $qb->addSelect('count(r) as hidden sort_param');
                break;
            case 'critic':
                $qb->addSelect('avg(r.critic_duration_weeks) as hidden sort_param');
                break;
            case 'usefulness':
                $qb->addSelect('avg(r.critic_usefulness) as hidden sort_param');
                break;
            case 'publish':
                $qb->addSelect('avg(r.publish_duration_weeks) as hidden sort_param');
                break;
            case 'rating':
                $qb->addSelect('2*j.positive_review_count + j.neutral_review_count - 2*j.negative_review_count as hidden sort_param');
                break;
        }


        $perPage = 20;
        $totalPages = $this->em->createQueryBuilder()
            ->select('count(distinct j)')
            ->from(\App\Models\Journal::class, 'j')
            ->andWhere('j.hidden = 0')
            ->leftJoin('j.reviews','r')
            ->andWhere('r.moderated = 1')
            ->getQuery()
            ->getSingleScalarResult() / $perPage;
       
        $pagination = new \Bingo\Pagination($perPage, $this->getPage(), $totalPages, $pattern = false, $qb->getQuery());
    
        $this->data['filter_form'] = $filterForm;
        $this->data['sorted_by'] = $sortBy;
        $this->data['sorted_order'] = $sortOrder;
        $this->data['pagination'] = $pagination->get(4);
        $this->data['journals'] = $pagination->result();
        $this->data['title'] = _t('Все журналы');
        $this->view('journal_table_list');

    }
}