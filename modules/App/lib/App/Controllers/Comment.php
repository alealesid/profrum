<?php

namespace App\Controllers;

class Comment extends Base {
    
    public function comment_edit($ownerType, $ownerId, $id) {         
        $ownerTypes = \App\Models\Comment::getOwnerTypes();
        if (!isset($ownerTypes[$ownerType])) return true;
        
        $ownerClass = $ownerTypes[$ownerType];
        $owner = $ownerClass::find($ownerId);
        if (!$owner) return true;
        
        $comment = \App\Models\Comment::findOrCreate($id);
        $form = new \App\Forms\Comment('post', [], $owner);
        
        if ($form->validate()) {
            $form->fill($comment);
            $comment->user = $this->user ?: null;
            $comment->setOwner($owner);
            $comment->save();

            redirect("/comment-edit/$ownerType/$ownerId");
        }
        
        $this->data['comment_form'] = $form;
        $this->view('partial/comment_form');
    }
    
    public function comment_list($ownerType, $ownerId, $id) { 
        $ownerTypes = \App\Models\Comment::getOwnerTypes();
        if (!isset($ownerTypes[$ownerType])) return true;
        
        $ownerClass = $ownerTypes[$ownerType];
        $owner = $ownerClass::find($ownerId);
        if (!$owner) return true;

        $pattern = [
            url("/comment-list/$ownerType/$ownerId?p=1"),
            url("/comment-list/$ownerType/$ownerId?p={page}")
        ];

        $query = \App\Models\Comment::findByQuery([
            'owner_class' => $ownerClass,
            'owner_id' => $ownerId
        ], "created DESC");

        $pagination = new \Bingo\Pagination(3, $this->getPage(), $total = false, $pattern, $query);
        
        $this->data['pagination'] = $pagination->get(5);
        $this->data['comments'] = $pagination->result();
        $this->view('partial/comment_list');
    }
}