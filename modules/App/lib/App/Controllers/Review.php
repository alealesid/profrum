<?php

namespace App\Controllers;

abstract class Review extends Base {

    protected $review_class;

    protected function review($id, $view) {
        $review_class = $this->review_class;
        $review = $review_class::find($id);
        if (!$review) return true;
        if (!$review->moderated) return true;

        $this->data['comment_form'] = new \App\Forms\Comment('post', [], $review);
        $this->data['review'] = $review;
        if ($review_class == \App\Models\JournalReview::class) {
            $journalName = $review->journal->name;
            $this->data['title'] = str_replace('{name}', $journalName, _t('Отзыв к журналу {name}'));
        } else if ($review_class == \App\Models\ConferenceReview::class) {
            $conferenceName = $review->conference->name;
            $this->data['title'] = str_replace('{name}', $conferenceName, _t('Отзыв к конференции {name}'));
        }
        $this->view($view);
    }
    
    protected function review_list($criteria, $view, $hideAnonymousCheckbox = false) { 
        $review_class = $this->review_class;

        $filterForm = new \CMS\FilterForm();
        if (!$hideAnonymousCheckbox) $filterForm->checkbox('anonymous', _t('анонимные'), '', false);
        $filterForm->checkbox('positive', _t('положительные'), '', false);
        $filterForm->checkbox('negative', _t('отрицательные'), '', false);
        $filterForm->checkbox('neutral', _t('нейтральные'), '', false);
        
        $criteria['moderated'] = true;

        if ($filterForm->validate()) {
            $criteria['attitude'] = [];
            if (!$hideAnonymousCheckbox) {
                if ($filterForm->values['anonymous'])
                    $criteria['anonymous'] = true;
            }

            if ($filterForm->values['positive'])
                $criteria['attitude'][] = \App\Models\Review::ATTITUDE_POSITIVE;

            if ($filterForm->values['negative'])
                $criteria['attitude'][] = \App\Models\Review::ATTITUDE_NEGATIVE;

            if ($filterForm->values['neutral'])
                $criteria['attitude'][] = \App\Models\Review::ATTITUDE_NEUTRAL;
        }
        
        $query = $review_class::findByQuery($criteria, "created DESC");
        $pagination = new \Bingo\Pagination(5, $this->getPage(), $total = false, $pattern = false, $query);
        
        $this->data['review_class'] = $review_class;
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(5);
        $this->data['reviews'] = $pagination->result();
        $this->view($view);
    }

    protected function stashFormData() {
        $sessionName = 'review_'.uniqid();
        $stashFormSession = new \Session\SessionNamespace($sessionName);
        $stashFormSession->setExpirationSeconds(24*60*60);
        $stashFormSession->active = true;
        $stashFormSession->formData = $_POST;

        return $sessionName;
    }

    protected function getStashedFormData() {
        if (isset($_GET['stash_token'])) {
            $stashFormSession = new \Session\SessionNamespace($_GET['stash_token']);
            if ($stashFormSession->active) {
                $stashFormSession->active = false;
                return $stashFormSession->formData;
            }
        }
        
        return false;
    }
}