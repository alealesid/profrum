<?php

namespace App\Controllers;

class IMHOTop extends Base {
    
    public function standart_imhotop($id) {
        $imhotop = \App\Models\IMHOTop::findOneBy(['id' => $id, 'type' => \App\Models\IMHOTop::TYPE_STANDART]);
        if (!$imhotop) return true;
        if (($this->user && $imhotop->user->id != $this->user->id) && !$imhotop->published) return true;


        if ($this->user && $this->user == $imhotop->user) {
            $title = str_replace('{categoryname}', $imhotop->category->name, _t('Мой ИМХО-ТОП в категории {categoryname}'));
        } else {
            $title = str_replace(
                ['{username}', '{categoryname}'],
                [ $imhotop->user->name, $imhotop->category->name ],
                _t('ИМХО-ТОП пользователя {username} в категории {categoryname}')
            );
        }

        $this->data['imhotop'] = $imhotop;
        $this->data['title'] = $title;
        $this->data['editLink'] =  url('standart-imhotop-edit/'.$imhotop->category->id);
        $this->data['shareLink'] = url('standart-imhotop/'.$imhotop->id);
        $this->data['topListLink'] = url('my-standart-imhotops-list');
        $this->view('imhotop');
    }
    
    public function nonstandart_imhotop($id) {
        $imhotop = \App\Models\IMHOTop::findOneBy(['id' => $id, 'type' => \App\Models\IMHOTop::TYPE_NONSTANDART]);
        if (!$imhotop) return true;
        if (($this->user && $imhotop->user->id != $this->user->id) && !$imhotop->published) return true;

        if ($this->user == $imhotop->user) {
            $title = str_replace('{name}', $imhotop->name, _t('Мой нестандартный ТОП {name}'));
        } else {
            $title = str_replace('{name}', $imhotop->name, _t('Нестандартный ИМХО-ТОП {name}'));
        }

        $this->data['imhotop'] = $imhotop;
        $this->data['title'] = $title;
        $this->data['editLink'] = url('nonstandart-imhotop-edit/'.$imhotop->id);
        $this->data['shareLink'] = url('nonstandart-imhotop/'.$imhotop->id);
        $this->data['topListLink'] = url('my-nonstandart-imhotops-list');
        $this->view('imhotop');
    }

    public function standart_imhotop_edit($categoryId) {
        if (!$this->user) redirect('login');
        
        if (!$categoryId) return true; 
    
        $category = \App\Models\Category::find($categoryId);
        if (!$category) return true;
        
        
        $imhotopType = \App\Models\IMHOTop::TYPE_STANDART;

        $imhotop = \App\Models\IMHOTop::findOneBy([ 'user' => $this->user, 'category' => $category, 'type' => $imhotopType ]);
        if (!$imhotop) return true;

        $imhotops = $this->user->getTops($imhotopType);
        if (!in_array($imhotop, $imhotops)) $imhotops[] = $imhotop;

        $title = _t('Мой ИМХО-ТОП');
        $description = _t('Пожалуйста выберете от 1 до 10 российских журналов по этому направлению, которые Вы считаете ведущими в своей области.');
        
        $this->imhotop_edit_base($imhotop, $title, $description, $imhotops,$russianJournalsOnly = true);
    }

    public function nonstandart_imhotop_edit($id) {
        if (!$this->user) redirect('login');
        
        if (!$id) return true;

        $imhotopType = \App\Models\IMHOTop::TYPE_NONSTANDART;

        $imhotop = \App\Models\IMHOTop::findOneBy( [ 'id' => $id, 'type' => $imhotopType, 'user' => $this->user ] );
        if (!$imhotop) return true;

        if (!$imhotop->name) $imhotop->name = _t('Без названия');

        $imhotops = $this->user->getTops($imhotopType);
        if (!in_array($imhotop, $imhotops)) $imhotops[] = $imhotop;
        
        $title = _t('Нестандартный ТОП');
        $description = '<p>'._t('Вы можете предложить и опубликовать свой авторский рейтинг журналов на основе любого критерия. В том числе вы можете включить в рейтинг зарубежные журналы.').'</p>';
        $description .= '<p>'._t('Ваш рейтинг будет размещен на сайте, другие пользователи смогут комментировать его.').'</p>';
        $description .= '<p>'._t('Данный ТОП никак влияет на положение журналов в стандартных ТОПах.').'</p>';

        $this->imhotop_edit_base($imhotop, $title, $description, $imhotops);
    }
    public function my_standart_imhotops_list() {
        if (!$this->user) redirect('login');
        
        $imhotops = \App\Models\IMHOTop::findBy( [ 'type' => \App\Models\IMHOTop::TYPE_STANDART, 'user' => $this->user ] );

        $createdTopsCategories = array_map(function ($imhotop) {return $imhotop->category;}, $imhotops);
        $qb = $this->em->createQueryBuilder();
        $categoriesWithoutTops = $qb->select('c')
            ->from(\App\Models\Category::class,'c');
        if ($createdTopsCategories) {
            $qb->where($qb->expr()->notIn('c',':createdTopsCategories'))
                ->setParameter('createdTopsCategories', $createdTopsCategories);
        }
        $categoriesWithoutTops = $qb->getQuery() 
                                    ->getResult();

        $categoryOptions = array_reduce($categoriesWithoutTops, function($options, $category) {
            $options[$category->name] = $category->id;
            return $options;
        }, [ _t('Выберите сферу')=> NULL]);

        $notCompetentForm = new \Form();
        $submitCompetentClass = $this->user->competent ? "competent" : "";
        $notCompetentForm->submit(_t('Сохранить'), 'submit_competent', false, [ 'class' => "btn btn-primary $submitCompetentClass", 'id' => 'submit-competent' ])
                        ->checkbox('not_competent', _t('Не считаю себя достаточно компетентным ни в одной из сфер'), '', !$this->user->competent);

        $addTopForm = new \Form();
        $addTopForm->submit(_t('Добавить'), 'submit_top', false, [ 'class' => 'btn btn-primary' ])
                ->select('top_category', false, $categoryOptions, [ function($category) use($imhotops) {
                    if (isset($_POST['not_competent']) && $_POST['not_competent']) return $category;
                    
                    if ($category == NULL) {
                        throw new \ValidationException(_t('Выберите категорию, пожалуйста'));
                    }
                    if (count($imhotops) >= 5) {
                        throw new \ValidationException(_t('Нельзя составить больше 5 топов'));
                    }
                    return $category;
                }], NULL, ['class'=>'form-control category-select custom-select']);     
        
        if (isset($_POST['submit_competent'])) {
            if ($notCompetentForm->validate()) {
                $this->user->competent = !$notCompetentForm->values['not_competent'];
                if (!$this->user->competent) {
                    foreach ($this->user->getTops(\App\Models\IMHOTop::TYPE_STANDART) as $top) {
                        $top->delete(false);
                    }
                }
                $this->user->save();
                redirect( $this->user->competent ? 'my-standart-imhotops-list' : 'standart-imhotop-top10' );
            }
        }

        if (isset($_POST['submit_top'])) {
            if ($addTopForm->validate()) {
                $category = \App\Models\Category::find($addTopForm->values['top_category']);
                $top = new \App\Models\IMHOTop();
                $top->category = $category;
                $top->user = $this->user;
                $top->type = \App\Models\IMHOTop::TYPE_STANDART;
                $top->save();
                redirect('standart-imhotop-edit/'.$category->id);
            }
        }
        
        $this->data['title'] = _t("Мои ИМХО-ТОПЫ");
        $this->data['description'] = _t("Выберите категорию, в которых Вы считаете себя экспертом.");
        $this->data['addTopForm'] = $addTopForm;
        $this->data['notCompetentForm'] = $notCompetentForm;
        $this->data['imhotops'] = $imhotops;
        $this->view('my_standart_imhotops_list');
    } 

    public function my_nonstandart_imhotops_list() {
        if (!$this->user) redirect('login');
        
        $imhotops = \App\Models\IMHOTop::findBy( [ 'type' => \App\Models\IMHOTop::TYPE_NONSTANDART, 'user' => $this->user ] );

        $form = new \Form();
        $form->submit(_t('Добавить'), 'submit', false, [ 'class' => 'btn btn-primary' ])
                ->text('top_name', false, [ function($topName) use ($imhotops) {
                    if ($topName == "") {
                        throw new \ValidationException(_t('Название топа не может быть пустым'));
                    }
                    if (count($imhotops) >= 5) {
                        throw new \ValidationException(_t('Нельзя составить больше 5 топов'));
                    }
                    foreach ($imhotops as $top) {
                        if ($top->name == $topName) {
                            throw new \ValidationException(_t('Топ с таким именем уже создан'));
                        }
                    }
                    return $topName;
                }], false, [ 'class' => 'form-control', 'id' => 'new_top_name_form_input' ]);        
               

        if ($form->validate()) {
            $top = new \App\Models\IMHOTop();
            $top->name = $form->values['top_name'];
            $top->user = $this->user;
            $top->type = \App\Models\IMHOTop::TYPE_NONSTANDART;
            $top->save();
            redirect('nonstandart-imhotop-edit/'.$top->id);
        }
       
        $this->data['title'] = _t("Мои нестандартные ТОПЫ");
        $this->data['description'] = _t("Введите название для Вашего нового топа");
        $this->data['form'] = $form;
        $this->data['imhotops'] = $imhotops;
        $this->view('my_nonstandart_imhotops_list');
    }

    private function imhotop_edit_base($imhotop, $title, $description, $imhotops, $russianJournalsOnly = false) {
        if (!$this->user) redirect('login');

        $criteria = [];
        if ($russianJournalsOnly)
            $criteria['included_to_russian_journals'] = true;
        
        $form = new \App\Forms\IMHOTop('post', [], $imhotop, $this->user); 
        if ($form->validate()) {
                
            $imhotop->user = $this->user;
            if ($imhotop->type == \App\Models\IMHOTop::TYPE_NONSTANDART) {
                $imhotop->name = $form->values['name'];
                $imhotop->published = true;
            } else {
                $imhotop->published = $form->values['published'];
            }
            $imhotop->save();

            $imhotopItems = $imhotop->items->toArray();
            while($topItem = array_pop($form->values['items'])) {
                $criteria['id'] = $topItem["journal"];
                if (!($journal = \App\Models\Journal::findOneBy($criteria))) continue;

                if (!($imhotopItem = array_pop($imhotopItems))) $imhotopItem = new \App\Models\IMHOTopItem();

                $imhotopItem->journal = $journal;
                $imhotopItem->position = $topItem['position'];
                $imhotopItem->notes = $topItem["notes"];
                $imhotopItem->user = $this->user;
                $imhotopItem->imhotop = $imhotop;
                $imhotopItem->save();
            }

            while($imhotopItem = array_pop($imhotopItems)) $imhotopItem->delete();
            
            set_flash('info',_t('Изменения сохранены успешно'));
            $redirect_uri = ($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART ? 'standart-imhotop/' : 'nonstandart-imhotop/').$imhotop->id;
            redirect($redirect_uri);
        }
        
        $this->data['form'] = $form;
        $this->data['title'] = $title;
        $this->data['description'] = $description;
        $this->data['imhotop'] = $imhotop;
        $this->data['nonstandart'] = $imhotop->type == \App\Models\IMHOTop::TYPE_NONSTANDART;
        $this->data['user_not_competent_class'] = ( !$this->user->competent && $imhotop->type == \App\Models\IMHOTop::TYPE_STANDART ? 'user-not-competent' : '' );
        
        if ( $imhotop->type == \App\Models\IMHOTop::TYPE_STANDART ) {
            $this->data['topListLink'] = url('my-standart-imhotops-list');
        } else {
            $this->data['topListLink'] = url('my-nonstandart-imhotops-list');
        }
        
        $this->view('imhotop_edit');
    }

    public function standart_imhotop_top10() { 
        
        $filterForm = new \CMS\FilterForm;
        $categoryOptions = \App\Models\Category::selectOptions([], 'name', [_t('Все') => NULL]);
        $filterForm->select('category', false, $categoryOptions, '', NULL, ['class'=>'form-control category-select custom-select'])
            ->object_filter(\App\Models\Category::class);

        $category = reset($categoryOptions);

        $topsCriteria = ['type' => \App\Models\IMHOTop::TYPE_STANDART, 'published' => true];
        if ($filterForm->validate()) {
            $category = $filterForm->values['category'];
            if ($category) $topsCriteria['category'] = $category;
        }

        $this->data['categoryName'] = @$category->name ?: _t("Все");
        $this->data['journals'] = \App\Models\IMHOTop::getStandartTop10Journals($category);
        $this->data['tops'] = \App\Models\IMHOTop::findBy($topsCriteria, 'created DESC', 0, 5);
        $this->data['filter_form'] = $filterForm;
        $this->data['title'] = _t('Рейтинг ТОП 10');
        $this->view('imhotop_top10');
    }

    public function imhotop_delete($id) {
        if (!$this->user) redirect('login');
        
        if (!($imhotop = \App\Models\IMHOTop::find($id))) return true; 
        if ($imhotop->user != $this->user) return true; 

        $imhotopType = $imhotop->type;
        $imhotop->delete();

        $redirectUri = $imhotopType == \App\Models\IMHOTop::TYPE_STANDART? 
            'my-standart-imhotops-list' : 'my-nonstandart-imhotops-list';
        redirect($redirectUri);
    }

    function imhotop_list() {
        
        $filterForm = new \CMS\FilterForm;
        $categoryOptions = \App\Models\Category::selectOptions([], 'name', [_t('Все') => NULL]);
        $filterForm->select('category', false, $categoryOptions, '', NULL, ['class'=>'form-control custom-select'])
            ->object_filter(\App\Models\Category::class);
        $filterForm->checkbox('standart', _t('стандартные'));
        $filterForm->checkbox('not_standart', _t('нестандартные'));

        $topsCriteria = [ 'published' => true ];
        if ($filterForm->validate()) {
            $category = $filterForm->values['category'];
            if ($category) $topsCriteria['category'] = $category;
            if ($filterForm->values['standart']) {
                $topsCriteria['type'] = \App\Models\IMHOTop::TYPE_STANDART;
            }
            if ($filterForm->values['not_standart']) {
                $topsCriteria['type'] = \App\Models\IMHOTop::TYPE_NONSTANDART;
            }
        }

        $query = \App\Models\IMHOTop::findByQuery($topsCriteria, 'created DESC');
        $pagination = new \Bingo\Pagination(10, $this->getPage(), false, $pattern = false, $query);

        $this->data['tops'] = $pagination->result();
        $this->data['pagination'] = $pagination->get(4);
        $this->data['filter_form'] = $filterForm;
        $this->data['title'] = _t('ИМХО топы');
        $this->view('imhotop_list');
    }
}