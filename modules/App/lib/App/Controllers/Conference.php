<?php

namespace App\Controllers;

class Conference extends Base {
    
    public function conference($id) {  
        
        $conference = \App\Models\Conference::find($id);
        if (!$conference) redirect('/');
        if ($conference->hidden) return true;

        $this->data['anonymousUserSession'] = $this->anonymousUserSession;
        $this->data['conference'] = $conference;
        $this->data['title'] = $conference->name;
        $this->view('conference');
    }
    

    public function conference_list() {  

        $categories = [];
        $categoriesOptions = [_t('Все') => 'none'];
        foreach(\App\Models\Category::findAll() as $category) {
            $categories[$category->id] = $category;
            $categoriesOptions[$category->name] = $category->id;
        }
        
        $filterForm = new \CMS\FilterForm; 
        $filterForm->attribute_text('name',false,false,false,["id"=>"conferences", "class"=>"autocomplete form-control", "url"=> url('autocomplete?entities=conference')]); 
        $categoryOptions = \App\Models\Category::selectOptions([], 'name', [_t('Все') => NULL]); 
        $filterForm->select('category', false, $categoryOptions, '', false, [ 'class' => 'custom-select' ]) 
            ->object_filter(\App\Models\Category::class); 
 
        $qb = $this->em->createQueryBuilder() 
            ->select('c') 
            ->from(\App\Models\Conference::class, 'c') 
            ->andWhere('c.hidden = 0'); 
 
        $search_params = []; 
        if ($filterForm->validate()) { 
            $name = trim($filterForm->values['name']); 
            if ($name) { 
                $search_params['name'] = $name; 
                $qb->andWhere('c.name LIKE :name') 
                    ->setParameter('name', '%'.$name.'%'); 
            } 
            $category = $filterForm->values['category']; 
            if ($category) { 
                $search_params['category'] = $category; 
                $qb->leftJoin('c.categories', 'cat') 
                    ->andWhere('cat.id = :category') 
                    ->setParameter('category', $category); 
            } 
        }

        $perPage = 5;
        $total = \DoctrineExtensions\Paginate\Paginate::getTotalQueryResults($qb->getQuery(), true);

        $qb->addSelect('count(r) as hidden sort_param') 
            ->leftJoin('c.reviews', 'r') 
            ->andWhere('r.moderated = :moderated OR r IS NULL')
            ->setParameter('moderated', true)
            ->groupBy('c') 
            ->orderBy('sort_param', 'desc'); 
 
        $pagination = new \Bingo\Pagination($perPage, $this->getPage(), $total, false, $qb->getQuery());
        
        $this->data['search_params'] = $search_params;
        $this->data['filter_form'] = $filterForm;
        $this->data['pagination'] = $pagination->get(4);
        $this->data['conferences'] = $pagination->result();
        $this->data['reviews'] = \App\Models\ConferenceReview::findBy(['moderated' => true], 'created DESC', 0, 3);
        $this->data['questions'] = \App\Models\Question::findBy(['owner_class'=>\App\Models\Conference::class], 'created DESC', 0, 3);
        $this->data['title'] = _t('Конференции');
        $this->view('conference_list');
    }
}