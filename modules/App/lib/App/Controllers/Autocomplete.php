<?php

namespace App\Controllers;

class Autocomplete extends Base {

    /**
     * Process urls : entityClass=field,entityClass=field, ...
     * returns json array of strings where each entity of each entity class
     * represented by specified field.
     * If field is not specified, 'name' field used.
    */
    public function autocomplete () {

        $classes = [
            'journal' => \App\Models\Journal::class,
            'conference' => \App\Models\Conference::class
        ];

        $entitySlugs = isset($_GET['entities']) && !empty($_GET['entities']) ? explode(',', $_GET['entities']) : [];
        $term = isset($_GET['term']) ? $_GET['term'] : '';

        $suggestions = [];
        
        foreach($entitySlugs as $slug) {
            $slugParts = explode('=', $slug);
            $entityClass = $classes[$slugParts[0]];
            $field = isset($slugParts[1]) ? $slugParts[1] : 'name';

            $qb = $this->em->createQueryBuilder()
            ->select('en.'.$field)
            ->from($entityClass, 'en')
            ->where('en.'.$field .' LIKE :term')
            ->setParameters([
                'term' => '%'.$term.'%'
            ]);

            if (isset($_GET['russian'])) {
                $qb->andWhere('en.included_to_russian_journals = 1');
            }
            
            $entitySuggestions = $qb->getQuery()
                                    ->getResult();

            $suggestions = array_merge($suggestions, array_map('current', $entitySuggestions));
        }
        
        if ($suggestions) {
            echo json_encode($suggestions);
        } else {
            echo json_encode( [ "Не найдено" ] );
        }
        return;
    }
}