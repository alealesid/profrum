<?php

namespace App\Controllers;

class JournalReview extends Review {

    protected $review_class = \App\Models\JournalReview::class;
    
    public function journal_review_edit($journal_id, $review_id) {

        if (isset($_GET['redirect'])) {
            $transitUrl = urldecode($_GET['redirect']) . '?' . http_build_query([
                'redirect' => urlencode(\Bingo\Routing::$uri),
                'stash_token' => $this->stashFormData()
            ]);
            redirect($transitUrl);
        }

        $journal = \App\Models\Journal::find($journal_id);
        $review = \App\Models\JournalReview::findOrCreate($review_id);

        if (!$review->moderated) return true;
        
        $form = new \App\Forms\JournalReview('post', ['class' => $review->article_status], $review, $journal);

        if ($form->validate($this->getStashedFormData())) {  
            $form->fill($review);

            if (in_array($review->article_status, [
                \App\Models\JournalReview::ARTICLE_STATUS_REFUSED,
                \App\Models\JournalReview::ARTICLE_STATUS_ON_CRITIC
            ])) {
                $review->has_critic = false;
            } elseif (in_array($review->article_status, [
                \App\Models\JournalReview::ARTICLE_STATUS_CRITIC_REFUSE,
                \App\Models\JournalReview::ARTICLE_STATUS_REPEAT_CRITIC
            ])) {
                $review->has_critic = true;
            }
            if (!$review->has_critic) {
                $review->critic_usefulness = NULL;
                $review->critic_duration_weeks = NULL;
            }
            if ($review->article_status != \App\Models\JournalReview::ARTICLE_STATUS_PUBLISHED) {
                $review->publish_duration_weeks = NULL;
            }

            $review->user = $this->user ? $this->user : NULL;
            if (!$review->user) {
                $review->anonymous = true;
                $review->moderated = false;
            }
            $review->save();
                     
            if ($review->user) { 
                set_flash('info',_t('Спасибо за ваш отзыв!'));
                $redirect_uri = 'journal-review/'.$review->id;
            } else { 
                set_flash('info',_t('Cпасибо, ваш отзыв отправлен на модерацию')); 
                $redirect_uri = 'journal/'.$review->journal->id; 
            } 

            redirect($redirect_uri);
        }
        
        $filterForm = new \CMS\FilterForm;

        $this->data['title'] = _t('Отзыв к журналу');
        $this->data['review_form'] = $form; 
        $this->data['filter_form'] = $filterForm;
        $this->data['review'] = $review; 
        $this->data['article_validation_status'] = @$form->values['article_status'] ?: '';
        $this->view('journal_review_edit');

    }
    
    public function journal_review($id) {
        $this->data['shareLink'] = url('journal-review/'.$id);
        $this->review($id, 'journal_review');
    }
    
    protected function journal_review_list_base($journal_id, $view) { 
        $criteria = [];
        if ($journal_id) {
            $journal = \App\Models\Journal::find($journal_id);
            if (!$journal) return true;
            $criteria['journal'] = $journal;
        }
        $this->review_list($criteria, $view);
    }
    
    public function journal_review_list($journal_id) {
        $this->data['title'] = _t('Список отзывов'); 
        $this->journal_review_list_base($journal_id, 'review_list');
    }
    
    public function journal_review_list_partial($journal_id) { 
        $this->journal_review_list_base($journal_id, 'partial/review_list');
    }
    
    public function journal_review_list_user_partial($user_id) { 
        $criteria = [];
        if ($user_id) {
            $user = \App\Models\User::find($user_id);
            if (!$user) return true;
            $criteria['user'] = $user;
            if ($user != $this->user) $criteria['anonymous'] = false;
        }
        $this->review_list($criteria, 'partial/review_list', true);
    }
}