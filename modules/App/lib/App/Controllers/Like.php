<?php

namespace App\Controllers;

class Like extends Base {
    
    public function like($ownerType, $ownerId) { 
        if (!isset(\App\Models\Like::getOwnerTypes()[$ownerType])) return true;

        $ownerClass = \App\Models\Like::getOwnerTypes()[$ownerType];
        $owner = $ownerClass::find($ownerId);
        if (!$owner) return true;

        $session = $this->anonymousUserSession;

        $user = @$this->user ?: null;
        
        if ($user) {
            $like = \App\Models\Like::findOneBy([ 
                'owner_class' => $ownerClass, 
                'owner_id' => $ownerId, 
                'user' => $user 
            ]);
        } else if (!empty($session->userLikes)) {
            $like = null;
            foreach ($session->userLikes as $userLike) {
                if ($userLike['owner_class'] == $ownerClass && $userLike['owner_id'] == $ownerId) {
                    $like = \App\Models\Like::find($userLike['id']); 
                }
            }
        } else {
            $like = null;
        }
        
        $liked = $like != NULL;

        if (!$liked) {
            $like = new \App\Models\Like();
            $like->user = $user;
            $like->setOwner($owner);
            $like->save();
            if (!$user) {
                $session->userLikes[] = [
                    'owner_class' => $ownerClass,
                    'owner_id' => $ownerId, 
                    'id' => $like->id,
                ];
            }
        } else {
            $like->delete();
            if (!empty($session->userLikes)) {
                $session->userLikes = array_filter($session->userLikes, function ($like) use ($ownerClass, $ownerId) {
                    if ($like['owner_class'] != $ownerClass && $like['owner_id'] != $ownerId) {
                        return $like;
                    }
                });
            }
        }
        
        echo json_encode([
            'liked' => !$liked,
            'likeCount' => $owner->getLikesCount()
        ]);
        return;
    }
}