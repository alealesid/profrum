<?php

namespace App\Forms;

class ConferenceReview extends Review {
    public function __construct($method, $atts = [], $review = null) {
        parent::__construct($method, $atts, $review);

        $this->select('difficult', _t('Насколько сложно стать участником конференции'), [ 1,2,3,4,5,6,7,8,9,10 ], '', 
                        @$review->difficult, [ 'class' => 'star-rating' ]);

        $this->select('level', _t('Уровень докладов'), [ 1,2,3,4,5,6,7,8,9,10 ], '', 
                        @$review->level, [ 'class' => 'star-rating' ]);

        $this->select('usefulness', _t('Практическая польза'), [ 1,2,3,4,5,6,7,8,9,10 ], '', 
                        @$review->usefulness, [ 'class' => 'star-rating' ]);

        $this->select('coffeebreaks', _t('Кофе-брейки/фуршет'), [ 1,2,3,4,5,6,7,8,9,10 ], '', 
                        @$review->coffeebreaks, [ 'class' => 'star-rating' ]);

        $this->select('culture', _t('Культурная программа'), [ 1,2,3,4,5,6,7,8,9,10 ], '', 
                        @$review->culture, [ 'class' => 'star-rating' ]);
    
        $this->checkbox('has_culture', _t('Культурная программа'), '', @$review->has_culture); 

        $this->attribute_text('conference', _t('Введите название конференции'), [function ($conference) { 
            if (!$conference)
                throw new \ValidationException(_t('Необходимо указать конференцию'));
            return $conference;
        }], $review->conference, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=conference'),])
            ->object_filter(\App\Models\Conference::class, 'name');
    }
}