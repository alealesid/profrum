<?php

namespace App\Forms;

class Review extends \Form {
    public function __construct($method, $atts = [], $review = null) {
        parent::__construct($method, $atts);
        
        $this->checkbox('anonymous', _t('Анонимно'), '', @$review->anonymous);
        
        $this->add(new \App\Forms\Elements\ToggleRadioSelect([
            'name' => 'attitude',
            'label' => _t('Впечатления'),
            'options' => [
                _t('Положительные') => \App\Models\Review::ATTITUDE_POSITIVE,
                _t('Нейтральные') => \App\Models\Review::ATTITUDE_NEUTRAL,
                _t('Негативные') => \App\Models\Review::ATTITUDE_NEGATIVE,
            ],
            'rules' => 'required',
            'value' => @$review->attitude,
            'atts' => ['class'=>'toggle-radio form-group'],
            'label_classes' => [
                \App\Models\Review::ATTITUDE_POSITIVE => 'success', 
                \App\Models\Review::ATTITUDE_NEUTRAL => 'neutral', 
                \App\Models\Review::ATTITUDE_NEGATIVE => 'danger'
            ]
        ]));
        
        $this->textarea('notes', _t('Комментарии'), [function ($value, $formElement, $form) {
            if (count(explode(' ', $value)) > 250) {
                throw new \ValidationException(_t('Текст не должен превышать 250 слов'));
            }
            return $value;
        }], @$review->notes, ['class'=>'form-control']);
    }
       
    public function checkbox($name, $label, $rules='', $value=false, $atts=false) {
        $this->add(new \App\Forms\Elements\BootstrapCheckbox(compact('name','label','rules','value','atts')));
        return $this;
    }

    public function text_date($name,$label,$rules='',$value=false,$atts=false) {
        $format = $atts && isset($atts['format']) ? $atts['format'] : "d.m.Y";

        $this->add(new \Bingo\FormElement_Text(compact('name','label','rules','value','atts')));
        $this->add_filter(new \Bingo\FormFilter_DateTime($format))->add_class("date");
        return $this;
    }

    public function validate($list = false) {
        if (!$list) {
            if ($this->atts['method']=='post') $list = $_POST;
            if ($this->atts['method']=='get') $list = $_GET;
        }

        if (empty($list)) return false;
        
        $this->nameHash = array();
        foreach ($this->elements as $el) {
            $name = $el->name;
            if ($name) $this->nameHash[$name] = $el;
        }
        return $this->validate_value($list);
    }
}

