<?php

namespace App\Forms;

class  Comment extends \Form {
    public function __construct($method = 'post', $atts = [], $owner = null, $comment = null) {

        if (!array_key_exists('action', $atts) && isset($owner)) {
            $commentOwnerTypes = array_flip(\App\Models\Comment::getOwnerTypes());
            $ownerType = $commentOwnerTypes[get_class($owner)];
            $ownerId = $owner->id;
            $id = isset($comment) ? $comment->id : '';
            $atts['action'] = "comment-edit/$ownerType/$ownerId/$id";
        } elseif (!array_key_exists('action', $atts)) {
            $atts['action'] = '';
        }

        parent::__construct($method, $atts);

        $this->text('text', _t('Текст'), 'required', false, ['class'=>'form-control', 'placeholder' => _t('Комментарий')]);
    }
}