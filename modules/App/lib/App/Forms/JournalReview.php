<?php

namespace App\Forms;

class JournalReview extends Review {
    public function __construct($method, $atts = [], $review = null, $journal = null) {
        parent::__construct($method, $atts, $review);
        
        $this->checkbox('has_critic', _t('Была рецензия'), '', @$review->has_critic);
        $this->checkbox('special', _t('Это был специальный выпуск'), '', @$review->special);
        
        $this->textarea('article_withdraw_reasons', _t('Причины отзыва статьи'), [function ($value, $formElement, $form) {
            if (count(explode(' ', $value)) > 250) {
                throw new \ValidationException(_t('Текст не должен превышать 250 слов'));
            }
            return $value;
        }], @$review->article_withdraw_reasons, ['class'=>'form-control']);
                
        $this->text_date('article_date', _t('Год и месяц подачи статьи'), ['required', function ($value, $formElement, $form) {
            /** @var $value DateTime */
            if ($value > new \DateTime('now')) {
                throw new \ValidationException(_t('Невозможная дата'));
            }
            return $value;
        }], @$review->article_date, ['class'=>'form-control', 'format' => "m.Y"]);

        $this->slider('critic_duration_weeks', _t('Сколько времени прошло с момента подачи статьи до получения первых рецензий (в неделях)?'), [
            function ($value, $formElement, $form) {
                $data = $this->atts['method']=='post'?$_POST:$_GET; 

                if (
                    $data['article_status'] == \App\Models\JournalReview::ARTICLE_STATUS_PUBLISHED
                    && isset($data['publish_duration_weeks']) 
                    && is_numeric($data['publish_duration_weeks'])
                ) {
                    if ($value > $data['publish_duration_weeks']) {
                        throw new \ValidationException(_t('Длительность рецензирования не может превышать времени до публикации'));
                    }
                }
                
                return $value;
            }
        ], @$review->critic_duration_weeks, 1, 100);

        $this->slider('publish_duration_weeks', _t('Сколько времени прошло с момента подачи статьи до печати статьи (в неделях)?'), '', @$review->publish_duration_weeks, 1, 100);
        $this->select('critic_usefulness', _t('Насколько полезными были рецензии?'), [ 1,2,3,4,5,6,7,8,9,10 ], '', @$review->critic_usefulness, [ 'class' => 'star-rating' ]);
        
        $statuses = array_flip(\App\Models\JournalReview::getArticleStatusLabels());
        $this->select('article_status', _t('Статус статьи'), $statuses, 'required', @$review->article_status, ['class'=>'form-control custom-select']);

        $this->attribute_text('journal', _t('Введите название журнала'), [function ($journal) { 
            if (!$journal)
                throw new \ValidationException(_t('Необходимо указать журнал'));
            return $journal;
        }], $journal, ['class' => 'form-control combobox', 'url'=> url('autocomplete?entities=journal'),])
            ->object_filter(\App\Models\Journal::class, 'name');
    }
}

