<?php

namespace App\Forms;

class IMHOTop extends \Form {

    public function __construct($method, $atts = [], $imhotop, $user) {
        parent::__construct($method, $atts);

        if (isset($_POST['items'])) {
            foreach($_POST['items'] as $key => $item) {
                $_POST['items'][$key]['position_label'] = $_POST['items'][$key]['position'];
            }
        }

        $topItemForm = new \Form();
        $topItemForm->html('<strong>{value}<strong>', ['name' => 'position_label']);

        $criteria = [];
        if ($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART) {
            $criteria['included_to_russian_journals'] = true;
            $url = 'autocomplete?entities=journal&russian=true';
        } else {
            $url = 'autocomplete?entities=journal';
        }

        $topItemForm->attribute_text('journal', _t('Журнал'), $this->getJournalValidators(), false, ['class' => 'form-control combobox', 'url'=> url($url),])
            ->object_filter(\App\Models\Journal::class, 'name');

        $topItemForm->add(new \Bingo\FormElement_Hidden([
            'name' => 'position',
            'value' => false,
            'rules' => $this->getPositionValidators()
        ]));
        $topItemForm->text('notes', _t('Заметки'), false, false, ['class'=>'form-control']);

        if ($imhotop->type == \App\Models\IMHOTop::TYPE_NONSTANDART) {
            $this->text('name', false, '', $imhotop->name, ['class'=>'form-control', 'placeholder' => _t('Введите название топа')]);
        }
        $this->ruled_form_list('items', $topItemForm, $this->getItemsValidators(), $this->serializeIMHOTop($imhotop), true, ['locked' =>true, 'class' => 'table']);
        $this->checkbox('not_competent', _t('Не считаю себя достаточно компетентным ни в одной из сфер'), '', !$user->competent);
        $this->checkbox('published', _t('Публиковать на сайте'), '', $imhotop->published, [ 'class' => 'pull-left' ]);

    }
        
    public function ruled_form_list($name, $form, $rules='', $value=false, $as_table=false, $atts=false) {
        $this->add(new \Bingo\FormElement_Array(compact('name', 'form', 'rules', 'value', 'as_table', 'atts')));
        return $this;
    }

    private function serializeIMHOTop($imhotop) {
        $imhotopItems = $this->getCompleteItemList($imhotop->items->toArray());

        return array_reduce($imhotopItems, function ($items, $imhotopItem) {
            $items[$imhotopItem->position] = [
                'journal' => $imhotopItem->journal,
                'notes' => $imhotopItem->notes,
                'position' => $imhotopItem->position,
                'position_label' => $imhotopItem->position
            ];
            return $items;
        }, []);
    }

    public function getCompleteItemList($items) {
        $imhotopItems = array_reduce($items, function($imhotopItems, $imhotopItem) {
            $imhotopItems[$imhotopItem->position] = $imhotopItem;
            return $imhotopItems;
        }, []);

        $completeImhotopList = [];
        for($position = 1; $position <= \App\Models\IMHOTop::JOURNAL_COUNT_IN_TOP; $position++) {
            if (isset($imhotopItems[$position])) {
                $completeImhotopList[$position] = $imhotopItems[$position]; 
            }
            else {
                $item = new \App\Models\IMHOTopItem();
                $item->position = $position;
                $completeImhotopList[$position] = $item;
            }
        }

        return $completeImhotopList;
    }

    private function getPositionValidators() {
        return [
            function ($position) {
                if ($position < 1 || $position > \App\Models\IMHOTop::JOURNAL_COUNT_IN_TOP) 
                    trigger_error(_t('Недоступная позиция топа'));
                return $position;
            }
        ];
    }

    private function getJournalValidators() {
        return [
            function ($journalId) {
                if (!empty($journalId) && !\App\Models\Journal::find($journalId))
                    trigger_error(_t('Несуществующий журнал'));
                return $journalId;
            }
        ];
    }
    private function getItemsValidators() {
        return [
            function ($items) {
                $journals = [];
                
                foreach($items as $item) {
                    $journal = $item['journal'];
                    if (empty($journal)) continue;
                    if (in_array($journal, $journals, true)) { 
                        throw new \ValidationException(_t("Один из журналов встречается в топе несколько раз"));
                    } else {
                        $journals[] = $journal;
                    }
                }

                return $items;
            }, 
            function ($items) {
                $topEmpty = array_reduce($items, function ($empty, $item) {
                    return $empty && empty($item['journal']);
                }, true);

                if ($topEmpty) {
                    throw new \ValidationException(_t("Топ пуст"));
                }

                return $items;
            }
        ];
    }
}