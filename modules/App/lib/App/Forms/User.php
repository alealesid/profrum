<?php

namespace App\Forms;

class User extends \Form {
    public function __construct($method, $atts = [], $user = null) {
        parent::__construct($method, $atts);

        $newUser = !$user;

        $this->text('name', _t('ФИО'), ['required'], @$user->name, ['class'=>'form-control']);

        $this->text('email', _t('Email'), ['required', 'valid_email', function($val) use ($user) {

            $loginExists = \App\Models\User::findOneBy(['email' => $val]);
            if (!$loginExists) return $val;
            if ($user && $loginExists->id == $user->id) return $val;

            throw new \ValidationException(_t('Такой email уже зарегистрирован'));
        }], @$user->email, ['class'=>'form-control']);

        $this->password('password', _t('Пароль'), $this->getPasswordValidators($newUser), false, ['class'=>'form-control']);

        $this->password('password_again', _t('Пароль ещё раз'), $newUser ? 'required' : '', false, ['class'=>'form-control']);
        
        $this->text('work', _t('Место работы (учёбы)'), [], @$user->work, ['class'=>'form-control']);

        $this->text('position', _t('Должность'), [], @$user->position, ['class'=>'form-control']);
        
        $this->text("avatar", _t("Аватар"), '', @$user->avatar, ['class'=>'form-control browse-avatar']);

        $degrees = array_flip(\App\Models\User::getDegreeLabels());
        $this->select('degree', _t('Учёная степень'), $degrees, [], @$user->degree, ['class'=>'form-control']);

        $this->text('status', _t('Статус'), ['required'], @$user->status, ['class'=>'form-control']);
    }

    protected function getPasswordValidators($required) {
        $request_data = $this->atts['method']=='post'?$_POST:$_GET;
        $validators = [
            function ($password) use ($request_data) {
                if ($password && $password != $request_data['password_again'])
                    throw new \ValidationException(_t('Пароли не совпадают'));
                return $password;
            }
        ];
        if ($required) $validators[] = 'required';

        return $validators;
    }
}

