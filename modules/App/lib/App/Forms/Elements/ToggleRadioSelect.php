<?php

namespace App\Forms\Elements;

class ToggleRadioSelect extends \Bingo\FormElement_RadioSelect {

    function render_element() {
        $name = $this->name;

        $options = $this->options;
        $value = $this->value;
        $out_value = $this->out('value');

        echo "<div {$this->attr('class')}>";
        foreach ($options as $key=>$val) {
            if ($this->filter) {
                $val = $this->filter->from($val);
            }
            else {
                $val = htmlspecialchars($val,ENT_QUOTES);
            }
            if ($this->multiple) {
                if (in_array($val,$out_value))
                    $sel = "checked";
                else
                    $sel = "";
            } else {
                if ($val==$out_value)
                    $sel = "checked";
                else
                    $sel = "";
            }
            $labelClass = $this->args['label_classes'][$val];
            echo "<input id = '$name\_$key' type='radio' {$this->attr('name')} value='$val' $sel ><label class='$labelClass' for = '$name\_$key'>$key</label>";
        }
        echo "</div>";
    }
    
}