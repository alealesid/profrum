<?php

namespace App\Forms\Elements;

class FormElement_Slider extends \Bingo\FormElement_Hidden {

    public function __construct($args = []) {
        if (is_string($args['rules'])) $args['rules'] = explode('|',$args['rules']);
        if (!is_array($args['rules'])) $args['rules'] = [$args['rules']];

        $args['rules'][] = 'numeric';
        $args['rules'][] = function ($value, $formElement, $form) use ($args) {
            if ($value > $args['max']) {
                throw new \ValidationException(_t('Слишком большое значение'));
            }
            return $value;
        };
        $args['rules'][] = function ($value, $formElement, $form) use ($args) {
            if ($value < $args['min']) {
                throw new \ValidationException(_t('Слишком маленькое значение'));
            }
            return $value;
        };

        parent::__construct($args);
    }

    public function render_label() {
        if ($this->label)
            echo "<label {$this->attr('id','for')}>{$this->label}</label>";
    }

    public function render_error() {
        if ($this->error)
            echo "<span class='error'>{$this->error}</span>";
    }
    public function render_element() {
        parent::render_element();
        $class = isset($this->args['class']) ? $this->args['class'] : '';
        echo "<div class='slider' {$this->attr('name','for')} {$this->attr('min')} {$this->attr('max')}></div>";
    }

    public function render_value() {
        $class = isset($this->args['class']) ? $this->args['class'] : '';
        echo "<span class='slider-value $class' {$this->attr('name','for')}></span>";
    }
    
    public function render() {
    }

}