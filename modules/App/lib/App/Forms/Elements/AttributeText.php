<?php

namespace App\Forms\Elements;

class AttributeText extends \Bingo\FormElement {

    public function __construct($args=[]) {
        parent::__construct($args);
        $this->attributes = $args['atts'];
    }

    function render_element() {
        $additionalAttributes = "";
        foreach($this->attributes as $key=>$attr) {
            $additionalAttributes .= ' ' . $this->attr( $key);
        }
        echo "<input type='text' {$this->attr('name')} {$this->attr('value')} $additionalAttributes />";
    }
    
}