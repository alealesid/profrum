<?php

namespace App\Forms\Elements;

class BootstrapCheckbox extends \Bingo\FormElement_Checkbox {
    
    function render_label() {
        echo "<label {$this->attr('class')}>{$this->label}</label>";
    }

    function render_element() {
        $check = ($this->value) ? "checked" : '';
        echo "<input type='checkbox' {$this->attr('id')} {$this->attr('name')} $check/>";
    }
}