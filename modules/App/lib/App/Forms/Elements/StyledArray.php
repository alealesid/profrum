<?php

namespace App\Forms\Elements;

class StyledArray extends \Bingo\FormElement_Array {

    public function __construct($args) {
        parent::__construct($args);
        $atts = $args['atts'];
        $this->showDeleteButton = isset($atts['showDeleteButton']) ? $atts['showDeleteButton'] : true;
        $this->showAddButton = isset($atts['showAddButton']) ? $atts['showAddButton'] : true;
        $this->numerated = isset($atts['numerated']) ? $atts['numerated'] : false;
        $this->itemClass = isset($atts['itemClass']) ? $atts['itemClass'] : '';
    }

    public function validate($value,$addErrorClass=true) {
        $childrenValidated = parent::validate($value,$addErrorClass);
        if (! $childrenValidated) $this->error = true;

        $validateRules = new \ReflectionMethod(get_parent_class(get_parent_class($this)), 'validate');
        $parentValidated = $validateRules->invoke($this, $this->value, $addErrorClass);
        
        return $childrenValidated && $parentValidated;
    }

    public function render_error() {
        if (is_string($this->error))
            echo "<span class='error'>{$this->error}</span>";
    }

    function render() {
        $this->render_label();
        $this->render_error();
        $form = $this->form;

        $as_table = $this->as_table;


        if ($as_table) {
        /*$labels = array_reduce($form->elements, function($labels, $el) {
            $labels[$el->original_name] = $el->original_label;
            $el->label = false;
            return $labels;
        }, []);

        echo var_dump($labels);*/

            echo "<table {$this->attr('class')}>";
            echo "<tr>";
            if ($as_table && $this->numerated) echo "<th></th>";
            foreach ($form->elements as $el) {
                if ($el->label) $el->original_label = $el->label;
                $el->label = false;
                echo "<th class='item_{$el->original_name}  {$this->itemClass}'>".$el->original_label."</th>";
            }
            if (!$this->locked)
                echo "<th></th>";
            echo "</tr>";

            $container = "tr";
            $pre = "<td>";
            $post = "</td>";
        } else {
            $container = "fieldset";
            $pre = $post = "";
        }

        $array = $this->value;
        if ($this->filter)
            $array = $this->filter->from($array);
        
        $rowNumber = 0;
        if ($array) foreach ($array as $i=>$item) {
            $rowNumber++;
            if ('{id}'===$i) continue;
            echo "<$container class='item {$this->itemClass}'>";
            foreach ($form->elements as $el) {
                $key = $el->original_name;
                if ($key) {
                    $el->value = isset($item[$key]) ? $item[$key] : null;
                    $el->name = "{$this->name}[$i][{$key}]";
                    $el->error = @$this->form_errors[$i][$key];
                    if ($el->error)
                        $el->class = $el->original_class . " error";
                    else
                        $el->class = $el->original_class;
                }
            }
            
            if ($as_table && $this->numerated) echo "<th>$rowNumber</th>";
            foreach ($form->root->children as $el) {
                echo $pre;
                $el->render();
                echo $post;
            }
            
            if (!$this->locked && $this->showDeleteButton) $this->render_delete_button();
                
            echo "</$container>";
        }
        if (!$this->locked) {
            echo "<$container class='array_element_template item {$this->itemClass}' style='display:none'>";


            foreach ($form->elements as $el) {
                $key = $el->original_name;
                if ($key) {
                    $el->value = $el->original_value;
                    $el->name = "{$this->name}[{id}][{$key}]";
                }
                $el->error = false;
            }
            foreach ($form->root->children as $el) {
                echo $pre;
                $el->render();
                echo $post;
            }
            if (!$this->locked && $this->showDeleteButton) $this->render_delete_button();
            echo "</$container>";
        }

        if ($as_table) echo "</table>";


        if (!$this->locked && $this->showAddButton) $this->render_add_button();
    }

    public function render_add_button() {
        echo "<a class='array_element_add' href=#>"._t('Add element','bingo')."</a>";
    }

    public function render_delete_button() {
        if ($this->as_table) {
            $pre_del = "<td class='array_element_del_td'>";
            $post_del = "</td>";
        } else {
            $pre_del = $post_del = "";
        }

        echo $pre_del."<a class='array_element_del' href=#>"._t('Remove','bingo')."</a>".$post_del;
    }
    
}