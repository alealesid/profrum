<?php

namespace App\Models;


/** 
* @Entity
* @Table(name="comments")
*/
class Comment extends \ActiveEntity {
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(length=1500) */
    public $text;
    
    /** @ManyToOne(targetEntity="User") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(type="integer", nullable=false) */
    public $owner_id;
    
    /** @Column(length=256, nullable=false) */
    public $owner_class;
    
    public function __construct() {
        $this->created = new \DateTime('now');  
    }
    
    public function getOwner() {
        $owner_class = $this->owner_class;
        return $owner_class::find($this->owner_id);
    }

    public function setOwner($owner) {
        $this->owner_id = $owner->id;
        $this->owner_class = get_class($owner);
    }
    
    public static function getOwnerTypes() {
        return [
            'journal-review' => \App\Models\JournalReview::class,
            'conference-review' => \App\Models\ConferenceReview::class,
            'question' => \App\Models\Question::class,
            'imhotop' => \App\Models\IMHOTop::class
        ];
    }

    public function getOwnerUrl() {
        $owner_type = array_flip(self::getOwnerTypes())[$this->owner_class];
        return "/$owner_type/".$this->owner_id;
    }
}