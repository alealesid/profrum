<?php

namespace App\Models\EventSubscribers;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class CommentDeleteSubscriber implements EventSubscriber {

    public function postRemove(LifecycleEventArgs $event) {
        $em = $event->getEntityManager();
        $entity = $event->getEntity();
        if (in_array(get_class($entity), \App\Models\Comment::getOwnerTypes())) {
            $qb = $em->createQueryBuilder()
                ->delete(\App\Models\Comment::class, 'c')
                ->where('c.owner_id = :owner_id')
                ->andWhere('c.owner_class = :owner_class')
                ->setParameters([
                    'owner_id' => $entity->id,
                    'owner_class' => get_class($entity)
                ])
                ->getQuery()
                ->getResult();
        }
    }

    public function getSubscribedEvents() {
        return [Events::postRemove];
    }
}