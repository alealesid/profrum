<?php

namespace App\Models\EventSubscribers;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class RefreshResearchAuthorRating implements EventSubscriber {

    public function postLoad(LifecycleEventArgs $event) {
        $em = $event->getEntityManager();
        $entity = $event->getEntity();
        if ($entity instanceof \App\Models\Research) {
            $this->countReviewsAttitudes($entity);
        }
    }

    public function postPersist(LifecycleEventArgs $event) {
        $em = $event->getEntityManager();
        $entity = $event->getEntity();
        if ($entity instanceof \App\Models\Review) {
            switch (get_class($entity)) {
                case \App\Models\JournalReview::class :
                    $research = $entity->journal;
                    break;
                case \App\Models\ConferenceReview::class :
                    $research = $entity->conference;
                    break;
                default:
                    $research = false;
                    break;
            }

            if ($research) $this->countReviewsAttitudes($research);
        }
    }

    private function countReviewsAttitudes($research) {
        $reviewAttitudeCounts = $research->countReviewsAttitudes();

        $research->positive_review_count = $reviewAttitudeCounts[\App\Models\Review::ATTITUDE_POSITIVE];
        $research->neutral_review_count = $reviewAttitudeCounts[\App\Models\Review::ATTITUDE_NEUTRAL];
        $research->negative_review_count = $reviewAttitudeCounts[\App\Models\Review::ATTITUDE_NEGATIVE];

        $research->save();
    }

    public function getSubscribedEvents() {
        return [Events::postPersist,Events::postLoad];
    }
}