<?php

namespace App\Models;

/** @MappedSuperclass */
class Review extends \ActiveEntity{
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(type="integer") */
    public $attitude;
    
    /** @Column(type="boolean") */
    public $anonymous;
    
    /** @Column(type="boolean") */
    public $moderated;
    
    /** @Column(length=1500) */
    public $notes;
    
    const ATTITUDE_NEUTRAL = 0;
    const ATTITUDE_NEGATIVE = 1;
    const ATTITUDE_POSITIVE = 2;
    
    public function __construct() {
        $this->created = new \DateTime('now');
        $this->anonymous = false;
        $this->moderated = true;
        $this->attitude = self::ATTITUDE_NEUTRAL;
    }
}

