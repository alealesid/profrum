<?php

namespace App\Models;

/** 
* @Entity
* @Table(name="imhotop_items")
*/
class IMHOTopItem extends \ActiveEntity {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
    */
    public $id;
        
    /** @ManyToOne(targetEntity="Journal", inversedBy="imhotop_items") 
     *  @JoinColumn(name="journal_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $journal;
        
    /** @ManyToOne(targetEntity="IMHOTop", inversedBy="items") 
     *  @JoinColumn(name="imhotop_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $imhotop;

    /**
     * @Column(type="integer")
    */
    public $position;
    
    /** @Column(length=1500, nullable=true) */
    public $notes;

}