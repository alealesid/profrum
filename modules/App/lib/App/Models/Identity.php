<?php

namespace App\Models;

/**
* @Entity
* @Table(name="identities")
*/
class Identity extends \ActiveEntity {
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /** @Column(length=1024) */
    public $identity;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="identities")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;
}