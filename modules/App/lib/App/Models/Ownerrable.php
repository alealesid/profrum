<?php

namespace App\Models;

trait Ownerrable {
    
    /** @Column(type="integer", nullable=false) */
    public $owner_id;
    
    /** @Column(length=256, nullable=false) */
    public $owner_class;
    
    public function getOwner() {
        $owner_class = $this->owner_class;
        return $owner_class::find($this->owner_id);
    }
}

