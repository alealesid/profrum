<?php

namespace App\Models;

/** 
* @Entity
* @HasLifecycleCallbacks
* @Table(name="questions")
*/
class Question extends \ActiveEntity {
    
    /** @ManyToOne(targetEntity="User", inversedBy="questions") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(length=1500) */
    public $text;
    
    /** @Column(type="boolean") */
    public $anonymous;
    
    /** @Column(type="boolean") */
    public $moderated;
    
    public function __construct() {
        $this->created = new \DateTime('now');
        $this->anonymous = false;
        $this->moderated = true;
    }
    
    /** @Column(type="integer", nullable=false) */
    public $owner_id;
    
    /** @Column(length=256, nullable=false) */
    public $owner_class;
    
    use \App\Models\Traits\Commentable;
    
    public function getOwner() {
        $owner_class = $this->owner_class;
        if (!$owner_class) return false;
        return $owner_class::find($this->owner_id);
    }

    public function setOwner($owner) {
        $this->owner_id = $owner->id;
        $this->owner_class = get_class($owner);
    }
    
    public function getUrl() {
        $ownerTypes = array_flip(self::getOwnerTypes());
        return 'question-edit/'.$ownerTypes[$this->owner_class].'/'.$this->owner_id .'/'.$this->id;
    }
    
    public function getOwnerUrl() {
        $type = array_flip(self::getOwnerTypes())[$this->owner_class];
        return "$type/". $this->owner_id;
    }
    
    public static function getOwnerTypes() {
        return [
            'journal' => \App\Models\Journal::class,
            'conference' => \App\Models\Conference::class
        ];
    }
}

