<?php

namespace App\Models\Traits;

trait Commentable {
    
    public function getComments() {
        return \App\Models\Comment::findBy([
            'owner_id' => $this->id,
            'owner_class' => get_called_class()
        ]);
    }
    /** @PreRemove */
    public function PreRemove () {
        $comments = \App\Models\Comment::findBy( [ 'owner_id' => $this->id ] );
        foreach ($comments as $comment) {
            $comment->delete(false);
        }
        \Bingo::$em->flush();
    }
}

