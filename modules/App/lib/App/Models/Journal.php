<?php

namespace App\Models;

/** 
* @Entity
* @Table(name="journals")
*/

class Journal extends Research {

    /** @Column(type="boolean") */
    public $included_to_scopus;

    /** @Column(type="boolean") */
    public $included_to_russian_journals;

    /** 
     * @ManyToMany(targetEntity="Category", inversedBy="journals") 
     * @JoinTable(name="journal_categories") 
     */ 
    public $categories; 

    /**
     * @OneToMany(targetEntity="JournalReview", mappedBy="journal", cascade={"ALL"}, indexBy="id")
     */
    public $reviews;

    /**
     * @OneToMany(targetEntity="IMHOTopItem", mappedBy="journal", cascade={"ALL"}, indexBy="id")
     */
    public $imhotop_items;
    
    public function getReviews($showAll = false) {
        
        $criteria = \Doctrine\Common\Collections\Criteria::create();
        if (!$showAll) {
            $criteria->where(\Doctrine\Common\Collections\Criteria::expr()->eq('moderated', true));
        }
        return $this->reviews->matching($criteria);
    }
    public $top10_rating;
    public $top10_position;
    
    public function getModeratedReviews() {
        return \App\Models\JournalReview::findBy(['journal' => $this, 'moderated' => true]);
    }
    
    public function __construct() {
        parent::__construct();
        $this->included_to_scopus = false;
        $this->included_to_russian_journals = false;
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();

        $this->top10_rating = false;
        $this->top10_position = false;
    }
    
    public function getAverageCriticDurationWeeks() {
        $qb = self::getEntityManager()->createQueryBuilder();
        return $qb->select($qb->expr()->avg('r.critic_duration_weeks'))
            ->from(\App\Models\Journal::class,'j')
            ->innerJoin('j.reviews','r')
            ->where('j.id = :id')
            ->andWhere('r.moderated = :true')
            ->andWhere('r.has_critic = :true')
            ->setParameters([
                'id' => $this->id,
                'true' => true
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function getAveragePublishDurationWeeks() {
        $qb = self::getEntityManager()->createQueryBuilder();

        return $qb->select($qb->expr()->avg('r.publish_duration_weeks'))
            ->from(\App\Models\Journal::class,'j')
            ->innerJoin('j.reviews','r')
            ->where($qb->expr()->eq('j.id', ':id'))
            ->andWhere($qb->expr()->eq('r.moderated', ':true'))
            ->andWhere($qb->expr()->neq('r.article_status', ':refused'))
            ->andWhere($qb->expr()->neq('r.article_status', ':critic_refuse'))
            ->setParameters([
                'id' => $this->id,
                'refused' => \App\Models\JournalReview::ARTICLE_STATUS_REFUSED,
                'critic_refuse' => \App\Models\JournalReview::ARTICLE_STATUS_CRITIC_REFUSE,
                'true' => true
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function getAverageCriticUsefulness() {
        $qb = self::getEntityManager()->createQueryBuilder();

        return $qb->select($qb->expr()->avg('r.critic_usefulness'))
            ->from(\App\Models\Journal::class,'j')
            ->innerJoin('j.reviews','r')
            ->where($qb->expr()->eq('j.id', ':id'))
            ->andWhere($qb->expr()->eq('r.has_critic', ':true'))
            ->andWhere($qb->expr()->eq('r.moderated', ':true'))
            ->setParameters([
                'id' => $this->id,
                'true' => true
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function __toString() {
        return $this->name;
    }
}