<?php

namespace App\Models;

/** 
* @Entity
* @HasLifecycleCallbacks
* @Table(name="imhotop")
*/
class IMHOTop extends \ActiveEntity {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
    */
    public $id;

    /** @ManyToOne(targetEntity="Category", inversedBy="imhotops") 
     *  @JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
    */
    public $category;

    /** @ManyToOne(targetEntity="User", inversedBy="imhotops") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
    */
    public $user;

    /**
     * @OneToMany(targetEntity="IMHOTopItem", mappedBy="imhotop", indexBy="id")
     * @OrderBy({"position" = "ASC"})
     */
    public $items;

    
    /** @Column(type="datetime") */
    public $created;

    /** @Column(type="string", length=100) */
    public $type;
    
    /** @Column(type="string", length=100, nullable=true) */
    public $name;

    /** @Column(type="boolean") */
    public $published;

    use \App\Models\Traits\Commentable;

    const TYPE_STANDART = 'standart';
    const TYPE_NONSTANDART = 'nonstandart';
    const JOURNAL_COUNT_IN_TOP = 10;
    
    public function __construct() {
        $this->type = self::TYPE_STANDART;
        $this->published = false;
        $this->created = new \DateTime('now');  
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }   

    public function getTitle() {
        return $this->type == self::TYPE_STANDART ?
            $this->category->name : $this->name;
    }

    public function getUrl() {
        return url($this->type == self::TYPE_STANDART 
            ? 'standart-imhotop-edit/'.$this->category->id
            : 'nonstandart-imhotop-edit/'.$this->id
        );
    }

    public function hasNotes() {
        foreach ($this->items as $item) {
            if ($item->notes && $item->notes != "") {
                return true;
            }
        }
        return false;
    }

    public static function getStandartTop10Journals($category = false) {
        return self::getTopJournals($category, 10, self::TYPE_STANDART);
    }
    public static function getTop100Journals($category = false) {
        return self::getTopJournals($category, 100);
    }
    private static function getTopJournals($category = false, $limit = 10, $type = false) {
        $qb = self::getEntityManager()->createQueryBuilder()
            ->select('j as journal, sum(11-i.position) AS rating')
            ->from(\App\Models\Journal::class,'j')
            ->leftJoin('j.imhotop_items','i')
            ->innerJoin('i.imhotop','im');

        if ($category) {
            $qb->innerJoin('im.category','c', 'WITH', 'c.id = :cat_id')
                ->setParameter('cat_id', $category->id);
        } else {
            $qb->innerJoin('im.category','c');
        }
        
        if ($type) {
            $qb->where('im.type = :type')
                ->setParameter('type', $type);
        }

        $journalPositions = $qb->groupBy('j.id')
            ->orderBy('rating', 'DESC')
            ->addOrderBy('j.name', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return array_map(function ($journalPosition, $position) {
            $journal = $journalPosition['journal'];
            $journal->top10_rating = $journalPosition['rating'];
            $journal->top10_position = $position + 1;
            return $journal;
        }, $journalPositions, array_keys($journalPositions));
    }

}