<?php

namespace App\Models;

/** 
* @Entity
* @HasLifecycleCallbacks
* @Table(name="journal_reviews")
*/
class JournalReview extends Review {
        
    /** @ManyToOne(targetEntity="Journal", inversedBy="reviews") 
     *  @JoinColumn(name="journal_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $journal;
    
    /** @ManyToOne(targetEntity="User", inversedBy="journal_reviews") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;
    
    /** @Column(type="boolean") */
    public $special;

    /** @Column(type="datetime") */
    public $article_date;
    
    /** @Column(type="boolean") */
    public $has_critic;
    
    /** @Column(type="integer", nullable=true) */
    public $critic_duration_weeks;
    
    /** @Column(type="integer", nullable=true) */
    public $critic_usefulness;
    
    /** @Column(type="integer", nullable=true) */
    public $publish_duration_weeks;
    
    /** @Column(length=256) */
    public $article_status;
    
    /** @Column(length=1500, nullable=true) */
    public $article_withdraw_reasons;
    
    use \App\Models\Traits\Commentable;
    
    const ARTICLE_STATUS_REFUSED = 'article-status-refused';
    const ARTICLE_STATUS_CRITIC_REFUSE = 'article-status-critic-refused';
    const ARTICLE_STATUS_ON_CRITIC = 'article-status-on-critic';
    const ARTICLE_STATUS_REPEAT_CRITIC = 'article-status-repeat-critic';
    const ARTICLE_STATUS_ACCEPTED = 'article-status-accepted';
    const ARTICLE_STATUS_PUBLISHED = 'article-status-published';
    const ARTICLE_STATUS_WITHDRAW = 'article-status-withdraw';
    
    public function __construct() {
        parent::__construct();
        
        $this->special = false;
        $this->has_critic = true;
        $this->article_status = self::ARTICLE_STATUS_PUBLISHED;
    }
    
    public static function getArticleStatusLabels() {
        return [
            self::ARTICLE_STATUS_REFUSED => _t('Отказ без рецензии'),
            self::ARTICLE_STATUS_CRITIC_REFUSE => _t('Отказ после рецензии'),
            self::ARTICLE_STATUS_ON_CRITIC => _t('На рецензировании'),
            self::ARTICLE_STATUS_REPEAT_CRITIC => _t('Повторное рецензирование'),
            self::ARTICLE_STATUS_ACCEPTED => _t('Принята к печати'),
            self::ARTICLE_STATUS_PUBLISHED => _t('Опубликована'),
            self::ARTICLE_STATUS_WITHDRAW => _t('Отозвана автором')
        ];
    }
    
    public function getArticleStatusLabel() {
        $labels = self::getArticleStatusLabels();
        return $labels[$this->article_status];
    }
    
    public function getUrl() {
        return 'journal-review-edit/'.$this->journal->id.'/'.$this->id;
    }
}