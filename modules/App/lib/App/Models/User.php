<?php

namespace App\Models;

/**
* @Entity
* @Table(name="users")
*/
class User extends \Auth\Models\User {
    
    /** @Column(length=255, unique=true) */
    public $email;
    
    /** @Column(length=256) */
    public $name;
    
    /** @Column(length=256) */
    public $work;
    
    /** @Column(length=256) */
    public $position;
    
    /** @Column(type="integer") */ 
    public $degree;
   
    /** @Column(length=256) */
    public $status;
    
    /** @Column(type="boolean") */
    public $competent;
    
    /** @Column(type="datetime", nullable=true) */
    public $last_email_sent;
    
    const DEGREE_NONE = 0;
    const DEGREE_CANDIDATE = 1;
    const DEGREE_DOCTOR = 2; 
    const DEGREE_PHD = 3;

    const MAX_IMHOTOP_COUNT = 5;

    const POINTS_FOR_REVIEW = 10;
    const POINTS_FOR_QUESTION = 5;
    const POINTS_FOR_COMMENT = 1;  
    const POINTS_FOR_CANDIDATE = 50;
    const POINTS_FOR_PHD = 100;
    const POINTS_FOR_DOCTOR = 100;
    const POINTS_FOR_PROFILE = 10;  
    const POINTS_FOR_IMHOTOP = 5;   
    
    /** 
     * @OneToMany(targetEntity="JournalReview", mappedBy="user") 
     */ 
    public $journal_reviews; 

    /**
     * @OneToMany(targetEntity="Identity", mappedBy="user")
     */
    public $identities;

    /**
     * @OneToMany(targetEntity="Feedback", mappedBy="user")
     */
    public $feedbacks;
    
    /**
     * @OneToMany(targetEntity="Like", mappedBy="user")
     */
    public $likes; 
    
    /** 
     * @OneToMany(targetEntity="Question", mappedBy="user") 
     */ 
    public $questions;
    
    /** @Column(length=1024, nullable=true) */
    public $avatar; 
    
    /** @Column(type="boolean") */
    public $blocked;
    
    public function __construct() {
        $this->competent = true;
        $this->blocked = false;
    }

    public function getJournalReviews($showAll = false) {
        
        $criteria = \Doctrine\Common\Collections\Criteria::create();
        if (!$showAll) {
            $criteria->where(\Doctrine\Common\Collections\Criteria::expr()->eq('moderated', true));
        }
        return $this->journal_reviews->matching($criteria);
    }
    
    public function getQuestions() {
        return $this->questions;
    }
    
    public function getUnansweredQuestionsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT q)')
            ->from(\App\Models\Question::class, 'q')
            ->where('q.user = :user')
            ->leftJoin(\App\Models\Comment::class, 'c', 'WITH', 'c.owner_class = :question_class AND c.owner_id = q.id')
            ->andWhere('c.id IS NULL')
            ->andWhere('q.moderated = :moderated')
            ->setParameters([
                'user' => $this,
                'question_class' => \App\Models\Question::class,
                'moderated' => true
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function setPassword($password) {
        $this->password = md5($password);
    }
    
    public static function getDegreeLabels() {
        return [
            self::DEGREE_NONE => _t('нет'),
            self::DEGREE_CANDIDATE => _t('Кандидат наук'),
            self::DEGREE_DOCTOR => _t('Доктор наук'),
            self::DEGREE_PHD => _t('Ph. D')
        ];
    }

    public function canViewOtherTops() { 
        if (!$this->competent) return true;
        if ($this->getTops(\App\Models\IMHOTop::TYPE_STANDART)) return true; 
        return false;
    } 

    public function getTops($type) { 
        return \App\Models\IMHOTop::findBy([ 
            'user' => $this, 
            'type' => $type 
        ]); 
    }

    public static function getPathToAvatars() {
        return 'uploads/user/avatars/';
    } 

    public static function getDefaultAvatarImagePath() {
        return url('assets/img/avatar-guest.png');
    }

    public function getActivityRating() {
        $rating = 0;
                
        $rating += $this->getQuestionsCount() * self::POINTS_FOR_QUESTION;
        
        $rating += $this->getCommentsCount() * self::POINTS_FOR_COMMENT;

        $rating += $this->getJournalReviewsCount() * self::POINTS_FOR_REVIEW;

        $rating += $this->getConferenceReviewsCount() * self::POINTS_FOR_REVIEW;

        $rating += $this->getImhotopsCount() * self::POINTS_FOR_IMHOTOP;

        if ($this->degree == self::DEGREE_CANDIDATE) {
            $rating += self::POINTS_FOR_CANDIDATE;
        } else if ($this->degree == self::DEGREE_DOCTOR) {
            $rating += self::POINTS_FOR_DOCTOR;
        } else if ($this->degree == self::DEGREE_PHD) {
            $rating += self::POINTS_FOR_PHD;
        }

        if ($this->name != '' && $this->position != '' && $this->status != '' && $this->work != '') {
            $rating += self::POINTS_FOR_PROFILE;
        }

        return $rating;
    }

    public function getQuestionsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT q)')
            ->from(\App\Models\Question::class, 'q')
            ->where('q.user = :user')
            ->setParameters([
                'user' => $this
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getCommentsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT c)')
            ->from(\App\Models\Comment::class, 'c')
            ->where('c.user = :user')
            ->setParameters([
                'user' => $this
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getJournalReviewsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT jr)')
            ->from(\App\Models\JournalReview::class, 'jr')
            ->where('jr.user = :user')
            ->setParameters([
                'user' => $this
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getConferenceReviewsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT cr)')
            ->from(\App\Models\ConferenceReview::class, 'cr')
            ->where('cr.user = :user')
            ->setParameters([
                'user' => $this
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getImhotopsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT i)')
            ->from(\App\Models\IMHOTop::class, 'i')
            ->where('i.user = :user')
            ->setParameters([
                'user' => $this
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
}