<?php

namespace App\Models;


/** 
* @Entity
* @Table(name="likes", uniqueConstraints={
    @UniqueConstraint(name="single_like", columns={"user_id", "owner_id", "owner_class"})
  })
*/
class Like extends \ActiveEntity {
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @ManyToOne(targetEntity="User") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(type="integer", nullable=false) */
    public $owner_id;
    
    /** @Column(length=255, nullable=false) */
    public $owner_class;
    
    public function __construct() {
        $this->created = new \DateTime('now');  
    }
    
    public function getOwner() {
        $owner_class = $this->owner_class;
        return $owner_class::find($this->owner_id);
    }
    
    public function setOwner($owner) {
        $this->owner_class = get_class($owner);
        $this->owner_id = $owner->id;
    }
    
    public static function getOwnerTypes() {
        return [
            'journal' => \App\Models\Journal::class,
            'conference' => \App\Models\Conference::class
        ];
    }

}