<?php

namespace App\Models;

/** 
* @Entity
* @HasLifecycleCallbacks
* @Table(name="conference_reviews")
*/
class ConferenceReview extends Review {
        
    /** @ManyToOne(targetEntity="Conference", inversedBy="reviews") 
     *  @JoinColumn(name="conference_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    public $conference;
    
    /** @ManyToOne(targetEntity="User", inversedBy="conference_reviews") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;
    
    /** @Column(type="integer", nullable=true) */
    public $difficult;
    
    /** @Column(type="integer", nullable=true) */
    public $level;
    
    /** @Column(type="integer", nullable=true) */
    public $usefulness;
    
    /** @Column(type="integer", nullable=true) */
    public $coffeebreaks;
    
    /** @Column(type="integer", nullable=true) */
    public $culture;
    
    /** @Column(type="boolean") */
    public $has_culture;
    
    use \App\Models\Traits\Commentable;
    
    public function getUrl() {
        return 'conference-review-edit/'.$this->conference->id.'/'.$this->id;
    }
}