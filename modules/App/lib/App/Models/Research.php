<?php

namespace App\Models;


/** 
* @MappedSuperclass
* @HasLifecycleCallbacks
 */
class Research extends \ActiveEntity {
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(length=256) */
    public $name;
    
    /** @Column(type="text") */ 
    public $description;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(length=256) */
    public $site;
    
    /** @Column(type="integer", nullable=true) */
    public $positive_review_count;
    
    /** @Column(type="integer", nullable=true) */
    public $neutral_review_count;
    
    /** @Column(type="integer", nullable=true) */
    public $negative_review_count;

    /** @Column(type="boolean") */
    public $hidden;
    
    public function __construct() {
        $this->created = new \DateTime('now');
        $this->hidden = false;
    }
    
    public function getQuestions() {
        return new \Doctrine\Common\Collections\ArrayCollection(Question::findBy([
            'owner_id' => $this->id,
            'owner_class' => get_called_class(),
            'moderated' => true
        ]));
    }
    
    public function getUnansweredQuestionsCount() {
        return self::getEntityManager()->createQueryBuilder()
            ->select('count(DISTINCT q)')
            ->from(\App\Models\Question::class, 'q')
            ->where('q.owner_id = :research_id')
            ->andWhere('q.owner_class = :research_class')
            ->leftJoin(\App\Models\Comment::class, 'c', 'WITH', 'c.owner_class = :question_class AND c.owner_id = q.id')
            ->andWhere('c.id IS NULL')
            ->andWhere('q.moderated = :moderated')
            ->setParameters([
                'research_id' => $this->id,
                'research_class' => get_called_class(),
                'question_class' => \App\Models\Question::class,
                'moderated' => true
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    public function getRating() {
        $config = \Bingo\Config::get('config', 'research'); 
        if (count($this->getModeratedReviews()) > $config['review_threshold']) {
            $rating = 2*$this->positive_review_count
                + $this->neutral_review_count
                - 2*$this->negative_review_count;
                            
            return $rating;
        }
        return false;
    }
    
    public function countReviewsAttitudes() {
        $qb = self::getEntityManager()->createQueryBuilder();
        $responceAttitudeAmounts = $qb->select(['r.attitude, count(r) as review_count'])
            ->from(get_called_class(),'j')
            ->innerJoin('j.reviews','r')
            ->where($qb->expr()->eq('j.id', ':id'))
            ->groupBy('r.attitude')
            ->andWhere($qb->expr()->eq('r.moderated', ':true'))
            ->setParameters([
                'id' => $this->id,
                'true' => true
            ])
            ->getQuery()
            ->getResult();
           
        $reviewAttitudeAmounts = [
            Review::ATTITUDE_POSITIVE => 0,
            Review::ATTITUDE_NEUTRAL => 0,
            Review::ATTITUDE_NEGATIVE => 0
        ];

        foreach($responceAttitudeAmounts as $attitude) {
            $reviewAttitudeAmounts[$attitude['attitude']] = $attitude['review_count'];
        }

        return $reviewAttitudeAmounts;
    }
    
    public function getPositiveReviewsCount() {
        return $this->positive_review_count;
    }
    
    public function getNegativeReviewsCount() {
        return $this->negative_review_count;
    }
    
    public function getNeutralReviewsCount() {
        return $this->neutral_review_count;
    }
    
    public function getLikesCount() {
        $qb = self::getEntityManager()->createQueryBuilder();

        return $qb
            ->select($qb->expr()->count('l'))
            ->from(\App\Models\Like::class,'l')
            ->where($qb->expr()->eq('l.owner_id',':research_id'))
            ->andWhere($qb->expr()->eq('l.owner_class',':research_class'))
            ->setParameters([
                'research_id' => $this->id,
                'research_class' => get_called_class()
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function isLikedBy($user, $session = null) {
        if ($user) {
            return \App\Models\Like::findOneBy([
                'owner_class' => get_called_class(),
                'owner_id' => $this->id, 
                'user' => $user
            ]) != NULL;
        }

        if ($session && !empty($session->userLikes)) {
            $that = $this;
            $entityLikesByUser = array_filter( $session->userLikes , function($like) use (&$that) {
                if ($like['owner_class'] == get_class($this) && $like['owner_id'] == $this->id) {
                    return $like;
                }
            });
            return !empty($entityLikesByUser);
        }

        return false;
    }
    
    public function joinResearch($research) {
        $researchNameForReview = self::getEntityManager()
            ->getClassMetadata(get_called_class())->associationMappings['reviews']['mappedBy'];
        
        foreach($research->reviews as $review) {
            $review->$researchNameForReview = $this;
            $review->save(false);
        }
        $research->reviews = [];
        self::getEntityManager()->flush();
        
        $qb = self::getEntityManager()->createQueryBuilder();

        $existingLikes = \App\Models\Like::findBy( [ 'owner_class' => get_called_class(), 'owner_id' => $this->id ] );
        $existingLikesIDS = [];
        foreach ($existingLikes as $existingLike) {
            $existingLikesIDS[] = $existingLike->user->id;
        }

        $qb->update(\App\Models\Like::class, 'l')
            ->where('l.owner_class = :owner_class')
            ->andWhere('l.owner_id = :old_id');

        if ($existingLikesIDS) {
            $qb->andWhere( $qb->expr()->notIn(
                'IDENTITY(l.user)',
                implode(",", $existingLikesIDS)
            ));
        }
        
        $qb->set('l.owner_id', ':new_id')
            ->setParameters([
                'owner_class' => get_called_class(),
                'old_id' => $research->id,
                'new_id' => $this->id
            ])
            ->getQuery()
            ->execute();


        self::getEntityManager()->createQueryBuilder()
            ->delete(\App\Models\Like::class, 'l')
            ->where('l.owner_class = :owner_class')
            ->andWhere('l.owner_id = :new_id')
            ->setParameters([
                'owner_class' => get_called_class(),
                'new_id' => $research->id
            ])
            ->getQuery()
            ->execute();
    
        self::getEntityManager()->createQueryBuilder()
            ->update(\App\Models\Question::class, 'l')
            ->where('l.owner_class = :owner_class')
            ->andWhere('l.owner_id = :old_id')
            ->set('l.owner_id', ':new_id')
            ->setParameters([
                'owner_class' => get_called_class(),
                'old_id' => $research->id,
                'new_id' => $this->id
            ])
            ->getQuery()
            ->execute();
    }
    /** @PreRemove */
    public function PreRemove () {
        $questions = \App\Models\Question::findBy( [ 'owner_id' => $this->id ] );
        foreach ($questions as $question) {
            $question->delete(false);
        }
        if (get_class($this) == \App\Models\Conference::class) {
            $this->prev->next = NULL;
            $this->prev = NULL;
            $this->next->prev = NULL;
            $this->next = NULL;
            $this->save(false);
        }
        \Bingo::$em->flush();
    }
}