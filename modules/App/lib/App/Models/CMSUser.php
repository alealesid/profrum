<?php

namespace App\Models;

/**
* @Entity
* @Table(name="cms_users_extend")
*/
class CMSUser extends \Auth\Models\User {
    
    /** @Column(type="json_array") */
    public $roles;
    
    const ROLE_ADMIN = 'admin';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_SUPER_MODERATOR = 'super-moderator';

    public function __construct() {
        $this->roles = [];
    }
    
    public function hasOneOfRoles($roles) {
        foreach($roles as $role) {
            if ($this->hasRole($role)) return true;
        }
        
        return false;
    }
    
    public function hasRole($role) {
        return in_array($role, $this->roles);
    }
    
    public function addRole($role) {
        if (!$this->hasRole($role)) $this->roles[] = $role;
    }
    
    public function removeRole($role) {
        if ($this->hasRole($role)) unset($this->roles[$user]);
    }
}