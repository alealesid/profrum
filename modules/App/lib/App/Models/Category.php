<?php

namespace App\Models;

/** 
* @Entity
* @Table(name="category")
*/
class Category extends \ActiveEntity {
        
    /**
     * @ManyToMany(targetEntity="Journal", mappedBy="categories")
     */
    public $journals;
        
    /**
     * @ManyToMany(targetEntity="Conference", mappedBy="categories")
     */
    public $conferences;
    
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(length=256) */
    public $name;

    public static function selectOptions($criteria,$title_field,$init=false) {
        if (@$criteria[0] instanceof \ActiveEntity)
            $list = $criteria;
        else
            $list = \App\Models\Category::findBy($criteria, 'name ASC');

        $ret = $init;
        foreach ($list as $obj) $ret[$obj->$title_field] = $obj;
        return $ret;
    }

}