<?php

namespace App\Models;

/** 
* @Entity
* @Table(name="conferences")
*/

class Conference extends Research {

    /**
     * @OneToOne(targetEntity="Conference")
     * @JoinColumn(name="prev_id", referencedColumnName="id", nullable=true)
     */
    public $prev;

    /**
     * @OneToOne(targetEntity="Conference")
     * @JoinColumn(name="next_id", referencedColumnName="id", nullable=true)
     */
    public $next;

    /** 
     * @ManyToMany(targetEntity="Category", inversedBy="conferences") 
     * @JoinTable(name="conference_categories") 
     */ 
    public $categories; 

    /**
     * @OneToMany(targetEntity="ConferenceReview", mappedBy="conference", cascade={"ALL"}, indexBy="id")
     */
    public $reviews;
    
    /** @Column(length=256) */
    public $location;
    
    /** @Column(type="datetime") */
    public $start;
    
    /** @Column(type="datetime") */
    public $finish;
    
    public function __construct() {
        parent::__construct();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function getModeratedReviews() {
        return \App\Models\ConferenceReview::findBy(['conference' => $this, 'moderated' => true]);
    }

    public function getAverageDifficult() {
        return $this->getAverageReviewFieldQueryBuilder('difficult')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAverageLevel() {
        return $this->getAverageReviewFieldQueryBuilder('level')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAverageUsefulness() {
        return $this->getAverageReviewFieldQueryBuilder('usefulness')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAverageCoffeebreaks() {
        return $this->getAverageReviewFieldQueryBuilder('coffeebreaks')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getAverageCulture() {
        return $this->getAverageReviewFieldQueryBuilder('culture')
            ->andWhere('r.has_culture = :true')
            ->getQuery()
            ->getSingleScalarResult();
    }
    
    private function getAverageReviewFieldQueryBuilder($field) {
        $qb = self::getEntityManager()->createQueryBuilder();
        return $qb->select($qb->expr()->avg('r.'.$field))
            ->from(\App\Models\Conference::class,'c')
            ->innerJoin('c.reviews','r')
            ->where($qb->expr()->eq('c.id', ':id'))
            ->andWhere($qb->expr()->eq('r.moderated', ':true'))
            ->setParameters([
                'id' => $this->id,
                'true' => true
            ]);
    }
    
}