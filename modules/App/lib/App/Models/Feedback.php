<?php

namespace App\Models;

/** 
* @Entity
* @Table(name="feedbacks")
*/

class Feedback extends \ActiveEntity {
    
    /** @ManyToOne(targetEntity="User", inversedBy="feedbacks") 
     *  @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $user;

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    public $id;
    
    /** @Column(type="datetime") */
    public $created;
    
    /** @Column(type="integer") */
    public $type;
    
    /** @Column(type="json_array") */
    public $contents;
    
    const TYPE_JOURNAL_NOT_FOUND = 0;
    const TYPE_CONFERENCE_NOT_FOUND = 1;
    
    public function __construct() {
        $this->created = new \DateTime('now');  
        $this->contents = [];
    }

    function &__get($name) {
        if (array_key_exists($name, $this->contents)) { 
            $value = _h($this->contents[$name]); 
            return $value; 
        }
        return parent::__get($name);
    }
    

}