<form id="review_form" class="form-horizontal review-form <?= @$article_validation_status ?: $review->article_status ?> <?= $review_form->findElement('has_critic')->value ? 'review-has-critic' : '' ?>" method="POST">
    <p class="text-center">
        <?php if ($user) : ?>
            <?php $review_form->findElement('anonymous')->render(); ?>
        <?php endif; ?>
    </p>
    <div class="ui-widget">
        <?php $review_form->findElement('journal')->render(); ?>
    </div>
    <p class="text-center">
        <a href="<?= url('feedback/journal') ?>" class="btn btn-default"><?=_t('Не нашёл журнал')?></a>
    </p>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10 col-xs-12 form-inline">
                <div class="pull-left pull-none-xs">
                    <?php $review_form->findElement('attitude')->render_label(); ?>
                </div>

                <div class="pull-right pull-none-xs">
                    <?php $review_form->findElement('attitude')->render_element(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <?php $review_form->findElement('attitude')->render_error(); ?>
            </div>
            <div class="col-sm-3">
                <?php $review_form->findElement('anonymous')->render_error(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 form-inline picker">
                <?php $review_form->findElement('article_date')->render_label(); ?>
                <div class="input-group">
                <?php $review_form->findElement('article_date')->render_element(); ?>
                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                </div>
            </div>
            <div class="col-md-5 form-inline picker">
                <?php $review_form->findElement('article_status')->render_label(); ?>
                <div class="input-group">
                    <?php $review_form->findElement('article_status')->render_element(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <?php $review_form->findElement('article_date')->render_error(); ?>
            </div>
            <div class="col-sm-3">
                <?php $review_form->findElement('article_status')->render_error(); ?>
            </div>
        </div>
        <div class="row critic-group">
            <div class="col-sm-9 form-inline">
                <?php $review_form->findElement('critic_duration_weeks')->render_label(); ?>
                <?php $review_form->findElement('critic_duration_weeks')->render_element(); ?>
            </div>
            <div class="col-sm-3">
                <strong><?php $review_form->findElement('critic_duration_weeks')->render_value(); ?></strong> <?= _t('(недель)') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <?php $review_form->findElement('critic_duration_weeks')->render_error(); ?>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <div class="row published_group">
            <div class="col-sm-9">
                <?php $review_form->findElement('publish_duration_weeks')->render_label(); ?>
                <?php $review_form->findElement('publish_duration_weeks')->render_element(); ?>
            </div>
            <div class="col-sm-3">
                <strong><?php $review_form->findElement('publish_duration_weeks')->render_value(); ?></strong> <?= _t('(недель)') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <?php $review_form->findElement('publish_duration_weeks')->render_error(); ?>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9 form-inline">
                <?php $review_form->findElement('special')->render_label(); ?>
            </div>
            <div class="col-sm-3">
                <?php $review_form->findElement('special')->render_element(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php $review_form->findElement('special')->render_error(); ?>
            </div>
        </div>
        <div class="row critic-group">
            <div class="col-sm-6 form-inline">
                <?php $review_form->findElement('critic_usefulness')->render_label(); ?>
            </div>
            <div class="col-sm-6 text-right">
                <strong><?php $review_form->findElement('critic_usefulness')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <?php $review_form->findElement('critic_usefulness')->render_error(); ?>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <div class="row has-critic">
            <div class="col-sm-9 form-inline">
                <?php $review_form->findElement('has_critic')->render_label(); ?>
            </div>
            <div class="col-sm-3">
                <?php $review_form->findElement('has_critic')->render_element(); ?>
            </div>
        </div>
        <div class="row has-critic">
            <div class="col-sm-9">
                <?php $review_form->findElement('has_critic')->render_error(); ?>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="review-form-input">
                    <?php $review_form->findElement('notes')->render(); ?>
                </div>
            </div>
            <div class="col-xs-12" id = "withdraw_group">
                <div class="review-form-input">
                    <?php $review_form->findElement('article_withdraw_reasons')->render(); ?>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="form-group text-center">
        <button class="btn btn-success btn-login"><?= _t('Отправить') ?></button>
    </div>
</form>

<?php if (!$user) : ?>
    <div id="review_form_confirm" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?=_t('Анонимный отзыв')?></h4>
                </div>
                <div class="modal-body">
                    <p><?=_t('Вы не зарегистрировались или не вошли в систему. Ваш отзыв будет опубликован анонимно после проверки модератором. Вы не сможете его редактировать. Хотите продолжить?')?></p>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-default"><?=_t('Отправить отзыв')?></a>
                    <a href="<?=url(\Bingo\Routing::$uri) . '?' . http_build_query(['redirect' => urlencode('login')])?>" type="button" class="btn btn-primary"><?=_t('Автозироваться')?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>