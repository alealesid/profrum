<table class="table table-bordered">
    <tr class="imhotop-header">
        <?php if ($imhotop->hasNotes()) : ?>
            <th class="col-md-5">
                <?php if ($imhotop->user->avatar) : ?> 
                    <img class="img-rounded imhotop-avatar" height="40" width="40" src="<?= (strpos($imhotop->user->avatar, 'http') === false) ? url($imhotop->user->avatar) : $imhotop->user->avatar ?>"> 
                <?php endif; ?>
                <a href="<?=url('profile/'.$imhotop->user->id)?>"><strong><?=_h($imhotop->user->name)?> - <?= $imhotop->user->getActivityRating() ?></strong></a>
                <?=_h($imhotop->user->status)?>
            </th>
            <th class="col-md-7 imhotop-name-holder">
                <span class="label label-primary imhotop-identity">
                    <?php if ($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART) : ?>
                        <?=_h($imhotop->category->name)?>
                    <?php else : ?>
                        <?=_h($imhotop->name)?>
                    <?php endif; ?>
                </span>
                <a class="btn btn-default btn-sm pull-right imhotop-link" 
                    href="<?=(($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART) ? url('standart-imhotop/'.$imhotop->id) : url('nonstandart-imhotop/'.$imhotop->id) )?>">
                    <?=_t('Перейти к топу')?>
                </a>
            </th>
        <?php else : ?>
            <th class="col-xs-12 full-width">
                <div class="row centered">
                    <div class="col-sm-9 col-xs-12">
                        <?php if ($imhotop->user->avatar) : ?> 
                            <img class="img-rounded imhotop-avatar" height="40" width="40" src="<?= (strpos($imhotop->user->avatar, 'http') === false) ? url($imhotop->user->avatar) : $imhotop->user->avatar ?>"> 
                        <?php endif; ?>
                        <span class="imhotop-with-notes-user-info">
                            <a href="<?=url('profile/'.$imhotop->user->id)?>" class="user-name"><strong><?=$imhotop->user->name?> - <?= $imhotop->user->getActivityRating() ?></strong></a>
                            <span class="user-status"><?=_h($imhotop->user->status)?></span>
                        </span>
                            
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="link-and-identity-holder">
                            <span class="label label-primary imhotop-identity">
                                <?php if ($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART) : ?>
                                    <?=_h($imhotop->category->name)?>
                                <?php else : ?>
                                    <?=_h($imhotop->name)?>
                                <?php endif; ?>
                            </span>
                            <a class="btn btn-default btn-sm imhotop-link" 
                                href="<?=(($imhotop->type == \App\Models\IMHOTop::TYPE_STANDART) ? url('standart-imhotop/'.$imhotop->id) : url('nonstandart-imhotop/'.$imhotop->id) )?>">
                                <?=_t('Перейти к топу')?>
                            </a>
                        </div>
                    </div>
                </div>
            </th>
        <?php endif; ?>
    </tr>
    <?php foreach($imhotop->items as $imhotopItem) : ?>
        <tr>
            <?php if ( $imhotop->hasNotes() ) : ?>
                <td class="col-md-5">
                    <strong><?=($imhotopItem->position)?></strong>. 
                    <a href="<?=url('journal/'.$imhotopItem->journal->id)?>"><?=_h($imhotopItem->journal->name)?></a>
                </td>
                <td class="col-md-7"><?=_h($imhotopItem->notes)?></td>
            <?php else : ?>
                <td class="col-xs-12">
                    <strong><?=($imhotopItem->position)?></strong>. 
                    <a href="<?=url('journal/'.$imhotopItem->journal->id)?>"><?=_h($imhotopItem->journal->name)?></a>
                </td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
</table>
<div class = "row commentable"> 
    <div class  ="col-md-10 col-md-offset-2"> 
        <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/imhotop/' . $imhotop->id) ?>"></div> 
        <?php $comment_form = new \App\Forms\Comment('post', [], $imhotop); ?> 
        <?php include partial('partial/comment_form') ?> 
    </div> 
</div> 