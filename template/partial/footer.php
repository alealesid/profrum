<footer>
    <div class="container">
        <div class="row">
            <div class="navigation col-md-6 col-md-offset-3 col-sm-12 text-center">
                <a href="<?= url('faq') ?>"><?= _t('FAQ') ?></a>
                <a href="<?= url('rules') ?>"><?= _t('Правила сообщества') ?></a>
            </div>
            <div class="col-xs-12">
                <p class="text-center mail"><strong><?= _t('Контакты:') ?> </strong>info@profrum.com</p>
                <p class="text-center copyright"><?= _t('Все права защищены') ?> <i class="fa fa-copyright" aria-hidden="true"></i> <?= date('Y') ?></p>
            </div>
        </div>
    </div>
</footer>