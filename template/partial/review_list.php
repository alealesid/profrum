<?php
$reviewPartials = [
    App\Models\JournalReview::class => 'partial/journal_review',
    App\Models\ConferenceReview::class => 'partial/conference_review'
];
$partialView = $reviewPartials[$review_class];
?>
<form class="form-inline filter-form" method='get'>
    <label><?= _t('Фильтры:') ?></label>
    <?php if ( $anonymousChecbox = $filter_form->findElement('anonymous') ) : ?>
        <div class="checkbox">
            <?php $anonymousChecbox->render(); ?>
        </div>
    <?php endif; ?>
    <div class="checkbox">
        <?php $filter_form->findElement('positive')->render(); ?>
    </div>
    <div class="checkbox">
        <?php $filter_form->findElement('negative')->render(); ?>
    </div>
    <div class="checkbox">
        <?php $filter_form->findElement('neutral')->render(); ?>
    </div>
    <div class="form-group pull-right filter-action">
        <input type="submit" class="btn btn-primary btn-sm" value="<?=_t('применить')?>">
    </div>
    <div class="clearfix"></div>
</form>
<?php foreach($reviews as $review) : ?>
   <?php include partial($partialView) ?>
<?php endforeach; ?>
<div class="pagination center-block text-center">
    <?= $pagination ?>
</div>