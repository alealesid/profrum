<form class="form-inline filter-form" method='get'>
    <label><?= _t('Фильтры:') ?></label>
    <div class="checkbox">
        <?php $filter_form->findElement('unanswered')->render(); ?>
    </div>
    <div class="checkbox">
        <?php $filter_form->findElement('answered')->render(); ?>
    </div>
    <div class="form-group pull-right filter-action">
        <input type="submit" class="btn btn-primary btn-sm" value="<?=_t('применить')?>">
    </div>
    <div class="clearfix"></div>
</form>
<?php foreach($questions as $question) : ?>
   <?php include partial('partial/question') ?>
<?php endforeach; ?>
<div class="pagination center-block text-center">
    <?= $pagination ?>
</div>