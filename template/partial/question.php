<!-- Question --> 

<?php 
    $commentsBlockId = 'comments_q_'.$question->id;
?>

<div class="panel panel-question" >
    <div class="question-journal">
        <?= _t('Вопрос к') ?> <a href="<?= url($question->getOwnerUrl()) ?>"><?= _t($question->getOwner()->name) ?></a>
    </div>
    <div class="question-heading">
        <div class="pull-left user">
            <? if ($question->user) : ?>
                <?php if ($question->user->avatar) : ?> 
                    <img class="img-rounded question-avatar" height="50" width="50" src="<?= (strpos($question->user->avatar, 'http') === false) ? url($question->user->avatar) : $question->user->avatar ?>"> 
                <?php endif; ?> 
                <span class="question-name-status-holder">
                    <p>
                        <a href="<?=url('profile/'.$question->user->id)?>"><strong><?= _t($question->user->name) ?> - <?= $question->user->getActivityRating() ?></strong></a>
                    </p>
                    <p>
                        <?= _h($question->user->status) ?>
                    </p>
                </span>
            <? else : ?>
                <p><strong><?= _t("Гость") ?></strong></p>
            <? endif; ?>
        </div>
        <div class="pull-right text-right">
            <?php if (isset($question->user) && ($user) && $question->user->login == $user->login) : ?>
                <a href="<?= url($question->getUrl()) ?>">
                    <strong><?= _t('изменить') ?></strong>
                </a>
            <br>
            <?php endif; ?>
            <a class="question-date" href = "<?= url('question/'.$question->id) ?>">
                <?= $question->created->format('j F') ?><br><?= $question->created->format('G:i') ?>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="question-text">
        <div class="review-notes">
            <p><?= _h($question->text) ?></p>
        </div>
    </div>    
    <p class="text-right">
        <a class="show-comments-button" role="button" data-toggle="collapse" href="#<?=$commentsBlockId?>" aria-expanded="false" aria-controls="<?=$commentsBlockId?>">
            <span class="comments-count"><?= count($question->getComments()) ?></span>
            <i class="fa fa-comments" aria-hidden="true"></i>
        </a>
    </p>
</div>

<div id = "<?=$commentsBlockId?>" class = "row commentable collapse">
    <div class="col-md-10 col-md-offset-2">
        <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/question/' . $question->id) ?>"></div>
        <?php $comment_form = new \App\Forms\Comment('post', [], $question); ?>
        <?php include partial('partial/comment_form') ?>
    </div>
</div>
<!-- END Question -->