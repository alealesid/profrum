<div id = "commentFormModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= _t('Новый комментарий') ?></h4>
            </div>
            <div class="modal-body">
                <?php include partial('partial/comment_form') ?>
            </div>
        </div>
    </div>
</div>