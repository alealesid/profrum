<div id = "comment<?=$comment->id?>"class="panel panel-default">
    <div class="panel-body">
        <div class="col-sm-6">
            <? if ($comment->user) : ?>
                <?php if ($comment->user->avatar) : ?> 
                    <img class="img-rounded comment-avatar" height="50" width="50" src="<?= (strpos($comment->user->avatar, 'http') === false) ? url($comment->user->avatar) : $comment->user->avatar ?>"> 
                <?php endif; ?> 
                <span class="comment-name-status-holder">
                    <p>
                        <strong><a href="<?=url('profile/'.$comment->user->id)?>"><?=_h($comment->user->name)?> - <?= $comment->user->getActivityRating() ?></a></strong> 
                        <?= _h($comment->user->status) ?>
                    </p> 
            <? else : ?>
                <p><strong><?= _t("Гость") ?></strong></p>
            <? endif; ?>
            <p><a href="<?= url($comment->getOwnerUrl().'#comment'.$comment->id) ?>"><?= $comment->created->format('j F G:i') ?></a></p>
            <?php if ($comment->user) :?>
                </span>
            <?php endif; ?>    
        </div>
        <div class="col-sm-6"><?= _h($comment->text) ?></div>
    </div>
</div>