<form action="<?=url($comment_form->atts['action'])?>" method="<?=$comment_form->atts['method']?>" class="comment-form">
    <div class="row">
        <div class="col-xs-12">
            <div class="input-group">
                <?php $comment_form->findElement('text')->render_element(); ?>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><?=_t('Добавить')?></button>
                </div>
            </div>
        </div>
    </div>
</form>