<!-- REVIEW -->
<?php 
    $attitudeClasses = [
        App\Models\Review::ATTITUDE_NEUTRAL => 'panel-neutral',
        App\Models\Review::ATTITUDE_NEGATIVE  => 'panel-danger',
        App\Models\Review::ATTITUDE_POSITIVE  => 'panel-success',
    ];
    $commentsBlockId = 'comments_jr_'.$review->id;
?>
<div class="panel <?= $attitudeClasses[$review->attitude] ?>" >
    <div class="panel-heading">
        <?php if (isset($review->user) && !$review->anonymous) : ?>
            <a href="<?=url('profile/'.$review->user->id)?>">
                <?php if ($review->user->avatar) : ?>
                    <img class="img-rounded" height="40" width="40" src="<?= (strpos($review->user->avatar, 'http') === false) ? url($review->user->avatar) : $review->user->avatar ?>">
                <?php endif; ?>
                <strong><?= _h($review->user->name) ?> - <?= $review->user->getActivityRating() ?></strong>
            </a> <?= _h($review->user->status) ?>
        <?php else: ?>
            <?= _t('Анонимный отзыв') ?>
        <?php endif; ?>
        | <a href="<?= url('journal/'.$review->journal->id) ?>"><?= _h($review->journal->name) ?></a>
        <span class="pull-right panel-date">
            <?php if (isset($review->user) && ($user) && $review->user->login == $user->login) : ?>
                <a href="<?= url($review->getUrl()) ?>">
                    <strong><?= _t('изменить') ?></strong>
                </a>
            <?php endif; ?>
            <a href = "<?= url('journal-review/'.$review->id) ?>">
                <?= $review->created->format('Y-m-d H:i') ?>
            </a>
        </span>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <div class="col-xs-12">
            <p> 
                <span class="label label-primary"><?= $review->getArticleStatusLabel() ?></span>
                <?php if ($review->special) : ?>
                    <span class="label label-primary"><?=_t('Специальный выпуск')?></span>
                <?php endif; ?>
                <?php if (!$review->has_critic) : ?>
                    <span class="label label-primary"><?=_t('Без рецензирования')?></span>
                <?php endif; ?>
            </p>
        </div>
        
        <div class="col-xs-12">
            <table class="table">
                <tr>
                    <th class="col-sm-6"><?=_t('Дата подачи статьи')?></th>
                    <td class="col-sm-6">
                        <?= $review->article_date->format('m.Y') ?>
                    </td>
                </tr>
                <?php if ($review->has_critic) : ?>
                    <tr>
                        <th class="col-sm-6"><?=_t('Время до рецензии')?></th>
                        <td class="col-sm-6"><?= $review->critic_duration_weeks ?> <?=_t('недель')?></td>
                    </tr>
                    <tr>
                        <th class="col-sm-6"><?=_t('Полезность рецензий')?></th>
                        <td class="col-sm-6">
                            <?php for ($i=1; $i <=10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $review->critic_usefulness) ? 'on' : '') ?>"></span>
                            <?php endfor; ?>
                            <span class="visible-xs"><span class="badge"><?= $review->critic_usefulness ?></span></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($review->article_status == \App\Models\JournalReview::ARTICLE_STATUS_PUBLISHED) : ?>
                    <tr>
                        <th class="col-sm-6"><?=_t('Время до публикации')?></th>
                        <td class="col-sm-6"><?= $review->publish_duration_weeks ?> <?=_t('недель')?></td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    
        <?php if (!empty($review->article_withdraw_reasons) && $review->article_status == App\Models\JournalReview::ARTICLE_STATUS_WITHDRAW) : ?>
            <div class="col-xs-12 review-withdraw-reasons">
                <strong><?=_t('Причины отзыва статьи')?></strong>
                <p><?= _h($review->article_withdraw_reasons) ?></p>
            </div>
        <?php endif; ?>

        <?php if (!empty($review->notes)) : ?>
            <div class="col-xs-12 review-notes">
                <strong><?=_t('Комментарии')?></strong>
                <p><?= _h($review->notes) ?></p>
            </div>
        <?php endif; ?>
         
        <p class="text-right">
            <a class="show-comments-button" role="button" data-toggle="collapse" href="#<?=$commentsBlockId?>" aria-expanded="false" aria-controls="<?=$commentsBlockId?>">
                <span class="comments-count"><?= count($review->getComments()) ?></span>
                <i class="fa fa-comments" aria-hidden="true"></i>
            </a>
        </p>
    </div>
</div>

<div id = "<?=$commentsBlockId?>" class = "row commentable collapse">
    <div class="col-md-10 col-md-offset-2">
        <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/journal-review/' . $review->id) ?>"></div>
        <?php $comment_form = new \App\Forms\Comment('post', [], $review); ?>
        <?php include partial('partial/comment_form') ?>
    </div>
</div>
<!-- END REVIEW -->