<form id="conference_review_form" class="form-horizontal review-form" method="POST">
    <p class="text-center">
        <?php if ($user) : ?>
            <?php $review_form->findElement('anonymous')->render(); ?>
        <?php endif; ?>
    </p>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="ui-widget">
                    <?php $review_form->findElement('conference')->render(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-xs-12 form-inline">
                <div class="pull-left pull-none-xs">
                    <?php $review_form->findElement('attitude')->render_label(); ?>
                </div>

                <div class="pull-right pull-none-xs">
                    <?php $review_form->findElement('attitude')->render_element(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <?php $review_form->findElement('attitude')->render_error(); ?>
            </div>
            <div class="col-md-2">
                <?php $review_form->findElement('anonymous')->render_error(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('difficult')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <strong><?php $review_form->findElement('difficult')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('difficult')->render_error(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('level')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <strong><?php $review_form->findElement('level')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('level')->render_error(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('usefulness')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <strong><?php $review_form->findElement('usefulness')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('usefulness')->render_error(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('coffeebreaks')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <strong><?php $review_form->findElement('coffeebreaks')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('coffeebreaks')->render_error(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
            </div>
        </div>        
        <div class="row">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('has_culture')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('has_culture')->render_element(); ?>
            </div>
        </div>
        <div class="row culture">
            <div class="col-sm-6 col-xs-12  form-inline text-right">
                <?php $review_form->findElement('culture')->render_label(); ?>
            </div>
            <div class="col-sm-6 col-xs-12 ">
                <strong><?php $review_form->findElement('culture')->render_element(); ?></strong>
            </div>
        </div>
        <div class="row culture">
            <div class="col-sm-6 col-xs-12 ">
                <?php $review_form->findElement('culture')->render_error(); ?>
            </div>
            <div class="col-sm-6 col-xs-12  ">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="review-form-input">
                    <?php $review_form->findElement('notes')->render(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="form-group text-center">
        <button class="btn btn-success btn-login"><?= _t('Отправить') ?></button>
    </div>
</form>

<?php if (!$user) : ?>
    <div id="review_form_confirm" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?=_t('Анонимный отзыв')?></h4>
                </div>
                <div class="modal-body">
                    <p><?=_t('Вы не зарегистрировались или не вошли в систему. Ваш отзыв будет опубликован анонимно после проверки модератором. Вы не сможете его редактировать. Хотите продолжить?')?></p>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-default"><?=_t('Отправить отзыв')?></a>
                    <a href="<?=url(\Bingo\Routing::$uri) . '?' . http_build_query(['redirect' => urlencode('login')])?>" type="button" class="btn btn-primary"><?=_t('Автозироваться')?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>