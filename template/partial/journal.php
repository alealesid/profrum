<div class="panel panel-default"> 
    <div class="panel-body">
        <?php if (isset($journal->top10_position)) : ?>
            <h4 class="pull-left" style="margin-left:20px;font-size:28px"><strong><?=$journal->top10_position?></strong></h4> 
        <?php endif; ?>
       <div class="panel-info">
           <p class="text-center panel-categories">
               <?php foreach($journal->categories as $category) : ?>
                    <a href="<?= url('journal-list?category='.$category->id) ?>"><?=_h($category->name)?></a>
                <?php endforeach; ?>
           </p>
           <h4 class="text-center panel-name"> 
               <a href="<?=url('journal/'.$journal->id)?>"><?=_h($journal->name)?></a> 
           </h4>
            <p class="text-center panel-description">
                <?= nl2br($journal->description) ?>
            </p>
       </div>
       <div class="panel-stats">
           <div class="pull-left">
                <p>
                    <span class="stat"><i class="fa fa-lg fa-heart-o" aria-hidden="true"></i> <?=$journal->getLikesCount()?></span>
                    <span class="stat"><i class="fa fa-lg fa-comment-o" aria-hidden="true"></i> <?=count($journal->getModeratedReviews())?></span>
                    <span class="stat"><i class="fa fa-lg fa-question-circle-o" aria-hidden="true"></i> <?=$journal->getQuestions()->count()?></span>
                </p>
            </div>
            <div class="pull-right">
                <p>
                    <?php if (isset($journal->top10_rating)) : ?> 
                        <span class="pull-right"><?=_t('Баллы:')?> <?=$journal->top10_rating?></span> 
                    <?php endif; ?> 
                </p>
            </div>
            <div class="clearfix"></div>
       </div>        
    </div>
</div>