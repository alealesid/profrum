<div class="panel panel-default"> 
    <div class="panel-body">
        <div>
            <span class="pull-left conference-location"><?=_h($conference->location)?></span>
            <?php if ($conference->start && $conference->finish): ?>
                <span class="pull-right conference-date"><?=$conference->start->format('Y-m-d')?> - <?=$conference->finish->format('Y-m-d')?></span>
            <? endif ?>
            <span class="clearfix"></span>
        </div>
        
       <div class="panel-info">
           <p class="text-center panel-categories">
               <?php foreach($conference->categories as $category) : ?>
                    <a href="<?= url('conference-list?category='.$category->id) ?>"><?=_h($category->name)?></a>
                <?php endforeach; ?>
           </p>
           <h4 class="text-center panel-name"> 
               <a href="<?=url('conference/'.$conference->id)?>"><?=_h($conference->name)?></a> 
           </h4>
            <p class="text-center panel-description">
                <?= nl2br($conference->description) ?>
            </p>
       </div>
       <div class="panel-stats">
           <div class="pull-left">
                <p>
                    <span class="stat"><i class="fa fa-lg fa-heart-o" aria-hidden="true"></i> <?=$conference->getLikesCount()?></span>
                    <span class="stat"><i class="fa fa-lg fa-comment-o" aria-hidden="true"></i> <?=count($conference->getModeratedReviews())?></span>
                    <span class="stat"><i class="fa fa-lg fa-question-circle-o" aria-hidden="true"></i> <?=$conference->getQuestions()->count()?></span>
                </p>
            </div>
            <div class="clearfix"></div>
       </div>        
    </div>
</div>