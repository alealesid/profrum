<?php include partial('layout') ?>

<?php startblock('content') ?>


<div class="row">
    <div class = "col-md-10 col-md-offset-1">
        <h1 class="text-center"><?= $title ?></h1>
        <?php if ($imhotop->items->count() > 0) : ?>
            <div class="tops tab-content">
                <div class="item tab-pane active">
                    <table class='table'>   
                        <tr>
                            <th></th>
                            <th class='item_journal  '><?= _t('Журнал') ?></th>
                            <th class='item_position  '></th>
                            <th class='item_notes  '><?= _t('Заметки') ?></th>
                        </tr>
                    <?php foreach($imhotop->items as $item) : ?>
                        <tr class='item '>
                            <th>
                                <?=($item->position)?>
                            </th>
                            <td>
                                <?=($item->journal->name)?>
                            </td>
                            <td></td>
                            <td>
                                <?=($item->notes)?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </table>
                    <div class = "row commentable"> 
                        <div class ="col-md-10 col-md-offset-2"> 
                            <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/imhotop/' . $imhotop->id) ?>"></div>
                            <?php $comment_form = new \App\Forms\Comment('post', [], $imhotop); ?> 
                            <?php include partial('partial/comment_form') ?> 
                        </div> 
                    </div>   
                </div>
            </div>
        <?php else: ?>
            <p class="text-center"><?= _t("В данный топ журналы еще не добавлены") ?></p>
        <?php endif; ?>
         <div class="imhotop-buttons">
            <?php if ($user && $imhotop->user->name == $user->name) : ?>
                <a href="<?= $editLink ?>" class="btn btn-primary btn-sm pull-right"><?=_t('Редактировать')?></a>
                <a href="<?= $topListLink ?>" class="btn btn-primary btn-sm pull-right"><?=_t('К списку топов')?></a>
            <?php endif;?>
            <?php if ($imhotop->published) : ?>
                <?php include partial('partial/facebook_share') ?>  
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php endblock() ?>










