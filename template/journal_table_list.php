<?php include partial('layout') ?>

<?php 
    $getSortButton = function ($sortBy, $label) use ($sorted_by, $sorted_order) {

        if ($sorted_by == $sortBy) {
            if ($sorted_order == 'ASC') {
                $sorted_order = 'DESC';
                $class = '&#8593;';
            } else {
                $sorted_order = 'ASC';
                $class = '&#8595;';
            }
        } else {
            $sorted_order = 'DESC';
            $class = '';
        } 

        $get = $_GET;
        $get['sort_by'] = $sortBy;
        $get['sort_order'] = $sorted_order;

        $url = url(\Bingo\Routing::$uri) . '?' . http_build_query($get);

        return "<a href='$url'>$class $label</a>";
    };
?>

<?php startblock('content') ?>
<form class="journal-table">
    <h1 class="text-center"><?=_t('Журналы')?></h1>
    <div class="pull-right journal-table-list-select"><?php $filter_form->findElement('category')->render(); ?></div>
    <div class="clearfix"></div>
    <?php $filter_form->findElement('sort_by')->render(); ?>
    <?php $filter_form->findElement('sort_order')->render(); ?>
    <div class="journal-table-list-wrapper">
        <table class="table table-bordered">
            <tr>
                <th class="col-md-6"><?=_t('Название журнала')?></th>
                <td class="col-md-1"><small><?=$getSortButton('likes', _t('Хороший журнал').' <span class="glyphicon glyphicon-heart journal-table-list-heart" aria-hidden="true"></span>')?></small></td>
                <td class="col-md-1"><small><?=$getSortButton('reviews',_t('Отзывы'))?></small></td>
                <td class="col-md-1"><small><?=$getSortButton('critic',_t('Среднее время рецензии'))?></small></td>
                <td class="col-md-1"><small><?=$getSortButton('usefulness',_t('Средняя полезность рецензий'))?></small></td>
                <td class="col-md-1"><small><?=$getSortButton('publish',_t('Среднее время публикации'))?></small></td>
                <td class="col-md-1"><small><?=$getSortButton('rating',_t('Рейтинг'))?></small></td>
            </tr>
            <?php foreach($journals as $journal) : ?>
                <tr>
                    <td><a href="<?=url('journal/'.$journal->id)?>"><?=_h($journal->name)?></a></td>
                    <td><?=$journal->getLikesCount()?></td>
                    <td><?=count($journal->getModeratedReviews())?></td>
                    <td><?=number_format($journal->getAverageCriticDurationWeeks(), 2)?></td>
                    <td><?=number_format($journal->getAverageCriticUsefulness(), 2)?></td>
                    <td><?=number_format($journal->getAveragePublishDurationWeeks(), 2)?></td>
                    <td><?=$journal->getRating()?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

    <div class="pagination center-block text-center">
        <?= $pagination ?>
    </div>
</form>
<?php endblock() ?>