<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="well auth-block col-lg-4 col-lg-offset-4">
        <h2 class="text-center">Забыли пароль?</h2>
        <p class="text-center">Введите email, который вы используете для входа на сайт</p>
        <form id="login_form" method="POST">
            <div class="form-group">
                <?php $form->findElement('email')->render(); ?>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-primary btn-login"><?=_t('Отправить')?></button>
            </div>
        </form>
    </div>
<?php endblock() ?>