<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="feedback-edit-text">
        <h1 class="text-center"><?= $title ?></h1>
        <p>Конференций очень много и каждый год появляются новые, потому с большой вероятностью Вы не обнаружили интересующее Вас мероприятие в списке.</p>
        <p>На сайте может быть размещена новая конференция при условии, что…</p>
        <ul>
            <li>Имеет регулярный характер (проводилась ранее и будет проводиться дальше)</li>
            <li>Отбор участников на конференцию имеет публичных характер.</li>
            <li>Это очная конференция с хорошей репутацией.</li>
        </ul>
        <p>Пожалуйста, заполните следующую информацию о конференции:</p>
    </div>
    <form method="post">
        <div class="form-group">
            <?php $form->findElement('name')->render(); ?>
        </div>
        <div class="form-group">
            <?php $form->findElement('location')->render(); ?>
        </div>
        <div class="form-group">
            <?php $form->findElement('site')->render(); ?>
        </div>
        <div class="form-group">
            <?php $form->findElement('notes')->render(); ?>
        </div>
    <div class="form-group pull-right">
        <button class="btn btn-success btn-login"><?= _t('Отправить') ?></button>
    </div>
    </form>
<?php endblock() ?>