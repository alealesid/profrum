<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="feedback-edit-text">
        <h1 class="text-center"><?= $title ?></h1>
        <p> Если Вы не обнаружили в списке какой-то достойный по Вашему мнению журнал, это не заговор и не коррупция, а, скорее всего, ошибка. Сайт находится в тестовом режиме, мы можем добавить журнал, если он выпал из списка по ошибке.</p>
        <p>Пожалуйста, убедитесь, что…</p>
        <ul>
            <li>вы вводите полное, а не сокращенное название журнала</li>
            <li>журнал, который Вы хотите добавить является авторитетным (имеет не нулевой импакт-фактор) и добросовестным (т.е. не публикует статьи за деньги, не замечен в плагиате и пр.).</li>
        </ul>
        <p>После этого заполните форму ниже:</p>
    </div>
    <form method="post">
        <div class="form-group">
            <?php $form->findElement('name')->render(); ?>
        </div>
        <div class="form-group">
            <?php $form->findElement('site')->render(); ?>
        </div>
        <div class="form-group">
            <?php $form->findElement('notes')->render(); ?>
        </div>
    <div class="form-group pull-right">
        <button class="btn btn-success btn-login"><?= _t('Отправить') ?></button>
    </div>
    </form>
<?php endblock() ?>