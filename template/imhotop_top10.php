<?php include partial('layout') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-10 col-md-offset-1">
        <p>Какой журнал лучше?</p>
        <p>На странице представлен топ-10 российских журналов по категориям. Рейтинг создан на основе индивидуального выбора зарегистрированных пользователей. Никакой научной точности и экспертности, только скромное мнение участников сообщества.</p>

        <form id="top10_form" class="form-inline">
            <h2 class="text-center"><?=_t('Топ ИМХО в категории')?> <?php $filter_form->findElement('category')->render(); ?></h2>
        </form>
    
        <?php foreach($journals as $journal) : ?>
            <?php include partial('partial/journal') ?>
        <?php endforeach; ?>
        <?php if (count($journals) == 0) : ?>
            <p class="text-center"><?=_t('В этой категории ещё никто не оставлял рейтингов')?></p>
        <?php endif; ?>
        
        
        <p class="text-center imho-top10-buttons">
            <a href="<?=url('my-standart-imhotops-list')?>" class="btn btn-default btn-lg"><?=_t('Мой ИМХО-топ')?></a>
            <a href="<?=url('my-nonstandart-imhotops-list')?>" class="btn btn-default btn-lg"><?=_t('Предложить нестандартный топ')?></a>
            <a href="<?=url('imhotop-list')?>" class="btn btn-default btn-lg"><?=_t('Все топы')?></a>
        </p>

        <?php if (count($tops) > 0) : ?>
            <h3 class="text-center"><?=_t('Новые 5 рейтингов в разделе')?> <?=_h($categoryName)?></h3>
            
            <?php foreach($tops as $imhotop) : ?>
                <?php include partial('partial/imhotop') ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (count($tops) == 5) : ?>
            <p class="text-center">
                <a href="<?=url('imhotop-list')?>" class="btn btn-default btn-sm"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>


    </div>
</div>


<?php endblock() ?>