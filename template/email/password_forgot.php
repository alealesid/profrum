<html>
    <head>
        <title><?=_t('Восстановление пароля')?></title>
    </head>
    <body>
        <p><?=_t('Чтобы восстановить утерянный пароль, перейдите по ссылке:') ?> <a href="<?=$password_change_link?>"><?=$password_change_link?></a></p>
    </body>
</html>