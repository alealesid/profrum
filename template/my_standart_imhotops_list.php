<?php include partial('layout') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-10 col-md-offset-1">
        <h1 class="text-center"><?= $title ?></h1>
        <p class="text-center"><?= $description ?></p>
        <?php if (get_flash('info',false)): ?>
            <div div class="alert alert-info" role="alert">
                <?=get_flash('info',true)?>
            </div>
        <?php endif ?>
        
        <form id="not_competent_form" class="form-horizontal" method='post'>
            <div class="text-center"> 
                <p><?php $notCompetentForm->findElement('not_competent')->render(); ?></p>
                <p><?php $notCompetentForm->findElement('submit_competent')->render(); ?> </p>
            </div>             
        </form>
        <?php if ($user->competent) : ?>
            <form id = "standart_imhotop_form" class="form-horizontal" method='post'>

                <div class="text-center"> 
                    <?php $addTopForm->findElement('top_category')->render(); ?> 
                    <p><?php $addTopForm->findElement('submit_top')->render(); ?> </p>
                </div> 
                
                <div class="tops">
                <?php foreach($imhotops as $imhotop) : ?>
                    <?php include partial('partial/imhotop') ?>
                <?php endforeach; ?>
                </div>

            </form>
        <?php endif; ?>
    </div>
</div>


<?php endblock() ?>