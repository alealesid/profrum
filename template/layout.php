<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="<?=url('assets/img/favicon.png')?>" type="image/png">
        
        <link rel="stylesheet" href="<?=url('assets/lib/jquery-ui/jquery-ui.min.css')?>">
		<link rel="stylesheet" href="<?=url('assets/lib/bootstrap-3.3.7-dist/css/bootstrap.min.css')?>" >
		<link rel="stylesheet" href="<?=url('assets/css/style-ie.css')?>" >
		<link rel="stylesheet" href="<?=url('assets/css/font-awesome.min.css')?>" >
        <link rel="stylesheet" href="<?=url('assets/css/style.css')?>" >
        
		<script src="<?=url('assets/lib/jquery-3.1.1.min.js')?>" ></script>
		<script src="<?=url('assets/lib/bootstrap-3.3.7-dist/js/bootstrap.min.js')?>" ></script>
        <script src="<?=url('assets/lib/jquery-ui/jquery-ui.min.js')?>"></script>
        <script src="<?=url('assets/lib/jquery.ui.touch.min.js')?>"></script>
        <script src="<?=url('assets/lib/jquery.barrating.min.js')?>" ></script>

		<title><?= !empty($title) ? $title : '' ?></title>
		<script>var base_url = function () { return "<?= url('') ?>"};</script>
        <script src="<?=url('assets/js/core.js')?>"></script>
        <script src="<?=url('assets/js/autocomplete.js')?>"></script>
        <script src="//ulogin.ru/js/ulogin.js"></script>
        
        <meta property="og:title" content="<?= !empty($title) ? $title : '' ?>" />
        <meta property="og:url" content='<?= "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>' />
	</head>

	<body>

        <nav class="navbar theme-navbar navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= url('') ?>">PROFRUM</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-menu">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=_t('Журналы')?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= url('journal-list') ?>"><?= _t('Авторский опыт') ?></a></li>
                                <li><a href="<?= url('standart-imhotop-top10') ?>"><?= _t('Авторские рейтинги') ?></a></li>
                                <li><a href="<?= url('journal-table-list') ?>"><?= _t('Все журналы') ?></a></li>
                            </ul>
                        </li>
                        <li><a href="<?= url('conference-list') ?>"><?= _t('Конференции') ?></a></li>
                        <li><a href="<?= url('about') ?>"><?= _t('О проекте') ?></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-auth">
                        <?php if (!$user) : ?>
                            <li class=""><a class=" btn btn-default" href="<?=url('login')?>"><?= _t('Войти') ?></a></li>
                        <?php else : ?>
                            <li class="dropdown">
                                <a class=" btn btn-default user-button" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="login-button"><?=$user->email?></span> <span class="caret"></span></a>
                                <ul class="dropdown-menu authorized-options">
                                    <li><a href="<?= url('profile') ?>"><?= _t('Мой профиль') ?></a></li>
                                    <li><a href="<?= url('my-standart-imhotops-list') ?>"><?= _t('Мой ИМХО-Топ') ?></a></li>
                                    <li><a href="<?= url('my-nonstandart-imhotops-list') ?>"><?= _t('Нестандартный топ') ?></a></li>
                                    <li><a href="<?= url('logout')?>"><?= _t('Выйти') ?></a></li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="container">
            <?php if (get_flash('info',false)): ?> 
                <div div class="alert alert-info" role="alert"> 
                    <?=get_flash('info',true)?> 
                </div> 
            <?php endif ?> 
            <?php emptyblock('content') ?>
        </div>
        <?php include partial('partial/footer') ?>
	</body>
</html>