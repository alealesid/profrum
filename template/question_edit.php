<?php include partial('layout') ?>

<?php startblock('content') ?>

    <a href="<?= url($question->getOwnerUrl()) ?>">
        ← <?=  _t('Перейти к ') ?>  <?= _h($question->getOwner()->name) ?>
    </a>

<form class="container form-horizontal question-form" method="POST">
    <div class="form-group">
        <?php $form->findElement('text')->render(); ?>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-success btn-login"><?= _t('Отправить') ?></button>
    </div>
</form>


<?php if (!$user) : ?>
    <div id="question_form_confirm" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?=_t('Анонимный вопрос')?></h4>
                </div>
                <div class="modal-body">
                    <p><?=_t('Вы не зарегистрировались или не вошли в систему. Ваш вопрос будет опубликован анонимно после проверки модератором. Вы не сможете его редактировать. Хотите продолжить?')?></p>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-default"><?=_t('Отправить вопрос')?></a>
                    <a href="<?=url(\Bingo\Routing::$uri) . '?' . http_build_query(['redirect' => urlencode('login')])?>" type="button" class="btn btn-primary"><?=_t('Войти')?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php endblock() ?>