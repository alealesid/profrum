<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="col-md-10 col-md-offset-1">
        <h1 class="text-center"><?=_t('Конференции')?></h1>
        <form class="form-horizontal form-panel">
            <div class="row">
                <div class="col-xs-12">
                    <div class="conference-list-input-group">
                        <div class="input-group">
                            <?php $filter_form->findElement('name')->render_element(); ?>
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary"><?=_t('Поиск')?></button>
                            </div>
                        </div>
                        <div class="text-center  pull-left">
                            <?php $filter_form->findElement('category')->render_element(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php if (isset($search_params['name'])) : ?>
            <p><?=_t('Поиск по названию:')?>: <strong><?= $search_params['name'] ?></strong> (<a href="<?=url('/conference-list')?>"><?=_t('отменить фильтр и показать все')?></a>)</p>
        <?php endif; ?>

        <p class="text-center">
            <a href="<?=url('conference-review-edit')?>" class="btn btn-default"><?=_t('Оставить отзыв')?></a>
            <a href="<?=url('feedback/conference')?>" class="btn btn-default"><?=_t('Не нашёл конференцию')?></a>
        </p>
        <?php if (isset($search_params) && isset($conferences) && count($conferences) > 0 ) : ?>
            <?php foreach($conferences as $conference) : ?>
                <?php include partial('partial/conference') ?>
            <?php endforeach; ?>

            <div class="pagination center-block text-center">
                <?= $pagination ?>
            </div>
        <?php else : ?>
            <p class="text-center"><?= _t("По вашему запросу ничего не найдено") ?>
        <?php endif; ?>
        
    </div>

    <div class="col-md-10 col-md-offset-1">
        <h2 class="text-center"><?= _t('Новые отзывы') ?></h2>
        <?php foreach($reviews as $review) : ?>
            <?php include partial('partial/conference_review') ?>
        <?php endforeach; ?>
        <?php if (count($reviews)==0) : ?>
            <p class="text-center"><?=_t('Нет отзывов')?></p>
        <?php endif; ?>
        <?php if (count($reviews)==3) : ?>
            <p class="text-center">
                <a href="<?= url('conference-review-list') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>

        <h2 class="text-center"><?= _t('Новые вопросы') ?></h2>
        <?php foreach($questions as $question) : ?>
            <?php include partial('partial/question') ?>
        <?php endforeach; ?>
        <?php if (count($questions)==0) : ?>
            <p class="text-center"><?=_t('Нет вопросов')?></p>
        <?php endif; ?>
        <?php if (count($questions)==3) : ?>
            <p class="text-center">
                <a href="<?= url('question-list/conference') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>
    </div>
<?php endblock() ?>