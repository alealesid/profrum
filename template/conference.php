<?php include partial('layout') ?>

<?php startblock('content') ?>
<div id="conference" class="likeable row">
    <div class="col-md-10 col-md-offset-1">
        <h1 class="text-center">
            <?= _h($conference->name) ?>
        </h1>
        <div class="conference-stat">
            <div class="pull-right likes">
                <?php $likes = $conference->getLikesCount(); ?>
                <a href="<?=url('like/conference/'.$conference->id)?>" class="glyphicon glyphicon-heart<?=(!$conference->isLikedBy(@$user, $anonymousUserSession))?'-empty':''?> like-button"></a>
                <span class=" like-count <?= $likes==0 ? 'hidden' : ''?>" aria-hidden="true"><?=$likes?></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="prev-next-conferences">
            <p class="col-md-6 text-left">
                <?php if (isset($conference->prev)) : ?>
                <a href="<?=url('conference/'.$conference->prev->id)?>"><?=_t('Предыдущая конференция')?></a>
                <?php endif; ?>
            </p>
            <p class="col-md-6 text-right">
                <?php if (isset($conference->next)) : ?>
                <a href="<?=url('conference/'.$conference->next->id)?>"><?=_t('Следующая конференция')?></a>
                <?php endif; ?>
            </p>
        </div>
    </div>
    <div class="col-md-9 col-md-offset-1">
        <table class="table research-info">
            <tr>
                <th class="col-xs-5 text-right"><?=_t('Город проведения:')?></th>
                <td class="col-xs-7"><?=_h($conference->location)?></td>
            </tr>
            <?php if (isset($conference->start) && ($conference->finish)) : ?>
            <tr>
                <th class="col-xs-5 text-right"><?=_t('Дата проведения:')?></th>
                <td class="col-xs-7"><?=$conference->start->format('Y-m-d')?> - <?=$conference->finish->format('Y-m-d')?></td>
            </tr>
            <?php endif; ?>
            <?php if (!empty($conference->site)) : ?>
            <tr>
                <th class="col-xs-5 text-right"><?=_t('Официальный сайт:')?></th>
                <td class="col-xs-7"><a href="<?=$conference->site?>" target="_blank"><?=_h($conference->site)?></a></td>
            </tr>
            <?php endif; ?>
            <?php if ($conference->categories->count() > 0) : ?>
            <tr>
                <th class="col-xs-5 text-right"><?=_t('Категории:')?></th>
                <td class="col-xs-7">
                <?php foreach ($conference->categories as $category) : ?>
                <a href="<?= url('conference-list?category='.$category->id) ?>"><span class="label label-primary"><?= _h($category->name) ?></span></a>
                <?php endforeach; ?>
                <td>
            </tr>
            <?php endif; ?>
            <tr>
                <th class="col-xs-5 text-right"></th>
                <td class="col-xs-7">
                <p><?=nl2br($conference->description)?></p>
                </td>
            </tr>
        </table>
    </div>
    
    <div class="col-md-10 col-md-offset-1">
        <h2 class="text-center">
            <?=_t('Рейтинг от участников')?>
            <?php if ($conference->getRating()) : ?>
            <span class="pull-right rating">
                <a href="<?= url('conference-review-edit/' . $conference->id) ?>" class="glyphicon glyphicon-signal"></a>
                <span><?=$conference->getRating()?></span>
            </span>
        <?php endif; ?>
        </h2>
    </div>
    
    <div class="row">
    <?php $review_count = count($conference->getModeratedReviews()); ?>
    <?php if ($review_count > 0) : ?>
            <div class="col-md-9 col-md-offset-1">
                <table class="table">
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Отзывов:')?></th>
                        <td class="col-md-7"><?=$review_count?></td>
                    </tr>
                    <?php $positive_review_ratio = number_format($conference->getPositiveReviewsCount() / $review_count * 100, 2) ?>
                    <?php $negative_review_ratio = number_format($conference->getNegativeReviewsCount() / $review_count * 100, 2) ?>
                    <?php $neutral_review_ratio = number_format(100 - (double)$positive_review_ratio - (double)$negative_review_ratio, 2) ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Положительные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?= $positive_review_ratio ?>%"><?= $positive_review_ratio ?>%</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Нейтральные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-neutral" role="progressbar" style="width: <?= $neutral_review_ratio ?>%"><?= $neutral_review_ratio?>%</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Отрицательные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?= $negative_review_ratio ?>%"><?= $negative_review_ratio ?>%</div>
                            </div>
                        </td>
                    </tr>
                    <?php if ($averageDifficult = $conference->getAverageDifficult()) : ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Сложность отбора:')?></th>
                        <td class="col-md-7">
                            <?php for ($i = 1; $i <= 10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $averageDifficult) ? 'on' : '') ?>"></span>
                            <?php endfor;?>
                            <span class="visible-xs"><?= number_format($averageDifficult,2) ?></span>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($averageLevel = $conference->getAverageLevel()) : ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Уровень докладов:')?></th>
                        <td class="col-md-7">
                            <?php for ($i = 1; $i <= 10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $averageLevel) ? 'on' : '') ?>"></span>
                            <?php endfor;?>
                            <span class="visible-xs"><?= number_format($averageLevel,2) ?></span>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($averageUsefulness = $conference->getAverageUsefulness()) : ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Практическая польза:')?></th>
                        <td class="col-md-7">
                            <?php for ($i = 1; $i <= 10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $averageUsefulness) ? 'on' : '') ?>"></span>
                            <?php endfor;?>
                            <span class="visible-xs"><?= number_format($averageUsefulness,2) ?></span>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($averageCoffeebreaks = $conference->getAverageCoffeebreaks()) : ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Кофе-брейки/Фуршет:')?></th>
                        <td class="col-md-7">
                            <?php for ($i = 1; $i <= 10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $averageCoffeebreaks) ? 'on' : '') ?>"></span>
                            <?php endfor;?>
                            <span class="visible-xs"><?= number_format($averageCoffeebreaks,2) ?></span>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($averageCulture = $conference->getAverageCulture()) : ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Культурная программа:')?></th>
                        <td class="col-md-7">
                            <?php for ($i = 1; $i <= 10; $i++) : ?>
                                <span class="hidden-xs star <?= ( ($i <= $averageCulture) ? 'on' : '') ?>"></span>
                            <?php endfor;?>
                            <span class="visible-xs"><?= number_format($averageCulture,2) ?></span>
                        </td>
                    </tr>
                    <?php endif; ?>
                </table>
            </div>
    <?php else : ?>
        <div class="col-md-12"> 
            <p class="text-center"><?=_t('Пользовательских отзывов пока не было.')?></p>
        </div>
    <?php endif; ?>
    </div>
</div>
   
<div class="row">
    <p class="text-center">
        <a href="<?= url('conference-review-edit/' . $conference->id) ?>" class="btn btn-default btn-lg"><?=_t('Оставить отзыв')?></a>
        <a href="<?= url('question-edit/conference/' . $conference->id) ?>" class="btn btn-default btn-lg"><?=_t('Задать вопрос')?></a>
    </p>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#reviews" aria-controls="home" role="tab" data-toggle="tab"><?=_t('Отзывы')?></a>
            </li>
            <li role="presentation">
                <a href="#questions" aria-controls="profile" role="tab" data-toggle="tab"><?=_t('Вопросы')?>
                    <?php if (($unansweredQuestionsCount = $conference->getUnansweredQuestionsCount()) > 0) : ?> 
                        <span class="badge"><?= $unansweredQuestionsCount ?></span>
                    <?php endif; ?>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="reviews">
                <div class = "initializible paginable filterable" url = "<?= url('/conference-review-list-partial/'.$conference->id) ?>"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="questions">
                <div class = "initializible paginable filterable" url = "<?= url('/question-list-partial/conference/'.$conference->id) ?>"></div>
            </div>
        </div>
    </div>
</div>
<?php endblock() ?>