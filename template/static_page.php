<?php include partial('layout') ?>

<?php startblock('content') ?>

<div id="static_page">
    <div class="col-md-10 col-md-offset-1">
        <h1 class="text-center">
            <?= $title; ?>
        </h1>
        <div class="static-page-content">
            <?= $content; ?>
        </div>
    </div>
</div>

<?php endblock() ?>