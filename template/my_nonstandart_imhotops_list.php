<?php include partial('layout') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-10 col-md-offset-1">
        <h1 class="text-center"><?= $title ?></h1>
        <p class="text-center"><?= $description ?></p>
        <?php if (get_flash('info',false)): ?>
            <div div class="alert alert-info" role="alert">
                <?=get_flash('info',true)?>
            </div>
        <?php endif ?>
        <form id="nonstandart_imhotop_form" class="form-horizontal" method='post'>
            <div class="text-center"> 
                <?php $form->findElement('top_name')->render(); ?> 
                <p><?php $form->findElement('submit')->render(); ?> </p>
            </div> 
        </form>
        <?php foreach($imhotops as $imhotop) : ?>
            <?php include partial('partial/imhotop') ?>
        <?php endforeach; ?>
    </div>
</div>


<?php endblock() ?>