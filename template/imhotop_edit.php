<?php include partial('layout') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-8 col-md-offset-2">
        <h1 class="text-center"><?= $title ?></h1>
        <p><?= _t($description) ?></p>

        <form id = "imhotop_form" class="form-horizontal <?= $user_not_competent_class ?>" method='post'> 
 
            <h2 class="text-center"><?=_h($imhotop->getTitle())?></h2> 
            <div class="imhotop-edit-buttons">
                <a href="<?= $topListLink ?>" class="btn btn-primary btn-sm pull-right">
                    <?=_t('Вернутся к списку топов')?>
                </a>
                <a href="<?= url( 'imhotop-delete/'.$imhotop->id )?>" class="btn btn-primary btn-sm pull-right">
                    <?=_t('Удалить этот топ')?>
                </a>
                <?php if (!$nonstandart) : ?> 
                    <?php $form->findElement('published')->render(); ?> 
                <?php endif; ?> 
                <div class="clearfix"></div>
            </div>
            <div class="tops tab-content">
                <?php if ($nonstandart): ?> 
                    <?php $form->findElement('name')->render(); ?> 
                <?php endif; ?> 
                <?php $form->findElement('items')->render(); ?>
            </div>
            <div class="imhotop-edit-buttons">
                <a href="<?= url('feedback/journal') ?>" class="btn btn-default btn-sm pull-left"><?=_t('Не нашёл журнал')?></a>
                <input type="submit" class="btn btn-primary btn-sm pull-right" value="<?=_t('Сохранить')?>">
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>


<?php endblock() ?>