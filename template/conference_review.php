<?php include partial('layout') ?>

<?php startblock('content') ?>
    <!-- REVIEW -->
<?php 
    $attitudeClasses = [
        App\Models\Review::ATTITUDE_NEUTRAL => 'panel-neutral',
        App\Models\Review::ATTITUDE_NEGATIVE  => 'panel-danger',
        App\Models\Review::ATTITUDE_POSITIVE  => 'panel-success',
    ];
?>
<div class="commentable panel <?= $attitudeClasses[$review->attitude] ?>" >
    <div class="panel-heading">
        <?php if (isset($review->user) && !$review->anonymous) : ?>
            <a href="<?=url('profile/'.$review->user->id)?>">
                <?php if ($review->user->avatar) : ?>
                    <img class="img-rounded" height="40" width="40" src="<?= (strpos($review->user->avatar, 'http') === false) ? url($review->user->avatar) : $review->user->avatar ?>">
                <?php endif; ?>
                <strong><?= _h($review->user->name) ?> - <?= $review->user->getActivityRating() ?></strong>
            </a> <?= _h($review->user->status) ?>
        <?php else: ?>
            <?= _t('Анонимный отзыв') ?>
        <?php endif; ?>
        | <a href="<?= url('conference/' . $review->conference->id) ?>"><?= _h($review->conference->name) ?></a>
        <span class="pull-right panel-date">
            <?php if (isset($review->user) && ($user) && $review->user->login == $user->login) : ?>
                <a href="<?= url('conference-review-edit/'. $review->conference->id .'/'. $review->id) ?>">
                    <strong><?= _t('изменить') ?></strong>
                </a>
            <?php endif; ?>
            <?= $review->created->format('Y-m-d H:i') ?>
        </span>
        <span class="clearfix"></span>
    </div>
    
    <div class="panel-body">
        <div class="col-xs-12">
           <table class="table">
                <tr>
                    <th class="col-lg-6"><?=_t('Насколько сложно стать участником конференции')?></th>
                    <td class="col-lg-6 text-right">
                        <?php for ($i = 1; $i <= 10; $i++) : ?>
                            <span class="hidden-xs star <?= ( ($i <= $review->difficult) ? 'on' : '') ?>"></span>
                        <?php endfor;?>
                        <span class="visible-xs"><?= $review->difficult ?></span>
                    </td>
                </tr>
                <tr>
                    <th class="col-lg-6"><?=_t('Уровень докладов')?></th>
                    <td class="col-lg-6 text-right">
                        <?php for ($i = 1; $i <= 10; $i++) : ?>
                            <span class="hidden-xs star <?= ( ($i <= $review->level) ? 'on' : '') ?>"></span>
                        <?php endfor;?>
                        <span class="visible-xs"><?= $review->level ?></span>
                    </td>
                </tr>
                <tr>
                    <th class="col-lg-6"><?=_t('Практическая польза')?></th>
                    <td class="col-lg-6 text-right">
                        <?php for ($i = 1; $i <= 10; $i++) : ?>
                            <span class="hidden-xs star <?= ( ($i <= $review->usefulness) ? 'on' : '') ?>"></span>
                        <?php endfor;?>
                        <span class="visible-xs"><?= $review->usefulness ?></span>
                    </td>
                </tr>
                <tr>
                    <th class="col-lg-6"><?=_t('Кофе-брейки/фуршет')?></th>
                    <td class="col-lg-6 text-right">
                        <?php for ($i = 1; $i <= 10; $i++) : ?>
                            <span class="hidden-xs star <?= ( ($i <= $review->coffeebreaks) ? 'on' : '') ?>"></span>
                        <?php endfor;?>
                        <span class="visible-xs"><?= $review->coffeebreaks ?></span>
                    </td>
                </tr>
                <?php if ($review->has_culture) : ?>
                <tr>
                    <th class="col-lg-6"><?=_t('Культурная программа')?></th>
                    <td class="col-lg-6 text-right">
                        <?php for ($i = 1; $i <= 10; $i++) : ?>
                            <span class="hidden-xs star <?= ( ($i <= $review->culture) ? 'on' : '') ?>"></span>
                        <?php endfor;?>
                        <span class="visible-xs"><?= $review->culture ?></span>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
        
        <?php if (!empty($review->notes)) : ?>
            <div class="col-xs-12 review-notes">
                <strong><?=_t('Комментарии')?></strong>
                <p><?= _h($review->notes) ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- END REVIEW -->

<div class="review-facebook-share">
    <?php include partial('partial/facebook_share') ?> 
</div>

<div class = "row commentable">
    <div class="col-md-10 col-md-offset-2">
        <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/conference-review/' . $review->id) ?>"></div>
        <?php $comment_form = new \App\Forms\Comment('post', [], $review); ?>
        <?php include partial('partial/comment_form') ?>
    </div>
</div>

<?php endblock() ?>