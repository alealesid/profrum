<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="well auth-block col-lg-4 col-lg-offset-4">
        <p class="text-center">Восстановление пароля для <?=$target_user->name?></p>
        <form id="login_form" method="POST">
            <div class="form-group">
                <?php $form->findElement('password')->render(); ?>
            </div>
            <div class="form-group">
                <?php $form->findElement('password_again')->render(); ?>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-primary btn-login"><?=_t('Изменить')?></button>
            </div>
        </form>
    </div>
<?php endblock() ?>