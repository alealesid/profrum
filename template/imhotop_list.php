<?php include partial('layout') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-10 col-md-offset-1">

        <form id="top10_form" class="form-inline">
            <h2 class="text-center">
                <?=_t('ИМХО топы в категории')?> <?php $filter_form->findElement('category')->render(); ?>
            </h2>
            <div class="form-inline text-center">
                <label><?=_t('Где искать:')?> </label>
                <?php $filter_form->findElement('standart')->render(); ?>
                <?php $filter_form->findElement('not_standart')->render(); ?>
            </div>
        </form>
        
        <?php foreach($tops as $imhotop) : ?>
            <?php include partial('partial/imhotop') ?>
        <?php endforeach; ?>
        <?php if (count($tops) == 0) : ?>
            <p class="text-center"><?=_t('Топов нет')?></a></p>
        <?php endif; ?>

        <div class="pagination center-block text-center">
            <?= $pagination ?>
        </div>

    </div>
</div>


<?php endblock() ?>