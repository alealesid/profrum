<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 user-profile">
            <div class="row">
                <div class="<?= $profile_user->avatar ? 'col-sm-9' : 'col-sm-12' ?> col-xs-12">
                    <h1><?=_h($profile_user->name)?></h1>
                    <?php if ($profile_user->avatar) : ?>
                        <p class="user-profile-image visible-xs">
                            <img src="<?= (strpos($profile_user->avatar, 'http') === false) ? url($profile_user->avatar) : $profile_user->avatar ?>">
                        </p>
                    <?php endif; ?>
                    <div class="row user-profile-items-row">
                        <?php if ($profile_user->degree != \App\Models\User::DEGREE_NONE) : ?>
                        <div class=" col-sm-4 col-xs-12 user-profile-item"><strong><?= \App\Models\User::getDegreeLabels()[$profile_user->degree]?></strong></div>
                        <?php endif; ?>
                        <div class=" col-sm-4 col-xs-12 user-profile-item"><strong><?= _t('Статус') ?>: </strong><?=_h($profile_user->status) ?></div>
                        <div class=" col-sm-4 col-xs-12 user-profile-item"><strong><?= _t("Рейтинг: ") ?></strong><?= $profile_user->getActivityRating() ?></div>
                    </div>
                    <div class="row user-profile-items-row">
                    <?php if ($profile_user->work) : ?>
                        <div class=" col-sm-4 col-xs-12 user-profile-item">
                            <strong><?=_t('Место работы (учёбы) :')?></strong>
                            <?=_h($profile_user->work)?>
                        </div>
                    <?php endif; ?>
                    <?php if ($profile_user->position) : ?>
                        <div class=" col-sm-4 col-xs-12 user-profile-item">
                            <strong><?=_t('Должность :')?></strong>
                            <?=_h($profile_user->position)?>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>    
                <?php if ($profile_user->avatar) : ?>
                    <div class="col-sm-3 hidden-xs">            
                        <p class="user-profile-image">
                            <img src="<?= (strpos($profile_user->avatar, 'http') === false) ? url($profile_user->avatar) : $profile_user->avatar ?>">
                        </p>
                    </div>
                <?php endif; ?>
            </div>
            
            

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#journal_reviews" aria-controls="home" role="tab" data-toggle="tab">
                        <?=_t('Отзывы к журналам')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#conference_reviews" aria-controls="home" role="tab" data-toggle="tab">
                        <?=_t('Отзывы к конференциям')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#questions" aria-controls="profile" role="tab" data-toggle="tab">
                        <?=_t('Вопросы')?>
                        <?php if (($unansweredQuestionsCount = $profile_user->getUnansweredQuestionsCount()) > 0) : ?>
                            <span class="badge"><?= $unansweredQuestionsCount ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="journal_reviews">
                    <div class = "initializible paginable filterable" url = "<?= url('/journal-review-list-user-partial/'.$profile_user->id) ?>"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="conference_reviews">
                    <div class = "initializible paginable filterable" url = "<?= url('/conference-review-list-user-partial/'.$profile_user->id) ?>"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="questions">
                    <div class = "initializible paginable filterable" url = "<?= url('/question-user-list-partial/'.$profile_user->id) ?>"></div>
                </div>
            </div>

        </div>
    </div>
<?php endblock() ?>