<?php include partial('layout_main') ?>

<?php startblock('content') ?>
<div class="row">
    <div class = "col-md-10 col-md-offset-1">
        <?php if (isset($search_params['name'])) : ?>
        <div class="search-block">
            <p><?=_t('Поиск по названию:')?>: <strong><?= _h($search_params['name']) ?></strong></p>
            
            <?php if ($journals) : ?>
                <h2 class="text-center"><?= _t('Журналы') ?></h2>
                <?php $journalsOffset = (count($journals) <= 5) ? count($journals) : 5; ?>
                <?php for ($i = 0; $i < $journalsOffset; $i++) : ?>
                    <?php $journal = $journals[$i]; ?>
                    <?php include partial('partial/journal') ?>
                <?php endfor; ?>
                <?php if (count($journals) > 5) : ?>
                    <div class="text-center">
                        <a href="<?= url('journal-list?name='.$search_params['name'].'&category=none') ?>" class="btn btn-default btn-all-results"><?= _t("Все результаты") ?></a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if ($conferences) : ?>
                <h2 class="text-center"><?= _t('Конференции') ?></h2>
                <?php $conferencesOffset = (count($conferences) <= 5) ? count($conferences) : 5; ?>
                <?php for ($i = 0; $i < $conferencesOffset; $i++) : ?>
                    <?php $conference = $conferences[$i]; ?>
                    <?php include partial('partial/conference') ?>
                <?php endfor; ?>
                <?php if (count($conferences) > 5) : ?>
                    <div class="text-center">
                        <a href="<?= url('conference-list?name='.$search_params['name'].'&category=none'); ?>" class="btn btn-default btn-all-results"><?= _t("Все результаты") ?></a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if (count($journals) == 0 && count($conferences) == 0) : ?>
                <p class="text-center"><?=_t('Журналы и конференции не найдены')?></p>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <div class="text-center">
            <h1 class="text-center"><?= _t('Для чего нужен PROFRUM?') ?></h1>
            <div class="main-page-nav">
                <div class="main-page-nav-item">
                    <img src="<?= url('/') ?>assets/img/journalicon.png" alt="Авторский опыт">
                    <h3>обобщать</h3>
                    <a class=" btn btn-default" href="<?= url('journal-list') ?>"><?= _t('Авторский опыт') ?></a>
                </div>
                <div class="main-page-nav-item">
                    <img src="<?= url('/') ?>assets/img/ratingicon.png" alt="Авторские рейтинги">
                    <h3>создавать</h3>   
                    <a class=" btn btn-default" href="<?= url('standart-imhotop-top10') ?>"><?= _t('Авторскиие рейтинги') ?></a>
                </div>
                <div class="main-page-nav-item">
                    <img src="<?= url('/') ?>assets/img/conficon.png" alt="Участие в конференциях">
                    <h3>обсуждать</h3>
                    <a class=" btn btn-default" href="<?= url('conference-list') ?>"><?= _t('Участие в конференциях') ?></a>
                </div>
            </div>
        </div>

        <h2 class="text-center"><?= _t('Новые отзывы о журналах') ?></h2>
        <?php foreach($journal_reviews as $review) : ?>
            <?php include partial('partial/journal_review') ?>
        <?php endforeach; ?>
        <?php if (count($journal_reviews)==0) : ?>
            <p class="text-center"><?=_t('Нет отзывов')?></p>
        <?php endif; ?>
        <?php if (count($journal_reviews)==3) : ?>
            <p class="text-center">
                <a href="<?= url('journal-review-list') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>


        <h2 class="text-center"><?= _t('Новые отзывы о конференциях') ?></h2>
        <?php foreach($conference_reviews as $review) : ?>
            <?php include partial('partial/conference_review') ?>
        <?php endforeach; ?>
        <?php if (count($conference_reviews)==0) : ?>
            <p class="text-center"><?=_t('Нет отзывов')?></p>
        <?php endif; ?>
        <?php if (count($conference_reviews)==3) : ?>
            <p class="text-center">
                <a href="<?= url('conference-review-list') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>

        <h2 class="text-center"><?= _t('Новые вопросы о журналах') ?></h2>
        <?php foreach($journal_questions as $question) : ?>
            <?php include partial('partial/question') ?>
        <?php endforeach; ?>
        <?php if (count($journal_questions)==0) : ?>
            <p class="text-center"><?=_t('Нет вопросов')?></p>
        <?php endif; ?>
        <?php if (count($journal_questions)==3) : ?>
            <p class="text-center">
                <a href="<?= url('question-list/journal') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>

        <h2 class="text-center"><?= _t('Новые вопросы о конференциях') ?></h2>
        <?php foreach($conference_questions as $question) : ?>
            <?php include partial('partial/question') ?>
        <?php endforeach; ?>
        <?php if (count($conference_questions)==0) : ?>
            <p class="text-center"><?=_t('Нет вопросов')?></p>
        <?php endif; ?>
        <?php if (count($conference_questions)==3) : ?>
            <p class="text-center">
                <a href="<?= url('question-list/conference') ?>" class="btn btn-default"><?=_t('Посмотреть все')?></a>
            </p>
        <?php endif; ?>

    </div>
</div>


<?php endblock() ?>