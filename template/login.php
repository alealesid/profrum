<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="well auth-block col-lg-4 col-lg-offset-4">
        <form id="login_form" method="POST" action="<?=$login_url?>">
            <div class="form-group">
                <?php $login_form->findElement('email')->render(); ?>
            </div>
            <div class="form-group">
                <?php $login_form->findElement('password')->render(); ?>
            </div>
            <div class="text-center" id="uLogin" data-ulogin="redirect_uri=<?=$ulogin_redirect_url?>;display=buttons;theme=flat;fields=first_name,last_name,nickname,email,photo_big;providers=vkontakte,google,facebook,yandex;hidden=twitter,flickr,youtube,googleplus,instagram;mobilebuttons=0;">
                <div class="social-icon google" data-uloginbutton="google"></div>
                <div class="social-icon vk" data-uloginbutton="vkontakte"></div>
                <div class="social-icon facebook" data-uloginbutton="facebook"></div>
                <div class="social-icon yandex" data-uloginbutton="yandex"></div>
            </div>
            <div class="form-group text-center">
                <p><a href="<?= url('forgot-password') ?>"><?=_t('Забыли пароль?')?></a></p>
                <div class="form-group inline">
                    <div class="checkbox">
                        <?php $login_form->findElement('remember_me')->render(); ?>
                    </div>
                </div>
                <button class="btn btn-primary btn-login"><?=_t('Войти')?></button>
                <a href="<?=$signup_url?>" class="btn btn-primary btn-signup"><?=_t('Регистрация')?></a>
            </div>
        </form>
    </div>
<?php endblock() ?>