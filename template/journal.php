<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div id = "journal" class="likeable">
        <div class="col-md-10 col-md-offset-1">
            <h1 class="text-center">
                <?= _h($journal->name) ?>
            </h1>
            <div class="journal-stat">
                <div class="pull-right likes">
                    <?php $likes = $journal->getLikesCount(); ?>
                    <a href="<?=url('like/journal/'.$journal->id)?>" class="glyphicon glyphicon-heart<?=(!$journal->isLikedBy(@$user, $anonymousUserSession))?'-empty':''?> like-button"></a>
                    <span class=" like-count <?= $likes==0 ? 'hidden' : ''?>" aria-hidden="true"><?=$likes?>
                </div>
                <div class="clearfix"></div>
            </div>
            <table class="table research-info">
                <tr>
                    <th class="col-md-5 text-right"><?=_t('Входит в базы данных:')?></th>
                    <td class="col-md-7">
                        <? $databases = []; ?>
                        <? if ($journal->included_to_scopus) $databases[] = _t('Scopus'); ?>
                        <? if ($journal->included_to_russian_journals) $databases[] = _t('Российские журналы'); ?>
                        <?=join(', ', $databases)?>
                    </td>
                </tr>
                <tr>
                    <th class="col-md-5 text-right"><?=_t('Официальный сайт:')?></th>
                    <td class="col-md-7">
                        <?php if ($journal->site && $journal->site != _t("Сайт не указан")) : ?>
                            <a href="<?=$journal->site?>" target="_blank"><?=_h($journal->site)?></a>
                        <?php else: ?>
                            <?=_t("Сайт не указан")?>
                        <?php endif; ?>
                    </td> 
                </tr>
                <tr>
                    <th class="col-md-5 text-right"><?=_t('Категории:')?></th>
                    <td class="col-md-7">
                        <?php foreach ($journal->categories as $category) : ?>
                            <a href="<?= url('journal-list?category='.$category->id) ?>"><span class="label label-primary"><?= _h($category->name) ?></span></a>
                        <?php endforeach; ?>
                    <td>
                </tr>
                <tr>
                    <th class="col-md-5 text-right"><?=_t('')?></th>
                    <td class="col-md-7">
                        <p><?=nl2br($journal->description)?></p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <h2 class="text-center">
            <?=_t('Рейтинг от авторов')?>
            <?php if ($journal->getRating()) : ?>
                <span class="pull-right rating">
                    <a href="<?= url('journal-review-edit/' . $journal->id) ?>" class="glyphicon glyphicon-signal"></a>
                    <span><?=$journal->getRating()?></span>
                </span>
            <?php endif; ?>
        </h2>
    </div>

    <?php $review_count = count($journal->getModeratedReviews()); ?>
    <?php if ($review_count > 0) : ?>
        <div class="row custom-table">
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Отзывов:')?></th>
                        <td class="col-md-7"><?=$review_count?></td>
                    </tr>
                    <?php $positive_review_ratio = number_format($journal->getPositiveReviewsCount() / $review_count * 100, 2) ?>
                    <?php $negative_review_ratio = number_format($journal->getNegativeReviewsCount() / $review_count * 100, 2) ?>
                    <?php $neutral_review_ratio = number_format(100 - (double)$positive_review_ratio - (double)$negative_review_ratio, 2) ?>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Положительные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?= $positive_review_ratio ?>%"><?= $positive_review_ratio ?>%</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Нейтральные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-neutral" role="progressbar" style="width: <?= $neutral_review_ratio ?>%"><?= $neutral_review_ratio?>%</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Отрицательные')?></th>
                        <td class="col-md-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?= $negative_review_ratio ?>%"><?= $negative_review_ratio ?>%</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Среднее время публикации (недель):')?></th>
                        <td class="col-md-7">
                            <?php if ($averagePublishTime = $journal->getAveragePublishDurationWeeks()) : ?>
                                <?=number_format($averagePublishTime, 2)?>
                            <?php else : ?>
                                <?= _t('Не было публикаций') ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Среднее время получения рецензии (недель):')?></th>
                        <td class="col-md-7">
                            <?php if ($averageCriticDuration = $journal->getAverageCriticDurationWeeks()) : ?>
                                <?=number_format($averageCriticDuration, 2)?>
                            <?php else : ?>
                                <?= _t('Не было рецензий') ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col-md-5 text-right"><?=_t('Средняя полезность рецензий:')?></th>
                        <td class="col-md-7">
                            <?php if ($averageUsefulness = $journal->getAverageCriticUsefulness()) : ?>
                                <?php for ($i = 1; $i <= 10; $i++) : ?>
                                    <span class="hidden-xs star <?= ( ($i <= $averageUsefulness) ? 'on' : '') ?>"></span>
                                <?php endfor;?>
                                <span class="visible-xs"><?= number_format($averageUsefulness,2) ?></span>
                            <?php else : ?>
                                <?= _t('Не было рецензий') ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php else : ?>
        <div class="col-md-10 col-md-offset-1">
            <p class="text-center"><?=_t('Пользовательских отзывов пока не было.')?></p>
        </div>
    <?php endif; ?>


    <div class="row" >
        <div class="col-md-10 col-md-offset-1">
            <h2 class="text-center">
                <?=_t('Категории ИМХО топов')?>
            </h2>
        </div>
    </div>
    <?php if (count($IMHOInfo) > 0) : ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 journal-imhotop-categories-container">
                <table class="table">
                    <tr>
                        <?php foreach($IMHOInfo as $imho) : ?>
                            <th class="text-center">
                                <?=$imho['category']?>: 
                                <a href="<?= url('standart-imhotop-top10?category='.$imho['category_id']) ?>">
                                    <?=$imho['top']?>
                                </a>
                            </th>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <?php foreach($IMHOInfo as $imho) : ?>
                            <th class="text-center"><?=_t('Баллы')?>: <?=$imho['score']?></th>
                        <?php endforeach; ?>
                    </tr>
                </table>
            </div>
        </div>
    <?php else : ?>
        <div class="col-md-10 col-md-offset-1">
            <p class="text-center"><?=_t('Журнал пока не попал ни в один топ.')?></p>
        </div>
    <?php endif; ?>
    <p class="text-center">
        <a href="<?= url('journal-review-edit/' . $journal->id) ?>" class="btn btn-default btn-lg"><?=_t('Оставить отзыв')?></a>
        <a href="<?= url('question-edit/journal/' . $journal->id) ?>" class="btn btn-default btn-lg"><?=_t('Задать вопрос')?></a>
    </p>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#reviews" aria-controls="home" role="tab" data-toggle="tab">
                        <?=_t('Отзывы')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#questions" aria-controls="profile" role="tab" data-toggle="tab">
                        <?=_t('Вопросы')?>
                        <?php if (($unansweredQuestionsCount = $journal->getUnansweredQuestionsCount()) > 0) : ?>
                            <span class="badge"><?= $unansweredQuestionsCount ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="reviews">
                    <div class = "initializible paginable filterable" url = "<?= url('/journal-review-list-partial/'.$journal->id) ?>"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="questions">
                    <div class = "initializible paginable filterable" url = "<?= url('/question-list-partial/journal/'.$journal->id) ?>"></div>
                </div>
            </div>

        </div>
    </div>
        
<?php endblock() ?> 
