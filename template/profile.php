<?php include partial('layout') ?>

<?php startblock('content') ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <form method="post" class="profile-form form-horizontal" enctype="multipart/form-data">
                <div class="container-fluid">
                    <div class="row">
                         <div class="col-md-6 col-xs-12">
                            <div class="from-group avatar-input">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="avatar-input-image col-xs-5">
                                            <span class="avatar-image-wrapper">
                                                <img class="img-responsive" height="125" width="125" src="<?= @$user->avatar ?: $default_avatar ?>" alt="<? $user->name ?: $user->email ?>">
                                            </span>
                                        </div>
                                        <div class="avatar-input-file col-xs-7">
                                            <?php $form->findElement('avatar')->render(); ?>
                                            <p class="text-center profile-rating"><strong><?= _t("Рейтинг: ") ?></strong><?= $user->getActivityRating() ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="from-group">
                                <?php $form->findElement('name')->render(); ?>
                            </div>
                            <div class="from-group">
                                <?php $form->findElement('email')->render(); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="from-group">
                                <?php $form->findElement('degree')->render(); ?>
                            </div>
                            <div class="from-group">
                                <?php $form->findElement('password')->render(); ?>
                            </div>
                            <div class="from-group">
                                <?php $form->findElement('password_again')->render(); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="from-group">
                                <?php $form->findElement('status')->render(); ?>
                            </div>
                            <div class="from-group">
                                <?php $form->findElement('work')->render(); ?>
                            </div>
                            <div class="from-group">
                                <?php $form->findElement('position')->render(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success btn-login"><?=_t('Сохранить')?></button>
                    </div>
                </div>
            </form>

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#journal_reviews" aria-controls="home" role="tab" data-toggle="tab">
                        <?=_t('Отзывы к журналам')?>
                    </a>
                </li>
                <li role="presentation" >
                    <a href="#conference_reviews" aria-controls="home" role="tab" data-toggle="tab">
                        <?=_t('Отзывы к конференциям')?>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#questions" aria-controls="profile" role="tab" data-toggle="tab">
                        <?=_t('Вопросы')?>
                        <?php if (($unansweredQuestionsCount = $user->getUnansweredQuestionsCount()) > 0) : ?>
                            <span class="badge"><?= $unansweredQuestionsCount ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="journal_reviews">
                    <div class = "initializible paginable filterable" url = "<?= url('/journal-review-list-user-partial/'.$user->id) ?>"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="conference_reviews">
                    <div class = "initializible paginable filterable" url = "<?= url('/conference-review-list-user-partial/'.$user->id) ?>"></div>
                </div>
                <div role="tabpanel" class="tab-pane" id="questions">
                    <div class = "initializible paginable filterable" url = "<?= url('/question-user-list-partial/'.$user->id) ?>"></div>
                </div>
            </div>


        </div>
    </div>
<?php endblock() ?>