<?php include partial('layout') ?>

<?php startblock('content') ?>
<!-- Question -->            
<div class="commentable panel panel-warning question-view" >
    <div class="panel-heading">
        <? if ($question->user) : ?>
            <a href="<?=url('profile/'.$question->user->id)?>"> 
                <?php if ($question->user->avatar) : ?> 
                    <img class="img-rounded" height="25" width="25" src="<?= (strpos($question->user->avatar, 'http') === false) ? url($question->user->avatar) : $question->user->avatar ?>"> 
                <?php endif; ?> 
                <strong><?= _h($question->user->name) ?> - <?= $question->user->getActivityRating() ?></strong> 
            </a> <?= _h($question->user->status) ?>
        <? else : ?>
        <strong><?= _t("Гость") ?></strong>
        <? endif; ?>
        | <a href="<?= url($question->getOwnerUrl()) ?>"><?= _h($question->getOwner()->name) ?></a>
        <span class="pull-right panel-date">
            <?php if (isset($question->user) && ($user) && $question->user->login == $user->login) : ?>
                <a href="<?= url($question->getUrl()) ?>">
                    <strong><?= _t('изменить') ?></strong>
                </a>
            <?php endif; ?>
            <?= $question->created->format('Y-m-d H:i') ?>
        </span>
        <span class="clearfix"></span>
    </div>
    
    <div class="panel-body">
        <div class="review-notes">
            <p><?= _h($question->text) ?></p>
        </div>
    </div>
</div>
<!-- END Question -->

<div class = "row commentable">
    <div class="col-md-10 col-md-offset-2">
        <div class = "initializible comments-container paginable" url = "<?= url('/comment-list/question/' . $question->id) ?>"></div>
        <?php $comment_form = new \App\Forms\Comment('post', [], $question); ?>
        <?php include partial('partial/comment_form') ?>
    </div>
</div>

<?php endblock() ?>