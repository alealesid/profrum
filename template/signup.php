<?php include partial('layout') ?>

<?php startblock('content') ?>
    <p><?=_t('Для чего нужна регистрация?')?></p>
    <p><?=_t('Прежде всего, мы сможем быть уверены в том, что Вы реальный человек, несущий ответственность за свои слова.')?></p>
    <p><?=_t('Вы сможете:')?></p>
    <ol>
        <li><?=_t('Оставлять отзывы, в том числе анонимные, без проверки модераторами сайта')?></li>
        <li><?=_t('Редактировать, оставленные ранее отзывы')?></li>
        <li><?=_t('Заполнять и публиковать ИМХО-топы журналов')?></li>
        <li><?=_t('Оставлять вопросы и отвечать на них')?></li>
        <li><?=_t('Делится своими отзывами и вопросами в социальных сетях')?></li>
    </ol>
    
    <div class="well auth-block col-lg-4 col-lg-offset-4">
            <div class="text-center" id="uLogin" data-ulogin="redirect_uri=<?=$ulogin_redirect_url ?>;display=buttons;theme=flat;fields=first_name,last_name,nickname,email,photo_big;providers=vkontakte,google,facebook,yandex;hidden=twitter,flickr,youtube,googleplus,instagram;mobilebuttons=0;">
                <div class="social-icon google" data-uloginbutton="google"></div>
                <div class="social-icon vk" data-uloginbutton="vkontakte"></div>
                <div class="social-icon facebook" data-uloginbutton="facebook"></div>
                <div class="social-icon yandex" data-uloginbutton="yandex"></div>
            </div>
    <form id="login_form"  method="POST" action="<?=$signup_url?>">
            <div class="form-group">
                <?php $signup_form->findElement('name')->render(); ?>
            </div>
            <div class="form-group">
                <?php $signup_form->findElement('email')->render(); ?>
            </div>
            <div class="form-group">
                <?php $signup_form->findElement('password')->render(); ?>
            </div>
            <div class="form-group"> 
                <?php $signup_form->findElement('password_again')->render(); ?> 
            </div> 
            <div class="form-group">
                <?php $signup_form->findElement('work')->render(); ?>
            </div>
            <div class="form-group">
                <?php $signup_form->findElement('position')->render(); ?>
            </div>
            <div class="form-group">
                <?php $signup_form->findElement('degree')->render(); ?>
            </div>
            <div class="form-group">
                <?php $signup_form->findElement('status')->render(); ?>
            </div>
            <p>Регистрируясь на сайте, Вы подтверждаете свое согласие с <a href="<?= url('rules') ?>">правилами сообщества</a></p>
            <div class="form-group text-center">
                <button class="btn btn-success btn-login">Зарегистрировать</button>
            </div>
        </form>
    </div>
<?php endblock() ?>
    