<?php include partial('layout') ?>

<?php startblock('content') ?>
    <h1 class="text-center"><?=  _t('Отзыв к журналу') ?></h1>
    <div class="col-md-10 col-md-offset-1">
        <p>
            Опишите свой опыт взаимодействия с журналом. Речь идет только об опыте, связанном с рецензированием и публикацией статьи.
        </p>
        <ol>
            <li>Будьте вежливыми.</li>
            <li>Пишите правду.</li>
        </ol>
        <p>
            Читать все <a href="<?= url('rules') ?>">правила</a>.
        </p>
        <h2 class="text-center  anonymous-question">Анонимно или нет <i class="fa fa-question-circle-o popup-tip-pointer" title="" aria-hidden="true"></i></h2>
        <div class="popup-tip">
            <span class="pull-right visible-sm"><i class="fa fa-times close-tip" aria-hidden="true"></i></span>
            <p>
                Возможны следующие варианты размещения отзыва:
            </p>
            <ul>
                <li>Как участник: Отзыв опубликуется немедленно, он не будет анонимным.</li>
                <li>Как анонимный участник: Отзыв опубликуется немедленно, он будет анонимным, только администратор будет знать, кто оставил отзыв.</li>
                <li>Как анонимный гость: Отзыв будет опубликован после проверки модератором. Никто не будет знать, кто его оставил. Этот отзыв нельзя редактировать. См. <a>правила сообщества</a></li>
            </ul>
        </div>
        <?php include partial('partial/journal_review_form') ?>
    </div>
<?php endblock() ?>